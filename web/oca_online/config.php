<?php
///////////////////////
// Файл конфигураций //
///////////////////////

defined( '__DDD__' ) or die();



////////////// НАЧАЛО РУЧНЫХ УСТАНОВОК //////////////

// Настройки базы данных
$db_host = "luminanc.mysql.ukraine.com.ua";
$db_user = "luminanc_planeta";
$db_pass = "m8balkgp";
$db_name = "luminanc_planeta";

// Приставка таблиц в базе данных
$CONFIG['db_prefix'] = "oca2_";

// Приставка для E-mail ("robot", "noreply")
$CONFIG['robot_email'] = "robot";
// Имя отправителя для E-mail ("OCA Online", "Центр тестирования")
$CONFIG['robot_sender'] = "OCA Online";

// Ссылка на техподдержку
$CONFIG['support_link'] = 'http://redtriangle.net/oca-support/';

/*
Требования к хостингу:
1. Выполнение функции glob().
2. Выполнение функции mail().
2. PHP графика.
*/

///////////////// КОНЕЦ РУЧНЫХ УСТАНОВОК /////////////////



// Версия программы
$CONFIG['version'] = '2.4';

// Возможные кодировки писем
$CONFIG['charsets'] = array('UTF-8', 'CP1251', 'KOI8-R'); // for iconv()
$CONFIG['charsets_header'] = array('UTF-8' => 'UTF-8', 'CP1251' => 'Windows-1251', 'KOI8-R' => 'KOI8-R'); // for header()

// Домен сайта без www
$CONFIG['site'] = str_replace('www.', '', $_SERVER['HTTP_HOST']);

// Домашняя папка
$CONFIG['root_folder'] = 'oca_online';

// Устанавливаем соединение с базой данных
$CONFIG['link'] = mysql_connect($db_host, $db_user, $db_pass);
if (!$CONFIG['link']) { echo '<div class="notok">Не могу соединиться с сервером базы данных</div>'; exit(); }
if (!mysql_select_db($db_name, $CONFIG['link'])) { echo '<div class="notok">Не могу выбрать базу данных</div>'; exit(); }
mysql_query("SET NAMES 'utf8'");



///////////////// НАЧАЛО УСТАНОВОК ПОЧТЫ /////////////////
$CONFIG['from_name'] = $CONFIG['robot_sender'];
$CONFIG['from_email'] = $CONFIG['robot_email'].'@'.$CONFIG['site'];

// На всякий случай указываем настройки для дополнительного (внешнего) SMTP сервера.
$CONFIG['smtp_mode'] = 'disabled'; // enabled or disabled (включен или выключен)
$CONFIG['smtp_host'] = null;
$CONFIG['smtp_port'] = null;
$CONFIG['smtp_username'] = null;
///////////////// КОНЕЦ УСТАНОВОК ПОЧТЫ //////////////////
?>