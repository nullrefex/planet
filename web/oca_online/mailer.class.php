<?php
////////////////////////////////////////////////////
// Класс предустановленных настроек для PHPMailer //
////////////////////////////////////////////////////

defined( '__DDD__' ) or die();



require_once($_SERVER['DOCUMENT_ROOT'].'/'.$CONFIG['root_folder'].'/PHPMailer/class.phpmailer.php');

class FreakMailer extends PHPMailer {
	
    var $priority = 3;
    var $to_name;
    var $to_email;
    var $From = null;
    var $FromName = null;
    var $Sender = null;
  
    function FreakMailer() {
		global $CONFIG;
	  
		// Берем из файла config.php настройки
		if($site['smtp_mode'] == 'enabled') {
			
			$this->Host = $CONFIG['smtp_host'];
			$this->Port = $CONFIG['smtp_port'];
		
			if($CONFIG['smtp_username'] != '') {
				$this->SMTPAuth  = true;
				$this->Username  = $CONFIG['smtp_username'];
				$this->Password  =  $CONFIG['smtp_password'];
			}
			$this->Mailer = "smtp";
		}
    
		if(!$this->CharSet) $this->CharSet = 'UTF-8';
	
		if(!$this->From) $this->From = $CONFIG['from_email'];

		if(!$this->FromName) $this->FromName = $CONFIG['from_name'];
		
		if(!$this->Sender) $this->Sender = $CONFIG['from_email'];
      
		$this->Priority = $this->priority;
	}
}
?>