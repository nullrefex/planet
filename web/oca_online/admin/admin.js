// Определяет ширину окна браузера
function getClientWidth(){
  return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientWidth:document.body.clientWidth;
}


 
// Определяет высоту окна браузера
function getClientHeight(){
  return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientHeight:document.body.clientHeight;
}



// Создаёт кроссбраузерный объект XMLHTTP
function getXmlHttp() {
	var xmlhttp;
	try {
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (E) {
			xmlhttp = false;
		}
	}
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}



// Установка курсора в окно формы
function setSelectionRange(input, selectionStart, selectionEnd) {
  if (input.setSelectionRange) {
	input.focus();
	input.setSelectionRange(selectionStart, selectionEnd);
  }
  else if (input.createTextRange) {
	var range = input.createTextRange();
	range.collapse(true);
	range.moveEnd('character', selectionEnd);
	range.moveStart('character', selectionStart);
	range.select();
  }
}



function setCaretToPos (input, pos) {
	setSelectionRange(input, pos, pos);
}



// Открытие окна формы
function SingleFormOpen(id, lang_id) {

	document.getElementById('show_' + id + "_" + lang_id).style.display = 'none';
	document.getElementById('edit_' + id + "_" + lang_id).style.display = '';
		
	// Установка курсора в конец текста
	var length = document.getElementById('input_' + id + "_" + lang_id).value.length;
	setCaretToPos(document.getElementById('input_' + id + "_" + lang_id), length);
}



// Сохранение данных из окна формы
function SubmitSingleForm(id, lang_id, link) {
	
	// Получаем значение поля
	var value = document.getElementById('input_' + id + "_" + lang_id).value;
	value = encodeURIComponent(value);
	var show = document.getElementById('show_' + id + '_' + lang_id)
	var edit = document.getElementById('edit_' + id + '_' + lang_id)
	var input = document.getElementById('input_' + id + '_' + lang_id)
	
	// Отправляем
	var xmlhttp = getXmlHttp();
	xmlhttp.open('POST', link, true);
	xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xmlhttp.send('id=' + id + '&lang_id=' + lang_id + '&value=' + value);
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if(xmlhttp.status == 200) {
				var response = xmlhttp.responseText;

				input.innerHTML = response;
				edit.style.display = 'none';

				show.style.display = '';
				show.innerHTML = response;
				
				show.style.border = "2px solid #ff0000";
			}
		}
	}
}


// Отправляем тест во фрейме
function OpenTest(test_id, back_url) {
	top.frames['frame2'].location.href = 'test_graph?test_id=' + test_id + '&back_url=' + back_url;
	document.getElementById('tr_' + test_id).className = "list_table_tr_seen";
}