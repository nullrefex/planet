<?php
///////////////////////////////////////
// OCA 2.2                           //
// Распечатка сравнительного графика //
///////////////////////////////////////

defined( '__DDD__' ) or die();



$color = intval($_REQUEST['color']);



?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<title>ОКСФОРДСКИЙ ТЕСТ АНАЛИЗА ЛИЧНОСТИ</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
body {
	color: #000000;
	font-family: Arial;
	font-size: 16px;
}
#content {
	width: 972px;
	margin: 0 auto;
}
h1 {
	color: #000000;
	font-family: Arial;
	font-size: 20px;
	text-align: center;
	font-weight: bold;
	padding: 0;
	margin: 10px 0 10px 0;
}
h2 {
	color: #000000;
	font-family: Arial;
	font-size: 18px;
	text-align: center;
	font-weight: normal;
}
p {
	padding: 5px 0;
	margin: 0;
	display: block;
	border-bottom: 1px solid #999999;
}
table {
	border-collapse: collapse;
}
.h1_sup {
	color: #000000;
	font-family: Arial;
	font-size: 14px;
	font-weight: bold;
}
.answer {
	border: 1px solid #000000;
	width: 14px;
	height: 14px;
}
</style>
<body>

<div id="content">

<?php
// Есть графики для сравнения
if (count($_SESSION['compare']) > 0) echo '<img src="compare_img?color='.$color.'" class="graph_img">';

// Не выбраны тесты для сравнения
else echo '<br><div class="alert" align="center">Не заданы тесты для сравнения</div>';
?>
</div>

<script type="text/javascript">print();</script>

</body>
</html>