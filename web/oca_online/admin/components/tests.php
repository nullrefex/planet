<?php
///////////////////
// OCA 2.2       //
// Список тестов //
///////////////////

defined( '__DDD__' ) or die();



// List of tests
function Show_Tests_List($res, $components, $cols) {
	global $CONFIG;
	
	?>
	<table border="0" cellspacing="0" cellpadding="5" align="left" class="list_table">
	<?php
	while ($test = mysql_fetch_array($res)) {
		
		$test['lfs_name'] = $test['last_name'].' '.$test['first_name'].' '.$test['second_name'];
		
		$test['age'] = Get_Age($test['birth_date'], $test['test_date']);
		
		if ($test['seen']) $tr_class = 'list_table_tr_seen';
		else $tr_class = 'list_table_tr';
		
		?>
		<tr class="<?php echo $tr_class?>" onclick="OpenTest('<?php echo $test['test_id']?>', '<?php echo urlencode($_SERVER['REQUEST_URI'])?>')" id="tr_<?php echo $test['test_id']?>">
		
			<td valign="top" class="list_table_td" width="10">
				<?php
				echo '<span class="list_test_id">#'.$test['test_id'].'</span><br><span class="list_test_date">'.norm_date_short($test['test_date']).'</span>';
				?>
			</td>
			
			<td valign="top" class="list_table_td">
				<div style="position: relative;"><?php echo $test['lfs_name']?></div>
				<?php
				if ($test['notes'])  echo '<div class="test_notes">'.str_replace("\n", '<br>', $test['notes']).'</div>';
				if ($CONFIG['site'] != $test['source_site']) echo '<div><span class="list_test_source">↓'.$test['source_site'].'</span></div>';
				?>
			</td>

			<td valign="top" class="list_table_td" width="50" align="center">
				<div class="list_test_age" title="Возраст при заполнении теста"><b><?php echo $test['age']?></b> лет</div>
				<?php
				if ($test['grah_mailed']) {
					?>
					<img class="grah_mailed" src="images/letter.png" border="0" title="<?php echo norm_date($test['grah_mailed'])?> график был отправлен на E-mail">
					<?php
				}
				?>
			</td>
			
		</tr>
		<?php
	}
	?>
	</table>
	<?php
}


// Pagination
function Show_Pagination($num, $test_per_page, $p) {
	$btns = 3; // кнопок с одной стороны
	if ($_GET['search']) $lnk = 'tests?search=1&';
	else $lnk = 'tests?';
	
	$p_num = ceil($num / $test_per_page);
	
	if ($p < ($btns + 1)) {
		$fr = 1;
		$tl = ($btns * 2) + 1;
	
	} else if ($p > ($p_num - $btns)) {
		$fr = $p_num - ($btns * 2);
		$tl = $p_num;
		
	} else {
		$fr = $p - $btns;
		$tl = $p + $btns;
	}
	
	if ($fr < 1) $fr = 1;
	if ($tl > $p_num) $tl = $p_num;
		
	if ($fr > 1) {
		$md = $p - 10;
		if ($md < 1) $md = 1;
		echo '<a class="pagination_page_more" href="'.$lnk.'p='.$md.'"><< -10</a>';
	}
	for ($i=$fr; $i<=$tl; $i++) {
		
		if ($i == $p) {
			echo '<span class="pagination_page_selected">'.$i.'</span>';

		} else {
			echo '<a class="pagination_page" href="'.$lnk.'p='.$i.'">'.$i.'</a>';
		}
	}
	if ($p_num > $tl) {
		$pd = $p + 10;
		if ($pd > $p_num) $pd = $p_num;
		echo '<a class="pagination_page_more" href="'.$lnk.'p='.$pd.'">+10 >></a>';
	}

}



$date_now = date('Y-m-d');

$search = $_GET['search'];

$p = intval($_GET['p']);
if ($p < 1) $p = 1;
$limit_start = $admin['admin_test_per_page'] * ($p - 1);

// Clearing filter
if ($_POST['clear']) {
	unset($_SESSION['test_id']);
	unset($_SESSION['from']);
	unset($_SESSION['till']);
	unset($_SESSION['last_name']);
	unset($_SESSION['first_name']);
	unset($_SESSION['second_name']);
	unset($_SESSION['sex']);
	unset($_SESSION['birth_year_from']);
	unset($_SESSION['birth_year_till']);
	unset($_SESSION['email']);
	unset($_SESSION['phone']);
	unset($_SESSION['address']);
	unset($_SESSION['post']);
	
	header('Location: '.$_SERVER['REQUEST_URI']);
	exit();
}



// Delete test
if ($_REQUEST['delete'] && $_REQUEST['test_id']) {

	mysql_query(' UPDATE '.$CONFIG['db_prefix'].'tests SET deleted = 1 WHERE test_id = '.$_REQUEST['test_id'].' LIMIT 1 ');
	header('Location: '.$_SERVER['REQUEST_URI']);
	exit;
}



// Save filter
if ($_REQUEST['show']) {
	
	$sf = standart_date(trim($_POST['n_from']));
	$st = standart_date(trim($_POST['n_till']));
	if (is_date($sf)) $_SESSION['from'] = $sf; else unset($_SESSION['from']);
	if (is_date($st)) $_SESSION['till'] = $st; else unset($_SESSION['till']);
	
	$test_id = intval(trim($_POST['test_id']));
	if ($test_id > 0) $_SESSION['test_id'] = $test_id;
	else unset($_SESSION['test_id']);
	
	$last_name = trim($_POST['last_name']); $_SESSION['last_name'] = $last_name;
	$first_name = trim($_POST['first_name']); $_SESSION['first_name'] = $first_name;
	$second_name = trim($_POST['second_name']); $_SESSION['second_name'] = $second_name;
	
	$email = trim($_POST['email']); $_SESSION['email'] = $email;
	$phone = trim($_POST['phone']); $_SESSION['phone'] = $phone;
	$address = trim($_POST['address']); $_SESSION['address'] = $address;
	$post = trim($_POST['post']); $_SESSION['post'] = $post;
	
	$_SESSION['sex'] = $_POST['sex'];
	
	$birth_year_from = intval(trim($_POST['birth_year_from']));
	if ($birth_year_from > 0) $_SESSION['birth_year_from'] = $birth_year_from;
	else unset($_SESSION['birth_year_from']);
	
	$birth_year_till = intval(trim($_POST['birth_year_till']));
	if ($birth_year_till > 0) $_SESSION['birth_year_till'] = $birth_year_till;
	else unset($_SESSION['birth_year_till']);
	
	if ($birth_year_from > $birth_year_till) $birth_year_from = $birth_year_till;

	header('Location: '.$_SERVER['REQUEST_URI']);
	exit();
}



// Add tab
/*
if ($_GET['add']) {
	$add = intval($_GET['add']);
	if ($add > 0) $_SESSION['tabs'][$add] = 1;
}
*/



?>
<style type="text/css">@import url("calendar/calendar-win2k-cold-2.css");</style>
<script type="text/javascript" src="calendar/calendar.js"></script>
<script type="text/javascript" src="calendar/calendar-ru.js"></script>
<script type="text/javascript" src="calendar/calendar-setup.js"></script>



<div style="margin: 0px;">
	<a class="submenu_item" style="text-decoration: none;" href="tests">Все тесты</a>
	<a class="submenu_item" style="text-decoration: none;" href="tests?search=1">Поиск тестов</a>
</div>
<div style="margin: 5px;">
	<?php
	if ($search) echo 'Экспорт рез-тов поиска в <a href="export_csv?tpl=blank&source=search" download>csv</a> | <a href="export_xls?tpl=blank&source=search" download>xls</a>';
	else echo 'Экспорт всех тестов в <a href="export_csv?tpl=blank&source=all" download>csv</a> | <a href="export_xls?tpl=blank&source=all" download>xls</a>';
	?>
</div>			
<?php
// Поиск тестов
if ($search) {
	?>

	<form method="post" action="tests?search=1">
	<table border="0" cellspacing="0" cellpadding="5" class="clear_table">
		<tr style="border-bottom: 1px solid #dddddd;">
			<td valign="top" style="border-right: 1px solid #dddddd;">ID</td>
			<td align="left"><input type="text" name="test_id" style="width: 60px;" value="<?php echo $_SESSION['test_id']; ?>"></td>
		</tr>

		<tr style="border-bottom: 1px solid #dddddd;">
			<td valign="top" style="border-right: 1px solid #dddddd;">Дата<br>заполнения</td>
			<td align="left">
				ОТ <nobr><input id="from" type="text" name="n_from" style="width: 90px;" value="<?php echo norm_date($_SESSION['from']); ?>" readonly="readonly">
				<img src="calendar/calendar.gif" width="19" height="16" align="absmiddle" id="idate_from" style="cursor: pointer;"></nobr>
				<script type="text/javascript">Calendar.setup({inputField : "from", ifFormat : "%d.%m.%Y", button: "idate_from"})</script>

				ДО <nobr><input id="till" type="text" name="n_till" style="width: 90px;" value="<?php echo norm_date($_SESSION['till']); ?>" readonly="readonly">
				<img src="calendar/calendar.gif" width="19" height="16" align="absmiddle" id="idate_till" style="cursor: pointer;"></nobr>
				<script type="text/javascript">Calendar.setup({inputField : "till", ifFormat : "%d.%m.%Y", button: "idate_till"})</script>
			</td>
		</tr>
						
		<tr style="border-bottom: 1px solid #dddddd;">
			<td valign="top" style="border-right: 1px solid #dddddd;">Фамилия</td>
			<td align="left">
				<input type="text" name="last_name" style="width: 95%;" value="<?php echo htmlspecialchars($_SESSION['last_name']); ?>"><br>
			</td>
		</tr>

		<tr style="border-bottom: 1px solid #dddddd;">
			<td valign="top" style="border-right: 1px solid #dddddd;">Имя</td>
			<td align="left">
				<input type="text" name="first_name" style="width: 95%;" value="<?php echo htmlspecialchars($_SESSION['first_name']); ?>"><br>
			</td>
		</tr>

		<tr style="border-bottom: 1px solid #dddddd;">
			<td valign="top" style="border-right: 1px solid #dddddd;">Отчество</td>
			<td align="left">
				<input type="text" name="second_name" style="width: 95%;" value="<?php echo htmlspecialchars($_SESSION['second_name']); ?>">
			</td>
		</tr>
		
		<tr style="border-bottom: 1px solid #dddddd;">
			<td valign="top" style="border-right: 1px solid #dddddd;">Пол</td>
			<td align="left">
				<input type="radio" name="sex" value=""<?php if (!$_SESSION['sex']) echo ' checked'; ?>>ВСЕ
				<input type="radio" name="sex" value="M"<?php if ($_SESSION['sex'] == 'M') echo ' checked'; ?>>MУЖ
				<input type="radio" name="sex" value="F"<?php if ($_SESSION['sex'] == 'F') echo ' checked'; ?>>ЖЕН
			</td>
		</tr>
		
		<tr style="border-bottom: 1px solid #dddddd;">
			<td valign="top" style="border-right: 1px solid #dddddd;">Год рождения</td>
			<td align="left">
				ОТ <input type="text" name="birth_year_from" style="width: 45px;" value="<?php echo $_SESSION['birth_year_from']; ?>">
				ДО <input type="text" name="birth_year_till" style="width: 45px;" value="<?php echo $_SESSION['birth_year_till']; ?>">
			</td>
		</tr>

		<tr style="border-bottom: 1px solid #dddddd;">
			<td valign="top" style="border-right: 1px solid #dddddd;">Телефон</td>
			<td align="left">
				<input type="text" name="phone" style="width: 95%;" value="<?php echo htmlspecialchars($_SESSION['phone']); ?>"><br>
			</td>
		</tr>

		<tr style="border-bottom: 1px solid #dddddd;">
			<td valign="top" style="border-right: 1px solid #dddddd;">E-mail</td>
			<td align="left"><input type="text" name="email" style="width: 95%;" value="<?php echo htmlspecialchars($_SESSION['email']); ?>"></td>
		</tr>

		<tr style="border-bottom: 1px solid #dddddd;">
			<td valign="top" style="border-right: 1px solid #dddddd;">Адрес</td>
			<td align="left">
				<input type="text" name="address" style="width: 95%;" value="<?php echo htmlspecialchars($_SESSION['address']); ?>"><br>
			</td>
		</tr>
		
		<tr style="border-bottom: 1px solid #dddddd;">
			<td valign="top" style="border-right: 1px solid #dddddd;">Должность</td>
			<td align="left">
				<input type="text" name="post" style="width: 95%;" value="<?php echo htmlspecialchars($_SESSION['post']); ?>"><br>
			</td>
		</tr>

		<tr>
			<td></td>
			<td align="left"><input type="submit" name="show" value="Найти"><input type="submit" name="clear" value="Очистить"></td>
		</tr>
	</table>
	</form>

	<?php
	if ($_SESSION['from']) { $sql .= ' AND test_date >= "'.$_SESSION['from'].'" '; $set_search = true; }
	if ($_SESSION['till']) { $sql .= ' AND test_date <= "'.$_SESSION['till'].'" '; $set_search = true; }
	if ($_SESSION['test_id'] > 0) { $sql .= ' AND test_id LIKE "%'.$_SESSION['test_id'].'%" '; $set_search = true; }
	if ($_SESSION['sex']) { $sql .= ' AND sex =  "'.$_SESSION['sex'].'" '; $set_search = true; }
	if ($_SESSION['birth_year_from'] > 0) { $sql .= ' AND birth_date >=  "'.$_SESSION['birth_year_from'].'-01-01" '; $set_search = true; }
	if ($_SESSION['birth_year_till'] > 0) { $sql .= ' AND birth_date <=  "'.$_SESSION['birth_year_till'].'-12-31" '; $set_search = true; }
	if ($_SESSION['last_name']) { $sql .= ' AND last_name LIKE "%'.$_SESSION['last_name'].'%" '; $set_search = true; }
	if ($_SESSION['first_name']) { $sql .= ' AND first_name LIKE "%'.$_SESSION['first_name'].'%" '; $set_search = true; }
	if ($_SESSION['second_name']) { $sql .= ' AND second_name LIKE "%'.$_SESSION['second_name'].'%" '; $set_search = true; }
	if ($_SESSION['email']) { $sql .= ' AND email LIKE "%'.$_SESSION['email'].'%" '; $set_search = true; }
	if ($_SESSION['phone']) { $sql .= ' AND phone LIKE "%'.$_SESSION['phone'].'%" '; $set_search = true; }
	if ($_SESSION['address']) { $sql .= ' AND address LIKE "%'.$_SESSION['address'].'%" '; $set_search = true; }
	if ($_SESSION['post']) { $sql .= ' AND post LIKE "%'.$_SESSION['post'].'%" '; $set_search = true; }
		
	// Условия поиска заданы
	if ($set_search) {

		// Считаем количество тестов без пагинации
		$res_num = mysql_query(' SELECT COUNT(*) FROM '.$CONFIG['db_prefix'].'tests WHERE deleted = 0 '.$sql);
		$row = mysql_fetch_row($res_num);
		$num = $row[0];
			
		$res = mysql_query(' SELECT * FROM '.$CONFIG['db_prefix'].'tests WHERE deleted = 0 '.$sql.' ORDER BY test_id DESC LIMIT '.$limit_start.', '.$admin['admin_test_per_page'].' ');
		if (mysql_num_rows($res) > 0) {

			echo '<div class="pagination">';
			Show_Pagination($num, $admin['admin_test_per_page'], $p);
			echo '</div>';
			
			Show_Tests_List($res, $components, $cols);
			
			echo '<div class="pagination">';
			Show_Pagination($num, $admin['admin_test_per_page'], $p);
			echo '</div>';
			
		} else echo '<br><div class="alert">Не найдены тесты, соответствующие условиям поиска</div>';
		
	// Условия поиска НЕ заданы
	} else echo '<br><div class="alert">Не заданы условия поиска</div>';
				
				
// Последние тесты
} else {
	
	// Считаем количество тестов без пагинации
	$res_num = mysql_query(' SELECT COUNT(*) FROM '.$CONFIG['db_prefix'].'tests WHERE deleted = 0 ');
	$row = mysql_fetch_row($res_num);
	$num = $row[0];
		
	$res = mysql_query(' SELECT * FROM '.$CONFIG['db_prefix'].'tests WHERE deleted = 0 ORDER BY test_id DESC LIMIT '.$limit_start.', '.$admin['admin_test_per_page'].' ');
	if (@mysql_num_rows($res) > 0) {
		
		echo '<div class="pagination">';
		Show_Pagination($num, $admin['admin_test_per_page'], $p);
		echo '</div>';
		
		Show_Tests_List($res, $components, $cols);
		
		echo '<div class="clearfix"></div><div class="pagination">';
		Show_Pagination($num, $admin['admin_test_per_page'], $p);
		echo '</div>';
	}
}
?>
<br>