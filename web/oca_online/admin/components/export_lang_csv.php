<?php
///////////////////
// OCA 2.2       //
// Экспорт в CSV //
///////////////////

defined( '__DDD__' ) or die();



// Получаем параметры
$lang_id = $_GET['lang_id'];
$type = $_GET['type'];

if ($lang_id && $type) {

	// Заголовки скачивания .CSV
	header('Content-Type: application/force-download');
	header('Content-Type: application/octet-stream');
	header('Content-Type: application/download');
	header('Content-Disposition: attachment;filename=OCA-'.strtoupper($type).'-'.$lang_id.'.csv');
	header('Content-Transfer-Encoding: binary');



	// Вопросы теста
	if ($type == 'questions') {
		
		$sql = ' SELECT "'.$type.'", "'.$lang_id.'", question_id, question_text FROM '.$CONFIG['db_prefix'].'questions WHERE lang_id = "'.$lang_id.'" AND question_text <> "" ORDER BY question_id ASC ';

	// ИНфоблоки
	} else if ($type == 'iblocks') {
		
		$sql = ' SELECT "'.$type.'", "'.$lang_id.'", i.iblock_name, t.iblock_text FROM '.$CONFIG['db_prefix'].'iblocks i
				LEFT JOIN '.$CONFIG['db_prefix'].'iblocks_translations t ON i.iblock_name = t.iblock_name  
				WHERE t.lang_id = "'.$lang_id.'" AND t.iblock_text <> "" ';

	// Основные поля
	} else if ($type == 'fields') {

		$sql = ' SELECT "'.$type.'", "'.$lang_id.'", f.field_name, t.field_title, t.field_info, t.field_testvalue FROM '.$CONFIG['db_prefix'].'fields f
				LEFT JOIN '.$CONFIG['db_prefix'].'fields_translations t ON f.field_name = t.field_name 
				WHERE t.lang_id = "'.$lang_id.'" AND (t.field_title <> "" OR t.field_info <> "" OR t.field_testvalue <> "") ';

	}

	
	
	// Пишем в CSV
	if ($sql) {
		
		$res = mysql_query($sql);
		if (@mysql_num_rows($res) > 0) {

			ob_start();
			
			$fp = fopen("php://output", 'w');
			
			// Пишем язык
			$res_l = mysql_query(' SELECT "languages", lang_id, lang_name FROM '.$CONFIG['db_prefix'].'languages WHERE lang_id = "'.$lang_id.'" ');
			$row = mysql_fetch_row($res_l);
			fputcsv($fp, $row);
			
			// Пишем перевод
			while ($row = mysql_fetch_row($res)) fputcsv($fp, $row);

			fclose($fp);
			
			echo ob_get_clean();
		}
		
	}
	
}
?>