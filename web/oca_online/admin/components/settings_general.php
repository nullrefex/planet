<?php
/////////////////////
// OCA 2.2         //
// Общие настройки //
/////////////////////

defined( '__DDD__' ) or die();



if (!$admin['admin_status']) $disabled = ' disabled="disabled"';

// Saving
if ($_POST['save'] && $admin['admin_status']) {

	// Read old settings
	$res_old = mysql_query(' SELECT * FROM '.$CONFIG['db_prefix'].'settings WHERE id = 1 ');
	$settings_old = mysql_fetch_array($res_old);
	
	$debug_mode = intval($_POST['debug_mode']);
	$pers_info_consent = intval($_POST['pers_info_consent']);
		
	$redirect = trim($_POST['redirect']);
	if ($redirect) {
		$rf = file_get_contents($redirect.'/client/test.php?check=1');
		$rf = trim($rf);
		if (!$rf) {
			$_SESSION['notification'][] = '<div class="nok">Ошибка: неверный путь переадрессации тестов. Скрипт не найден.</div>';
			$redirect = '';
		} else if ($rf <> $CONFIG['version']) {
			$_SESSION['notification'][] = '<div class="nok">Ошибка: неверная версия программы ('.$rf.') по указанному пути переадрессации. 
				Должна быть '.$CONFIG['version'].', как на этом сайте.</div>';
			$redirect = '';
		}
	} else $redirect = '';
	
	$redirect_leave_copy = intval($_POST['redirect_leave_copy']);
		
	// Saving settings
	$res = mysql_query(' UPDATE '.$CONFIG['db_prefix'].'settings SET debug_mode = '.$debug_mode.', pers_info_consent = '.$pers_info_consent.', 
		redirect = "'.addslashes($redirect).'", redirect_leave_copy = '.$redirect_leave_copy.' WHERE id = 1 ');
	if ($res) $_SESSION['notification'][] = '<div class="ok">Настройки сохранены.</div>';
	else $_SESSION['notification'][] = '<div class="nok">Ошибка: не удалось сохранить настройки, ошибка БД.</div>';
	
	header('Location: '.$_SERVER['REQUEST_URI']);
	exit(); 
}

// Get test settings
$res_conf = mysql_query(' SELECT * FROM '.$CONFIG['db_prefix'].'settings WHERE id = 1 ');
$settings = mysql_fetch_array($res_conf);
?>

<h1>Общие настройки</h1>
	
<?php
if ($admin['admin_status']) echo '<form method="post">';
?>

<table border="1" cellspacing="0" cellpadding="5" align="center" class="settings_table">
	<tr>
		<th class="table-1" width="180"><b>Режим отладки</b></th>
		<td class="table-2" style="max-width: 800px;">
			<p class="field_info">
			<input type="checkbox" <?php echo $disabled?> name="debug_mode" value="1"<?php if ($settings['debug_mode']) echo ' checked'; ?>>
			- При включенном режиме отладки форма теста на сайте будет заполняться демо-данными.</p>
		</td>
	</tr>
	<tr>
		<th class="table-1" width="180"><b>Согласие на обработку данных</b></th>
		<td class="table-2" style="max-width: 800px;">
			<p class="field_info">
			<input type="checkbox" <?php echo $disabled?> name="pers_info_consent" value="1"<?php if ($settings['pers_info_consent']) echo ' checked'; ?>>
			- Перед отправкой теста будет показываться окошко, где нужно поставить галочку, подтверждая согласие на обработку 
			персональных данных. Текст можно заменить в инфоблоках.</p>
		</td>
	</tr>
	<tr>
		<th class="table-1" width="180"><b>Отправлять тесты на другой сайт</b></th>
		<td class="table-2" style="max-width: 800px;">
			<div><input type="text" <?php echo $disabled?> name="redirect" style="width: 95%;" value="<?php echo htmlspecialchars($settings['redirect']); ?>"></div>
			<p class="field_info">Введите путь к скриптам программы <i><b>OCA Online <?php echo $CONFIG['version']?></b></i> на другом сайте, например, "<i><b>http://othersite.com/oca_online</b></i>" 
			(без слеша в конце), куда будут переадрессовываться все тесты OCA, заполненные на этом сайте. Это может быть необходимо, если у вас несколько сайтов 
			с формами теста, а обрабатывать их вы хотите в одной админке.</p>
				
			<div><input type="checkbox" <?php echo $disabled?> name="redirect_leave_copy" value="1" <?php if ($settings['redirect_leave_copy']) echo ' checked'; ?>>
			- оставлять копии тестов OCA на этом сайте</div>
		</td>
	</tr>
</table>
<br>
<?php
if ($admin['admin_status']) echo '<div align="center"><input type="submit" name="save" value="Сохранить"></div></form><br>';
?>