<?php
////////////////////////////
// OCA 2.2                //
// Профиль администратора //
////////////////////////////

defined( '__DDD__' ) or die();



function Get_Ext ($img) {
	$pos = strpos($img, '.');
	while ($pos > -1) {
		$img = substr($img, $pos+1);
		$pos = strpos($img, '.');
	}
	$img = strtolower($img);
	if ($img == 'jpeg') $img = 'jpg';
	if (($img == 'jpg') || ($img == 'gif') || ($img == 'png')) return $img;
	else return false;
}

function Resise_And_Copy ($img1, $img2, $ext, $x_max, $y_max) {
	
	// Получаем название файла новой картинки
	$img2 = $img2.'.'.$ext;
	
	// Создаем образ закачанной картинки
	if ($ext == 'jpg') $im = imagecreatefromjpeg($img1);
	if ($ext == 'gif') $im = imagecreatefromgif($img1);
	if ($ext == 'png') $im = imagecreatefrompng($img1);
	
	// Измеряем закачанную картинку
	$x = imagesx($im);
	$y = imagesy($im);
	
	// Если картинка не превышает допустимые размеры или изменеие не требуется ()
	if (($x_max == 0) || ($y_max == 0) || (($x <= $x_max) && ($y <= $y_max))) {

		// Создаем образ новой картинки по размерам закачанной 
		$new_im = ImageCreateTrueColor($x,$y);
		// Накладываем закачанную картинку на образ
		ImageCopy($new_im,$im,0,0,0,0,$x,$y);

	// Если картинка укладывается в допустимые размеры
	} else {

		/////////////// Рассчитываем новые размеры картинки
		// Если не укладывается длина
		if (($x > $x_max) && ($y <= $y_max)) {
			$new_x = $x_max;
			$new_y = intval($y * $x_max / $x);
			
		// Если не укладывается высота
		} else if (($x <= $x_max) && ($y > $y_max)) {
			$new_y = $y_max;
			$new_x = intval($x * $y_max / $y);
		
		// Если не укладывается и длина и высота
		} else {
			// Уменьшаем по длине
			$new_x = $x_max;
			$new_y = intval($y * $x_max / $x);
			
			// Если не укладывается новая высота, то заново уменьшаем по высоте
			if ($new_y > $y_max) {
				$new_y = $y_max;
				$new_x = intval($x * $y_max / $y);
			}
		}
		
		// Создаем образ новой картинки по расчетным уменьшенным размерам
		$new_im = ImageCreateTrueColor($new_x,$new_y);
		// Накладываем закачанную картинку на образ, сжимая ее до нужного размера
		ImageCopyResampled($new_im,$im,0,0,0,0,$new_x,$new_y,$x,$y);
	
	}
	
	// Создаем файл из образа
	if ($ext == 'jpg') imagejpeg($new_im, $img2);
	if ($ext == 'gif') imagegif($new_im, $img2);
	if ($ext == 'png') imagepng($new_im, $img2); 
	
	// Уничтожаем образ
	imagedestroy($im);
	imagedestroy($new_im);
	
	return $ext;
}



function Cut_And_Copy ($img1, $img2, $ext, $new_x, $new_y) {
	
	// Получаем название файла новой картинки
	$img2 = $img2.'.'.$ext;
	
	// Создаем образ закачанной картинки
	if ($ext == 'jpg') $im = imagecreatefromjpeg($img1);
	if ($ext == 'gif') $im = imagecreatefromgif($img1);
	if ($ext == 'png') $im = imagecreatefrompng($img1);
	
	// Измеряем закачанную картинку
	$x = imagesx($im);
	$y = imagesy($im);

	// Создаем образ новой картинки по расчетным уменьшенным размерам
	$new_im = ImageCreateTrueColor($new_x,$new_y);
	// Накладываем закачанную картинку на образ, сжимая ее до нужного размера
	ImageCopyResampled($new_im,$im,0,0,0,0,$new_x,$new_y,$x,$y);
	
	// Создаем файл из образа
	if ($ext == 'jpg') imagejpeg($new_im, $img2);
	if ($ext == 'gif') imagegif($new_im, $img2);
	if ($ext == 'png') imagepng($new_im, $img2); 

	// Уничтожаем образ
	imagedestroy($im);
	imagedestroy($new_im);
	
	return $ext;
}



// Save settings
if ($_POST['save']) {

	// Read old settings
	$res = mysql_query(' SELECT * FROM '.$CONFIG['db_prefix'].'admins WHERE admin_id = '.$my_id);
	$admin_settings_old = mysql_fetch_array($res);
	
	////////////////// Updating simple fields //////////////////
	$admin_name = trim($_POST['admin_name']);
	$admin_email_subject = trim($_POST['admin_email_subject']);
	$admin_email_text = trim($_POST['admin_email_text']);
	$admin_test_notification = intval($_POST['admin_test_notification']);
	$admin_email_charset = trim($_POST['admin_email_charset']);
	$admin_auth_type = intval($_POST['admin_auth_type']);
		
	$admin_test_per_page = intval($_POST['admin_test_per_page']);
	if ($admin_test_per_page < 10) $admin_test_per_page = 10;
	if ($admin_test_per_page > 1000) $admin_test_per_page = 1000;
	
	$admin_columns = serialize($_POST['cols']);

	// Saving
	$res1 = mysql_query(' UPDATE '.$CONFIG['db_prefix'].'admins SET admin_name = "'.addslashes($admin_name).'", admin_test_notification = '.$admin_test_notification.', 
		admin_email_charset = "'.$admin_email_charset.'", admin_auth_type = '.$admin_auth_type.', admin_test_per_page = '.$admin_test_per_page.', 
		admin_columns = "'.addslashes($admin_columns).'", admin_email_subject = "'.addslashes($admin_email_subject).'", 
		admin_email_text = "'.addslashes($admin_email_text).'" WHERE admin_id = '.$my_id);
	if ($res1) $_SESSION['notification'][] = '<div class="ok">Настройки сохранены.</div>';
	else $_SESSION['notification'][] = '<div class="nok">Ошибка: настройки не сохранены, ошибка БД.</div>';

	////////////////////////////////// IMAGES //////////////////////////////////
	
	// Deleting images
	/*
	$delete_img = $_POST['delete_img'];
	if (is_array($delete_img) && (count($delete_img) > 0)) {
		foreach ($delete_img as $del_im => $one) {
			unlink ($_SERVER['DOCUMENT_ROOT'].'/'.$CONFIG['root_folder'].'/admin/bg_img/'.$my_id.'/full_'.$del_im);
			unlink ($_SERVER['DOCUMENT_ROOT'].'/'.$CONFIG['root_folder'].'/admin/bg_img/'.$my_id.'/lb_'.$del_im);
		}
	}
	
	// Если задана big картинка, то сохраняем ее
	if (strlen($_FILES['img_b']['name']) > 0) {
		
		// Находим расширение файла big картинки
		$ext_big = Get_Ext ($_FILES['img_b']['name']);
		
		// Если расширение соответствует, то...
		if ($ext_big) {
		
			// Создаем новое название big картинки
			$img_big = time();
			
			// Сохраняем big картинку
			Resise_And_Copy($_FILES['img_b']['tmp_name'], $_SERVER['DOCUMENT_ROOT'].'/'.$CONFIG['root_folder'].'/admin/bg_img/'.$my_id.'/full_'.$img_big, $ext_big, 1920, 3000);
			
			// Сохраняем small картинку
			Cut_And_Copy($_FILES['img_b']['tmp_name'], $_SERVER['DOCUMENT_ROOT'].'/'.$CONFIG['root_folder'].'/admin/bg_img/'.$my_id.'/lb_'.$img_big, $ext_big, 170, 96);
	
		} else $_SESSION['notification'][] = '<div class="nok">Ошибка: не разрешенное расширение файла изображения.</div>';
	}
	*/
		
	
	
	////////////////////////////////// E-mail confirm //////////////////////////////////
	$admin_email_code = trim($_POST['admin_email_code']);
	
	// Code entered, email was not confirmed before
	if ($admin_email_code && $admin_settings_old['admin_email_code']) {
		
		// Code is correct
		if ($admin_email_code == $admin_settings_old['admin_email_code']) {
		
			// Saving
			$res3 = mysql_query(' UPDATE '.$CONFIG['db_prefix'].'admins SET admin_email_code = "" WHERE admin_id = '.$my_id);
			if ($res3) $_SESSION['notification'][] = '<div class="ok">E-mail подтвержден.</div>';
			else $_SESSION['notification'][] = '<div class="nok">Ошибка: E-mail не подтвержден, ошибка БД.</div>';
			
		} else $_SESSION['notification'][] = '<div class="nok">Ошибка: E-mail не подтвержден, введен неправильный код.</div>';
		
	} // no confirmation
	
	////////////////// Change dangerous fields //////////////////
	$admin_email = trim($_POST['admin_email']);
	$admin_password_old = $_POST['admin_password_old'];
	$admin_password = $_POST['admin_password'];
	$admin_password_confirm = $_POST['admin_password_confirm'];
	
	// Dangerous fields entered
	if ($admin_email || $admin_password || $admin_password_confirm || $admin_password_old) {
		
		// Current pass is correct
		if (md5($admin_password_old) == $admin_settings_old['admin_password']) {
			
			// Change E-mail
			if ($admin_email) {
				
				// E-mail is valid
				if(filter_var($admin_email, FILTER_VALIDATE_EMAIL)) {
					
					// Create validation code
					$admin_email_code = rand(1000, 9999);
					
					// Saving
					$res2 = mysql_query(' UPDATE '.$CONFIG['db_prefix'].'admins SET admin_email = "'.addslashes($admin_email).'", 
						admin_email_code = "'.$admin_email_code.'" WHERE admin_id = '.$my_id);
					if ($res2) {
						$_SESSION['notification'][] = '<div class="ok">E-mail изменен. На новый E-mail выслан код для подтверждения.</div>';
						
						// Send code to admin E-mail
						$email_subject = "Изменен E-mail пользователя программы";
						$email_text = "Изменен E-mail в админке ".$CONFIG['robot_sender']." на ".$CONFIG['site']."\n";
						$email_text = "Новый E-mail: ".$admin_email."\n";
						$email_text .= "Вход в админку: ".$CONFIG['http_pgm_root']."/admin/\n";
						$email_text .= "Код подтверждения: ".$admin_email_code."\n\n";
						$email_text .= "------------------------\n";
						$email_text .= "Не отвечайте на это письмо, оно отправлено автоматически.\n";
						
						if ($CONFIG['robot_email']) $headers .= "From: ".$CONFIG['robot_sender']." <".$CONFIG['robot_email']."@".$CONFIG['site'].">\r\n";

						if (!$admin_email_charset) $admin_email_charset = 'UTF-8';
						//$header .= 'Content-type: text/plain; charset="'.$CONFIG['charsets_header'][$admin_email_charset].'"';

						if ($admin_email_charset <> 'UTF-8') {
							$email_subject = iconv('UTF-8', $admin_email_charset.'//IGNORE', $email_subject);
							$email_text = iconv('UTF-8', $admin_email_charset.'//IGNORE', $email_text);
						}
						mail($admin_email, $email_subject, $email_text, $headers);
						
					} else $_SESSION['notification'][] = '<div class="nok">Ошибка: E-mail не изменен, ошибка БД.</div>';
					
				} else $_SESSION['notification'][] = '<div class="nok">Ошибка: E-mail не изменен, новый E-mail не валиден.</div>';
				
			} // E-mail not entered

			// Change pass
			if ($admin_password || $admin_password_confirm) {
			
				// Pass and repass the same
				if ($admin_password === $admin_password_confirm) {
					
					// Pass >= 6 symbols
					if (strlen($admin_password) >= 6) {
						
						// Saving
						$res3 = mysql_query(' UPDATE '.$CONFIG['db_prefix'].'admins SET admin_password = "'.md5($admin_password).'" 
							WHERE admin_id = '.$my_id);
						if ($res3) $_SESSION['notification'][] = '<div class="ok">Пароль изменен.</div>';
						else $_SESSION['notification'][] = '<div class="nok">Ошибка: пароль не изменен, ошибка БД.</div>';
					
					} else $_SESSION['notification'][] = '<div class="nok">Ошибка: пароль не изменен, новый пароль содержит менее 6 символов.</div>';
				
				} else $_SESSION['notification'][] = '<div class="nok">Ошибка: пароль не изменен, новый прароль и повторный ввод пароля не совпадают.</div>';

			} // New pass not entered


			
		} else $_SESSION['notification'][] = '<div class="nok">Ошибка: E-mail и пароль не изменены, неверный текущий пароль.</div>';
		
	} // no dangerous fields
	
	// Unset authorization if auth type change
	if ($admin_settings_old['admin_auth_type'] <> $admin_auth_type) {
		mysql_query(' UPDATE '.$CONFIG['db_prefix'].'admins SET admin_cookie = "" WHERE admin_id = '.$my_id.' LIMIT 1 ');
		setcookie('testcookie', '', time()-3600);
		unset($_SESSION['my_id']);
		unset($my_id);
	}
	
	header('Location: '.$_SERVER['REQUEST_URI']);
	exit();
}



// Read admin settings
$res = mysql_query(' SELECT * FROM '.$CONFIG['db_prefix'].'admins WHERE admin_id = '.$my_id);
$admin_settings = mysql_fetch_array($res);
$cols = unserialize($admin_settings['admin_columns']);
if (!$admin_settings['admin_email_charset']) $admin_settings['admin_email_charset'] = 'UTF-8';



?>
<script type="text/javascript">
document.getElementById('tab_label_text_0').innerHTML = 'Настройки<br>профиля';
</script>


	<h1>Настройки профиля</h1>
	
	<form method="post" enctype="multipart/form-data">
	<table border="1" cellspacing="0" cellpadding="5" align="center" class="settings_table">
		
		<tr>
			<th class="table-1" width="180"><b>Имя</b></td>
			<td class="table-2" style="max-width: 800px;"><input type="text" name="admin_name" style="width: 90%;" value="<?php echo htmlspecialchars($admin_settings['admin_name']); ?> " autocomplete="off"></td>
		</tr>
		
		<tr>
			<th class="table-1" width="180"><b>Статус</b></td>
			<td class="table-2" style="max-width: 800px;">
			<?php
			if ($admin_settings['admin_status'] == 1) echo '<div>Администратор программы: <span style="color: green;">да</span></div>';
			else echo '<div>Администратор программы: <span style="color: red;">нет</span></div>';
			echo '<div>Администратор тестов: <span style="color: green;">да</span></div>';
			?>
			</td>
		</tr>
		
		<tr>
			<th class="table-1" width="180"><b>E-mail</b></td>
			<td class="table-2" style="max-width: 800px;">
				<?php
				if ($admin_settings['admin_email_code'] <> '') {
					echo '<div><span style="background: #ffcccc">'.$admin_settings['admin_email'].'</span>
						<input type="text" name="admin_email_code" autocomplete="off" value=" "></div>';
					echo '<div class="alert">Ваш E-mail не подтвержден. Введите код, который был отправлен на адрес
						'.$admin_settings['admin_email'].'. Для повторной отправки кода введите E-mail и текущий пароль ниже.</div>';
					
				} else echo $admin_settings['admin_email'].' <img src="images/check.png" border="0">';
				?>
				<p class="field_info">Автоматически отправляемые письма часто попадают в папку со спамом. В своем почтовом ящике настройте фильтр, 
				который НЕ будет отправлять в спам письма с адреса <b><?php echo $CONFIG['robot_email']?>@<?php echo $CONFIG['site']?></b> или содержащие в ТЕМЕ текст "<b>OCA</b>" - 
				три большие латинские буквы подряд.</p>
			</td>
		</tr>

		<tr>
			<th class="table-1" width="180"><b>Сменить E-mail и пароль</b></td>
			<td class="table-2" style="max-width: 800px;">
				<div style="margin: 0 0 10px 0;"><input type="password" name="admin_password_old" style="width: 200px;" autocomplete="off"> - текущий пароль</div>
				<div><input type="email" name="admin_email" style="width: 250px;" autocomplete="off"> - новый E-mail</div>
				<div><input type="password" name="admin_password" style="width: 200px;" autocomplete="off"> - новый пароль (мин. 6 символов)</div>
				<div><input type="password" name="admin_password_confirm" style="width: 200px;" autocomplete="off"> - повторите ввод нового пароля</div>
				<p class="field_info">В целях безопасности для смены E-mail и пароля требуется ввести текущий пароль.</p>
				<p class="field_info">* Если вы забыли текущий пароль, то осуществите выход из админки и воспользуйтесь 
				ссылкой на восстановление пароля через E-mail.</p>
				<p class="field_info">** Если вы забыли текущий пароль и, к тому же, потеряли доступ к E-mail, то обратитесь к
				администратору программы для смены E-mail. Затем воспользуйтесь восстановлением пароля через новый E-mail.</p>
			</td>
		</tr>

		<tr>
			<th class="table-1" width="180"><b>Способ авторизации</b></td>
			<td class="table-2" style="max-width: 800px;">
				<input type="radio" name="admin_auth_type" value="0"<?php if (!$admin_settings['admin_auth_type']) echo ' checked'; ?>>
				- авторизация по COOKIE
				<p class="field_info">* Если при входе поставить галочку "запомнить компьютер", то авторизация сохраняется навсегда, 
				точнее пока не будет осуществлен выход посредством кнопки "Выход". Если галочку "запомнить компьютер" не ставить, то 
				авторизация сохраняется строго до закрытия браузера.</p>
				<p class="field_info">** Нельзя работать в одном аккаунте с разных компьютеров одновременно.</p>
				
				<input type="radio" name="admin_auth_type" value="1"<?php if ($admin_settings['admin_auth_type']) echo ' checked'; ?>>
				- авторизация по SESSION
				<p class="field_info">* Авторизация сохраняется до закрытия браузера или до перезагрузки компьютера (зависит от типа браузера). 
				Кнопка "запомнить компьютер" ничего не дает.</p>
				<p class="field_info">** Можно работать в одном аккаунте с разных компьютеров одновременно.</p>
			</td>
		</tr>

		<tr>
			<th class="table-1" width="180"><b>Уведомлять меня на E-mail о новых тестах</b></td>
			<td class="table-2" style="max-width: 800px;">
				<input type="checkbox" name="admin_test_notification" value="1"<?php if ($admin_settings['admin_test_notification']) echo ' checked'; ?>>
				 - вам на почту будет приходить уведомление о новых тестах с данными отправителя. График теста можно будет увидеть, зайдя в админку.
				<?php
				if ($admin_settings['admin_email_code'] <> '') echo '<div class="alert">Ваш E-mail должен быть подтвержден.</div>';
				?>
			</td>
		</tr>

		<tr>
			<th class="table-1" width="180"><b>Кодировка писем-уведомлений</b></td>
			<td class="table-2" style="max-width: 800px;">
				<div>
					<select name="admin_email_charset">
						<?php
						foreach ($CONFIG['charsets'] as $chs) {
							if ($chs == $admin_settings['admin_email_charset']) echo '<option value="'.$chs.'" selected>'.$chs.'</option>';
							else echo '<option value="'.$chs.'">'.$chs.'</option>';
						}
						?>
					</select><br>
					Установите такую кодировку, в которой письма не будут приходить в виде "иероглифов" или оставьте UTF-8, если изначально все нормально.
				</div>
			</td>
		</tr>
		
		<tr>
			<th class="table-1" width="180"><b>Заготовка письма тестируемому</b></td>
			<td class="table-2" style="max-width: 800px;">
				Тема письма<br>
				<input type="text" name="admin_email_subject" style="width: 90%;" value="<?php echo htmlspecialchars($admin_settings['admin_email_subject']); ?>">
				<br>
				Текст письма<br>
				<textarea name="admin_email_text" style="width: 90%; height: 200px;"><?php echo htmlspecialchars($admin_settings['admin_email_text']); ?></textarea>
				<br>
				<p class="field_info"><b>Напишите:</b><br>
					{name} - для подстановки имени человека, например <i>"Здравствуйте, {name}!"</i><br>
					{admin-name} - ваше имя<br>
					{admin-email} - ваш E-mail
				</p>
				<p class="field_info">* График OCA будет отправлен во вложении к письму.</p>
			</td>
		</tr>
		
		<tr>
			<th class="table-1" width="180"><b>Тестов в списке на одну страницу</b></td>
			<td class="table-2" style="max-width: 800px;">
				<input type="text" name="admin_test_per_page" style="width: 50px;" value="<?php echo $admin_settings['admin_test_per_page']; ?>">
				- от 10 до 1000 в списке тестов на страницу
			</td>
		</tr>
		
		<tr>
			<th class="table-1" width="180"><b>Настройки экспорта</b></td>
			<td class="table-2" width="550">
				<p class="field_info">Выберите нужные колонки для файла экспорта</p>
				
					<div><input type="checkbox" name="cols[test_id]" value="1"<?php if ($cols['test_id']) echo ' checked'; ?>> ID теста</div>
					<div><input type="checkbox" name="cols[test_date]" value="1"<?php if ($cols['test_date']) echo ' checked'; ?>> Дата</div>
						
					<div><input type="checkbox" name="cols[last_name]" value="1"<?php if ($cols['last_name']) echo ' checked'; ?>> Фамилия</div>
					<div><input type="checkbox" name="cols[first_name]" value="1"<?php if ($cols['first_name']) echo ' checked'; ?>> Имя</div>
					<div><input type="checkbox" name="cols[second_name]" value="1"<?php if ($cols['second_name']) echo ' checked'; ?>> Отчество</div>
					<div><input type="checkbox" name="cols[lfs_name]" value="1"<?php if ($cols['lfs_name']) echo ' checked'; ?>> ФИО в одной графе</div>
						
					<div><input type="checkbox" name="cols[birth_date]" value="1"<?php if ($cols['birth_date']) echo ' checked'; ?>> Дата рождения</div>
					<div><input type="checkbox" name="cols[age]" value="1"<?php if ($cols['age']) echo ' checked'; ?>> Возраст</div>
					<div><input type="checkbox" name="cols[sex]" value="1"<?php if ($cols['sex']) echo ' checked'; ?>> Пол</div>
						
					<div><input type="checkbox" name="cols[phone]" value="1"<?php if ($cols['phone']) echo ' checked'; ?>> Телефон</div>
					<div><input type="checkbox" name="cols[email]" value="1"<?php if ($cols['email']) echo ' checked'; ?>> E-mail</div>
					<div><input type="checkbox" name="cols[address]" value="1"<?php if ($cols['address']) echo ' checked'; ?>> Адрес</div>
					<div><input type="checkbox" name="cols[post]" value="1"<?php if ($cols['post']) echo ' checked'; ?>> Должность</div>
					<div><input type="checkbox" name="cols[efields]" value="1"<?php if ($cols['efields']) echo ' checked'; ?>> Доп. информация</div>
						
					<div><input type="checkbox" name="cols[source_site]" value="1"<?php if ($cols['source_site']) echo ' checked'; ?>> Сайт-источник</div>
					<div><input type="checkbox" name="cols[notes]" value="1"<?php if ($cols['notes']) echo ' checked'; ?>> Примечание админа тестов</div>
					<div><input type="checkbox" name="cols[answers]" value="1"<?php if ($cols['answers']) echo ' checked'; ?>> Ответы на тест</div>
					
					<br>
			</td>
		</tr>


	</table>
	<br>
	<div align="center"><input type="submit" name="save" value="Сохранить"></div>
	</form>
	<br>