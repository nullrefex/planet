<?php
///////////////////////////////
// OCA 2.2                   //
// Редактирование инфоблоков //
///////////////////////////////

defined( '__DDD__' ) or die();



?>

<h1>Информационные блоки</h1>

<?php
if ($admin['admin_status']) echo '<p class="field_info" align="center">Используйте HTML в инфоблоках. Для редактирования кликните на ячейку.</p>';
	
// Read translations to array
$res = mysql_query(' SELECT iblock_name, lang_id, iblock_text FROM '.$CONFIG['db_prefix'].'iblocks_translations ORDER BY id ASC ');
while($trans = mysql_fetch_array($res)) {
	$tr_texts[$trans['iblock_name']][$trans['lang_id']] = $trans['iblock_text'];
}

// Read langs
$res_lang = mysql_query(' SELECT lang_id, lang_name FROM '.$CONFIG['db_prefix'].'languages WHERE lang_enabled = 1 ORDER BY lang_name ASC ');
if (@mysql_num_rows($res_lang) > 0) {
	?>
	<table border="1" cellspacing="0" cellpadding="5" align="center" class="settings_table">
		<?php
		echo '<tr class="table-1">';
		echo '<th> </th>';
			
		// Write table titles
		while($lang = mysql_fetch_array($res_lang)) {
			$lang_ids[] = $lang['lang_id'];
			echo '<th>'.$lang['lang_name'].' ('.$lang['lang_id'].')</th>';
		}
		echo '</tr>';
			
		// Read blocks
		$res_iblock = mysql_query(' SELECT iblock_name FROM '.$CONFIG['db_prefix'].'iblocks ORDER BY iblock_name ASC ');
		while($iblock = mysql_fetch_array($res_iblock)) {
				
			echo '<tr>';
			echo '<th class="table-1" width="150">'.$iblock['iblock_name'].'</td>';
				
			// Go langs
			foreach ($lang_ids as $lang_id) {
				if ($admin['admin_status']) {
					?>
					<td class="table-2 active_block" style="max-width: 800px;" id="show_<?php echo $iblock['iblock_name'].'_'.$lang_id; ?>" 
					onclick="SingleFormOpen('<?php echo $iblock['iblock_name']; ?>', '<?php echo $lang_id; ?>')">
					<?php echo $tr_texts[$iblock['iblock_name']][$lang_id]; ?>
					</td>
						
					<td class="table-2" style="max-width: 800px; display: none;" id="edit_<?php echo $iblock['iblock_name'].'_'.$lang_id; ?>">
						<textarea id="input_<?php echo $iblock['iblock_name'].'_'.$lang_id; ?>" 
						style="width: 95%; height: 120px;" onblur="SubmitSingleForm('<?php echo $iblock['iblock_name']; ?>', '<?php 
						echo $lang_id; ?>', 'iblock_save&tpl=blank')"><?php echo htmlspecialchars($tr_texts[$iblock['iblock_name']][$lang_id]); ?></textarea>
						<div align="center"><input type="button" value="Сохранить"></div>
					</td>
					<?php
				} else {
					?>
					<td class="table-2" style="max-width: 800px;"><?php echo $tr_texts[$iblock['iblock_name']][$lang_id]; ?> </td>
					<?php
				}
			}
				
			echo '</tr>';
		}
		?>
	</table>
	<br>
	<?php
		
} else echo '<div class="alert">Не включен ни один язык</div>';
?>