<?php
/////////////
// OCA 2.2 //
// Языки   //
/////////////

defined( '__DDD__' ) or die();


// Для правильной работы fgetcsv()
setlocale(LC_ALL, 'en_US.utf8');


if (!$admin['admin_status']) $disabled = ' disabled="disabled"';


// Устанавливаем языковой пакет
if (strlen($_FILES['langfile']['name']) > 0) {
	
	// Читаем csv
	$fp = fopen($_FILES['langfile']['tmp_name'], "r");
	
	// Идем по строкам
	$i = 0;
	while (($data = fgetcsv($fp, 0, ',', '"')) !== FALSE) {
		
		// Начало строчки правильное
		if ((strlen($data[0]) > 0) && (strlen($data[1]) == 3)) {

			// Проверяем наличие языка
			$res_l = mysql_query(' SELECT lang_id FROM '.$CONFIG['db_prefix'].'languages WHERE lang_id = "'.$data[1].'" ');
				
			// Язык не установлен - устанавливаем
			if (!@mysql_num_rows($res_l)) mysql_query(' INSERT INTO '.$CONFIG['db_prefix'].'languages (lang_id, lang_name, lang_enabled) VALUES ("'.$data[1].'", "", 1) ');
			
			// Язык
			if ($data[0] == 'languages') {
				
				mysql_query(' UPDATE '.$CONFIG['db_prefix'].'languages SET lang_name = "'.addslashes($data[2]).'" WHERE lang_id = "'.$data[1].'" ');
				
			// Вопрос
			} else if ($data[0] == 'questions') {
						
				$data[2] = intval($data[2]);
						
				// Номер вопроса задан
				if (($data[2] >= 1) && ($data[2] <= 200)) {
							
					$data[3] = trim($data[3]);
							
					// Удаляем строку
					mysql_query(' DELETE FROM '.$CONFIG['db_prefix'].'questions WHERE question_id = '.$data[2].' AND lang_id = "'.$data[1].'" ');
							
					// Пишем строку
					$res = mysql_query(' INSERT INTO '.$CONFIG['db_prefix'].'questions (question_id, lang_id, question_text) VALUES ('.$data[2].', "'.$data[1].'", "'.addslashes($data[3]).'") ');
							
					if ($res) $i++;
				}
											
			// Инфоблок
			} else if ($data[0] == 'iblocks') {
					
				// Проверяем наличие инфоблока
				$res_i = mysql_query(' SELECT iblock_name FROM '.$CONFIG['db_prefix'].'iblocks WHERE iblock_name = "'.$data[2].'" ');
					
				// Инфоблок существует
				if (@mysql_num_rows($res_i) > 0) {
						
					$data[3] = trim($data[3]);
					
					// Ищем запись
					$res = mysql_query(' SELECT iblock_name FROM '.$CONFIG['db_prefix'].'iblocks_translations WHERE iblock_name = "'.$data[2].'" AND lang_id = "'.$data[1].'" ');
					
					// Запись есть - обновляем
					if (@mysql_num_rows($res) > 0) {
						$res = mysql_query(' UPDATE '.$CONFIG['db_prefix'].'iblocks_translations SET iblock_text = "'.addslashes($data[3]).'" WHERE iblock_name = "'.$data[2].'" AND lang_id = "'.$data[1].'" ');
						
					// Записи нет - создаем
					} else {
						$res = mysql_query(' INSERT INTO '.$CONFIG['db_prefix'].'iblocks_translations (iblock_name, lang_id, iblock_text) VALUES ("'.$data[2].'", "'.$data[1].'", "'.addslashes($data[3]).'") ');
					}
					
					if ($res) $i++;
				}
					
			// Поле
			} else if ($data[0] == 'fields') {

				// Проверяем наличие поля
				$res_f = mysql_query(' SELECT field_name FROM '.$CONFIG['db_prefix'].'fields WHERE field_name = "'.$data[2].'" ');
					
				// Поле существует
				if (@mysql_num_rows($res_f) > 0) {
					
					$data[3] = trim($data[3]);
					$data[4] = trim($data[4]);
					$data[5] = trim($data[5]);

					// Ищем запись
					$res = mysql_query(' SELECT field_name FROM '.$CONFIG['db_prefix'].'fields_translations WHERE WHERE field_name = "'.$data[2].'" AND lang_id = "'.$data[1].'" ');

					// Запись есть - обновляем
					if (@mysql_num_rows($res) > 0) {
						$res = mysql_query(' UPDATE '.$CONFIG['db_prefix'].'fields_translations SET field_title = "'.addslashes($data[3]).'", field_info = "'.addslashes($data[4]).'", field_testvalue = "'.addslashes($data[5]).'" WHERE field_name = "'.$data[2].'" AND lang_id = "'.$data[1].'" ');
					
					// Записи нет - создаем
					} else {	
						$res = mysql_query(' INSERT INTO '.$CONFIG['db_prefix'].'fields_translations (field_name, lang_id, field_title, field_info, field_testvalue) VALUES ("'.$data[2].'", "'.$data[1].'", "'.addslashes($data[3]).'", "'.addslashes($data[4]).'", "'.addslashes($data[5]).'") ');
					}
					
					if ($res) $i++;
				}
			}
					
			
				
		// Последняя строка
		} else if ($data[0] == null) {
			
		// Неверная строка
		} else {
			$_SESSION['notification'][] = '<div class="nok">Ошибка: неверная строка файла, установка прервана.</div>';
		}
		
	}
	
	fclose($fp);
	
	if ($i > 0) $_SESSION['notification'][] = '<div class="ok">Языковой пакет установлен ('.$i.' записей).</div>';
	else $_SESSION['notification'][] = '<div class="nok">Ошибка: не удалось установить языковой пакет.</div>';
	
	header('Location: '.$_SERVER['REQUEST_URI']);
	exit();
}

			
			
// Сохраняем языки
if ($_POST['save'] && $admin['admin_status']) {
	
	// languages
	mysql_query(' DELETE FROM '.$CONFIG['db_prefix'].'languages WHERE lang_id = "" ');
	
	// questions
	mysql_query(' DELETE FROM '.$CONFIG['db_prefix'].'questions WHERE lang_id = "" OR question_id < 1 OR question_id > 200 ');
	
	// iblocks
	mysql_query(' DELETE FROM '.$CONFIG['db_prefix'].'iblocks_translations WHERE lang_id = "" ');
	
	// удалить записи, где не стандартный iblock_name
	$res = mysql_query(' SELECT iblock_name FROM '.$CONFIG['db_prefix'].'iblocks WHERE iblock_name ');
	while($in = mysql_fetch_assoc($res)) $ins[] = $in['iblock_name'];
	$res = mysql_query(' SELECT id, iblock_name FROM '.$CONFIG['db_prefix'].'iblocks_translations WHERE iblock_name ');
	while($it = mysql_fetch_assoc($res)) {
		if (!in_array($it['iblock_name'], $ins)) mysql_query(' DELETE FROM '.$CONFIG['db_prefix'].'iblocks_translations WHERE id = '.$it['id'].' ');
	}

	// Остальное
	mysql_query(' DELETE FROM '.$CONFIG['db_prefix'].'fields_translations WHERE lang_id = "" ');
	mysql_query(' DELETE FROM '.$CONFIG['db_prefix'].'efields_translations WHERE lang_id = "" ');
	mysql_query(' DELETE FROM '.$CONFIG['db_prefix'].'cards_translations WHERE lang_id = "" ');

	
	$lg = $_POST['lg'];
	foreach($lg as $lang_id => $lang) {
		
		
		
		//////////////////////////////// в следующей версии ///////////////////////////////
		// questions
		// удалить дубликаты по: question_id,lang_id
		
		// iblocks_translations
		// удалить дубликаты по: iblock_name,lang_id
		
		// fields_translations
		// удалить дубликаты по: field_name,lang_id
		// удалить записи, где не стандартный field_name

		// efields_translations
		// удалить дубликаты по: efield_id,lang_id
		// удалить записи, где не существующий efield_id
		
		// cards_translations
		// удалить дубликаты по: card_name,lang_id
		// удалить записи, где не существующий card_name
		///////////////////////////////////////////////////////////////////////////////////
		
		
		
		if ($lang['lang_enabled'] <> 1) $lang['lang_enabled'] = 0;
	
		$res = mysql_query(' UPDATE '.$CONFIG['db_prefix'].'languages SET lang_enabled = '.$lang['lang_enabled'].' WHERE lang_id = "'.$lang_id.'" ');
	}

	if ($res) $_SESSION['notification'][] = '<div class="ok">Настройки сохранены.</div>';
	else $_SESSION['notification'][] = '<div class="nok">Ошибка: не удалось сохранить настройки, ошибка БД.</div>';
	
	header('Location: '.$_SERVER['REQUEST_URI']);
	exit();
}


?>

<h1>Языки</h1>

<h2>Установка языков и переводов</h2><br>

<div align="center">
	<div style="text-align: left; max-width: 900px;">
		<p class="field_info">Файлы с переводами можно скачать на сайте разработчика, в меню "Техподдержка". Есть три типа файлов: вопросы, инфоблоки и основные поля персональных данных. Не забудьте перевести дополнительных поля самостоятельно.</p>
		<p class="field_info">Повторная установка перевода уничтожит исправления, которые вы вносили вручную. Чтобы сделать повторную установку обратимой, сначала скачайте на компьютер файл со своим (текущим) переводом.</p>
	</div>
</div>



<?php if ($admin['admin_status']) { ?>
<form action="" method="post" enctype="multipart/form-data">
<div align="center">
	<div style="text-align: left; max-width: 900px;">

		<input type="file" name="langfile" value="<? echo $_FILES['langfile']['tmp_name']; ?>"><br>
		<input type="submit" name="save" value="Установить языковой пакет">
		
	</div>
</div>
</form>
<?php } ?>



<h2>Установленные языки</h2>
<div align="center">
	<div style="text-align: left; max-width: 900px;">
		<p class="field_info">Язык должен быть включен, чтобы тест на сайте работал.</p>
	</div>
</div>



<?php if ($admin['admin_status']) echo '<form method="post">'; ?>

<table border="1" cellspacing="0" cellpadding="5" align="center" class="settings_table">
	
	<tr class="table-1">
		<th>Включить</th>
		<th>Язык</th>
		<th>ISO 639-3</th>
		<th>Вопросы</th>
		<th>Инфоблоки</th>
		<th>Основные поля</th>
	</tr>
	
	<?php
	// Read langs
	$res = mysql_query(' SELECT lang_id, lang_name, lang_enabled FROM '.$CONFIG['db_prefix'].'languages ORDER BY lang_name ASC ');
	while($lang = mysql_fetch_array($res)) {
			
		?>
		<input type="hidden" name="lg[<?php echo $lang['lang_id']; ?>][lang_id]" value="<?php echo $lang['lang_id']; ?>">
		<?php
		if ($lang['lang_enabled']) $bg = 'enabled';
		else $bg = 'disabled';
		?>
		
		<tr class="<?php echo $bg; ?>">

			<td width="10" align="center">
				<div><input type="checkbox" <?php echo $disabled; ?> name="lg[<?php echo $lang['lang_id']; ?>][lang_enabled]" value="1"<?php if ($lang['lang_enabled']) echo ' checked'; ?>></div>
			</td>

			<td width="200"><b><?php echo $lang['lang_name']; ?></b></td>
			
			<td width="80" align="center"><b><?php echo $lang['lang_id']; ?></b></td>
			
			<td width="200">
				<?
				// Проверяем, есть ли перевод вопросов
				$res_q = mysql_query(' SELECT question_id FROM '.$CONFIG['db_prefix'].'questions WHERE lang_id = "'.$lang['lang_id'].'" AND question_text <> "" ');
				$num_q = @mysql_num_rows($res_q);
				if ($num_q > 0) {
					echo $num_q.' записей';
					echo '<div><a href="export_lang_csv?tpl=blank&lang_id='.$lang['lang_id'].'&type=questions">скачать .csv</a></div>';
				} else {
					echo '<div class="nothing">НЕТ</div>';
				}
				?>
			</td>
			
			<td width="200">
				<?
				// Проверяем, есть ли перевод инфоблоков
				$res_i = mysql_query(' SELECT id FROM '.$CONFIG['db_prefix'].'iblocks_translations WHERE lang_id = "'.$lang['lang_id'].'" AND iblock_text <> "" ');
				$num_i = @mysql_num_rows($res_i);
				if ($num_i > 0) {
					echo $num_i.' записей';
					echo '<div><a href="export_lang_csv?tpl=blank&lang_id='.$lang['lang_id'].'&type=iblocks">скачать .csv</a></div>';
				} else {
					echo '<div class="nothing">НЕТ</div>';
				}
				?>
			</td>
			
			<td width="200">
				<?
				// Проверяем, есть ли перевод полей
				$res_f = mysql_query(' SELECT id FROM '.$CONFIG['db_prefix'].'fields_translations WHERE lang_id = "'.$lang['lang_id'].'" AND (field_title <> "" OR field_info <> "" OR field_testvalue <> "") ');
				$num_f = @mysql_num_rows($res_f);
				if ($num_f > 0) {
					echo $num_f.' записей';
					echo '<div><a href="export_lang_csv?tpl=blank&lang_id='.$lang['lang_id'].'&type=fields">скачать .csv</a></div>';
				} else {
					echo '<div class="nothing">НЕТ</div>';
				}
				?>
			</td>
			
		</tr>
		<?php
	}
	?>
</table>
<br>
	
<?php if ($admin['admin_status']) echo '<div align="center"><input type="submit" name="save" value="Сохранить"></div></form><br>'; ?>
	
