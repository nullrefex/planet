<?php
//////////////////////
// OCA 2.2          //
// Картинка графика //
//////////////////////

define('__DDD__', 1);



//$font_link = $_SERVER['DOCUMENT_ROOT'].'/'.$CONFIG['root_folder'].'/admin/fonts/damase_v.2.ttf';
//$font_link = $_SERVER['DOCUMENT_ROOT'].'/'.$CONFIG['root_folder'].'/admin/fonts/LiberationSans-Italic.ttf';
$font_link = $_SERVER['DOCUMENT_ROOT'].'/'.$CONFIG['root_folder'].'/admin/fonts/LiberationSans-BoldItalic.ttf';

	


if (!$tmp_img) {
	
	// Получаем параметры
	$test_id = intval($_REQUEST['test_id']);
	$graph = $_REQUEST['graph']; // 'full' default, 'short', 'public'
	$action = $_REQUEST['action']; // 'src' default, 'download'

	// Создаем объект теста
	$oca = new OCA;

	// Оцениваем тест без черт и синдромов (только маники)
	$oca->Evaluate_OCA($test_id, 0);

	// Ошибка, нет картинки
	if (isset($oca->error)) exit;
}	

	
////////////////////////////////////// Нет ошибок, рисуем картинку //////////////////////////////////////

/*Создаем картинку из фона*/
if ($graph == 'short') $im = ImageCreateFromPng ('images/graph_print_short.png');
else if ($graph == 'public') $im = ImageCreateFromPng ('images/graph_print_public.png');
else $im = ImageCreateFromPng ('images/graph_print_ranges.png');



/*Цвета*/
$line_round = imagecolorallocate($im,221,0,0);
$color_round = imagecolorallocate($im,0,0,0);
$color_point = imagecolorallocate($im,0,0,0);
$txtcol = imagecolorallocate($im, 0, 0, 0);


				
/*Базовые координаты по X (одинаково для всех типов графика)*/
$x['a'] = 96;	$x['b'] = 183;	$x['c'] = 269;	$x['d'] = 356;	$x['e'] = 442;
$x['f'] = 529;	$x['g'] = 615;	$x['h'] = 702;	$x['i'] = 788;	$x['j'] = 875;

if ($graph == 'short') {
	$a0 = 259; /*координата нулевой линии*/
	$a100 = 97; /*координата 100 линии*/

} else if ($graph == 'public') {
	$a0 = 324; /*координата нулевой линии*/
	$a100 = 162; /*координата 100 линии*/

} else {
	$a0 = 324; /*координата нулевой линии*/
	$a100 = 162; /*координата 100 линии*/
}



/*Координаты точек по Y*/
$y['a'] = $a0 - ($oca->results_arr['a'] / 100 * ($a0 - $a100));
$y['b'] = $a0 - ($oca->results_arr['b'] / 100 * ($a0 - $a100));
$y['c'] = $a0 - ($oca->results_arr['c'] / 100 * ($a0 - $a100));
$y['d'] = $a0 - ($oca->results_arr['d'] / 100 * ($a0 - $a100));
$y['e'] = $a0 - ($oca->results_arr['e'] / 100 * ($a0 - $a100));
$y['f'] = $a0 - ($oca->results_arr['f'] / 100 * ($a0 - $a100));
$y['g'] = $a0 - ($oca->results_arr['g'] / 100 * ($a0 - $a100));
$y['h'] = $a0 - ($oca->results_arr['h'] / 100 * ($a0 - $a100));
$y['i'] = $a0 - ($oca->results_arr['i'] / 100 * ($a0 - $a100));
$y['j'] = $a0 - ($oca->results_arr['j'] / 100 * ($a0 - $a100));

/*Толщина линии*/
imagesetthickness ($im, 5);

/*Вывод линий*/
imageline($im,$x['a'],$y['a'],$x['b'],$y['b'],$line_round);
imageline($im,$x['b'],$y['b'],$x['c'],$y['c'],$line_round);
imageline($im,$x['c'],$y['c'],$x['d'],$y['d'],$line_round);
imageline($im,$x['d'],$y['d'],$x['e'],$y['e'],$line_round);
imageline($im,$x['e'],$y['e'],$x['f'],$y['f'],$line_round);
imageline($im,$x['f'],$y['f'],$x['g'],$y['g'],$line_round);
imageline($im,$x['g'],$y['g'],$x['h'],$y['h'],$line_round);
imageline($im,$x['h'],$y['h'],$x['i'],$y['i'],$line_round);
imageline($im,$x['i'],$y['i'],$x['j'],$y['j'],$line_round);
			
/*Толщина линии*/
imagesetthickness ($im, 2);

/*Вывод кружочков*/
if ($oca->results_arr['m_b']) {
	ImageEllipse ($im, $x['b'], $y['b'], 20, 20, $color_round);
	ImageEllipse ($im, $x['b'], $y['b'], 21, 21, $color_round);
	ImageEllipse ($im, $x['b'], $y['b'], 22, 22, $color_round);
}
if ($oca->results_arr['m_e']) {
	ImageEllipse ($im, $x['e'], $y['e'], 20, 20, $color_round);
	ImageEllipse ($im, $x['e'], $y['e'], 21, 22, $color_round);
	ImageEllipse ($im, $x['e'], $y['e'], 22, 22, $color_round);
}
		
/*Вывод точек*/
if ($settings['debug_mode']) {
	ImageFilledEllipse ($im, $x['a'], $a0, 7, 7, $color_point); /*контрольная*/
	ImageFilledEllipse ($im, $x['a'], $a100, 7, 7, $color_point); /*контрольная*/
}
		
ImageFilledEllipse ($im, $x['a'], $y['a'], 7, 7, $color_point);
ImageFilledEllipse ($im, $x['b'], $y['b'], 7, 7, $color_point);
ImageFilledEllipse ($im, $x['c'], $y['c'], 7, 7, $color_point);
ImageFilledEllipse ($im, $x['d'], $y['d'], 7, 7, $color_point);
ImageFilledEllipse ($im, $x['e'], $y['e'], 7, 7, $color_point);
ImageFilledEllipse ($im, $x['f'], $y['f'], 7, 7, $color_point);
ImageFilledEllipse ($im, $x['g'], $y['g'], 7, 7, $color_point);
ImageFilledEllipse ($im, $x['h'], $y['h'], 7, 7, $color_point);
ImageFilledEllipse ($im, $x['i'], $y['i'], 7, 7, $color_point);
ImageFilledEllipse ($im, $x['j'], $y['j'], 7, 7, $color_point);



/********************** Пишем на графике **********************/
if ($graph != 'short') {
	
	$proc_str = 'A:'.$oca->results_arr['a'].'   ';
	if ($oca->results_arr['m_b']) $proc_str .= 'B:('.$oca->results_arr['b'].')   '; else $proc_str .= 'B:'.$oca->results_arr['b'].'   ';
	$proc_str .= 'C:'.$oca->results_arr['c'].'   ';
	$proc_str .= 'D:'.$oca->results_arr['d'].'   ';
	if ($oca->results_arr['m_e']) $proc_str .= 'E:('.$oca->results_arr['e'].')   '; else $proc_str .= 'E:'.$oca->results_arr['e'].'   ';
	$proc_str .= 'F:'.$oca->results_arr['f'].'   ';
	$proc_str .= 'G:'.$oca->results_arr['g'].'   ';
	$proc_str .= 'H:'.$oca->results_arr['h'].'   ';
	$proc_str .= 'I:'.$oca->results_arr['i'].'   ';
	$proc_str .= 'J:'.$oca->results_arr['j'];
}



if ($graph != 'short') {

	// Персональные данные
	ImageTTFText($im, 12, 0, 705, 26, $txtcol, $font_link, $oca->test['age']);
	ImageTTFText($im, 12, 0, 800, 26, $txtcol, $font_link, norm_date($oca->test['test_date']));
	ImageTTFText($im, 12, 0, 95, 26, $txtcol, $font_link, $oca->test['lfs_name']);
	if ($oca->test['address']) ImageTTFText($im, 12, 0, 112, 46, $txtcol, $font_link, $oca->test['address']);
	if ($oca->test['email']) ImageTTFText($im, 12, 0, 520, 66, $txtcol, $font_link, $oca->test['email']);
	if ($oca->test['phone']) ImageTTFText($im, 12, 0, 90, 66, $txtcol, $font_link, $oca->test['phone']);
	
	
	// Проценты
	ImageTTFText($im, 12, 0, 55, 638, $txtcol, $font_link, $proc_str);

}



// Выгрузка картинки
if ($tmp_img) {
	ImagePng($im, $tmp_img);
	
} else if ($action == 'download') {

	// Заголовки файла
	header('Content-Type: application/force-download');
	header('Content-Type: application/octet-stream');
	header('Content-Type: application/download');
	header('Content-Disposition: attachment;filename=OCA_'.$oca->test['test_date'].'_'.iconv('UTF-8', 'CP1251//IGNORE', $oca->test['last_name'].'-'.$oca->test['first_name'].'-'.$oca->test['second_name']).'.png');
	header('Content-Transfer-Encoding: binary');
	ImagePng($im);
	
} else {
	header ("Content-type: image/png");
	ImagePng($im);
}
	
ImageDestroy($im);
?>