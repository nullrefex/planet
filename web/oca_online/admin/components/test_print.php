<?php
//////////////////
// OCA 2.2      //
// Печать теста //
//////////////////

defined( '__DDD__' ) or die();



?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<title>ОКСФОРДСКИЙ ТЕСТ АНАЛИЗА ЛИЧНОСТИ</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
body {
	color: #000000;
	font-family: Arial;
	font-size: 16px;
}
#content {
	width: 972px;
	margin: 0 auto;
}
h1 {
	color: #000000;
	font-family: Arial;
	font-size: 20px;
	text-align: center;
	font-weight: bold;
	padding: 0;
	margin: 10px 0 10px 0;
}
h2 {
	color: #000000;
	font-family: Arial;
	font-size: 18px;
	text-align: center;
	font-weight: normal;
}
p {
	padding: 5px 0;
	margin: 0;
	display: block;
	border-bottom: 1px solid #999999;
}
table {
	border-collapse: collapse;
}
.h1_sup {
	color: #000000;
	font-family: Arial;
	font-size: 14px;
	font-weight: bold;
}
.answer {
	border: 1px solid #000000;
	width: 14px;
	height: 14px;
}
</style>
<body>

<div id="content">
<?php



$test_id = intval($_REQUEST['test_id']);
$graph = $_REQUEST['graph'];



// Создаем объект теста
$oca = new OCA;

// Оцениваем тест без черт и синдромов (только маники)
$oca->Evaluate_OCA($test_id, 0);

// Вывордим ошибки
if (isset($oca->error)) {
	foreach ($oca->error as $error) echo '<div class="nok">'.$error.'</div>';
}



// Есть результат оценки
if (isset($oca->results_arr)) {
	
	// 50% неуверен
	if ($oca->results_arr['doubt']) {

		echo '<div style="padding: 10px; font-size: 12px;"><b>БОЛЕЕ 50% ОТМЕТОК В СРЕДНЕЙ КОЛОНКЕ</b><br>
			Если более, чем пятьдесят процентов отметок находятся в средней колонке, тест не отражает настоящий характер человека, за исключением того факта, 
			что этот человек основательно застрял в многочисленных сомнениях и что у него отсутствует уверенность, или же что он не желает или не способен 
			додумать вопрос до конца и прийти к определенному ответу (задержка общения). Обычно в таких случаях вы действительно имеете дело с неспособностью 
			тестируемого человека сделать это.</div>';
	
	// Выводим график
	} else {
		
		// Картинка графика
		echo '<img src="graph_img?test_id='.$test_id.'&graph='.$graph.'" style="border: 1px solid #000000;">';
	}
	
}
?>
</div>

<script type="text/javascript">print();</script>

</body>
</html>