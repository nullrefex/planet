<?php
//////////////////////////
// OCA 2.2              //
// Профайл тестируемого //
//////////////////////////

defined( '__DDD__' ) or die();


$test_id = $_REQUEST['test_id'];
$back_url = $_REQUEST['back_url'];



// Создаем объект теста
$oca = new OCA;

// Оцениваем тест без черт и синдромов (только маники)
$oca->Evaluate_OCA($test_id, 0);



// Удаляем
if ($_REQUEST['delete']) {
	$res_del = mysql_query(' UPDATE '.$CONFIG['db_prefix'].'tests SET deleted = 1 WHERE test_id = '.$test_id.' LIMIT 1 '); 
	if ($res_del) {
		header('Location: /'.$CONFIG['root_folder'].'/admin/test_graph_blank?back_url='.urlencode($back_url));
		exit;
	}
}



// Сохраняем
if ($_REQUEST['save'] || $_REQUEST['do_mail']) {
	
	// Новая дата рождения
	$new['birth_d'] = intval(trim($_REQUEST['birth_d']));
	if (strlen($new['birth_d']) == 1) $new['birth_d'] = '0'.$new['birth_d'];
	
	$new['birth_m'] = intval(trim($_REQUEST['birth_m']));
	if (strlen($new['birth_m']) == 1) $new['birth_m'] = '0'.$new['birth_m'];
	
	$new['birth_y'] = intval(trim($_REQUEST['birth_y']));
	
	$new['birth_date'] = $new['birth_y'].'-'.$new['birth_m'].'-'.$new['birth_d'];
	if (is_date($new['birth_date'])) $oca->test['birth_date'] = $new['birth_date'];
		
	// Пол
	$new['sex'] = trim($_REQUEST['sex']);
	if (($new['sex'] == 'M') || ($new['sex'] == 'F')) $oca->test['sex'] = $new['sex'];
	
	// Основные поля
	$res_f = mysql_query(' SELECT field_name FROM '.$CONFIG['db_prefix'].'fields ORDER BY field_order ');
	while ($field = mysql_fetch_array($res_f)) {
		$oca->test[$field['field_name']] = trim($_REQUEST[$field['field_name']]);
	}

	// Дополнительные поля
	$oca->test['efields'] = serialize($_REQUEST['ef']);
	
	// Примечание
	$oca->test['notes'] = trim($_REQUEST['notes']);

	// График отправлен
	$oca->test['grah_mailed'] = intval($_REQUEST['grah_mailed']);
	
	// Сохраняем основные поля
	$res_new = mysql_query(' UPDATE '.$CONFIG['db_prefix'].'tests SET birth_date = "'.$oca->test['birth_date'].'", sex = "'.$oca->test['sex'].'", 
		first_name = "'.addslashes($oca->test['first_name']).'", second_name = "'.addslashes($oca->test['second_name']).'", 
		last_name = "'.addslashes($oca->test['last_name']).'", phone = "'.addslashes($oca->test['phone']).'", email = "'.addslashes($oca->test['email']).'", 
		address = "'.addslashes($oca->test['address']).'", post = "'.addslashes($oca->test['post']).'", efields = "'.addslashes($oca->test['efields']).'", 
		notes = "'.addslashes($oca->test['notes']).'", grah_mailed = '.$oca->test['grah_mailed'].' 
		WHERE test_id = '.$test_id.' LIMIT 1 ');

	if ($res_new) echo '<div class="ok">Данные успешно сохранены.</div>';
	else echo '<div class="nok">Ошибка сохранения: не могу изменить запись в БД.</div>';
}



// Оцениваем тест без черт и синдромов (только маники)
$oca->Evaluate_OCA($test_id, 0);



// Отправляем график на почту
if ($_REQUEST['do_mail']) {
	
	// Есть емейл
	if ($oca->test['email']) {
		
		// Правильный формат емейл
		if (filter_var($oca->test['email'], FILTER_VALIDATE_EMAIL)) {

			$email_subject = trim($_REQUEST['email_subject']);
			$email_text = trim($_REQUEST['email_text']);

			// Отправка с помощью PHPMailer
			if (class_exists('FreakMailer')) {

				// Инициализируем класс
				$mailer = new FreakMailer();
				
				// Устанавливаем кодировку письма
				if ($admin['admin_email_charset']) $mailer->CharSet = 'UTF-8';

				// Устанавливаем тему письма
				$mailer->Subject = $email_subject;
				
				// Задаем тело письма текст /////////////
				$mailer->Body = $email_text;
				
				
				
				///////////// Задаем тело письма HTML //////////////
				/*
				$email_text = str_replace("\n", "<br>", $email_text);
				$htmlBody = '<head><title>'.$email_subject.'</title></head><body>';
				$htmlBody .= $email_text;
				//$htmlBody .= '<img src="cid:card_g-rand.jpg" border="0">';
				$htmlBody .= '</body>';

				$mailer->Body = $htmlBody;
				$mailer->isHTML(true);
				*/
				
				// Вставка картинки в тело
				//$mailer->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'].'/'.$CONFIG['root_folder'].'/admin/images/card_g-rand.jpg', 'card_g-rand.jpg');
				////////////////////////////////////////////////////
				
				// Добавляем адрес в список получателей
				$mailer->AddAddress($oca->test['email'], $oca->test['lfs_name']);
				
				// Устанавливаем обратный адрессат по настройкам аккаунта
				$mailer->AddReplyTo($admin['admin_email'], $admin['admin_name']);

				
				
				// Приложение к письму, если нет ошибок в графике
				if (!isset($oca->error)) {
					
					// Прямое сохранение картинки в /tmp
					$tmp_img = $_SERVER['DOCUMENT_ROOT'].'/'.$CONFIG['root_folder'].'/tmp/'.$test_id.'.png';
					include($_SERVER['DOCUMENT_ROOT'].'/'.$CONFIG['root_folder'].'/admin/components/graph_img.php');
					
					// Отправляем картинку
					//$tmp_img_name = 'OCA_'.$oca->test['test_date'].'_'.iconv('UTF-8', 'CP1251//IGNORE', $oca->test['last_name'].'-'.$oca->test['first_name'].'-'.$oca->test['second_name']).'.png';
					$tmp_img_name = 'OCA_'.$oca->test['test_date'].'_'.$oca->test['last_name'].'-'.$oca->test['first_name'].'-'.$oca->test['second_name'].'.png';
					$mailer->AddAttachment($tmp_img, $tmp_img_name);
				}
				
				// Отправляем
				if(!$mailer->Send()) {
					echo '<div class="nok">Ошибка: не могу отправить письмо с графиком.</div>';
					
				} else {
					if (!isset($oca->error)) echo '<div class="ok">Письмо с графиком отправлено на '.$oca->test['email'].'.</div>';
					else echo '<div class="ok">Письмо БЕЗ графика отправлено на '.$oca->test['email'].'.</div>';
					
					// Пишем в базу
					$res_gr = mysql_query(' UPDATE '.$CONFIG['db_prefix'].'tests SET grah_mailed = 1 WHERE test_id = '.$test_id.' LIMIT 1 ');
					if ($res_gr) $oca->test['grah_mailed'] = 1;
				}
				
				$mailer->ClearAddresses();
				$mailer->ClearAttachments();
				
				// Уничтожаем картинку в /tmp
				if ($tmp_img) unlink($tmp_img);
			
			} else echo '<div class="nok">Ошибка: не могу отправить письмо с графиком.</div>';
			
		} else echo '<div class="nok">Ошибка: график не отправлен, неправильный формат E-mail.</div>';
		
	} else echo '<div class="nok">Ошибка: график не отправлен, отсутствует E-mail.</div>';
	
}



// Вывордим ошибки
if (isset($oca->error)) {
	foreach ($oca->error as $error) echo '<div class="nok">'.$error.'</div>';
}



// Есть результат оценки
if (isset($oca->results_arr)) {
	
	// 50% неуверен
	if ($oca->results_arr['doubt']) {

		echo '<div style="padding: 10px; font-size: 12px;"><b>БОЛЕЕ 50% ОТМЕТОК В СРЕДНЕЙ КОЛОНКЕ</b><br>
			Если более, чем пятьдесят процентов отметок находятся в средней колонке, тест не отражает настоящий характер человека, за исключением того факта, 
			что этот человек основательно застрял в многочисленных сомнениях и что у него отсутствует уверенность, или же что он не желает или не способен 
			додумать вопрос до конца и прийти к определенному ответу (задержка общения). Обычно в таких случаях вы действительно имеете дело с неспособностью 
			тестируемого человека сделать это.</div>';
	
	// Выводим график
	} else {
		
		$is_graph = true;
		
		// Картинка графика
		echo '<img src="graph_img?test_id='.$test_id.'&graph=short" class="graph_img">';
		
	}
	
}



// Есть график
if ($is_graph) {

	// Числовой вывод, скачать, кнопки оценки, сравнить
	?>
	<div align="center" width="100%">
		<table border="0" cellspacing="0" cellpadding="0" class="score" align="center">
			<tr>
				<td class="score_td"> </td>
				<td class="score_td"><b>A</b></td>
				<td class="score_td"><b>B</b></td>
				<td class="score_td"><b>C</b></td>
				<td class="score_td"><b>D</b></td>
				<td class="score_td"><b>E</b></td>
				<td class="score_td"><b>F</b></td>
				<td class="score_td"><b>G</b></td>
				<td class="score_td"><b>H</b></td>
				<td class="score_td"><b>I</b></td>
				<td class="score_td"><b>J</b></td>
			</tr>
			<tr>
				<td class="score_td"><b>числ.</b></td>
				<td class="score_td"><?php echo $oca->char['a']; ?></td>
				<td class="score_td"><?php echo $oca->char['b']; ?></td>
				<td class="score_td"><?php echo $oca->char['c']; ?></td>
				<td class="score_td"><?php echo $oca->char['d']; ?></td>
				<td class="score_td"><?php echo $oca->char['e']; ?></td>
				<td class="score_td"><?php echo $oca->char['f']; ?></td>
				<td class="score_td"><?php echo $oca->char['g']; ?></td>
				<td class="score_td"><?php echo $oca->char['h']; ?></td>
				<td class="score_td"><?php echo $oca->char['i']; ?></td>
				<td class="score_td"><?php echo $oca->char['j']; ?></td>
			</tr>
			<tr>
				<td class="score_td"><b>проц.</b></td>
				<td class="score_td"><?php echo $oca->results_arr['a']; ?></td>
				<td class="score_td">
					<?php
					if ($oca->results_arr['m_b']) echo '('.$oca->results_arr['b'].')';
					else echo $oca->results_arr['b'];
					?>
				</td>
				<td class="score_td"><?php echo $oca->results_arr['c']; ?></td>
				<td class="score_td"><?php echo $oca->results_arr['d']; ?></td>
				<td class="score_td">
					<?php
					if ($oca->results_arr['m_e']) echo '('.$oca->results_arr['e'].')';
					else echo $oca->results_arr['e'];
					?>
				</td>
				<td class="score_td"><?php echo $oca->results_arr['f']; ?></td>
				<td class="score_td"><?php echo $oca->results_arr['g']; ?></td>
				<td class="score_td"><?php echo $oca->results_arr['h']; ?></td>
				<td class="score_td"><?php echo $oca->results_arr['i']; ?></td>
				<td class="score_td"><?php echo $oca->results_arr['j']; ?></td>
			</tr>
		</table>
				
				
		<a style="float: left; margin: 0 0 0 5px; text-decoration: none;" href="graph_img?test_id=<?php echo $test_id?>&action=download" 
		title="Скачайте график на свой компьютер в виде картинки."><img src="images/download_graph.png" class="download_graph"></a>
				
		<a class="submenu_item" href="cards_chars?test_id=<?php echo $test_id?>" target="frame3" title="В колонке справа будут показаны описания черт.">Черты</a>
		<a class="submenu_item" href="cards_syndr?test_id=<?php echo $test_id?>" target="frame3" title="В колонке справа будут показаны описания синдромов.">Синдромы</a>
		<a class="submenu_item" href="cards_compare?add_test_id=<?php echo $test_id?>" target="frame3" title="Данный график будет совмещен на одном листе с другими графиками (3 максимум).">Сравнить</a>
				
	</div>
	<?php
}



// Печать
if ($is_graph || isset($oca->data)) {
	?>
	<hr>
	<div align="center">
		<?php
		if ($is_graph) {
			?>
			<a href="test_print?test_id=<?php echo $test_id?>" target="_blank">Печатать график</a> |
			<?php
		}
		
		if (isset($oca->data)) {
			?>
			<a href="answers_print?test_id=<?php echo $test_id?>" target="_blank">Печатать ответы</a>
			<?php
		}
		?>
	</div>
	<?php
}


// Персональные данные
if (isset($oca->test)) {
	
	?>
	<script type="text/javascript">
		function TestDelete() {
			if (confirm("Удалить тест #<?php echo $test_id?> «<?php echo $oca->test['lfs_name']?>»?")) {
				window.location = '<?php echo $_SERVER['REQUEST_URI']?>&delete=1';
				return true;
			} else return false;
		}

		function SavePush() {
			document.getElementById("save_button").className = "save_button_active";
		}
	</script>


	<form method="post" id="info_form" name="info_form" action="">

	<div class="tab_notes">
		<span class="tab_field_title" style="color: #ffffff;">Примечание сотрудника</span>
		<textarea name="notes" style="width: 90%; height: 60px;  color: #dd0000;" onChange="SavePush();"><?php echo htmlspecialchars($oca->test['notes']); ?></textarea>
		
		<div class="tab_buttons">
			<input type="submit" name="save" value="Сохранить" class="save_button" id="save_button" style="margin-left: 10px;" title="Сохранить изменения">
			<?php
			$del_url = '/'.$CONFIG['root_folder'].'/admin/';
			if ($search) $del_url .= '?search='.$search;
			?>
			<a target="_parent" href="javascript://" onClick="TestDelete(); return false;"><input style="color: #dd0000; float: right; margin-right: 10px;" type="submit" name="delete" value="Удалить тест" title="Тест будет удален из базы данных"></a>
		</div>

		<div>
			<input type="checkbox" name="grah_mailed" value="1" <?php if ($oca->test['grah_mailed']) echo ' checked'; ?>>
			<?php
			if ($is_graph) echo '<i style="color: #ffffff;">- график был отправлен на E-mail</i>';
			else echo '<i style="color: #ffffff;">- письмо было отправлено на E-mail</i>';
			?>
		</div>
		
		<?php
		// E-mail указан
		if ($oca->test['email']) {
						
			// Праваильный формат E-mail
			if (filter_var($oca->test['email'], FILTER_VALIDATE_EMAIL)) {
				
				?>
				<script type="text/javascript">
					function OpenMailForm() {
						document.getElementById('mail_form').style.display = '';
						document.getElementById('mail_link').style.display = 'none';
						document.getElementById("mail_subject").focus();
					}
					function CloseMailForm() {
						document.getElementById('mail_form').style.display = 'none';
						document.getElementById('mail_link').style.display = '';
					}
				</script>
				
				<?php
				if ($is_graph) {
					?>
					<a id="mail_link" href="javascript://" onClick="OpenMailForm()">Отправить письмо с графиком</a>
					<?php
				} else {
					?>
					<a id="mail_link" href="javascript://" onClick="OpenMailForm()">Отправить письмо</a>
					<?php
				}
				?>
							
				<div id="mail_form" style="display: none;">
							
					<div class="tab_field_title">Тема письма</div>
					<input type="text" name="email_subject" id="mail_subject" style="width: 90%;" value="<?php echo htmlspecialchars($admin['admin_email_subject']); ?>">
					
					<?php
					$admin['admin_email_text'] = str_replace('{name}', $oca->test['lfs_name'], $admin['admin_email_text']);
					$admin['admin_email_text'] = str_replace('{admin-name}', $admin['admin_name'], $admin['admin_email_text']);
					$admin['admin_email_text'] = str_replace('{admin-email}', $admin['admin_email'], $admin['admin_email_text']);
					?>
					<div class="tab_field_title">Текст письма</div>
					<textarea name="email_text" style="width: 90%; height: 200px;"><?php echo htmlspecialchars($admin['admin_email_text']); ?></textarea>
								
					<?php
					if ($is_graph) {
						?>
						<div><img src="images/jpg.png" border="0" align="top"> - <i>график прикреплен к письму</i></div>
						<?php
					}
					?>
								
					<div align="center">
						<input type="submit" name="do_mail" value="Отправить" title="Все введенные вами изменения будут сохранены, и программа попытается отправить график на указанный ниже E-mail.">
					</div>

					<img class="letter_logo" src="images/letter1.png" border="0">
					
					<div class="close_button" onClick="CloseMailForm()" title="Закрыть форму письма"></div>
				</div>
							
				<?php
			}
		}
		?>
		
	</div>			
				


	<?php
	////////////////////////////////// Персональные данные //////////////////////////////////
	?>
	<table border="0" cellspacing="0" cellpadding="3" width="100%">
		<tr>
			<td width="100"><span class="tab_field_title">№ и <?php echo $oca->iblocks['date']; ?></span></td>
			<td>
				<?php
				echo '№'.$test_id.' - '.norm_date($oca->test['test_date']);

				if ($oca->test['source_site'] == $site) echo '<div class="site_this" style="float: right;">'.$oca->test['source_site'].'</div>';
				else echo '<div class="site_that" style="float: right;">↓'.$oca->test['source_site'].'</div>';
				?>
			</td>
		</tr>

		<tr>
			<td><span class="tab_field_title"><?php echo $oca->iblocks['birth-date']; ?></span></td>
			<td>
				<input type="text" name="birth_d" maxlength="2" style="width: 20px;" value="<?php echo $oca->test['birth_d']?>" onChange="SavePush();">
				<input type="text" name="birth_m" maxlength="2" style="width: 20px;" value="<?php echo $oca->test['birth_m']?>" onChange="SavePush();">
				<input type="text" name="birth_y" maxlength="4" style="width: 40px;" value="<?php echo $oca->test['birth_y']?>" onChange="SavePush();">
						
				<div style="float: right;">
					<input type="radio" name="sex" value="M"<?php if ($oca->test['sex'] == 'M') echo ' checked'; ?> onChange="SavePush();"> - <?php echo $oca->iblocks['sex-m']; ?>
					<input type="radio" name="sex" value="F"<?php if ($oca->test['sex'] == 'F') echo ' checked'; ?> onChange="SavePush();"> - <?php echo $oca->iblocks['sex-f']; ?>
				</div>
			</td>
		</tr>


		<?php
		// Fields
		$res = mysql_query(' SELECT field_name, field_type FROM '.$CONFIG['db_prefix'].'fields ORDER BY field_order ASC ');
		while ($field = mysql_fetch_array($res)) {
					
			echo '<tr><td valign="top"><span class="tab_field_title">'.$oca->fields[$field['field_name']].'</span></td><td>';
			if ($field['field_type'] == 'text') {
				?>
				<input type="text" name="<?php echo $field['field_name']; ?>" style="width: 90%;" value="<?php echo htmlspecialchars($oca->test[$field['field_name']]); ?>" onChange="SavePush();">
				<?php
						
			} else if ($field['field_type'] == 'textarea') {
				?>
				<textarea name="<?php echo $field['field_name']; ?>" style="width: 90%; height: 60px;" onChange="SavePush();"><?php echo htmlspecialchars($oca->test[$field['field_name']]); ?></textarea>
				<?php
			}
			
			if ($field['field_name'] == 'email') {
				if (!$oca->test['email']) echo '<div class="alert2">Не указан E-mail для отправки графика.</div>';
				else if (!filter_var($oca->test['email'], FILTER_VALIDATE_EMAIL)) echo '<div class="alert2">Неправаильный формат E-mail для отправки графика.</div>';
			}
			
			echo '</td></tr>';
		}
		?>

		
		<?php
		// Efields
		$efields = unserialize($oca->test['efields']);
		if (count($efields)) {
			?>
			<tr><td colspan="2"><div class="efields_block"><div class="efields_block_text">ДОПОЛНИТЕЛЬНЫЕ ПОЛЯ</div></div></td></tr>
			<?php
			foreach ($efields as $question => $answer) {
				?>
				<tr>
					<td colspan="2" align="center">
						<div class="tab_field_title" style="text-align: left;"><?php echo $question?></div>
						<textarea name="ef[<?php echo $question?>]" style="width: 90%; height: 60px;" onChange="SavePush();"><?php echo htmlspecialchars($answer); ?></textarea>
					</td>
				</tr>
				<?php
			}
		}
		?>
	</table>
	
	</form>
	<br>
<?php
}



// Загружаем фрейм оценки теста
?>
<script type="text/javascript">
	top.frames['frame3'].location.href = 'cards_syndr?test_id=<?php echo $test_id?>';
</script>

<?php
// После измемения теста перезагружаем 1 и 3 фреймы
if ($_REQUEST['save'] || $_REQUEST['do_mail']) {
	?>
	<script type="text/javascript">
		top.frames['frame1'].location.href = '<?php echo $back_url?>';
		top.frames['frame3'].location.href = 'cards_syndr?test_id=<?php echo $test_id?>';
	</script>
	<?php
}
?>