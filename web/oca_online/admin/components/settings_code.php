<?php
///////////////////
// OCA 2.2       //
// Код для сайта //
///////////////////

defined( '__DDD__' ) or die();



?>
<h1>Код для сайта</h1>
	
<script type="text/javascript">
	function Generate() {
			
		// Get variant
		var v = 0;
		if (document.getElementById("v1").checked) var v = 1;
		if (document.getElementById("v2").checked) var v = 2;
			
		// Get lang
		var lg = "";
		<?php
		$res = mysql_query(' SELECT lang_id, lang_name FROM '.$CONFIG['db_prefix'].'languages WHERE lang_enabled = 1 ORDER BY lang_id ASC ');
		while($lang = mysql_fetch_array($res)) {
			echo 'if (document.getElementById("'.$lang["lang_id"].'").checked) var lg = "'.$lang["lang_id"].'";';
		}
		?>
			
		// Get css
		var css = "";
		if (document.getElementById("css_yes").checked) var css = 'yes';
		if (document.getElementById("css_no").checked) var css = 'no';

		// Get init
		var init = "";
		if (document.getElementById("init_form").checked) var init = 'form';
		if (document.getElementById("init_button").checked) var init = 'button';
			
		// Assemble code
		var code = '';
		if ((v > 0) && (lg != "") && (css != "") && (init != "")) {

			code += "<!-- OCA --\>\n";
			code += "<div id=\"testengine\"><div id=\"testengine_result\"></div>\n";
			
			if (init == "form") {
				code += "<div id=\"testengine_form\"><div style=\"text-align: center; height: 300px;\"><img style=\"margin-top: 100px; border: none; height: 80px;\" src=\"/<?php echo $CONFIG['root_folder']?>/client/images/loading.gif\"></div></div></div>\n";
				code += "<script type=\"text/javascript\" src=\"/<?php echo $CONFIG['root_folder']?>/client/test_" + lg + ".js\"></script>\n";
				if (css == 'yes') code += "<link href=\"/<?php echo $CONFIG['root_folder']?>/client/template/test.css\" rel=\"stylesheet\" type=\"text/css\">\n";
				code += "<script type=\"text/javascript\">Get_Test_Form();</script>\n";
					
			} else if (init == "button") {
				var init_button_text = document.getElementById("init_button_text").value;
				init_button_text = init_button_text.replace(/(\"|\<|\>)+/gi,"");
				code += "<div id=\"testengine_form\"><div class=\"testengine_button_block\"><input type=\"button\" class=\"testengine_button\" value=\"" + init_button_text + "\" onClick=\"Get_Test_Form();\"></div></div></div>\n";
				code += "<script type=\"text/javascript\" src=\"/<?php echo $CONFIG['root_folder']?>/client/test_" + lg + ".js\"></script>\n";
				if (css == 'yes') code += "<link href=\"/<?php echo $CONFIG['root_folder']?>/client/template/test.css\" rel=\"stylesheet\" type=\"text/css\">\n";
			}

			code += "<!-- /OCA --\>";
		}
			
		// Print code
		document.getElementById("code_generated").innerHTML = code;
	}
</script>

<table border="1" cellspacing="0" cellpadding="5" align="center" class="settings_table">
	<tr class="table-1">
		<th>Способ заполнения</th>
		<th>Язык теста</th>
		<th>CSS</th>
		<th>Запуск теста</th>
	</tr>
	<tr class="table-2">
		<td width="200" valign="top">
			<div><input type="radio" name="variant" id="v1" onClick="Generate();" checked> все вопросы на одной странице</div>
			<div><input type="radio" name="variant" id="v2" onClick="Generate();" disabled> каждый вопрос на новой странице</div>
		</td>
				
		<td width="200" valign="top">
			<?php
			// Read langs
			$res = mysql_query(' SELECT lang_id, lang_name  FROM '.$CONFIG['db_prefix'].'languages WHERE lang_enabled = 1 ORDER BY lang_name ASC ');
			if (@mysql_num_rows($res) > 0) {
				while($lang = mysql_fetch_array($res)) {
					?>
					<div><input type="radio" name="lang_id" id="<?php echo $lang['lang_id']; ?>" onClick="Generate();"> <?php echo $lang['lang_name'].' ('.$lang['lang_id'].')'; ?></div>
					<?php
				}
			} else echo '<div class="alert">Не включен ни один язык</div>';
			?>
		</td>

		<td width="250" valign="top">
			<div><input type="radio" name="css" id="css_yes" onClick="Generate();"> подкачивать стандартный CSS файл теста 
				/<?php echo $CONFIG['folder']?>/test.css</div>
			<div><input type="radio" name="css" id="css_no" onClick="Generate();"> использовать другой CSS файл (если вы знаете, 
				что делаете)</div>
		</td>

		<td width="250" valign="top">
			<div><input type="radio" name="init" id="init_form" onClick="Generate();"> показывать тест сразу</div>
			<div>
				<input type="radio" name="init" id="init_button" onClick="Generate();"> показывать кнопку запуска теста. Текст кнопки: 
				<input type="text" id="init_button_text" value="Пройти тест" onChange="Generate();" onkeyup="Generate();">
			</div>
		</td>
			
	</tr>
		
	<tr class="table-1">
		<th colspan="4">
			<span class="field_info" align="center">Код для сайта. Вставьте сгенерированный код в любое место на сайте, где должен отображаться тест.</span>
		</th>
	</tr>

	<tr class="table-2">
		<td align="center" valign="top" colspan="4">
			<textarea style="width: 95%; height: 200px;" id="code_generated"></textarea>
		</td>
	</tr>

</table>
	
