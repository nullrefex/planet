<?php
///////////////////
// OCA 2.2       //
// Экспорт в XLS //
///////////////////

defined( '__DDD__' ) or die();



function xlsBOF() {
	echo pack('ssssss', 0x809, 0x8, 0x0, 0x10, 0x0, 0x0); 
	return;
}

function xlsEOF() {
	echo pack('ss', 0x0A, 0x00);
	return;
}

function xlsWriteNumber($Row, $Col, $Value) {
	echo pack('sssss', 0x203, 14, $Row, $Col, 0x0);
	echo pack('d', $Value);
	return;
}

function xlsWriteLabel($Row, $Col, $Value ) {
	$Value = iconv('UTF-8', 'CP1251//IGNORE', $Value);
	$L = strlen($Value);
	echo pack('ssssss', 0x204, 8 + $L, $Row, $Col, 0x0, $L);
	echo $Value;
	return;
}



$source = $_GET['source'];



// Поиск тестов
if ($source == 'search') {

	if ($_SESSION['from']) { $sql .= ' AND test_date >= "'.$_SESSION['from'].'" '; $set_search = true; }
	if ($_SESSION['till']) { $sql .= ' AND test_date <= "'.$_SESSION['till'].'" '; $set_search = true; }
	if ($_SESSION['test_id'] > 0) { $sql .= ' AND test_id LIKE "%'.$_SESSION['test_id'].'%" '; $set_search = true; }
	if ($_SESSION['sex']) { $sql .= ' AND sex =  "'.$_SESSION['sex'].'" '; $set_search = true; }
	if ($_SESSION['birth_year_from'] > 0) { $sql .= ' AND birth_date >=  "'.$_SESSION['birth_year_from'].'-01-01" '; $set_search = true; }
	if ($_SESSION['birth_year_till'] > 0) { $sql .= ' AND birth_date <=  "'.$_SESSION['birth_year_till'].'-12-31" '; $set_search = true; }
	if ($_SESSION['last_name']) { $sql .= ' AND last_name LIKE "%'.$_SESSION['last_name'].'%" '; $set_search = true; }
	if ($_SESSION['first_name']) { $sql .= ' AND first_name LIKE "%'.$_SESSION['first_name'].'%" '; $set_search = true; }
	if ($_SESSION['second_name']) { $sql .= ' AND second_name LIKE "%'.$_SESSION['second_name'].'%" '; $set_search = true; }
	if ($_SESSION['email']) { $sql .= ' AND email LIKE "%'.$_SESSION['email'].'%" '; $set_search = true; }
	if ($_SESSION['phone']) { $sql .= ' AND phone LIKE "%'.$_SESSION['phone'].'%" '; $set_search = true; }
	if ($_SESSION['address']) { $sql .= ' AND address LIKE "%'.$_SESSION['address'].'%" '; $set_search = true; }
	if ($_SESSION['post']) { $sql .= ' AND post LIKE "%'.$_SESSION['post'].'%" '; $set_search = true; }
		
	// Условия поиска заданы
	if ($set_search) {
		$sql = ' SELECT * FROM '.$CONFIG['db_prefix'].'tests WHERE deleted = 0 '.$sql.' ORDER BY test_id ASC ';
	}
				

				
// Все тесты
} else if ($source == 'all') {
	$sql = ' SELECT * FROM '.$CONFIG['db_prefix'].'tests WHERE deleted = 0 ORDER BY test_id ASC ';
}



// Запрос для БД сформирован
if ($sql) {
	
	// Читаем инфоблоки на RUS
	$res = mysql_query(' SELECT iblock_name, iblock_text FROM '.$CONFIG['db_prefix'].'iblocks_translations WHERE lang_id = "RUS" ');
	while ($iblock = mysql_fetch_array($res)) {
		$iblocks[$iblock['iblock_name']] = $iblock['iblock_text'];
	}

	// Читаем переводы основных полей на RUS
	$res_f = mysql_query(' SELECT field_name, field_title FROM '.$CONFIG['db_prefix'].'fields_translations WHERE lang_id = "RUS" ');
	while($field = mysql_fetch_array($res_f)) $field_titles[$field['field_name']] = $field['field_title'];

	
	$res = mysql_query($sql);

	
	
	// Заголовки файла
	header('Content-Type: application/force-download');
	header('Content-Type: application/octet-stream');
	header('Content-Type: application/download');
	header('Content-Disposition: attachment;filename='.date('Y-m-d_H-i-s').'_'.$source.'.xls');
	header('Content-Transfer-Encoding: binary');

	
	
	// Начинаем собирать файл
	xlsBOF(); 
	
	//xlsWriteNumber(3, 0, '1');
	//xlsWriteLabel(1, 0, 'Название');
	

	
	// Заголовки колонок
	$j = 0;
	if ($cols['test_id']) {
		xlsWriteLabel(0, $j, '#');
		$j++;
	}
	
	if ($cols['test_date']) {
		xlsWriteLabel(0, $j, 'Дата теста');
		$j++;
	}
	
	if ($cols['last_name']) {
		xlsWriteLabel(0, $j, $field_titles['last_name']);
		$j++;
	}
	
	if ($cols['first_name']) {
		xlsWriteLabel(0, $j, $field_titles['first_name']);
		$j++;
	}
	
	if ($cols['second_name']) {
		xlsWriteLabel(0, $j, $field_titles['second_name']);
		$j++;
	}
	
	if ($cols['lfs_name']) {
		xlsWriteLabel(0, $j, 'ФИО');
		$j++;
	}
	
	if ($cols['birth_date']) {
		xlsWriteLabel(0, $j, 'Дата рождения');
		$j++;
	}
	
	if ($cols['age']) {
		xlsWriteLabel(0, $j, 'Возраст');
		$j++;
	}
	
	if ($cols['sex']) {
		xlsWriteLabel(0, $j, 'Пол');
		$j++;
	}
	
	if ($cols['phone']) {
		xlsWriteLabel(0, $j, $field_titles['phone']);
		$j++;
	}
	
	if ($cols['email']) {
		xlsWriteLabel(0, $j, $field_titles['email']);
		$j++;
	}
	
	if ($cols['address']) {
		xlsWriteLabel(0, $j, $field_titles['address']);
		$j++;
	}
	
	if ($cols['post']) {
		xlsWriteLabel(0, $j, $field_titles['post']);
		$j++;
	}
	
	if ($cols['efields']) {
		xlsWriteLabel(0, $j, 'Доп. информация');
		$j++;
	}
	
	if ($cols['source_site']) {
		xlsWriteLabel(0, $j, 'Сайт-источник');
		$j++;
	}
	
	if ($cols['notes']) {
		xlsWriteLabel(0, $j, 'Примечание сотрудника');
		$j++;
	}
	
	if ($cols['answers']) {
		xlsWriteLabel(0, $j, 'Ответы теста');
		$j++;
	}
	
	
	
	// Тесты есть	
	if (@mysql_num_rows($res) > 0) {

		// Идем по тестам
		$i = 1;
		while ($test = mysql_fetch_array($res)) {
			
			$j = 0;
			
			if ($cols['test_id']) {
				xlsWriteNumber($i, $j, $test['test_id']);
				$j++;
			}
			
			if ($cols['test_date']) {
				xlsWriteLabel($i, $j, norm_date($test['test_date']));
				$j++;
			}
			
			if ($cols['last_name']) {
				xlsWriteLabel($i, $j, $test['last_name']);
				$j++;
			}
			
			if ($cols['first_name']) {
				xlsWriteLabel($i, $j, $test['first_name']);
				$j++;
			}
			
			if ($cols['second_name']) {
				xlsWriteLabel($i, $j, $test['second_name']);
				$j++;
			}
			
			if ($cols['lfs_name']) {
				xlsWriteLabel($i, $j, $test['last_name'].' '.$test['first_name'].' '.$test['second_name']);
				$j++;
			}
			
			if ($cols['birth_date']) {
				xlsWriteLabel($i, $j, norm_date($test['birth_date']));
				$j++;
			}
			
			if ($cols['age']) {
				xlsWriteNumber($i, $j, Get_Age($test['birth_date'], $test['test_date']));
				$j++;
			}
			
			if ($cols['sex']) {
				if ($test['sex'] == 'M') {
					xlsWriteLabel($i, $j, $iblocks['sex-m']);
					$j++;
				}
				if ($test['sex'] == 'F') {
					xlsWriteLabel($i, $j, $iblocks['sex-f']);
					$j++;
				}
			}
			
			if ($cols['phone']) {
				xlsWriteLabel($i, $j, $test['phone']);
				$j++;
			}
			
			if ($cols['email']) {
				xlsWriteLabel($i, $j, $test['email']);
				$j++;
			}
			
			if ($cols['address']) {
				xlsWriteLabel($i, $j, $test['address']);
				$j++;
			}
			
			if ($cols['post']) {
				xlsWriteLabel($i, $j, $test['post']);
				$j++;
			}
			
			if ($cols['efields']) {
				$ef_arr = unserialize($test['efields']);
				foreach ($ef_arr as $key => $value) $ef_arr[$key] = $key.' '.$value;
				$csv_arr[] = implode(' | ', $ef_arr);
				xlsWriteLabel($i, $j, implode(' | ', $ef_arr));
				$j++;
			}
			
			if ($cols['source_site']) {
				xlsWriteLabel($i, $j, $test['source_site']);
				$j++;
			}
			
			if ($cols['notes']) {
				xlsWriteLabel($i, $j, $test['notes']);
				$j++;
			}
			
			if ($cols['answers']) {
				xlsWriteLabel($i, $j, $test['answers']);
				$j++;
			}
			
			$i++;
			
		}
		
	}
	
	// Заканчиваем собирать файл
	xlsEOF(); 
}
?>