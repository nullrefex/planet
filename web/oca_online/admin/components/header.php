<?php
///////////////////
// OCA 2.2       //
// Шапка админки //
///////////////////

defined( '__DDD__' ) or die();

// Готовим ссылку на техподдержку
$support_query = 'v='.$CONFIG['version'].'&site='.$CONFIG['site'].'&id='.$admin['admin_id'].'&email='.$admin['admin_email'].'&name='.$admin['admin_name'];
$support_link = $CONFIG['support_link'].'?i='.urlencode(base64_encode($support_query));
?>



<!--header-->
<div class="header">

	<!--logo-->
	<a href="./" target="_parent" class="logo" style="text-decoration: none;">
		<img src="images/logo_oca_admin.png" class="logo_img" border="0">
	</a>
	<!--/logo-->
	
	<!--menu-->
	<div class="topmenu_block">
		<a class="topmenu_button" href="./" target="_parent">Тесты</a>
		<a class="topmenu_button" href="settings" target="_parent">Настройки</a>
		<a class="topmenu_button" href="<?php echo $support_link?>" target="_blank">Техподдержка</a>
	</div>
	<!--/menu-->
	
	<!--auth-->
	<table border="0" cellspacing="0" cellpadding="0" class="header_right">
		<tr>
			<td align="center" valign="middle">
				<a href="profile" target="_parent" title="Настройки профиля" class="gear_block"><img src="images/gear.png" style="height: 30px; margin-top: 10px; border: none;"></a>
			</td>
			<td valign="middle" class="admin_info_block"><b><?php echo $admin['admin_name']; ?></b><br><?php echo $admin['admin_email']; ?></td>
			<form method="post" target="_parent">
			<td valign="middle"><input title="Выйти из программы" class="exit_button" type="submit" name="exit" value="Выйти"></td>
			</form>
		</tr>
	</table>
	<!--/auth-->
	
	<div class="clearfix"></div>
			
</div>
<!--/header-->
