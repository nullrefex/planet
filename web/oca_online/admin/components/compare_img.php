<?php
/////////////////////////////////////
// OCA 2.2                         //
// Картинка сравнительного графика //
/////////////////////////////////////

define('__DDD__', 1);

/*Ссылка на файл шрифтов*/
//$font_link = $_SERVER['DOCUMENT_ROOT'].'/'.$CONFIG['root_folder'].'/admin/fonts/damase_v.2.ttf';
//$font_link = $_SERVER['DOCUMENT_ROOT'].'/'.$CONFIG['root_folder'].'/admin/fonts/LiberationSans-Italic.ttf';
$font_link = $_SERVER['DOCUMENT_ROOT'].'/'.$CONFIG['root_folder'].'/admin/fonts/LiberationSans-BoldItalic.ttf';



$action = $_REQUEST['action'];
$color = intval($_REQUEST['color']);



// Создаем объект теста
$oca = new OCA;


			
/*Создаем картинку из фона*/
$im = ImageCreateFromPng ('images/graph_compare.png');

/*Цвета*/
if ($color) {
	$line_round[1] = imagecolorallocate($im,255,0,0);
	$line_round[2] = imagecolorallocate($im,0,201,0);
	$line_round[3] = imagecolorallocate($im,0,0,255);
	$lw_1[1] = 5; $lw_1[2] = 5; $lw_1[3] = 5;
	$lw_2[1] = 2; $lw_2[2] = 2; $lw_2[3] = 2;
	
} else {
	$line_round[1] = imagecolorallocate($im,150,150,150);
	$line_round[2] = imagecolorallocate($im,90,90,90);
	$line_round[3] = imagecolorallocate($im,0,0,0);
	$lw_1[1] = 8; $lw_1[2] = 5; $lw_1[3] = 2;
	$lw_2[1] = 2; $lw_2[2] = 2; $lw_2[3] = 1;
}

//$color_round = imagecolorallocate($im,0,0,0);
$color_point = imagecolorallocate($im,0,0,0);
$txtcol = imagecolorallocate($im, 0, 0, 0);

/*Базовые координаты по X (одинаково для всех типов графика)*/
$x['a'] = 96;	$x['b'] = 183;	$x['c'] = 269;	$x['d'] = 356;	$x['e'] = 442;
$x['f'] = 529;	$x['g'] = 615;	$x['h'] = 702;	$x['i'] = 788;	$x['j'] = 875;

$a0 = 324; /*координата нулевой линии*/
$a100 = 162; /*координата 100 линии*/



/*Вывод точек*/
if ($settings['debug_mode']) {
	ImageFilledEllipse ($im, $x['a'], $a0, 7, 7, $color_point); /*контрольная*/
	ImageFilledEllipse ($im, $x['a'], $a100, 7, 7, $color_point); /*контрольная*/
}



// Идем по тестам
$compare = $_SESSION['compare'];
ksort($compare);
$i = 1;
foreach ($compare as $test_id => $one) {
	
	// Оцениваем тест без черт и синдромов (только маники)
	$oca->Evaluate_OCA($test_id, 0);

	// Ошибка, нет картинки
	if (isset($oca->error)) exit;

	/*Толщина линии*/
	imagesetthickness ($im, $lw_1[$i]);
				
	/*Координаты точек по Y*/
	$y['a'] = $a0 - ($oca->results_arr['a'] / 100 * ($a0 - $a100));
	$y['b'] = $a0 - ($oca->results_arr['b'] / 100 * ($a0 - $a100));
	$y['c'] = $a0 - ($oca->results_arr['c'] / 100 * ($a0 - $a100));
	$y['d'] = $a0 - ($oca->results_arr['d'] / 100 * ($a0 - $a100));
	$y['e'] = $a0 - ($oca->results_arr['e'] / 100 * ($a0 - $a100));
	$y['f'] = $a0 - ($oca->results_arr['f'] / 100 * ($a0 - $a100));
	$y['g'] = $a0 - ($oca->results_arr['g'] / 100 * ($a0 - $a100));
	$y['h'] = $a0 - ($oca->results_arr['h'] / 100 * ($a0 - $a100));
	$y['i'] = $a0 - ($oca->results_arr['i'] / 100 * ($a0 - $a100));
	$y['j'] = $a0 - ($oca->results_arr['j'] / 100 * ($a0 - $a100));

	/*Вывод линий*/
	imageline($im,$x['a'],$y['a'],$x['b'],$y['b'],$line_round[$i]);
	imageline($im,$x['b'],$y['b'],$x['c'],$y['c'],$line_round[$i]);
	imageline($im,$x['c'],$y['c'],$x['d'],$y['d'],$line_round[$i]);
	imageline($im,$x['d'],$y['d'],$x['e'],$y['e'],$line_round[$i]);
	imageline($im,$x['e'],$y['e'],$x['f'],$y['f'],$line_round[$i]);
	imageline($im,$x['f'],$y['f'],$x['g'],$y['g'],$line_round[$i]);
	imageline($im,$x['g'],$y['g'],$x['h'],$y['h'],$line_round[$i]);
	imageline($im,$x['h'],$y['h'],$x['i'],$y['i'],$line_round[$i]);
	imageline($im,$x['i'],$y['i'],$x['j'],$y['j'],$line_round[$i]);
					
	// Пишем дату теста
	ImageTTFText($im, 12, 0, 800, (26 + (($i - 1) * 20)), $txtcol, $font_link, norm_date($oca->test['test_date']));
				
	// Рисуем линию под датой
	imageline($im, 800, (30 + (($i - 1) * 20)), 910, (30 + (($i - 1) * 20)), $line_round[$i]);

	/*Толщина линии*/
	imagesetthickness ($im, $lw_2[$i]);

	/*Вывод кружочков*/
	if ($oca->results_arr['m_b']) {
		ImageEllipse ($im, $x['b'], $y['b'], 20, 20, $line_round[$i]);
		ImageEllipse ($im, $x['b'], $y['b'], 21, 21, $line_round[$i]);
		ImageEllipse ($im, $x['b'], $y['b'], 22, 22, $line_round[$i]);
	}
	if ($oca->results_arr['m_e']) {
		ImageEllipse ($im, $x['e'], $y['e'], 20, 20, $line_round[$i]);
		ImageEllipse ($im, $x['e'], $y['e'], 21, 22, $line_round[$i]);
		ImageEllipse ($im, $x['e'], $y['e'], 22, 22, $line_round[$i]);
	}
						
	//Вывод точек графика
	/*
	ImageFilledEllipse ($im, $x['a'], $y['a'], 7, 7, $color_point);
	ImageFilledEllipse ($im, $x['b'], $y['b'], 7, 7, $color_point);
	ImageFilledEllipse ($im, $x['c'], $y['c'], 7, 7, $color_point);
	ImageFilledEllipse ($im, $x['d'], $y['d'], 7, 7, $color_point);
	ImageFilledEllipse ($im, $x['e'], $y['e'], 7, 7, $color_point);
	ImageFilledEllipse ($im, $x['f'], $y['f'], 7, 7, $color_point);
	ImageFilledEllipse ($im, $x['g'], $y['g'], 7, 7, $color_point);
	ImageFilledEllipse ($im, $x['h'], $y['h'], 7, 7, $color_point);
	ImageFilledEllipse ($im, $x['i'], $y['i'], 7, 7, $color_point);
	ImageFilledEllipse ($im, $x['j'], $y['j'], 7, 7, $color_point);
	*/
	
	$i++;
}
			
// Персональные данные
ImageTTFText($im, 12, 0, 705, 26, $txtcol, $font_link, $oca->test['age']);
ImageTTFText($im, 12, 0, 95, 26, $txtcol, $font_link, $oca->test['lfs_name']);
if ($oca->test['address']) ImageTTFText($im, 12, 0, 112, 46, $txtcol, $font_link, $oca->test['address']);
if ($oca->test['email']) ImageTTFText($im, 12, 0, 380, 66, $txtcol, $font_link, $oca->test['email']);
if ($oca->test['phone']) ImageTTFText($im, 12, 0, 90, 66, $txtcol, $font_link, $oca->test['phone']);
	
	

// Выгрузка картинки
if ($action == 'download') {

	// Заголовки файла
	header('Content-Type: application/force-download');
	header('Content-Type: application/octet-stream');
	header('Content-Type: application/download');
	header('Content-Disposition: attachment;filename=OCAs_'.iconv('UTF-8', 'CP1251//IGNORE', $oca->test['last_name'].'-'.$oca->test['first_name'].'-'.$oca->test['second_name']).'.png');
	header('Content-Transfer-Encoding: binary');
}

/*Выводим картинку*/
header ("Content-type: image/png");
ImagePng($im); 
ImageDestroy($im);
?>