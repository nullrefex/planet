<?php
////////////////////
// OCA 2.2        //
// Печать ответов //
////////////////////

defined( '__DDD__' ) or die();



?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<title>ОКСФОРДСКИЙ ТЕСТ АНАЛИЗА ЛИЧНОСТИ</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
body {
	color: #000000;
	font-family: Arial;
	font-size: 16px;
}
#content {
	width: 972px;
	margin: 0 auto;
}
h1 {
	color: #000000;
	font-family: Arial;
	font-size: 20px;
	text-align: center;
	font-weight: bold;
	padding: 0;
	margin: 10px 0 10px 0;
}
h2 {
	color: #000000;
	font-family: Arial;
	font-size: 18px;
	text-align: center;
	font-weight: normal;
}
p {
	padding: 5px 0;
	margin: 0;
	display: block;
	border-bottom: 1px solid #999999;
}
table {
	border-collapse: collapse;
}
.h1_sup {
	color: #000000;
	font-family: Arial;
	font-size: 14px;
	font-weight: bold;
}
.answer {
	border: 1px solid #000000;
	width: 14px;
	height: 14px;
}
</style>
<body>

<div id="content">
<?php



$test_id = intval($_REQUEST['test_id']);
$graph = $_REQUEST['graph'];



// Создаем объект теста
$oca = new OCA;

// Получаем тест без оценки
$oca->Get_OCA($test_id);

// Вывордим ошибки
if (isset($oca->error)) {
	foreach ($oca->error as $error) echo '<div class="nok">'.$error.'</div>';
}



// Если есть данные тестируемого
if (isset($oca->test)) {

	?>
	<h1><?php echo $oca->iblocks['h1']?></h1>

	<div style="border: 2px dashed #000000; padding: 6px; margin-bottom: 10px;">
	
		<table border="0" cellspacing="0" cellpadding="5" width="100%">
			<tr>
				<td style="min-width: 300px; border-right: 1px solid #999999;">
					<p><b><?php echo $oca->iblocks['date']?>:</b> <?php echo norm_date($oca->test['test_date'])?></p>
					<p><b><?php echo $oca->iblocks['full-name']?>:</b> <?php echo $oca->test['lfs_name']; ?></p>
					<p><b><?php echo $oca->iblocks['sex']?>:</b> 
						<?php
						if ($oca->test['sex'] == 'M') echo 'МУЖ';
						if ($oca->test['sex'] == 'F') echo 'ЖЕН';
						?>
					</p>
					<p><b><?php echo $oca->iblocks['birth-date']?>:</b> 
						<?php
						echo norm_date($oca->test['birth_date']).' ('.$oca->test['age'].' лет)';
						?>
					</p>
				</td>
				<td>
					<p><b><?php echo $oca->fields['phone']?>:</b> <?php echo $oca->test['phone']?></p>
					<p><b><?php echo $oca->fields['email']?>:</b> <?php echo $oca->test['email']?></p>
					<p><b><?php echo $oca->fields['address']?>:</b> <?php echo $oca->test['address']?></p>
					<p><b><?php echo $oca->fields['post']?>:</b> <?php echo $oca->test['post']?></p>
				</td>
			</tr>
			
			<?php
			// Дополнительные поля
			$efields = unserialize($oca->test['efields']);
			if ($efields && count($efields)) {
				foreach ($efields as $question => $answer) {
					?>
					<tr><td colspan="2"><b><?php echo $question?></b> <?php echo $answer; ?></td><tr>
					<?php
				}
			}
			?>
		</table>
	</div>


	<table border="0" cellspacing="0" cellpadding="3" width="100%">
		<tr>
		<?php
		// Номер вопроса
		$i = 1;
			
		// Пять колонок
		for($td = 1; $td <= 5; $td++) {
			?>
			<td width="20%" align="center">
				<table border="0" cellspacing="0" cellpadding="3">
					<?php
						
					// Восемь блоков
					for($bl = 1; $bl <= 8; $bl++) {

						// Пять вопросов
						for($vp = 1; $vp <= 5; $vp++) {
							$answer = substr($oca->test['answers'], $i - 1, 1);
							?>
							<tr>
								<td width="30"><b><?php echo $i?>.</b></td>
								<td><img class="answer" src="images/<?php if ($answer == 1) echo 'black'; else echo 'white'; ?>.png"></td>
								<td><img class="answer" src="images/<?php if ($answer == 2) echo 'black'; else echo 'white'; ?>.png"></td>
								<td><img class="answer" src="images/<?php if ($answer == 3) echo 'black'; else echo 'white'; ?>.png"></td>
							</tr>
							<?php
							$i++;
						}
						?>
						<tr><td colspan="4" height="10"> </td></tr>
						<?php
					}
					?>
				</table>
			</td>
			<?php
		}
		?>
		</tr>
	</table>
	<?php
}
?>
</div>

<script type="text/javascript">print();</script>

</body>
</html>