<?php
////////////////////////
// OCA 2.2            //
// Карточки синдромов //
////////////////////////

// Для включения многоязычных карточек нужно в скрипте везде заменить "RUS" на "'.$oca->test['lang_id'].'"

defined( '__DDD__' ) or die();


$test_id = intval($_REQUEST['test_id']);



// Создаем объект теста
$oca = new OCA;

// Оцениваем тест с чертами и синдромами
$oca->Evaluate_OCA($test_id, 1);

// Вывордим ошибки
if (isset($oca->error)) {
	foreach ($oca->error as $error) echo '<div class="nok">'.$error.'</div>';
}



?>
<a class="submenu_item" style="text-decoration: none;" href="cards_compare">Сравнительный график</a>

<h1>Синдромы</h1>
<div class="alert" align="center">Внимание! Автоопределение синдромов является рекоммендательным как "стоит взглянуть". Особенно это касается синдромов 
из разделов В и Г.</div>
<!--
<div align="center">
	<div class="disabled flash_item">Не активно</div>
	<div class="enabled flash_item">Активно</div>
</div>
-->
<?php
	
	
	
// Синдромы, раздел А
?>
<h2>Раздел А</h2>
<?php
$res = mysql_query(' SELECT a.card_name, a.card_title, b.card_text FROM '.$CONFIG['db_prefix'].'cards a 
				LEFT JOIN '.$CONFIG['db_prefix'].'cards_translations b ON a.card_name = b.card_name
				WHERE a.card_type = "syndr_a" AND b.lang_id = "RUS" ORDER BY card_id ASC ');
while ($card = mysql_fetch_array($res)) {
	$auto = '';
	$class = '';
		
	// Активный синдром
	if ($oca->results_arr[$card['card_name']]) {
			
		// Подсветка
		$class = 'enabled';
			
		// Примечания автоопределения
		if ($card['card_name'] == 'g-up') $auto = 'Программа определила: все точки в диапазоне 4.';
		if ($card['card_name'] == 'g-rand') $auto = 'Программа определила: все точки в границах как на картинке.';
		if ($card['card_name'] == 'g-90_i-90') $auto = 'Программа определила: G +90, I +90.';
		if ($card['card_name'] == 'm_b') $auto = 'Программа определила: маник СЧАСТЬЕ.';
		if ($card['card_name'] == 'm_e') $auto = 'Программа определила: маник АКТИВНОСТЬ.';
	}

	?>
	<fieldset class="card_fieldset <?php echo $class?>">
		<legend class="card_legend <?php echo $class?>" title="<?php echo $card['card_name']?>"><?php echo $card['card_title']?></legend>
		<a href="images/cards/<?php echo $card['card_name']?>.jpg" onclick="return hs.expand(this)" class="card_img_a"><img src="images/cards/<?php echo $card['card_name']?>.jpg" class="card_img"><img src="images/lens.png" class="card_img_lens"></a>
		<?php echo $card['card_text']?>
		<?php
		if ($auto) echo '<div class="clearfix"></div><div class="pgm_comment">'.$auto.'</div>';
		?>
	</fieldset>
	<?php
}

	
	
// Синдромы, раздел Б
?>
<h2>Раздел Б</h2>
<?php
$res = mysql_query(' SELECT a.card_name, a.card_title, b.card_text FROM '.$CONFIG['db_prefix'].'cards a 
				LEFT JOIN '.$CONFIG['db_prefix'].'cards_translations b ON a.card_name = b.card_name
				WHERE a.card_type = "syndr_b" AND b.lang_id = "RUS" ORDER BY card_id ASC ');
while ($card = mysql_fetch_array($res)) {
	$auto = '';
	$class = '';
		
	// Активный синдром
	if ($oca->results_arr[$card['card_name']]) {
			
		// Подсветка
		$class = 'enabled';
			
		// Примечания автоопределения
		$auto = 'Программа определила: точки по зонам как в определени.';
	}

	?>
	<fieldset class="card_fieldset <?php echo $class?>">
		<legend class="card_legend <?php echo $class?>" title="<?php echo $card['card_name']?>"><?php echo $card['card_title']?></legend>
		<a href="images/cards/<?php echo $card['card_name']?>.jpg" onclick="return hs.expand(this)" class="card_img_a"><img src="images/cards/<?php echo $card['card_name']?>.jpg" class="card_img"><img src="images/lens.png" class="card_img_lens"></a>
		<?php echo $card['card_text']?>
		<?php
		if ($auto) echo '<div class="clearfix"></div><div class="pgm_comment">'.$auto.'</div>';
		?>
	</fieldset>
	<?php
}

	
	
// Синдромы, раздел В
?>
<h2>Раздел В</h2>
<?php
$res = mysql_query(' SELECT a.card_name, a.card_title, b.card_text FROM '.$CONFIG['db_prefix'].'cards a 
				LEFT JOIN '.$CONFIG['db_prefix'].'cards_translations b ON a.card_name = b.card_name
				WHERE a.card_type = "syndr_c" AND b.lang_id = "RUS" ORDER BY card_id ASC ');
while ($card = mysql_fetch_array($res)) {
	$auto = '';
	$class = '';
		
	// Активный синдром
	if ($oca->results_arr[$card['card_name']]) {
			
		// Подсветка
		$class = 'enabled';
			
		// Примечания автоопределения
		if ($card['card_name'] == 'abc-low') $auto = 'Программа определила: "A", "B", "C" как -18 и ниже.';
		if ($card['card_name'] == 'a-low_e-high') $auto = 'Программа определила: "A" как -18 и ниже, "E" как +33 и выше.';
		if ($card['card_name'] == 'abc-low_e-high') $auto = 'Программа определила: "A", "B", "C" как -18 и ниже, "E" как +33 и выше.';
		if ($card['card_name'] == 'aj-low') $auto = 'Программа определила: "A", "J" как -18 и ниже, остальные как -17 и выше.';
		if ($card['card_name'] == 'acg-low_f-high') $auto = 'Программа определила: "A", "C", "G" как -18 и ниже, "F" как +33 и выше.';
		if ($card['card_name'] == 'a-high_h-low') $auto = 'Программа определила: "A" как +33 и выше, "H" как -18 и ниже.';
		if ($card['card_name'] == 'a-high_d-middle') $auto = 'Программа определила: "A" как +33 и выше, "D" как +32 и ниже.';
		if ($card['card_name'] == 'bg-low_f-high') $auto = 'Программа определила: "B", "G" как -18 и ниже, "F" как +33 и выше.';
		if ($card['card_name'] == 'b-high_d-low') $auto = 'Программа определила: "B" как +33 и выше, "D" как -18 и ниже.';
		if ($card['card_name'] == 'ch-low') $auto = 'Программа определила: "C", "H" как -18 и ниже, остальные как -17 и выше.';
		if ($card['card_name'] == 'd-low_j-high') $auto = 'Программа определила: "D" как -18 и ниже, "J" как +33 и выше.';
		if ($card['card_name'] == 'd-low_g-high') $auto = 'Программа определила: "D" как -18 и ниже, "G" как +33 и выше.';
		if ($card['card_name'] == 'def-high_last-very-low') $auto = 'Программа определила: "D", "E", "F" как +33 и выше, остальные как -40 и ниже.';
		if ($card['card_name'] == 'dghi-low') $auto = 'Программа определила: "D", "G", "H", "I" как -18 и ниже.';
		if ($card['card_name'] == 'efj-low') $auto = 'Программа определила: "E", "F", "J" как -18 и ниже.';
		if ($card['card_name'] == 'ef-low') $auto = 'Программа определила: "E", "F" как -18 и ниже, остальные как -17 и выше.';
		if ($card['card_name'] == 'e-high_g-low') $auto = 'Программа определила: "E" как +33 и выше, "G" как -18 и ниже.';
		if ($card['card_name'] == 'f-high_gh-low') $auto = 'Программа определила: "F" как +33 и выше, "G", "H" как -18 и ниже.';
		if ($card['card_name'] == 'ijb-high_f-low-or-middle') $auto = 'Программа определила: "I", "J", "B" как +33 и выше, "F" как 32 и ниже.';
		if ($card['card_name'] == 'i-high_j-low') $auto = 'Программа определила: "I" как +33 и выше, "J" как -18 и ниже.';
		if ($card['card_name'] == 'acdghij-low') $auto = 'Программа определила: "A", "C", "D", "G", "H", "I", "J" как -18 и ниже, остальные как -17 и выше.';
	}
	
	?>
	<fieldset class="card_fieldset <?php echo $class?>">
		<legend class="card_legend <?php echo $class?>" title="<?php echo $card['card_name']?>"><?php echo $card['card_title']?></legend>
		<a href="images/cards/<?php echo $card['card_name']?>.jpg" onclick="return hs.expand(this)" class="card_img_a"><img src="images/cards/<?php echo $card['card_name']?>.jpg" class="card_img"><img src="images/lens.png" class="card_img_lens"></a>
		<?php echo $card['card_text']?>
		<?php
		if ($auto) echo '<div class="clearfix"></div><div class="pgm_comment">'.$auto.'</div>';
		?>
	</fieldset>
	<?php
}


	
// Синдромы, раздел Г
?>
<h2>Раздел Г</h2>
<?php
$res = mysql_query(' SELECT a.card_name, a.card_title, b.card_text FROM '.$CONFIG['db_prefix'].'cards a 
				LEFT JOIN '.$CONFIG['db_prefix'].'cards_translations b ON a.card_name = b.card_name
				WHERE a.card_type = "syndr_d" AND b.lang_id = "RUS" ORDER BY card_id ASC ');
while ($card = mysql_fetch_array($res)) {
	$auto = '';
	$class = '';

	// Активный синдром
	if ($oca->results_arr[$card['card_name']]) {
		
		// Подсветка
		$class = 'enabled';
			
		// Примечания автоопределения
		if ($card['card_name'] == 'c-higher') $auto = 'Программа определила: "C" выше всех остальных.';
		if ($card['card_name'] == 'd-higher') $auto = 'Программа определила: "D" выше всех остальных.';
		if ($card['card_name'] == 'e-higher-f') $auto = 'Программа определила: "E" выше "F" на 20 и более процентов.';
		if ($card['card_name'] == 'f-higher-e') $auto = 'Программа определила: "F" выше "E" на 20 и более процентов.';
		if ($card['card_name'] == 'i-higher') $auto = 'Программа определила: "I" выше всех остальных.';
	}
		
	?>
	<fieldset class="card_fieldset <?php echo $class?>">
		<legend class="card_legend <?php echo $class?>" title="<?php echo $card['card_name']?>"><?php echo $card['card_title']?></legend>
		<a href="images/cards/<?php echo $card['card_name']?>.jpg" onclick="return hs.expand(this)" class="card_img_a"><img src="images/cards/<?php echo $card['card_name']?>.jpg" class="card_img"><img src="images/lens.png" class="card_img_lens"></a>
		<?php echo $card['card_text']?>
		<?php
		if ($auto) echo '<div class="clearfix"></div><div class="pgm_comment">'.$auto.'</div>';
		?>
	</fieldset>
	<?php
}