<?php
//////////////////////
// OCA 2.2          //
// Сравнение тестов //
//////////////////////

defined( '__DDD__' ) or die();



$add_test_id = $_REQUEST['add_test_id'];
$del_test_id = $_REQUEST['del_test_id'];



// Добавляем тест в общий график
if ($add_test_id) {
	
	// Можно добавить тест
	if (count($_SESSION['compare']) < 3) {
		$_SESSION['compare'][$add_test_id] = 1;
		header('Location: /'.$CONFIG['root_folder'].'/admin/cards_compare');
		exit;
	
	// Нельзя добавит ттест (уже есть три)
	} else echo '<div class="nok">Нельзя сравнить более 3-х тестов.<br>Сначала удалите какой-то из имеющихся из сравнения.</div>';
	
}
	
	

// Удаляем тест из общего графика
if ($del_test_id) {
	unset($_SESSION['compare'][$del_test_id]);

	header('Location: /'.$CONFIG['root_folder'].'/admin/cards_compare');
	exit;
}



// Есть графики для сравнения
//unset($_SESSION['compare']);
if (count($_SESSION['compare']) > 0) {
	
	// Создаем объект теста
	$oca = new OCA;

	?>
	<a class="submenu_item" style="text-decoration: none;" href="cards_compare">Сравнительный график</a>

	<img src="compare_img?color=1" class="graph_img">

	
	<div style="padding: 10px;">
	
	
		<table border="1" cellpadding="5" cellspacing="0">
		<?php
		$compare = $_SESSION['compare'];
		ksort($compare);
		foreach ($compare as $test_id => $one) {
			
			// Получаем тест без оценки
			$oca->Get_OCA($test_id);

			// Вывордим ошибки
			if (isset($oca->error)) {
				foreach ($oca->error as $error) echo '<div class="nok">'.$error.'</div>';
			}

			?>
			<tr>
				<td>#<?php echo $test_id?></td>
				<td><?php echo norm_date($oca->test['test_date'])?></td>
				<td><?php echo $oca->test['lfs_name']?></td>
				<td><a href="cards_compare?del_test_id=<?php echo $test_id?>" style="color: #dd0000;">удалить</a></td>
			</tr>
			<?php
		}
		?>
		</table>
		
		<div>* Подразумевается, что это графики одного человека. Персональные данные на графике заполняются по самому позднему тесту.</div>
	
		<br>
		
		<div align="center">
			<a style="text-decoration: none;" href="compare_img?action=download&color=1" title="Скачайте график на свой компьютер в виде картинки.">
			<img src="images/download_graph.png" class="download_graph"></a>
		</div>

		<br>
		
		<div align="center">
			<a href="compare_print?color=1" target="_blank">Печатать цветной</a> | <a href="compare_print?color=0" target="_blank">Печатать черно-белый</a>
		</div>
		
	
	</div>
	<?php
	
	
} else echo '<br><div class="alert" align="center">Не заданы тесты для сравнения</div>';
?>