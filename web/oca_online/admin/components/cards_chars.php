<?php
////////////////////////////
// OCA 2.2                //
// Карточки характеристик //
////////////////////////////

// Для включения многоязычных карточек нужно в скрипте везде заменить "RUS" на "'.$oca->test['lang_id'].'"

defined( '__DDD__' ) or die();


$test_id = intval($_REQUEST['test_id']);



// Создаем объект теста
$oca = new OCA;

// Оцениваем тест с чертами и синдромами
$oca->Evaluate_OCA($test_id, 1);

// Вывордим ошибки
if (isset($oca->error)) {
	foreach ($oca->error as $error) echo '<div class="nok">'.$error.'</div>';
}



// Читаем маники
$res_card = mysql_query(' SELECT a.card_name, a.card_title, b.card_text FROM '.$CONFIG['db_prefix'].'cards a 
				LEFT JOIN '.$CONFIG['db_prefix'].'cards_translations b ON a.card_name = b.card_name
				WHERE (a.card_name = "m_b" OR a.card_name = "m_e") AND b.lang_id = "RUS" ');
while ($card = mysql_fetch_array($res_card)) $manics[$card['card_name']] = $card;

?>
<a class="submenu_item" style="text-decoration: none;" href="cards_compare">Сравнительный график</a>

<h1>Черты</h1>
<!--
<div align="center">
	<div class="disabled flash_item">Не активно</div>
	<div class="enabled flash_item">Активно</div>
	<div class="suspected flash_item">Синдром отменяет черту</div>
</div>
-->
<?php
$oca->points_arr = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j');
foreach ($oca->points_arr as $ar) {
		
	echo '<h2>'.$oca->iblocks['characteristic_'.$ar.'1'].' - '.$oca->iblocks['characteristic_'.$ar.'2'].'</h2>';
		
	// Маник СЧАСТЬЕ
	if ($ar == 'b') {
		if ($oca->results_arr['m_b']) $class = 'enabled';
		else $class = '';

		?>
		<fieldset class="card_fieldset <?php echo $class?>">
			<legend class="card_legend <?php echo $class?>"><?php echo $manics['m_b']['card_title']?></legend>
			<?php echo $manics['m_b']['card_text']?>
		</fieldset>
		<?php
	}
		
	// Маник АКТИВНОСТЬ
	if ($ar == 'e') {
		if ($oca->results_arr['m_e']) $class = 'enabled';
		else $class = '';

		?>
		<fieldset class="card_fieldset <?php echo $class?>">
			<legend class="card_legend <?php echo $class?>"><?php echo $manics['m_e']['card_title']?></legend>
			<?php echo $manics['m_e']['card_text']?>
		</fieldset>
		<?php
	}
		
	for ($i=1; $i<=4; $i++) {
		$suspect_note = '';
		$class = '';
		
		$res = mysql_query(' SELECT a.card_name, a.card_title, b.card_text FROM '.$CONFIG['db_prefix'].'cards a 
						LEFT JOIN '.$CONFIG['db_prefix'].'cards_translations b ON a.card_name = b.card_name
						WHERE a.card_name = "'.$ar.$i.'" AND b.lang_id = "RUS"  ');
		$card = mysql_fetch_array($res);
			
		if ($oca->results_arr[$ar.$i]) {
						
			if ($oca->results_arr['suspect_'.$ar]) {
				$class = 'suspected';
				$suspect_note = '<div class="clearfix"></div><div class="pgm_comment">Черта может быть ложной из-за наличия синдрома.</div>';
				
			} else {
				$class = 'enabled';
			}
			
		}
			
		?>
		<fieldset class="card_fieldset <?php echo $class?>">
			<legend class="card_legend <?php echo $class?>"><?php echo $card['card_title']?></legend>
			<?php echo $card['card_text']?>
			<?php echo $suspect_note?>
		</fieldset>
		<?php
	}
	
}