<?php
///////////////////
// OCA 2.2       //
// Экспорт в CSV //
///////////////////

defined( '__DDD__' ) or die();


// Настройки
$div1 = ";"; // Разделитель ячеек
$div2 = "\n"; // Разделитель строк



$source = $_GET['source'];



// Поиск тестов
if ($source == 'search') {

	if ($_SESSION['from']) { $sql .= ' AND test_date >= "'.$_SESSION['from'].'" '; $set_search = true; }
	if ($_SESSION['till']) { $sql .= ' AND test_date <= "'.$_SESSION['till'].'" '; $set_search = true; }
	if ($_SESSION['test_id'] > 0) { $sql .= ' AND test_id LIKE "%'.$_SESSION['test_id'].'%" '; $set_search = true; }
	if ($_SESSION['sex']) { $sql .= ' AND sex =  "'.$_SESSION['sex'].'" '; $set_search = true; }
	if ($_SESSION['birth_year_from'] > 0) { $sql .= ' AND birth_date >=  "'.$_SESSION['birth_year_from'].'-01-01" '; $set_search = true; }
	if ($_SESSION['birth_year_till'] > 0) { $sql .= ' AND birth_date <=  "'.$_SESSION['birth_year_till'].'-12-31" '; $set_search = true; }
	if ($_SESSION['last_name']) { $sql .= ' AND last_name LIKE "%'.$_SESSION['last_name'].'%" '; $set_search = true; }
	if ($_SESSION['first_name']) { $sql .= ' AND first_name LIKE "%'.$_SESSION['first_name'].'%" '; $set_search = true; }
	if ($_SESSION['second_name']) { $sql .= ' AND second_name LIKE "%'.$_SESSION['second_name'].'%" '; $set_search = true; }
	if ($_SESSION['email']) { $sql .= ' AND email LIKE "%'.$_SESSION['email'].'%" '; $set_search = true; }
	if ($_SESSION['phone']) { $sql .= ' AND phone LIKE "%'.$_SESSION['phone'].'%" '; $set_search = true; }
	if ($_SESSION['address']) { $sql .= ' AND address LIKE "%'.$_SESSION['address'].'%" '; $set_search = true; }
	if ($_SESSION['post']) { $sql .= ' AND post LIKE "%'.$_SESSION['post'].'%" '; $set_search = true; }
		
	// Условия поиска заданы
	if ($set_search) {
		$sql = ' SELECT * FROM '.$CONFIG['db_prefix'].'tests WHERE deleted = 0 '.$sql.' ORDER BY test_id ASC ';
	}
				

				
// Все тесты
} else if ($source == 'all') {
	$sql = ' SELECT * FROM '.$CONFIG['db_prefix'].'tests WHERE deleted = 0 ORDER BY test_id ASC ';
}



// Запрос для БД сформирован
if ($sql) {
	
	// Читаем инфоблоки на RUS
	$res = mysql_query(' SELECT iblock_name, iblock_text FROM '.$CONFIG['db_prefix'].'iblocks_translations WHERE lang_id = "RUS" ');
	while ($iblock = mysql_fetch_array($res)) {
		$iblocks[$iblock['iblock_name']] = $iblock['iblock_text'];
	}

	// Читаем переводы основных полей на RUS
	$res_f = mysql_query(' SELECT field_name, field_title FROM '.$CONFIG['db_prefix'].'fields_translations WHERE lang_id = "RUS" ');
	while($field = mysql_fetch_array($res_f)) $field_titles[$field['field_name']] = $field['field_title'];

	
	$res = mysql_query($sql);


	
	// Заголовки файла
	header('Content-Type: application/force-download');
	header('Content-Type: application/octet-stream');
	header('Content-Type: application/download');
	header('Content-Disposition: attachment;filename='.date('Y-m-d_H-i-s').'_'.$source.'.csv');
	header('Content-Transfer-Encoding: binary');

	
	
	// Заголовки колонок
	if ($cols['test_id']) $csv_titles_arr[] = '#';
	if ($cols['test_date']) $csv_titles_arr[] = 'Дата теста';
	if ($cols['last_name']) $csv_titles_arr[] = $field_titles['last_name'];
	if ($cols['first_name']) $csv_titles_arr[] = $field_titles['first_name'];
	if ($cols['second_name']) $csv_titles_arr[] = $field_titles['second_name'];
	if ($cols['lfs_name']) $csv_titles_arr[] = 'ФИО';
	if ($cols['birth_date']) $csv_titles_arr[] = 'Дата рождения';
	if ($cols['age']) $csv_titles_arr[] = 'Возраст';
	if ($cols['sex']) $csv_titles_arr[] = 'Пол';
	if ($cols['phone']) $csv_titles_arr[] = $field_titles['phone'];
	if ($cols['email']) $csv_titles_arr[] = $field_titles['email'];
	if ($cols['address']) $csv_titles_arr[] = $field_titles['address'];
	if ($cols['post']) $csv_titles_arr[] = $field_titles['post'];
	if ($cols['efields']) $csv_titles_arr[] = 'Доп. информация';
	if ($cols['source_site']) $csv_titles_arr[] = 'Сайт-источник';
	if ($cols['notes']) $csv_titles_arr[] = 'Примечание сотрудника';
	if ($cols['answers']) $csv_titles_arr[] = 'Ответы теста';
	
	$csv_titles_str = implode($div1, $csv_titles_arr);
	$csv_titles_str = iconv('UTF-8', 'CP1251//IGNORE', $csv_titles_str);
	echo $csv_titles_str.$div2;
	
	
	// Тесты есть	
	if (@mysql_num_rows($res) > 0) {

		// Идем по тестам
		while ($test = mysql_fetch_array($res)) {

			if ($cols['test_id']) $csv_arr[] = $test['test_id'];
			if ($cols['test_date']) $csv_arr[] = norm_date($test['test_date']);
			if ($cols['last_name']) $csv_arr[] = $test['last_name'];
			if ($cols['first_name']) $csv_arr[] = $test['first_name'];
			if ($cols['second_name']) $csv_arr[] = $test['second_name'];
			if ($cols['lfs_name']) $csv_arr[] = $test['last_name'].' '.$test['first_name'].' '.$test['second_name'];
			if ($cols['birth_date']) $csv_arr[] = norm_date($test['birth_date']);
			if ($cols['age']) $csv_arr[] = Get_Age($test['birth_date'], $test['test_date']);
			
			if ($cols['sex']) {
				if ($test['sex'] == 'M') $csv_arr[] = $iblocks['sex-m'];
				if ($test['sex'] == 'F') $csv_arr[] = $iblocks['sex-f'];
			}
			
			if ($cols['phone']) $csv_arr[] = $test['phone'];
			if ($cols['email']) $csv_arr[] = $test['email'];
			if ($cols['address']) $csv_arr[] = $test['address'];
			if ($cols['post']) $csv_arr[] = $test['post'];
			
			if ($cols['efields']) {
				$ef_arr = unserialize($test['efields']);
				foreach ($ef_arr as $key => $value) $ef_arr[$key] = $key.' '.$value;
				$csv_arr[] = implode(' | ', $ef_arr);
			}
			
			if ($cols['source_site']) $csv_arr[] = $test['source_site'];
			if ($cols['notes']) $csv_arr[] = $test['notes'];
			if ($cols['answers']) $csv_arr[] = $test['answers'];
			
			// Удаляем разделители ячеек
			foreach ($csv_arr as $key => $value) $value = str_replace($div1, '', $value);
			
			$csv_str = implode($div1, $csv_arr)."\n";
			unset($csv_arr);

			// Удаляем разделители строк
			$csv_str = str_replace($div2, ' ', $csv_str);
						
			$csv_str = iconv('UTF-8', 'CP1251//IGNORE', $csv_str);
			echo $csv_str.$div2;
		}
		
	}
}
?>