<?php
/////////////////////////////
// OCA 2.2                 //
// Редактирование карточек //
/////////////////////////////

defined( '__DDD__' ) or die();

class CardList {
	
	function __construct($lang_ids, $tr_texts, $admin) {
		$this->tr_texts = $tr_texts;
		$this->lang_ids = $lang_ids;
		$this->admin = $admin;
	}

	function ShowCardList ($res, $chapter_title) {
		
		echo '<tr><td colspan="'.(count($this->lang_ids) + 1).'" style="color: #ffffff; background: #424242;" align="center">'.$chapter_title.'</td>';
		
		while($card = mysql_fetch_array($res)) {
					
			echo '<tr>';
			echo '<th class="table-1" width="150" title="'.$card['card_name'].'">'.$card['card_title'].'</th>';
					
			// Go langs
			foreach ($this->lang_ids as $lang_id) {
				if ($this->admin['admin_status']) {
					?>
					<td class="table-2 active_block" style="max-width: 800px;" id="show_<?php echo $card['card_name'].'_'.$lang_id; ?>"
					onclick="SingleFormOpen('<?php echo $card['card_name']; ?>', '<?php echo $lang_id; ?>')">
					<?php echo $this->tr_texts[$card['card_name']][$lang_id]; ?>
					</td>
							
					<td class="table-2" style="max-width: 800px; display: none;" id="edit_<?php echo $card['card_name'].'_'.$lang_id; ?>">
						<textarea id="input_<?php echo $card['card_name'].'_'.$lang_id; ?>" 
						style="width: 95%; height: 120px;" onblur="SubmitSingleForm('<?php echo $card['card_name']; ?>', '<?php 
						echo $lang_id; ?>', 'card_save&tpl=blank')"><?php echo htmlspecialchars($this->tr_texts[$card['card_name']][$lang_id]); ?></textarea>
						<div align="center"><input type="button" value="Сохранить"></div>
					</td>
					<?php
				} else {
					?>
					<td class="table-2" style="max-width: 800px;"><?php echo $this->tr_texts[$card['card_name']][$lang_id]; ?></td>
					<?php
				}
			}
					
			echo '</tr>';
		}
	}
	
}



?>
<h1>Карточки синдромов</h1>

<?php
if ($admin['admin_status']) {
	?>
	<div align="center">
		<div style="max-width: 900px; text-align: left;">
			<p class="field_info" align="center">Для редактирования кликните на ячейку. В настоящий момент поддерживаются 
			только русскоязычные кароточки. При желании самостоятельно перевести карточки на другой язык - сообщите разработчику программы.</p>
		</div>
	</div>
	<?php
}


// Read langs
$res_lang = mysql_query(' SELECT lang_id, lang_name FROM '.$CONFIG['db_prefix'].'languages WHERE lang_enabled = 1 ORDER BY lang_name ASC ');
if (@mysql_num_rows($res_lang) > 0) {
	
	// Read translations to array
	$res = mysql_query(' SELECT card_name, lang_id, card_text FROM '.$CONFIG['db_prefix'].'cards_translations ORDER BY id ASC ');
	while($trans = mysql_fetch_array($res)) {
		$tr_texts[$trans['card_name']][$trans['lang_id']] = $trans['card_text'];
	}

	?>
	<table border="1" cellspacing="0" cellpadding="5" align="center" class="settings_table">
		<?php
		echo '<tr class="table-1">';
		echo '<th> </th>';
			
		// Для многоязычных карточек
		/*
		while($lang = mysql_fetch_array($res_lang)) {
			$lang_ids[] = $lang['lang_id'];
			echo '<th>'.$lang['lang_name'].' ('.$lang['lang_id'].')</th>';
		}
		*/

		// Для только русскоязычных карточек
		while($lang = mysql_fetch_array($res_lang)) {
			if ($lang['lang_id'] == 'RUS') {
				$lang_ids[] = $lang['lang_id'];
				echo '<th>'.$lang['lang_name'].' ('.$lang['lang_id'].')</th>';
			}
		}

		echo '</tr>';

		$cl = new CardList($lang_ids, $tr_texts, $admin);

		// Черты
		$res = mysql_query(' SELECT card_name, card_title FROM '.$CONFIG['db_prefix'].'cards WHERE card_type = "char" ORDER BY card_id ASC ');
		$cl->ShowCardList ($res, 'ЧЕРТЫ');
		
		// Синдромы, раздел А
		$res = mysql_query(' SELECT card_name, card_title FROM '.$CONFIG['db_prefix'].'cards WHERE card_type = "syndr_a" ORDER BY card_id ASC ');
		$cl->ShowCardList ($res, 'СИНДРОМЫ, раздел А');

		// Синдромы, раздел Б
		$res = mysql_query(' SELECT card_name, card_title FROM '.$CONFIG['db_prefix'].'cards WHERE card_type = "syndr_b" ORDER BY card_id ASC ');
		$cl->ShowCardList ($res, 'СИНДРОМЫ, раздел Б');

		// Синдромы, раздел В
		$res = mysql_query(' SELECT card_name, card_title FROM '.$CONFIG['db_prefix'].'cards WHERE card_type = "syndr_c" ORDER BY card_id ASC ');
		$cl->ShowCardList ($res, 'СИНДРОМЫ, раздел В');
		
		// Синдромы, раздел Г
		$res = mysql_query(' SELECT card_name, card_title FROM '.$CONFIG['db_prefix'].'cards WHERE card_type = "syndr_d" ORDER BY card_id ASC ');
		$cl->ShowCardList ($res, 'СИНДРОМЫ, раздел Г');

		
		?>
	</table>
	<?php
	
} else echo '<div class="alert">Не включен ни один язык</div>';
?>