<?php
//////////////////////////////
// OCA 2.2                  //
// Администраторы программы //
//////////////////////////////

defined( '__DDD__' ) or die();



// Save & create admins
if (($_POST['save'] || $_POST['create']) && $admin['admin_status']) {

	$usr = $_POST['usr'];
	
	// Saving admins
	foreach($usr as $admin_id => $admin) {
		
		// Deleting
		if ($admin['delete']) {
			$res = mysql_query(' DELETE FROM '.$CONFIG['db_prefix'].'admins WHERE admin_id = '.$admin_id);
		
		// Updating
		} else {
			$admin_name = trim($admin['admin_name']);
			$admin_email = trim($admin['admin_email']);
			$admin_test_notification = intval($admin['admin_test_notification']);
			$admin_auth_type = intval($admin['admin_auth_type']);
			$admin_enabled = intval($admin['admin_enabled']);

			$res = mysql_query(' UPDATE '.$CONFIG['db_prefix'].'admins SET admin_name = "'.addslashes($admin_name).'", 
				admin_email = "'.addslashes($admin_email).'", admin_test_notification = '.$admin_test_notification.', 
				admin_auth_type = '.$admin_auth_type.', admin_enabled = '.$admin_enabled.' WHERE admin_id = '.$admin_id);
		}
		
	}

	// Create new admin
	$new['admin_test_notification'] = 1;
	$new['admin_auth_type'] = 0;
	$new['admin_enabled'] = 1;
	$new['admin_email_charset'] = 'UTF-8';
	$new['admin_test_per_page'] = 30;
	$new['admin_columns'] = 'a:13:{s:7:"test_id";s:1:"1";s:9:"test_date";s:1:"1";s:9:"last_name";s:1:"1";s:10:"first_name";s:1:"1";s:11:"second_name";s:1:"1";s:10:"birth_date";s:1:"1";s:3:"age";s:1:"1";s:3:"sex";s:1:"1";s:5:"phone";s:1:"1";s:5:"email";s:1:"1";s:7:"address";s:1:"1";s:7:"efields";s:1:"1";s:5:"notes";s:1:"1";}';
	$new['admin_email_subject'] = 'Результат вашего теста OCA';
	$new['admin_email_text'] = "Здравствуйте, {name}!\nВ приложении к письму результаты теста OCA.\n\n---------------------------------------------\nВаш ответ направляйте по адресу {admin-email}\nС уважением, {admin-name}\nАдминистратор тестов";
	
	if ($_POST['create']) $res = mysql_query(' INSERT INTO '.$CONFIG['db_prefix'].'admins (admin_test_notification, admin_auth_type, admin_enabled, 
		admin_email_charset, admin_test_per_page, admin_columns, admin_email_subject, admin_email_text) VALUES ('.$new['admin_test_notification'].', 
		'.$new['admin_auth_type'].', '.$new['admin_enabled'].', "'.$new['admin_email_charset'].'", '.$new['admin_test_per_page'].', 
		"'.addslashes($new['admin_columns']).'", "'.addslashes($new['admin_email_subject']).'", "'.addslashes($new['admin_email_text']).'") ');
	
	if ($res) $_SESSION['notification'][] = '<div class="ok">Настройки сохранены.</div>';
	else $_SESSION['notification'][] = '<div class="nok">Ошибка: не удалось сохранить настройки, ошибка БД.</div>';
	
	header('Location: '.$_SERVER['REQUEST_URI']);
	exit();
}

?>
<h1>Пользователи программы</h1>

<div align="center">
	<div style="max-width: 800px; text-align: left;">
		<p class="field_info">В целях безопасности создавать, удалять и редактировать администраторов программы может только 
		технический специалист, обслуживающий сайт. Для создания нового администратора программы необходимо в базе данных в 
		таблице <i>*_admins</i> создать новую запись с E-mail и поставить "1" в поле <i>admin_status</i>. Или поставить "1" в поле 
		<i>admin_status</i> в ранее созданной записи администратора тестов.</p>
		<p class="field_info">Создавать, удалять и редактировать администраторов тестов может администратор программы.</p>
		<p class="field_info">После создания новой учетной записи, администратор должен самостоятельно получить пароль, 
		воспользовавшись функцией восстановления пароля через E-mail.</p>
	</div>
</div>
	
<?php
if ($admin['admin_status']) echo '<form method="post">';
?>

	
<table border="1" cellspacing="0" cellpadding="5" align="center" width="730" class="settings_table">
	<tr class="table-1">
		<th>Имя</th>
		<th>E-mail</th>
		<th>Тип авторизации</th>
		<th>Уведомлять о тестах</th>
		<th>Активирован</th>
	</tr>

	<tr><td class="table-1" colspan="5" align="center"><b>Администраторы программы</b></tr>
		
	<?php
	// Read admins
	$res = mysql_query(' SELECT admin_id, admin_name, admin_email, admin_test_notification, admin_auth_type, admin_email_code, admin_enabled 
		FROM '.$CONFIG['db_prefix'].'admins WHERE admin_status = 1 ORDER BY admin_id ');
	while ($adm = mysql_fetch_array($res)) {

		if ($adm['admin_enabled']) $bg = 'enabled';
		else $bg = 'disabled';
			
		?>
		<tr class="<?php echo $bg; ?>">
			<td><?php echo $adm['admin_name']; ?></td>
			<td>
				<?php
				echo $adm['admin_email'];
				if ($adm['admin_email_code'] <> '') echo '<div class="alert">E-mail не подтвержден</div>';
				?>
			</td>
			<td align="center"><?php if ($adm['admin_auth_type'] == 1) echo 'SESSION'; else echo 'COOKIE'; ?></td>
			<td align="center"><?php if ($adm['admin_test_notification']) echo 'ДА'; else echo 'НЕТ'; ?></td>
			<td align="center"><?php if ($adm['admin_enabled']) echo 'ДА'; else echo 'НЕТ'; ?></td>
		</tr>
		<?php
	}
	?>
		
	<tr><td class="table-1" colspan="5" align="center"><b>Администраторы тестов</b></td></tr>
		
	<?php
	// Read admins
	$res = mysql_query(' SELECT admin_id, admin_name, admin_email, admin_test_notification, admin_auth_type, admin_email_code, admin_enabled 
		FROM '.$CONFIG['db_prefix'].'admins WHERE admin_status = 0 ORDER BY admin_id ');
	while ($adm = mysql_fetch_array($res)) {

		if ($adm['admin_enabled']) $bg = 'enabled';
		else $bg = 'disabled';

		if ($admin['admin_status']) {
			?>
			<tr class="<?php echo $bg; ?>">
				<td>
					<input type="text" name="usr[<?php echo $adm['admin_id']; ?>][admin_name]" style="width: 200px;" value="<?php echo htmlspecialchars($adm['admin_name']); ?>">
					<div><input type="checkbox" name="usr[<?php echo $adm['admin_id']; ?>][delete]" value="1">удалить</div>
				</td>
					
				<td>
					<input type="text" name="usr[<?php echo $adm['admin_id']; ?>][admin_email]" style="width: 200px;" value="<?php echo htmlspecialchars($adm['admin_email']); ?>">
					<?php
					if ($adm['admin_email_code'] <> '') echo '<div class="alert">E-mail не подтвержден</div>';
					?>
				</td>
					
				<td>
					<div><input type="radio" name="usr[<?php echo $adm['admin_id']; ?>][admin_auth_type]" value="0"<?php if ($adm['admin_auth_type'] == 0) echo ' checked'; ?>>COOKIE</div>
					<div><input type="radio" name="usr[<?php echo $adm['admin_id']; ?>][admin_auth_type]" value="1"<?php if ($adm['admin_auth_type'] == 1) echo ' checked'; ?>>SESSION</div>
				</td>

				<td align="center"><input type="checkbox" name="usr[<?php echo $adm['admin_id']; ?>][admin_test_notification]" value="1"<?php if ($adm['admin_test_notification'] == 1) 
					echo ' checked'; ?>></td>
					
				<td align="center"><input type="checkbox" name="usr[<?php echo $adm['admin_id']; ?>][admin_enabled]" value="1"<?php if ($adm['admin_enabled'] == 1) 
					echo ' checked'; ?>></td>

			</tr>
			<?php
		} else {
			?>
			<tr class="<?php echo $bg; ?>">
				<td><?php echo $adm['admin_name']; ?></td>
				<td>
					<?php
					echo $adm['admin_email'];
					if ($adm['admin_email_code'] <> '') echo '<div class="alert">E-mail не подтвержден</div>';
					?>
				</td>
				<td align="center"><?php if ($adm['admin_auth_type'] == 1) echo 'SESSION'; else echo 'COOKIE'; ?></td>
				<td align="center"><?php if ($adm['admin_test_notification']) echo '<span>ДА</span>'; else echo '<span class="alert">НЕТ</span>'; ?></td>
				<td align="center"><?php if ($adm['admin_enabled']) echo '<span>ДА</span>'; else echo '<span class="alert">НЕТ</span>'; ?></td>
			</tr>
			<?php
		}
	}
	?>
		
</table>
<br>

<?php
if ($admin['admin_status']) echo '<div align="center"><input type="submit" name="save" value="Сохранить">
	<input type="submit" name="create" value="+ Добавить пользователя"></div></form><br>';
?>