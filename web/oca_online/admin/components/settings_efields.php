<?php
/////////////////////////////////////////
// OCA 2.2                             //
// Редактирование дополнительных полей //
/////////////////////////////////////////

defined( '__DDD__' ) or die();



if (!$admin['admin_status']) $disabled = ' disabled="disabled"';

// Cleaning field value
function Clear_Str($s) {
	$s = trim($s);
	$s = strip_tags($s);
	$s = str_replace('"', '', $s);
	$s = str_replace("'", '', $s);
	$s = str_replace("\\","",$s);

	return $s;
}

// Saving fields
if (($_POST['save'] || $_POST['create']) && $admin['admin_status']) {

	// Deleting fields
	if ($_POST['del']) {
		$del = $_POST['del'];
		foreach ($del as $efield_id => $one) {
			mysql_query(' DELETE FROM '.$CONFIG['db_prefix'].'efields WHERE efield_id = "'.$efield_id.'" ');
			mysql_query(' DELETE FROM '.$CONFIG['db_prefix'].'efields_translations WHERE efield_id = "'.$efield_id.'" ');
		}
	}
	
	$ef = $_POST['ef'];
	$eft = $_POST['eft'];
	
	// Saving fields settings
	foreach($ef as $efield_id => $efield) {
		$efield['efield_enabled'] = intval($efield['efield_enabled']);
		$efield['efield_required'] = intval($efield['efield_required']);
		if ($efield['efield_type'] <> 'textarea') $efield['efield_type'] = 'text';
		$efield['efield_order'] = intval($efield['efield_order']);
		$res = mysql_query(' UPDATE '.$CONFIG['db_prefix'].'efields SET efield_enabled = '.$efield['efield_enabled'].', efield_required = '.$efield['efield_required'].', 
			efield_type = "'.$efield['efield_type'].'", efield_order = '.$efield['efield_order'].' WHERE efield_id = "'.$efield_id.'" ');
	}

	// Create new field
	if ($_POST['create']) mysql_query(' INSERT INTO '.$CONFIG['db_prefix'].'efields (efield_enabled, efield_required, efield_type, efield_order) 
		VALUES (0, 0, "text", 0) ');
	
	// Read old translations to array
	$res_old = mysql_query(' SELECT efield_id, lang_id FROM '.$CONFIG['db_prefix'].'efields_translations ORDER BY id ASC ');
	while($trans_old = mysql_fetch_array($res_old)) $tr_old[$trans_old['efield_id']][$trans_old['lang_id']] = 1;

	// Read langs
	$res_lang = mysql_query(' SELECT lang_id FROM '.$CONFIG['db_prefix'].'languages WHERE lang_enabled = 1 ORDER BY lang_name ASC ');
	while($lang = mysql_fetch_array($res_lang)) {
		
		// Read fields
		$res_efield = mysql_query(' SELECT efield_id FROM '.$CONFIG['db_prefix'].'efields ORDER BY efield_id ASC ');
		while($efield = mysql_fetch_array($res_efield)) {
			$efield_id = $efield['efield_id'];
			$lang_id = $lang['lang_id'];
			
			$efield_title = trim($eft[$efield_id][$lang_id]['efield_title']);
			$efield_info = trim($eft[$efield_id][$lang_id]['efield_info']);
			$efield_testvalue = trim($eft[$efield_id][$lang_id]['efield_testvalue']);
						
			if ($tr_old[$efield_id][$lang_id]) $res = mysql_query(' UPDATE '.$CONFIG['db_prefix'].'efields_translations SET efield_title = "'.
				addslashes($efield_title).'", efield_info = "'.addslashes($efield_info).'", efield_testvalue = "'.addslashes($efield_testvalue).'" 
				WHERE efield_id = "'.$efield_id.'" AND lang_id = "'.$lang_id.'" ');
				
			else $res = mysql_query(' INSERT INTO '.$CONFIG['db_prefix'].'efields_translations (efield_id, lang_id, efield_title, efield_info, efield_testvalue) 
				VALUES ("'.$efield_id.'", "'.$lang_id.'", "'.addslashes($efield_title).'", "'.addslashes($efield_info).'", 
				"'.addslashes($efield_testvalue).'") ');
		}
	}

	if ($res) $_SESSION['notification'][] = '<div class="ok">Настройки сохранены.</div>';
	else $_SESSION['notification'][] = '<div class="nok">Ошибка: не удалось сохранить настройки, ошибка БД.</div>';
	
	header('Location: '.$_SERVER['REQUEST_URI']);
	exit();
}


?>
<h1>Дополнительные поля</h1>

<?php
// Red translations to array
$res = mysql_query(' SELECT efield_id, lang_id, efield_title, efield_info, efield_testvalue FROM '.$CONFIG['db_prefix'].'efields_translations ORDER BY id ASC ');
while($trans = mysql_fetch_array($res)) {
	$trs[$trans['efield_id']][$trans['lang_id']] = $trans;
}

// Read langs
$res_lang = mysql_query(' SELECT lang_id FROM '.$CONFIG['db_prefix'].'languages WHERE lang_enabled = 1 ORDER BY lang_name ASC ');
if (@mysql_num_rows($res_lang) > 0) {
		
	// Langs array
	while($lang = mysql_fetch_array($res_lang)) $lang_ids[] = $lang['lang_id'];
	
	if ($admin['admin_status']) echo '<form method="post">';
		
	?>
	<table border="1" cellspacing="0" cellpadding="5" align="center" width="" class="settings_table">
		<?php
		// Read fields
		$res_efield = mysql_query(' SELECT efield_id, efield_enabled, efield_required, efield_type, efield_order FROM '.$CONFIG['db_prefix'].'efields 
			ORDER BY efield_order ASC ');
		while($efield = mysql_fetch_array($res_efield)) {
				
			if ($efield['efield_enabled']) $bg = 'enabled';
			else $bg = 'disabled';
			?>
			<tr class="<?php echo $bg; ?>">
				<td align="center">
					<div>Удалить</div>
					<div><input <?php echo $disabled; ?> type="checkbox" name="del[<?php echo $efield['efield_id']; ?>]" value="1"></div>
				</td>

				<td>
					<div>Заголовок</div>
					<?php
					foreach ($lang_ids as $lang_id) {
						echo '<div><b>'.$lang_id.'</b><input '.$disabled.' type="text" name="eft['.$efield['efield_id'].']['.$lang_id.'][efield_title]" 
							style="width: 90%; max-width: 160px;" value="'.htmlspecialchars($trs[$efield['efield_id']][$lang_id]['efield_title']).'"></div>';
					}
					?>
				</td>

				<td>
					Порядок<br>
					<input <?php echo $disabled; ?> type="text" name="ef[<?php echo $efield['efield_id']; ?>][efield_order]" style="width: 40px;" value="<?php echo $efield['efield_order']; ?>">
				</td>
					
				<td>
					<div><input <?php echo $disabled; ?> type="checkbox" name="ef[<?php echo $efield['efield_id']; ?>][efield_enabled]" value="1"<?php if ($efield['efield_enabled']) echo ' checked'; ?>> показать</div>
					<div><input <?php echo $disabled; ?> type="checkbox" name="ef[<?php echo $efield['efield_id']; ?>][efield_required]" value="1"<?php if ($efield['efield_required']) echo ' checked'; ?>> обязательное</div>
				</td>

				<td>
					<div><input <?php echo $disabled; ?> type="radio" name="ef[<?php echo $efield['efield_id']; ?>][efield_type]" value="text"<?php if ($efield['efield_type'] == 'text') echo ' checked'; ?>> текстовая строка</div>
					<div><input <?php echo $disabled; ?> type="radio" name="ef[<?php echo $efield['efield_id']; ?>][efield_type]" value="textarea"<?php if ($efield['efield_type'] == 'textarea') echo ' checked'; ?>> текстовое поле</div>
				</td>
					
				<td>
					<div>Пояснительный текст</div>
					<?php
					foreach ($lang_ids as $lang_id) {
						echo '<div><b>'.$lang_id.'</b><textarea '.$disabled.' name="eft['.$efield['efield_id'].']['.$lang_id.'][efield_info]" 
							style="width: 90%; max-width: 160px; height: 40px;">'.htmlspecialchars($trs[$efield['efield_id']][$lang_id]['efield_info']).'</textarea>';
					}
					?>
				</td>
					
				<td>
					<div>Значение при отладке</div>
					<?php
					foreach ($lang_ids as $lang_id) {
						echo '<div><b>'.$lang_id.'</b><input '.$disabled.' type="text" name="eft['.$efield['efield_id'].']['.$lang_id.'][efield_testvalue]" 
							style="width: 90%; max-width: 160px;" value="'.htmlspecialchars($trs[$efield['efield_id']][$lang_id]['efield_testvalue']).'"></div>';
					}
					?>
				</td>

			</tr>
			<?php
		}
		?>
	</table>
	<br>
		
	<?php
	if ($admin['admin_status']) echo '<div align="center"><input type="submit" name="save" value="Сохранить">
		<input type="submit" name="create" value="+ Добавить поле"></div></form><br>';

} else echo '<div class="alert">Не включен ни один язык</div>';
?>