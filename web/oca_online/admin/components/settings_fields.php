<?php
//////////////////////////
// OCA 2.2              //
// Редактирование полей //
//////////////////////////

defined( '__DDD__' ) or die();



if (!$admin['admin_status']) $disabled = ' disabled="disabled"';

// Save fields
if ($_POST['save'] && $admin['admin_status']) {
	
	$f = $_POST['f'];
	$ft = $_POST['ft'];
	
	// Saving fields settings
	foreach($f as $field_name => $field) {
		$field['field_enabled'] = intval($field['field_enabled']);
		$field['field_required'] = intval($field['field_required']);
		if ($field['field_type'] <> 'textarea') $field['field_type'] = 'text';
		$field['field_order'] = intval($field['field_order']);
		$res = mysql_query(' UPDATE '.$CONFIG['db_prefix'].'fields SET field_enabled = '.$field['field_enabled'].', field_required = '.$field['field_required'].', 
			field_type = "'.$field['field_type'].'", field_order = '.$field['field_order'].' WHERE field_name = "'.$field_name.'" ');
	}
	
	// Read old translations to array
	$res_old = mysql_query(' SELECT field_name, lang_id FROM '.$CONFIG['db_prefix'].'fields_translations ORDER BY id ASC ');
	while($trans_old = mysql_fetch_array($res_old)) $tr_old[$trans_old['field_name']][$trans_old['lang_id']] = 1;

	// Read langs
	$res_lang = mysql_query(' SELECT lang_id FROM '.$CONFIG['db_prefix'].'languages WHERE lang_enabled = 1 ORDER BY lang_name ASC ');
	while($lang = mysql_fetch_array($res_lang)) {
		
		// Read fields
		$res_field = mysql_query(' SELECT field_name FROM '.$CONFIG['db_prefix'].'fields ORDER BY field_name ASC ');
		while($field = mysql_fetch_array($res_field)) {
			$field_name = $field['field_name'];
			$lang_id = $lang['lang_id'];
			
			$field_title = trim($ft[$field_name][$lang_id]['field_title']);
			$field_info = trim($ft[$field_name][$lang_id]['field_info']);
			$field_testvalue = trim($ft[$field_name][$lang_id]['field_testvalue']);
						
			if ($tr_old[$field_name][$lang_id]) $res = mysql_query(' UPDATE '.$CONFIG['db_prefix'].'fields_translations SET field_title = "'.
				addslashes($field_title).'", field_info = "'.addslashes($field_info).'", field_testvalue = "'.addslashes($field_testvalue).'" 
				WHERE field_name = "'.$field_name.'" AND lang_id = "'.$lang_id.'" ');
				
			else $res = mysql_query(' INSERT INTO '.$CONFIG['db_prefix'].'fields_translations (field_name, lang_id, field_title, field_info, field_testvalue) 
				VALUES ("'.$field_name.'", "'.$lang_id.'", "'.addslashes($field_title).'", "'.addslashes($field_info).'", 
				"'.addslashes($field_testvalue).'") ');
		}
	}
	
	if ($res) $_SESSION['notification'][] = '<div class="ok">Настройки сохранены.</div>';
	else $_SESSION['notification'][] = '<div class="nok">Ошибка: не удалось сохранить настройки, ошибка БД.</div>';
	
	header('Location: '.$_SERVER['REQUEST_URI']);
	exit();
}


?>
<h1>Основные поля</h1>

<?php
// Read translations to array
$res = mysql_query(' SELECT field_name, lang_id, field_title, field_info, field_testvalue FROM '.$CONFIG['db_prefix'].'fields_translations ORDER BY id ASC ');
while($trans = mysql_fetch_array($res)) {
	$trs[$trans['field_name']][$trans['lang_id']] = $trans;
}

// Read langs
$res_lang = mysql_query(' SELECT lang_id FROM '.$CONFIG['db_prefix'].'languages WHERE lang_enabled = 1 ORDER BY lang_name ASC ');
if (@mysql_num_rows($res_lang) > 0) {
		
	// Langs array
	while($lang = mysql_fetch_array($res_lang)) $lang_ids[] = $lang['lang_id'];
	
	if ($admin['admin_status']) echo '<form method="post">';
	
	?>
	<table border="1" cellspacing="0" cellpadding="5" align="center" width="" class="settings_table">
		<?php
		// Read fields
		$res_field = mysql_query(' SELECT field_name, field_enabled, field_required, field_type, field_order FROM '.$CONFIG['db_prefix'].'fields ORDER BY field_order ASC ');
		while($field = mysql_fetch_array($res_field)) {
				
			if ($field['field_enabled']) $bg = 'enabled';
			else $bg = 'disabled';
			?>
			<tr class="<?php echo $bg; ?>">
				<td>
					<div>Заголовок (<?php echo $field['field_name']; ?>)</div>
					<?php
					foreach ($lang_ids as $lang_id) {
						echo '<div><b>'.$lang_id.'</b><input '.$disabled.' type="text" name="ft['.$field['field_name'].']['.$lang_id.'][field_title]" 
							style="width: 90%; max-width: 150px;" value="'.htmlspecialchars($trs[$field['field_name']][$lang_id]['field_title']).'"></div>';
					}
					?>
				</td>

				<td>
					Порядок<br>
					<input <?php echo $disabled; ?> type="text" name="f[<?php echo $field['field_name']; ?>][field_order]" style="width: 40px;" value="<?php echo $field['field_order']; ?>">
				</td>
					
				<td>
					<div><input <?php echo $disabled; ?> type="checkbox" name="f[<?php echo $field['field_name']; ?>][field_enabled]" value="1"<?php if ($field['field_enabled']) echo ' checked'; ?>> показать</div>
					<div><input <?php echo $disabled; ?> type="checkbox" name="f[<?php echo $field['field_name']; ?>][field_required]" value="1"<?php if ($field['field_required']) echo ' checked'; ?>> обязательное</div>
				</td>

				<td>
					<div><input <?php echo $disabled; ?> type="radio" name="f[<?php echo $field['field_name']; ?>][field_type]" value="text"<?php if ($field['field_type'] == 'text') echo ' checked'; ?>> текстовая строка</div>
					<div><input <?php echo $disabled; ?> type="radio" name="f[<?php echo $field['field_name']; ?>][field_type]" value="textarea"<?php if ($field['field_type'] == 'textarea') echo ' checked'; ?>> текстовое поле</div>
				</td>
					
				<td>
					<div>Пояснительный текст</div>
					<?php
					foreach ($lang_ids as $lang_id) {
						echo '<div><b>'.$lang_id.'</b><textarea '.$disabled.' name="ft['.$field['field_name'].']['.$lang_id.'][field_info]" 
							style="width: 90%; width-max: 150px; height: 40px;">'.htmlspecialchars($trs[$field['field_name']][$lang_id]['field_info']).'</textarea>';
					}
					?>
				</td>
					
				<td>
					<div>Значение при отладке</div>
					<?php
					foreach ($lang_ids as $lang_id) {
						echo '<div><b>'.$lang_id.'</b><input '.$disabled.' type="text" name="ft['.$field['field_name'].']['.$lang_id.'][field_testvalue]" 
							style="width: 90%; max-width: 150px;" value="'.htmlspecialchars($trs[$field['field_name']][$lang_id]['field_testvalue']).'"></div>';
					}
					?>
				</td>

			</tr>
			<?php
		}
		?>
	</table>
	<br>
		
	<?php
	if ($admin['admin_status']) echo '<div align="center"><input type="submit" name="save" value="Сохранить"></div></form><br>';

} else echo '<div class="alert">Не включен ни один язык</div>';
?>
	
