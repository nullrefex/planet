<?php
///////////////////
// OCA 2.2       //
// Меню настроек //
///////////////////

defined( '__DDD__' ) or die();


?>
<div class="menu_block">
	<?php
	$submenu = array(
		'settings_general' => 'Общие настройки',
		'settings_admins' => 'Администраторы',
		'settings_fields' => 'Основные поля',
		'settings_efields' => 'Дополнительные поля',
		'settings_questions' => 'Вопросы теста',
		'settings_iblocks' => 'Инфоблоки',
		'settings_languages' => 'Языки',
		'settings_cards' => 'Карточки',
		'settings_code' => 'Код для сайта',
	);

	foreach ($submenu as $key => $value) {
		?>
		<a class="menu_item" href="<?php echo $key?>" target="frame2"><?php echo $value?></a>
		<?php
	}
	?>
</div>
