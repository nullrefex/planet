<?php
/////////////////////////////
// OCA 2.2                 //
// Редактирование вопросов //
/////////////////////////////

defined( '__DDD__' ) or die();



?>
<h1>Вопросы теста</h1>

<?php
if ($admin['admin_status']) echo '<p class="field_info" align="center">Не пишите номера вопросов. Для редактирования кликните на ячейку.</p>';
	
// Read questions to array
$res = mysql_query(' SELECT question_id, lang_id, question_text FROM '.$CONFIG['db_prefix'].'questions ORDER BY question_id ASC ');
while($question = mysql_fetch_array($res)) {
	$q_texts[$question['question_id']][$question['lang_id']] = $question['question_text'];
}

// Read langs
$res_lang = mysql_query(' SELECT lang_id, lang_name FROM '.$CONFIG['db_prefix'].'languages WHERE lang_enabled = 1 ORDER BY lang_name ASC ');
if (@mysql_num_rows($res_lang) > 0) {
	
	?>
	<table border="1" cellspacing="0" cellpadding="5" align="center" class="settings_table">
		<?php
		echo '<tr class="table-1">';
		echo '<th> </th>';
			
		// Write table titles
		while($lang = mysql_fetch_array($res_lang)) {
			$lang_ids[] = $lang['lang_id'];
			echo '<th>'.$lang['lang_name'].' ('.$lang['lang_id'].')</th>';
		}
		echo '</tr>';
			
		// Go questions
		for($question_id=1; $question_id<=200; $question_id++) {
				
			echo '<tr>';
			echo '<th class="table-1" width="40">'.$question_id.'</td>';
				
			// Go langs
			foreach ($lang_ids as $lang_id) {
				if ($admin['admin_status']) {
					?>
					<td class="table-2 active_block" style="max-width: 800px;" id="show_<?php echo $question_id.'_'.$lang_id; ?>" 
					onclick="SingleFormOpen('<?php echo $question_id; ?>', '<?php echo $lang_id; ?>')">
					<?php echo $q_texts[$question_id][$lang_id]; ?>
					</td>

					<td class="table-2" width="" style="max-width: 800px; display: none;" id="edit_<?php echo $question_id.'_'.$lang_id; ?>">
						<textarea id="input_<?php echo $question_id.'_'.$lang_id; ?>" 
						style="width: 95%; height: 120px;" onblur="SubmitSingleForm('<?php echo $question_id; ?>', '<?php 
						echo $lang_id; ?>', 'question_save&tpl=blank')"><?php echo htmlspecialchars($q_texts[$question_id][$lang_id]); ?></textarea>
						<div align="center"><input type="button" value="Сохранить"></div>
					</td>
					<?php
				} else {
					?>
					<td class="table-2" style="max-width: 800px;"><?php echo $q_texts[$question_id][$lang_id]; ?></td>
					<?php
				}
			}
			echo '</tr>';
		}
		?>
	</table>
	<br>
	<?php
		
} else echo '<div class="alert">Не включен ни один язык</div>';
?>
