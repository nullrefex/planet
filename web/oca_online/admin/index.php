<?php
//ini_set('error_reporting', E_PARSE);
//ini_set('error_reporting', E_ALL);

header('Content-Type: text/html; charset=utf-8');

define('__DDD__', 1);

ob_start();

require_once('../config.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/'.$CONFIG['root_folder'].'/mailer.class.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/'.$CONFIG['root_folder'].'/admin/processor.php');

session_start();

// Заглушка для старых или неполных версий PHP
if (!function_exists('filter_var')) {
	function filter_var($email) {
		if (!preg_match("/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\.?[a-z0-9]+)?\.[a-z]{2,5})$/i",trim($email))) return false;
		else return true;
	}
}



// "YYYY-mm-dd" -> "dd.mm.YYYY"
function norm_date ($some_day) {
	$day = substr($some_day, 8, 2);
	$month = substr($some_day, 5, 2);
	$year = substr($some_day, 0, 4);
	return $day.'.'.$month.'.'.$year;
}



// "YYYY-mm-dd" -> "dd.mm.YYYY"
function norm_date_br ($some_day) {
	$day = substr($some_day, 8, 2);
	$month = substr($some_day, 5, 2);
	$year = substr($some_day, 0, 4);
	return $day.'.'.$month.'<br>'.$year;
}



// "YYYY-mm-dd" -> "dd.mm.YYYY"
function norm_date_short ($some_day) {
	$day = substr($some_day, 8, 2);
	$month = substr($some_day, 5, 2);
	$year = substr($some_day, 2, 2);
	return $day.'.'.$month.'.'.$year;
}



// "dd.mm.YYYY" -> "YYYY-mm-dd"
function standart_date ($some_day) {
	$day = substr($some_day, 0, 2);
	$month = substr($some_day, 3, 2);
	$year = substr($some_day, 6, 4);
	return $year.'-'.$month.'-'.$day;
}



// Is this real date
function is_date($some_day) {

	// Format check YYYY-MM-DD
	$ok = preg_match('/^([0-2][0-9][0-9][0-9])-([0-1][0-9])-([0-3][0-9])$/', $some_day);
	if ($ok) {
		
		// Existance check
		$day = substr($some_day, 8, 2);
		$month = substr($some_day, 5, 2);
		$year = substr($some_day, 0, 4);
		if (checkdate($month, $day, $year)) $res = true;
		else $res = false;
		
	} else $res = false;
	
	return $res;
}



// Get age
function Get_Age($date1, $date2) {
	
	if (!is_date($date1) || !is_date($date2) || ($date1 >= $date2)) return 0;
	
	$year1 = substr($date1, 0, 4);
	$year2 = substr($date2, 0, 4);
	
	$age = $year2 - $year1;
	
	$date3 = substr($date2, 0, 4).substr($date1, 4, 6);
	
	if ($date3 > $date2) $age -= 1;
	
	return $age;
}



// Get sex translation
function Show_Sex($sex, $size) {
	global $CONFIG, $admin;
	
	if ($sex == 'M') return 'ЖЕН';
	else if ($sex == 'F') return 'МУЖ';
}



/*Проверяем авторизацию по SESSION*/
if ($_SESSION['my_id']) {

	/*Ищем админа с этим ID*/
	$res_adm = mysql_query(' SELECT admin_id FROM '.$CONFIG['db_prefix'].'admins WHERE admin_id = "'.$_SESSION['my_id'].'" AND admin_enabled = 1 LIMIT 1 ');
	if (@mysql_num_rows($res_adm)) {
			
		/*Устанавливаем (авторизуем) админа $my_id*/
		$my_id = $_SESSION['my_id'];

	} /*неверный ID*/

/*Проверяем авторизацию по COOKIE*/	
} else if ($_COOKIE['testcookie']) {

	/*Ищем админа с этим куки*/
	$res_adm = mysql_query(' SELECT admin_id FROM '.$CONFIG['db_prefix'].'admins WHERE admin_cookie = "'.$_COOKIE['testcookie'].'" AND admin_enabled = 1 LIMIT 1 ');
	if (@mysql_num_rows($res_adm)) {
		$admin = mysql_fetch_array($res_adm);
		
		/*Устанавливаем (авторизуем) админа $my_id*/
		$my_id = $admin['admin_id'];

	} /*неверные куки*/
	
}



// Определяем страницу
if ($_GET['com']) $com = $_GET['com'];
if ($_GET['tpl']) $tpl = $_GET['tpl'];



/*Выход (работает для любой страницы)*/
if ($_POST['exit']) {
	
	/*Снимаем текущую авторизацию*/
	mysql_query(' UPDATE '.$CONFIG['db_prefix'].'admins SET admin_cookie = "" WHERE admin_id = '.$my_id.' LIMIT 1 ');
	setcookie('testcookie', '', time()-3600);
	unset($_SESSION['my_id']);
	unset($my_id);
	
	header('Location: /'.$CONFIG['root_folder'].'/admin/login');
	exit;
}



/*Восстановление пароля на емейл*/
if ($_POST['restore'] && ($com == 'lost_password')) {
	
	$email = trim($_POST['email']);
	
	/*Проверяем емейл*/
	$res_adm = mysql_query(' SELECT admin_id, admin_security, admin_email_charset FROM '.$CONFIG['db_prefix'].'admins WHERE admin_email = "'.$email.'" AND admin_enabled = 1 LIMIT 1 ');
	if (@mysql_num_rows($res_adm) > 0) {
		
		$admin = mysql_fetch_array($res_adm);
		
		/*Генерируем код восстановления пароля*/
		$expire = time() + 3600;
		$admin_password_code = md5($expire.$email.$admin['admin_security']);
		
		/*Пишем код и время в настройки админа*/
		$res_code = mysql_query(' UPDATE '.$CONFIG['db_prefix'].'admins SET admin_password_code = "'.$admin_password_code.'", admin_password_code_time = '.$expire.' 
			WHERE admin_id = "'.$admin['admin_id'].'" LIMIT 1 ');
			
		/*Отправляем ссылку на почту админу*/
		if ($res_code) {
			
			$email_subject = "Восстановление пароля для ".$CONFIG['site'];
			
			$email_text .= $CONFIG['robot_sender']." на ".$CONFIG['site']."\n\n";
			$email_text .= "Был выполнен запрос на восстановление пароля на E-mail: ".$email."\n\n";
			$email_text .= "Программа на хранит ваши пароли, но вы можете установить новый пароль, перейдя по ссылке:\n";
			$email_text .= "http://".$CONFIG['site']."/".$CONFIG['root_folder']."/admin/lost_password?id=".$admin['admin_id']."&try=".$admin_password_code."\n\n";
			$email_text .= "Ссылка действительна в течение 1 часа.\n\n";
			$email_text .= "------------------------------------------------\n";
			$email_text .= "Не отвечайте на это письмо, оно отправлено автоматически.\n";

			if (!$admin['admin_email_charset']) $admin['admin_email_charset'] = 'UTF-8';
			
			if ($admin['admin_email_charset'] <> 'UTF-8') {
				$email_subject = iconv('UTF-8', $admin['admin_email_charset'].'//IGNORE', $email_subject);
				$email_text = iconv('UTF-8', $admin['admin_email_charset'].'//IGNORE', $email_text);
			}

			
			
			// Отправка с помощью PHPMailer
			if (class_exists('FreakMailer')) {

				// инициализируем класс
				$mailer = new FreakMailer();
				
				// Устанавливаем кодировку письма по настройкам аккаунта
				if ($admin['admin_email_charset']) $mailer->CharSet = $admin['admin_email_charset'];

				// Устанавливаем тему письма
				$mailer->Subject = $email_subject;
				
				// Задаем тело письма как текст
				$mailer->Body = $email_text.'[PHPMailer]';
				
				// Добавляем адрес в список получателей
				$mailer->AddAddress($email, '');
				
				// Отправляем
				if(!$mailer->Send()) echo 'Ошибка: не могу отправить письмо. Свяжитесь с разработчиком.';
				else echo '';
				
				$mailer->ClearAddresses();
				$mailer->ClearAttachments();

			
			
			// Обычная отправка
			} else {
				if ($CONFIG['robot_email']) $headers = "From: ".$CONFIG['robot_sender']." <".$CONFIG['robot_email']."@".$CONFIG['site'].">\r\n";
				$sent = mail($email, $email_subject, $email_text, $headers);
			}
			
		} /*else $email_error = 'Ошибка: не могу начать процедуру восстановления пароля, ошибка БД.';*/
	
	} /*else $email_error = 'Ошибка: неверный E-mail администратора.';*/
	
	$email_success = 'Если введенный вами E-mail верен, то на него отправлено письмо с дальнейшими инструкциями.';
}



/*Ввод нового пароля после восстановления*/
if ($_POST['setnew'] && ($com == 'lost_password') && $_GET['id'] && $_GET['try']) {

	/*Проверяем ссылку на восстановление пароля*/
	$id = intval($_GET['id']);
	$try = trim($_GET['try']);
	$res_adm = mysql_query(' SELECT admin_password_code, admin_password_code_time FROM '.$CONFIG['db_prefix'].'admins WHERE admin_id = '.$id.' 
		AND admin_enabled = 1 ');
	if (@mysql_num_rows($res_adm) > 0) {
		$admin = mysql_fetch_array($res_adm);
					
		/*Код совпадает*/
		if ($admin['admin_password_code'] == $try) {

			/*Ссылка еще не истекла*/
			if (time() <= $admin['admin_password_code_time']) {
							
				$password = $_POST['password'];
				$password_confirm = $_POST['password_confirm'];

				if ($password || $password_confirm) {
						
					/*Пароль и повторный ввод совпадают*/
					if ($password === $password_confirm) {
								
						/*Пароль >= 6 символов*/
						if (strlen($password) >= 6) {
							
							/*Пишем в БД*/
							$res = mysql_query(' UPDATE '.$CONFIG['db_prefix'].'admins SET admin_password = "'.md5($password).'", 
							admin_password_code = "", admin_password_code_time = 0 WHERE admin_id = '.$id);
								
							if ($res) $setnew_success = 'Пароль изменен';
							else $setnew_error = 'Ошибка: пароль не изменен, ошибка БД.';
								
						} else $setnew_error = 'Ошибка: пароль не изменен, новый пароль содержит менее 6 символов.';
							
					} else $setnew_error = 'Ошибка: пароль не изменен, новый прароль и повторный ввод пароля не совпадают.';
				
				} /*Не введены пароли*/
							
			} /*ссылка просрочена*/
		
		} /*направильный код восстановления*/
		
	} /*нет такого админа*/

}



/*Вход*/
if ($_POST['enter'] && ($com == 'login')) {
	
	$email = trim($_POST['email']);
	$password = $_POST['password'];
	
	/*Если заданы логин и пароль*/
	if ($email && $password) {
	
		$password_md5 = md5($password);
	
		/*Ищем админа*/
		$res_adm = mysql_query(' SELECT admin_id, admin_password, admin_cookie, admin_security, admin_auth_type FROM '.$CONFIG['db_prefix'].'admins 
			WHERE admin_email = "'.$email.'" AND admin_password = "'.$password_md5.'" AND admin_enabled = 1 LIMIT 1 ');
		if (@mysql_num_rows($res_adm)) {
			$admin = mysql_fetch_array($res_adm);
			
			/*Авторизуем админа*/
			$my_id = $admin['admin_id'];
			
			/*Запоминаем админа в session*/
			if ($admin['admin_auth_type']) {
				$_SESSION['my_id'] = $my_id;
			
			/*Запоминаем админа в cookie*/
			} else {
				$new_cookie = md5($admin['admin_id'].$admin['admin_security'].time());
				mysql_query(' UPDATE '.$CONFIG['db_prefix'].'admins SET admin_cookie = "'.$new_cookie.'" WHERE admin_id = '.$my_id.' LIMIT 1 ');
				/*Помнить меня*/
				if ($_POST['remember']) setcookie('testcookie', $new_cookie, time()+31622400);
				/*Не помнить меня*/
				else setcookie('testcookie', $new_cookie);
			}
			
			header('Location: /'.$CONFIG['root_folder'].'/admin/');
			exit;
			
		} else $setnew_error = 'Ошибка: не правильный E-mail или пароль.';
	
	} else $setnew_error = 'Ошибка: не правильный E-mail или пароль2.';
	
	/*Если не произошла переадрессация, то снимаем текущую авторизацию*/
	mysql_query(' UPDATE '.$CONFIG['db_prefix'].'admins SET admin_cookie = "" WHERE admin_id = '.$my_id.' LIMIT 1 ');
	setcookie('testcookie', '', time()-3600);
	unset($_SESSION['my_id']);
	unset($my_id);
}



/*Не авторизовн (только login и lost_password)*/
if (!$my_id) {



	/*Страница авторизации*/
	if ($com == 'login') {
		
		?><!DOCTYPE html>
		<html>
		<head>
			<title>Авторирзация</title>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<link rel="stylesheet" type="text/css" href="login.css?ver=<? echo filemtime($_SERVER['DOCUMENT_ROOT'].'/'.$CONFIG['root_folder'].'/admin/login.css'); ?>">
		</head>
		<body>

		<form method="post" action="">
		<div align="center" class="auth">
			<div class="auth_title">АВТОРИЗАЦИЯ</div>
			
			<?php
			if ($setnew_error) {
				echo '<div class="nok" style="margin-bottom: 10px;">'.$setnew_error.'</div>';
			}
			?>
			
			<div>E-mail</div>
			<div><input type="email" name="email" style="text-align: center; width: 300px;"></div>
			<div>Пароль</div>
			<div><input type="password" name="password" style="text-align: center; width: 300px;"></div>
			<div style="margin-top: 10px;"><input type="submit" name="enter" value="Войти"></div>
			
			<table border="0" cellspacing="0" cellpadding="0" align="center" style="margin-top: 10px; width: 100%;">
				<tr>
					<td align="left" valign="bottom"><input type="checkbox" name="remember" value="1"> - запомнить меня</a></td>
					<td align="right" valign="bottom"><a href="lost_password">восстановление пароля</a></td>
				</tr>
			</table>
			
		</div>
		</form>
		
		</body>
		</html>
		<?php
		
	/*Страница восстановления пароля*/
	} else if ($com == 'lost_password') {
		
		?><!DOCTYPE html>
		<html>
		<head>
			<title>Восстановление пароля</title>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<link rel="stylesheet" type="text/css" href="login.css?ver=<? echo filemtime($_SERVER['DOCUMENT_ROOT'].'/'.$CONFIG['root_folder'].'/admin/login.css'); ?>">
		</head>
		<body>

		<form method="post" action="">
		<div align="center" class="auth">
			
			<div class="auth_title">ВОССТАНОВЛЕНИЕ ПАРОЛЯ</div>
			
			<?php
			/*Пароль восстановлен*/
			if ($setnew_success) {
				echo '<div class="ok" style="margin-bottom: 10px;">'.$setnew_success.'</div>';
				?>
				<div style="margin-top: 10px;"><a href="login">вернуться на страницу авторизации</a></div>
				<?php
				
			/*Процедура восстановления*/
			} else {
				/*Введение нового пароля*/
				if ($_GET['try']) {
					$id = intval($_GET['id']);
					$try = $_GET['try'];

					/*Проверяем код восстановления*/
					$res_adm = mysql_query(' SELECT admin_password_code, admin_password_code_time FROM '.$CONFIG['db_prefix'].'admins 
						WHERE admin_id = '.$id.' AND admin_enabled = 1 ');
					if (@mysql_num_rows($res_adm) > 0) {
						$admin = mysql_fetch_array($res_adm);
						
						/*Код совпадает*/
						if ($admin['admin_password_code'] == $try) {

							/*Ссылка еще не истекла*/
							if (time() <= $admin['admin_password_code_time']) {
								
								if ($setnew_error) echo '<div class="nok" style="margin-bottom: 10px;">'.$setnew_error.'</div>';
								?>
								<div>Новый пароль</div>
								<div><input type="password" name="password" style="text-align: center; width: 300px;" autocomplete="off"></div>
								<div>Повторный ввод пароля</div>
								<div><input type="password" name="password_confirm" style="text-align: center; width: 300px;" autocomplete="off"></div>
								<div style="margin-top: 10px;"><input type="submit" name="setnew" value="Продолжить"></div>
								<div style="margin-top: 10px;"><a href="login">вернуться на страницу авторизации</a></div>
								<?php
							
							/*Ссылка истекла*/
							} else {
								?>
								<p class="alert">Ваша ссылка для восстановления пароля устарела. Получите ее снова.<p>
								<div style="margin-top: 10px;"><a href="lost_password">восстановление пароля</a></div>
								<?php
							}
						
						/*Код не совпадает*/
						} else {
							?>
							<p class="alert">Неверная ссылка для восстановления пароля.<p>
							<div style="margin-top: 10px;"><a href="lost_password">восстановление пароля</a></div>
							<?php
						}
						
					/*Неверный id админа*/
					} else {
						?>
						<p class="alert">Неверная ссылка для восстановления пароля.<p>
						<div style="margin-top: 10px;"><a href="lost_password">восстановление пароля</a></div>
						<?php
					}
					
				/*Запрос кода восстановления*/
				} else {
					
					if ($email_success) {
						echo '<div class="ok">'.$email_success.'</div>';
						
					} else if ($email_error) {
						echo '<div class="nok">'.$email_error.'</div>';
						?>
						<div style="margin-top: 10px;"><a href="lost_password">восстановление пароля</a></div>
						<?php
						
					} else {
						?>
						<p class="field_info" align="center">Для восстановления пароля укажите свой E-mail.
						
						<div>E-mail</div>
						<div><input type="email" name="email" style="text-align: center; width: 300px;" value="" autocomplete="off"></div>
						<div style="margin-top: 10px;"><input type="submit" name="restore" value="Продолжить"></div>
						<div style="margin-top: 10px;"><a href="login">вернуться на страницу авторизации</a></div>
						<?php
					}
				}
			}
			?>
			
		</div>
		</form>
		
		</body>
		</html>
		<?php



	/*Переадресуем на страницу авторизации*/
	} else {
		header('Location: /'.$CONFIG['root_folder'].'/admin/login');
		exit;
	}

	

/*Авторизован*/
} else if ($my_id) {



	if ($com == 'login') {
		header('Location: /'.$CONFIG['root_folder'].'/admin/');
		exit;
	}

	
	
	/*Читаем админа*/
	$res_adm = mysql_query(' SELECT * FROM '.$CONFIG['db_prefix'].'admins WHERE admin_id = '.$my_id.' LIMIT 1 ');
	$admin = mysql_fetch_array($res_adm);
	$cols = unserialize($admin['admin_columns']);
	
	
	
	/*Устанавливаем admin_security, если ранее не установлен*/
	if (!$admin['admin_security']) {
		$admin['admin_security'] = md5($my_id.$admin['admin_email'].time());
		mysql_query(' UPDATE '.$CONFIG['db_prefix'].'admins SET admin_security = "'.$admin['admin_security'].'" WHERE admin_id = '.$my_id.' LIMIT 1 ');
	}


	
	// Читаем настройки тестов
	$res_set = mysql_query(' SELECT * FROM '.$CONFIG['db_prefix'].'settings WHERE id = 1 ');
	$settings = mysql_fetch_array($res_set);



	/* КАРТИНКА */
	if (($com == 'graph_img') || ($com == 'compare_img') || ($com == 'test_print') || ($com == 'answers_print') || ($com == 'compare_print') || ($tpl == 'blank')) {
	
		include ('components/'.$com.'.php');



	/* ФРЕЙМСЕТ ТЕСТОВ */
	} else if ($com == '') {
	
		?><!DOCTYPE html>
		<html>
		<head>
		<title>OCA</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="icon" type="image/x-icon" href="favicon.ico">
		</head>
		<frameset rows="50,*" cols="100%">
			<frame src="header" name="frame0" id="frame0" noresize>
			<frameset cols="30%, 35%, 35%">
				<frame src="tests" name="frame1" id="frame1">
				<frame src="test_graph_blank" name="frame2" id="frame2">
				<frame src="test_cards_blank" name="frame3" id="frame3">
			</frameset>
		</frameset>
		</html>
		<?php

		
	
	/* ФРЕЙМСЕТ НАСТРОЕК */
	} else if ($com == 'settings') {
	
		?><!DOCTYPE html>
		<html>
		<head>
		<title>OCA</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="icon" type="image/x-icon" href="favicon.ico">
		</head>
		<frameset rows="50,*" cols="100%">
			<frame src="header" name="frame0" id="frame0" noresize>
			<frameset cols="200,*">
				<frame src="menu" name="frame1" id="frame1">
				<frame src="settings_blank" name="frame2" id="frame2">
			</frameset>
		</frameset>
		</html>
		<?php



						
	/* ПРОФИЛЬ */
	} else if ($com == 'profile') {

		?><!DOCTYPE html>
		<html>
		<head>
		<title>OCA</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="icon" type="image/x-icon" href="favicon.ico">
		</head>
		<frameset rows="50,*" cols="100%">
			<frame src="header" name="frame0" id="frame0" noresize>
			<frame src="profile_settings" id="frame1">
		</frameset>
		</html>
		<?php

		
	
	/* КОНТЕНТ ФРЕЙМА */
	} else {
	
		?><!DOCTYPE html>
		<html>
		<head>
		<title>OCA</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="icon" type="image/x-icon" href="favicon.ico">
		<link rel="stylesheet" type="text/css" href="admin.css?ver=<? echo filemtime($_SERVER['DOCUMENT_ROOT'].'/'.$CONFIG['root_folder'].'/admin/admin.css'); ?>">
		<script type="text/javascript" src="admin.js"></script>
		
		<script type="text/javascript" src="../highslide/highslide.js"></script>
		<link rel="stylesheet" type="text/css" href="../highslide/highslide.css">
		<script type="text/javascript">
			hs.graphicsDir = '../highslide/graphics/';
			hs.wrapperClassName = 'wide-border';
		</script>
		
		</head>
		<?php
		//if ($admin['admin_bg']) $bg = 'background-image: url(\'images/bg_img/'.$admin['admin_bg'].'\'); background-position: center center; background-size: 100% auto;';
		?>
		<body>

		<!--content-->
		<div id="content">
			<?php
			if (is_file('components/'.$com.'.php')) include ($_SERVER['DOCUMENT_ROOT'].'/'.$CONFIG['root_folder'].'/admin/components/'.$com.'.php');
			else echo 'Не найден скрипт-обработчик.';
					
			/*Уведомления*/
			if (is_array($_SESSION['notification'])) {
				$notification = $_SESSION['notification'];
				echo '<div id="note">';
				foreach ($notification as $note) echo $note;
				echo '</div>';
				unset($_SESSION['notification']);
			}
			?>
		</div>
		<!--/content-->

		</body>
		</html>
		<?php
	}

}
?>