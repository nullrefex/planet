<?php
////////////////////////////////////
// Форма теста и сохранение формы //
////////////////////////////////////

// Чтобы избежать конфликтов с сайтом-носителем, все "class", "id" и "name" начинаются как "testengine_"
// Все переменные JavaScript начинаются как "jstest_"

define('__DDD__', 1);



require_once ('../config.php');

if ($_GET['check']) {
	echo $CONFIG['version'];
	exit;
}

// Get test lang
$lang_id = $_REQUEST['lang_id'];

// Get test settings
$res_conf = mysql_query(' SELECT * FROM '.$CONFIG['db_prefix'].'settings WHERE id = 1 ');
$settings = mysql_fetch_array($res_conf);
$done = false;

// Get iblocks
$res_ib = mysql_query(' SELECT iblock_name, iblock_text FROM '.$CONFIG['db_prefix'].'iblocks_translations WHERE lang_id = "'.$lang_id.'" ORDER BY id ASC ');
while($iblock = mysql_fetch_array($res_ib)) {
	$iblocks[$iblock['iblock_name']] = $iblock['iblock_text'];
}

// Get template files
$path = $_SERVER['DOCUMENT_ROOT'].'/'.$CONFIG['root_folder'].'/client/template/*.php';
foreach (glob($path) as $file) {
	$tpl_name = basename($file, '.php');
	$TPL[$tpl_name] = file_get_contents($file);
}



// Save test
if ($_REQUEST['answers']) {

	$source_site = $_REQUEST['source_site'];
	if (!$source_site) $source_site = $CONFIG['site'];

	$lang_id = $_REQUEST['lang_id'];

	$birth_date = trim($_REQUEST['birth_date']);

	$sex = $_REQUEST['sex'];
	if (($sex <> 'M') && ($sex <> 'F')) $sex = '';
	
	$answers = $_REQUEST['answers'];
	
	$pers_info_consent = intval($_REQUEST['pers_info_consent']);

	// All fields as source settings may differ from collector
	$res_f = mysql_query(' SELECT field_name FROM '.$CONFIG['db_prefix'].'fields WHERE field_name <> "" ');
	while ($field = mysql_fetch_array($res_f)) {
		$value = trim($_REQUEST['testengine_'.$field['field_name']]);
		if ($value) $f_values[$field['field_name']] = $value;
	}
	
	// Extra fields with titles as in request
	$testengine_ef = $_REQUEST['testengine_ef'];
	$title_testengine_ef = $_REQUEST['title_testengine_ef'];
	foreach ($testengine_ef as $id => $efield_value) {
		$efield_value = trim($efield_value);
		$efield_title = trim($title_testengine_ef[$id]);
		$ef_values[$id] = $efield_value;
		$ef_titles[$id] = $efield_title;
	}
	
	
	
	// Send to site-collector (here is site-source with redirection)
	if (($source_site == $CONFIG['site']) && $settings['redirect']) {

		$url =  $settings['redirect'].'/client/test.php?source_site='.$CONFIG['site'];
		$url .= '&lang_id='.$lang_id;
		$url .= '&birth_date='.$birth_date;
		$url .= '&sex='.$sex;
		$url .= '&answers='.$answers;
			
		// Fields
		foreach ($f_values as $field_name => $field_value) {
			$url .= '&testengine_'.$field_name.'='.urlencode($field_value);
		}

		// Extra fields
		foreach ($ef_values as $id => $efield_value) {
			$url .= '&testengine_ef['.$id.']='.urlencode($efield_value);
			$url .= '&title_testengine_ef['.$id.']='.urlencode($ef_titles[$id]);
		}
			
		// Sending
		$res_redir = file_get_contents($url);
		$res_redir = trim($res_redir);
	}
		
		
	
	// Save here (here is site-collector, site-source without redirection, redirection failure, save copy)
	if ($res_redir <> 'success' || $settings['redirect_leave_copy']) {

		// Extra fields prepare for save
		foreach ($ef_values as $id => $efield_value) {
			$ef[$ef_titles[$id]] = $efield_value;
		}
		$ef_str = serialize($ef);

		// Saving fields
		$res_save = mysql_query(' INSERT INTO '.$CONFIG['db_prefix'].'tests (test_date, first_name, second_name, last_name, birth_date, sex, phone, email, 
			address,  post, lang_id, pers_info_consent, source_site, efields, answers) VALUES ("'.date('Y-m-d').'", "'.addslashes($f_values['first_name']).'", 
			"'.addslashes($f_values['second_name']).'", "'.addslashes($f_values['last_name']).'", "'.$birth_date.'", "'.$sex.'", 
			"'.addslashes($f_values['phone']).'", "'.addslashes($f_values['email']).'", "'.addslashes($f_values['address']).'", 
			"'.addslashes($f_values['post']).'", "'.$lang_id.'", '.$pers_info_consent.', "'.$source_site.'", "'.addslashes($ef_str).'", "'.$answers.'") ');
			
		// Get new test id
		$res_find = mysql_query(' SELECT test_id FROM '.$CONFIG['db_prefix'].'tests WHERE birth_date = "'.$birth_date.'" ORDER BY test_id DESC LIMIT 1 ');
		$find = mysql_fetch_array($res_find);
		$test_id = $find['test_id'];
		
		
			
		// E-mail to test administrator
		$res_usr = mysql_query(' SELECT admin_email, admin_email_charset FROM '.$CONFIG['db_prefix'].'admins WHERE admin_test_notification = 1 AND admin_email_code = "" ');
		if (@mysql_num_rows($res_usr) > 0) {
			
			$email_subject = "Новый тест OCA с ".$source_site;

			
			$email_text .= "Новый тест OCA на ".$CONFIG['site']."\n\n";
			
			if ($res_save) {
				$email_text .= "Дата рождения: ".$birth_date."\n";
				if ($sex == 'M') $email_text .= "Пол: муж\n";
				if ($sex == 'F') $email_text .= "Пол: жен\n";
				if ($f_values['last_name']) $fio .= $f_values['last_name']." ";
				if ($f_values['first_name']) $fio .= $f_values['first_name']." ";
				if ($f_values['second_name']) $fio .= $f_values['second_name']." ";
				if ($fio) $email_text .= "ФИО: ".$fio."\n";
				if ($f_values['email']) $email_text .= "E-mail: ".$f_values['email']."\n";
				if ($f_values['phone']) $email_text .= "Телефон: ".$f_values['phone']."\n";
				if ($f_values['address']) $email_text .= "Адрес: ".$f_values['address']."\n";
				if ($f_values['post']) $email_text .= "Должность: ".$f_values['post']."\n";
				$email_text .= "\nПерейдите по ссылке для просмотра теста: http://".$CONFIG['site']."/".$CONFIG['root_folder']."/admin/\n\n";
				
			} else {
				if ($test_id) $email_text .= "Ошибка создания записи в БД с результатами для теста #".$test_id.". Срочно обратитесь в техподдержку для устранения проблемы.\n";
				else $email_text .= "Ошибка создания записи для теста в БД. Срочно обратитесь в техподдержку для устранения проблемы.\n";
			}
			
			$email_text .= "------------------------------------------------\n";
			$email_text .= "Не отвечайте на это письмо, оно отправлено автоматически.\n";
			
			while ($admin = mysql_fetch_array($res_usr)) {
				if ($CONFIG['robot_email']) $headers = "From: ".$CONFIG['robot_sender']." <".$CONFIG['robot_email']."@".$CONFIG['site'].">\r\n";
				else $headers = '';
				
				if (!$admin['admin_email_charset']) $admin['admin_email_charset'] = 'UTF-8';
				//$header .= 'Content-type: text/plain; charset="'.$CONFIG['charsets_header'][$admin['admin_email_charset']].'"';

				if ($admin['admin_email_charset'] <> 'UTF-8') {
					$email_subject_1 = iconv('UTF-8', $admin['admin_email_charset'].'//IGNORE', $email_subject);
					$email_text_1 = iconv('UTF-8', $admin['admin_email_charset'].'//IGNORE', $email_text);

				} else {
					$email_subject_1 = $email_subject;
					$email_text_1 = $email_text;
				}
				
				mail($admin['admin_email'], $email_subject_1, $email_text_1, $headers);
			}
		}
		
		
	}
	
	
	// Send test notification
	if ($source_site == $CONFIG['site']) {
		if ($res_save || ($res_redir == 'success')) {
			$TPL['tpl'] = $TPL['tpl-send-ok'];
			$TPL['send-ok'] = $iblocks['send-ok'];
		
		} else {
			$TPL['tpl'] = $TPL['tpl-send-nok'];
			$TPL['send-nok'] = $iblocks['send-nok-1'];
		}
		
	} else {
		if ($res_save) echo 'success';
		else echo 'fail';
	}

	
	
// Printing test form
} else {
	
	// Fields translations
	$res_f = mysql_query(' SELECT field_name, field_title, field_info, field_testvalue FROM '.$CONFIG['db_prefix'].'fields_translations 
		WHERE lang_id = "'.$lang_id.'" ORDER BY id ASC ');
	while($field = mysql_fetch_array($res_f)) $fields[$field['field_name']] = $field;

	// Extra fields translations
	$res_ef = mysql_query(' SELECT efield_id, efield_title, efield_info, efield_testvalue FROM '.$CONFIG['db_prefix'].'efields_translations 
		WHERE lang_id = "'.$lang_id.'" ORDER BY id ASC ');
	while($efield = mysql_fetch_array($res_ef)) $efields[$efield['efield_id']] = $efield;

	// Debug test values
	if ($settings['debug_mode']) {
		$birth_d = '25';
		$birth_m = '03';
		$birth_y = '1995';
		$sex = 'M';
	}
	
	
	
	///////////////////////////////// Block1: TITLE BLOCK /////////////////////////////////
	$TPL['iblock-h1'] = $iblocks['h1'];
	$TPL['iblock-h1-note'] = $iblocks['h1-note'];
	
	
	
	///////////////////////////////// Block2: PERCONAL DATA /////////////////////////////////
	$TPL['iblock-h2-personal'] = $iblocks['h2-personal'];
	$TPL['iblock-required-note'] = $iblocks['required-note'];
	
	// Birth date
	$TPL['iblock-birth-date'] = $iblocks['birth-date'];
	$TPL['iblock-birth-note'] = $iblocks['birth-note'];
	
	$TPL['birth-d-options'] = '<option value=""></option>';
	for ($d=1; $d<=31; $d++) {
		if ($birth_d && ($birth_d == $d)) $TPL['birth-d-options'] .= '<option value="'.$d.'" selected>'.$d.'</option>';
		else $TPL['birth-d-options'] .= '<option value="'.$d.'">'.$d.'</option>';
	}

	$TPL['birth-m-options'] = '<option value=""></option>';
	for ($m=1; $m<=12; $m++) {
		if ($birth_m && ($birth_m == $m)) $TPL['birth-m-options'] .= '<option value="'.$m.'" selected>'.$m.'</option>';
		else $TPL['birth-m-options'] .= '<option value="'.$m.'">'.$m.'</option>';
	}

	$TPL['birth-y-options'] = '<option value=""></option>';
	$last_y = date('Y') - 14;
	$first_y = $last_y - 100;
	for ($y=$last_y; $y>=$first_y; $y--) {
		if ($birth_y && ($birth_y == $y)) $TPL['birth-y-options'] .= '<option value="'.$y.'" selected>'.$y.'</option>';
		else $TPL['birth-y-options'] .= '<option value="'.$y.'">'.$y.'</option>';
	}

	// Sex
	$TPL['iblock-sex'] = $iblocks['sex'];
	$TPL['iblock-sex-m'] = $iblocks['sex-m'];
	$TPL['iblock-sex-f'] = $iblocks['sex-f'];
	if ($sex == 'M') $TPL['sex-m-checked'] = ' checked ';
	if ($sex == 'F') $TPL['sex-f-checked'] = ' checked ';
	
	// Fields
	$res = mysql_query(' SELECT field_name, field_required, field_type FROM '.$CONFIG['db_prefix'].'fields WHERE field_enabled = 1 ORDER BY field_order ASC ');
	while ($field = mysql_fetch_array($res)) {

		// Debug test values
		if ($settings['debug_mode']) {
			if (!$new[$field['field_name']]) $new[$field['field_name']] = $fields[$field['field_name']]['field_testvalue'];
		}
					
		// Get field TPL
		if ($field['field_type'] == 'text') $fieldline = $TPL['tpl-data-text'];
		else if ($field['field_type'] == 'textarea') $fieldline = $TPL['tpl-data-textarea'];
		
		// Replacing
		$fieldline = str_replace('{data-title}', $fields[$field['field_name']]['field_title'], $fieldline);
		$fieldline = str_replace('{data-info}', $fields[$field['field_name']]['field_info'], $fieldline);
		if ($field['field_required']) $fieldline = str_replace('{data-required-star}', '*', $fieldline);
		$fieldline = str_replace('{title}', htmlspecialchars($fields[$field['field_name']]['field_title']), $fieldline);
		$fieldline = str_replace('{id}', 'testengine_'.$field['field_name'], $fieldline);
		$fieldline = str_replace('{name}', 'testengine_personal', $fieldline);
		$fieldline = str_replace('{value}', htmlspecialchars($new[$field['field_name']]), $fieldline);
		if ($field['field_required']) $fieldline = str_replace('{required}', ' required ', $fieldline);
		
		// Add to output
		$TPL['fields'] .= $fieldline;
	}

	// Extra fields
	$res = mysql_query(' SELECT efield_id, efield_required, efield_type FROM '.$CONFIG['db_prefix'].'efields WHERE efield_enabled = 1 ORDER BY efield_order ASC ');
	while ($efield = mysql_fetch_array($res)) {
				
		// Debug test values
		if ($settings['debug_mode']) {
			if (!$new['ef'][$efield['efield_id']]) $new['ef'][$efield['efield_id']] = $efields[$efield['efield_id']]['efield_testvalue'];
		}
		
		// Get efield TPL
		if ($efield['efield_type'] == 'text') $efieldline = $TPL['tpl-data-text'];
		else if ($efield['efield_type'] == 'textarea') $efieldline = $TPL['tpl-data-textarea'];
		
		// Replacing
		$efieldline = str_replace('{data-title}', $efields[$efield['efield_id']]['efield_title'], $efieldline);
		$efieldline = str_replace('{data-info}', $efields[$efield['efield_id']]['efield_info'], $efieldline);
		if ($efield['efield_required']) $efieldline = str_replace('{data-required-star}', '*', $efieldline);
		$efieldline = str_replace('{title}', htmlspecialchars($efields[$efield['efield_id']]['efield_title']), $efieldline);
		$efieldline = str_replace('{id}', 'testengine_ef['.$efield['efield_id'].']', $efieldline);
		$efieldline = str_replace('{name}', 'testengine_extra', $efieldline);
		$efieldline = str_replace('{value}', htmlspecialchars($new['ef'][$efield['efield_id']]), $efieldline);
		if ($efield['efield_required']) $efieldline = str_replace('{required}', ' required ', $efieldline);
		
		// Add to output
		$TPL['efields'] .= $efieldline;
		$TPL['efields'] .= '<input type="hidden" id="title_testengine_ef['.$efield['efield_id'].']" value="'.htmlspecialchars($efields[$efield['efield_id']]['efield_title']).'">';
	}

	
	
	///////////////////////////////// Block3: INSTRUCTIONS /////////////////////////////////
	$TPL['iblock-h2-instruction'] = $iblocks['h2-instruction'];
	$TPL['iblock-instruction'] = $iblocks['instruction'];
	$TPL['iblock-choice-1'] = $iblocks['choice-1'];
	$TPL['iblock-choice-2'] = $iblocks['choice-2'];
	$TPL['iblock-choice-3'] = $iblocks['choice-3'];
	
	
	
	///////////////////////////////// Block4: QUESTIONS /////////////////////////////////
	$TPL['iblock-h2-test'] = $iblocks['h2-test'];
	$TPL['iblock-send-button'] = $iblocks['send-button'];
	
	$res = mysql_query(' SELECT question_id, question_text FROM '.$CONFIG['db_prefix'].'questions WHERE lang_id = "'.$lang_id.'" ORDER BY question_id ASC ');
	while ($question = mysql_fetch_array($res)) {
		unset($checked);
			
		if ($settings['debug_mode']) {
			if (!$new['q'][$question['question_id']]) $new['q'][$question['question_id']] = rand(1,3);
		}
			
		if ($new['q'][$question['question_id']]) $checked[$new['q'][$question['question_id']]] = ' checked="checked" ';
			
		// Get efield TPL
		$qline = $TPL['tpl-question'];

		// Replacing
		$qline = str_replace('{question-id}', $question['question_id'], $qline);
		$qline = str_replace('{question-text}', $question['question_text'], $qline);
		$qline = str_replace('{id-1}', 'testengine_q'.$question['question_id'].'_1', $qline);
		$qline = str_replace('{id-2}', 'testengine_q'.$question['question_id'].'_2', $qline);
		$qline = str_replace('{id-3}', 'testengine_q'.$question['question_id'].'_3', $qline);
		$qline = str_replace('{id}', 'testengine_'.$question['question_id'], $qline);
		$qline = str_replace('{name}', 'q['.$question['question_id'].']', $qline);
		if ($new['q'][$question['question_id']]) $qline = str_replace('{question-'.$new['q'][$question['question_id']].'-checked}', ' checked ', $qline);
		
		// Add to output
		$TPL['questions'] .= $qline;
	}

	if ($settings['pers_info_consent']) {
		$TPL['consent'] = '<div id="testengine_consent_div">
			<input type="checkbox" class="testengine_input_checkbox" id="testengine_consent" value="1"> 
			'.$iblocks['pers_info_note'].'</div>';
	}
	
	
	
	///////////////////////////////// Block5: COPYRIGHT /////////////////////////////////
	if ($iblocks['copyright']) $TPL['iblock_copyright'] = $iblocks['copyright'];
	$TPL['version'] .= '<div style="font-size: 12px; color: #333333; font-family: Verdana; font-weight: normal;
		text-align: right !important;" align="right">v'.$CONFIG['version'].'</div>';
}



// Substituting output (brake infinit cycle by limit $i < 50)
$var_matches[] = '1';
$i = 0;
while (count($var_matches) && ($i < 50)) {
	preg_match_all("|\{(.+)\}|Ui", $TPL['tpl'], $var_matches, PREG_SET_ORDER);
	foreach ($var_matches as $match) {
		//echo $match[0].' - '.$match[1].' - '.htmlspecialchars($TPL[$match[1]]).'<BR>';
		if (isset($TPL[$match[1]])) $TPL['tpl'] = str_replace($match[0], $TPL[$match[1]], $TPL['tpl']);
		else $TPL['tpl'] = str_replace($match[0], '', $TPL['tpl']);
	}
	$i++;
}

// Printing output
echo $TPL['tpl'];
?>