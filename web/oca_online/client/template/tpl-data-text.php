<!-- field & extra field tpl if type=text -->

<tr>
	<td class="testengine_td testengine_td_field_title">
		<b>{data-title}</b> <span class="testengine_alert_bold">{data-required-star}</span>
	</td>
	<td class="testengine_td">
		<input class="testengine_input_text" title="{title}" id="{id}" name="{name}" style="width: 98%;" value="{value}" {required}>
		<div class="testengine_note">{data-info}</div>
	</td>
</tr>