<!-- main tpl -->
<!-- Don't change field id, name and JS function -->

<div class="testengine_block testengine_block_1">
	<div class="testengine_h1">{iblock-h1}</div>
	<div class="testengine_h1_note">{iblock-h1-note}</div>
</div>



<div class="testengine_block testengine_block_2">
	
	<div class="testengine_h2">{iblock-h2-personal}</div>
	
	<table class="testengine_table" border="1" cellspacing="0" cellpadding="3">
	
		<tr>
			<td class="testengine_td testengine_td_field_title">{iblock-birth-date} <span class="testengine_alert_bold">*</span></td>
			<td class="testengine_td">
				
				<select class="testengine_input_select" id="testengine_birth_d" name="birth_d" required>
					{birth-d-options}
				</select>
				
				<select class="testengine_input_select" id="testengine_birth_m" name="birth_m" required>
					{birth-m-options}
				</select>
				
				<select class="testengine_input_select" id="testengine_birth_y" name="birth_y" required>
					{birth-y-options}
				</select>
				
				<div class="testengine_alert_normal">{iblock-birth-note}</div>
			</td>
		</tr>

		<tr>
			<td class="testengine_td testengine_td_field_title">{iblock-sex} <span class="testengine_alert_bold">*</span></td>
			<td class="testengine_td" id="testengine_sex">
				<div>
					<input class="testengine_input_radio" type="radio" id="testengine_sex_m" name="sex" value="M" {sex-m-checked} required> {iblock-sex-m}
					<input class="testengine_input_radio" type="radio" id="testengine_sex_f" name="sex" value="F" {sex-f-checked} required> {iblock-sex-f}
				</div>
			</td>
		</tr>
		
		<form method="post" action="" name="testengine_form_1" id="testengine_form_1">
			{fields}
		</form>
		
		<form method="post" action="" name="testengine_form_2" id="testengine_form_2">
			{efields}
		</form>
		
	</table>
	
	<div class="testengine_required_note"><span class="testengine_alert_bold">*</span> <span class="testengine_note">{iblock-required-note}</span></div>
</div>



<div class="testengine_block testengine_block_3">
	
	<div class="testengine_h2">{iblock-h2-instruction}</div>
	
	<div class="testengine_instruction">{iblock-instruction}</div>
	
	<div align="center">
		<table class="testengine_table" border="1" cellspacing="0" cellpadding="3" width="100%">
			<tr>
				<td class="testengine_td testengine_td_radio" align="center"><input class="testengine_input_radio" type="radio" name="dummy1" checked="checked" disabled></td>
				<td class="testengine_td testengine_td_radio" align="center"><input class="testengine_input_radio" type="radio" name="dummy1" disabled></td>
				<td class="testengine_td testengine_td_radio" align="center"><input class="testengine_input_radio" type="radio" name="dummy1" disabled></td>
				<td class="testengine_td">{iblock-choice-1}</td>
			</tr>
			<tr>
				<td class="testengine_td testengine_td_radio" align="center"><input class="testengine_input_radio" type="radio" name="dummy2" disabled></td>
				<td class="testengine_td testengine_td_radio" align="center"><input class="testengine_input_radio" type="radio" name="dummy2" checked="checked" disabled></td>
				<td class="testengine_td testengine_td_radio" align="center"><input class="testengine_input_radio" type="radio" name="dummy2" disabled></td>
				<td class="testengine_td">{iblock-choice-2}</td>
			</tr>
			<tr>
				<td class="testengine_td testengine_td_radio" align="center"><input class="testengine_input_radio" type="radio" name="dummy3" disabled></td>
				<td class="testengine_td testengine_td_radio" align="center"><input class="testengine_input_radio" type="radio" name="dummy3" disabled></td>
				<td class="testengine_td testengine_td_radio" align="center"><input class="testengine_input_radio" type="radio" name="dummy3" checked="checked" disabled></td>
				<td class="testengine_td">{iblock-choice-3}</td>
			</tr>
		</table>
	</div>

</div>



<div class="testengine_block testengine_block_4">

	<div class="testengine_h2">{iblock-h2-test}</div>

	<table class="testengine_table" border="1" cellspacing="0" cellpadding="3" width="100%">
		{questions}
	</table>
	<br>
	
	{consent}
	
	<div class="testengine_button_block">
		<input class="testengine_button" id="testengine_button" type="button" value="{iblock-send-button}" onClick="Get_Test_Result(); return false;">
	</div>

</div>



<div class="testengine_block testengine_block_5">
	<div class="testengine_copyright">{iblock_copyright}</div>
	{version}
</div>
