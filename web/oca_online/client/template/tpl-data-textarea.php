<!-- field & extra field tpl if type=textarea -->

<tr>
	<td class="testengine_td testengine_td_field_title">
		<b>{data-title}</b> <span class="testengine_alert_bold">{data-required-star}</span>
	</td>
	<td class="testengine_td">
		<textarea class="testengine_textarea" title="{title}" id="{id}" name="{name}" style="width: 98%; height: 100px;" {required}>{value}</textarea>
		<div class="testengine_note">{data-info}</div>
	</td>
</tr>