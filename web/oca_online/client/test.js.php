<?php
//////////////////////////////
// Файл подкачки JavaScript //
//////////////////////////////

// Чтобы избежать конфликтов с сайтом-носителем, все "class", "id" и "name" начинаются как "testengine_"
// Все переменные JavaScript начинаются как "jstest_"

define('__DDD__', 1);



require_once ('../config.php');

// Get test lang
$lang_id = $_REQUEST['lang_id'];
$res_lang = mysql_query(' SELECT lang_name, lang_enabled FROM '.$CONFIG['db_prefix'].'languages WHERE lang_id = "'.$lang_id.'" ');

// Lang doesn't exist
if (!@mysql_num_rows($res_lang)) {
	echo 'document.getElementById("testengine_form").innerHTML = "Language \"'.$lang_id.'\" is not supported";';
	exit;
}

$lg = mysql_fetch_array($res_lang);

// Lang not enabled
if (!$lg['lang_enabled']) {
	echo 'document.getElementById("testengine_form").innerHTML = "Language \"'.$lang_id.'\" is not supported";';
	exit;
}

// Test settings
$res_set = mysql_query(' SELECT * FROM '.$CONFIG['db_prefix'].'settings WHERE id = 1 ');
$settings = mysql_fetch_array($res_set);
$done = false;

// Get iblocks
$res_ib = mysql_query(' SELECT iblock_name, iblock_text FROM '.$CONFIG['db_prefix'].'iblocks_translations 
	WHERE lang_id = "'.$lang_id.'" ORDER BY id ASC ');
while($iblock = mysql_fetch_array($res_ib)) {
	$iblocks[$iblock['iblock_name']] = addslashes($iblock['iblock_text']);
}
?>


	
	// Create XMLHTTP object
	function getXmlHttp() {
		var xmlhttp;
		try {
			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (E) {
				xmlhttp = false;
			}
		}
		if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
			xmlhttp = new XMLHttpRequest();
		}
		return xmlhttp;
	}


	
	// Get test form
	function Get_Test_Form() {
		
		var jstest_request = "lang_id=<?php echo $lang_id?>";
		
		var xmlhttp = getXmlHttp();
		xmlhttp.open('POST', '/<?php echo $CONFIG['root_folder']?>/client/test.php', true);
		xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlhttp.send(jstest_request);
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4) {
				if(xmlhttp.status == 200) {
					document.getElementById("testengine_form").innerHTML = xmlhttp.responseText;
				}
			}
		}
	}

	

	// Send test form
	function Get_Test_Result() {
		
		document.getElementById("testengine_button").disabled = true;
		
		var jstest_request = "";
		var jstest_error = "";
		
		// Age check
		var jstest_birth_d_n = document.getElementById("testengine_birth_d").options.selectedIndex;
		var jstest_birth_m_n = document.getElementById("testengine_birth_m").options.selectedIndex;
		var jstest_birth_y_n = document.getElementById("testengine_birth_y").options.selectedIndex;
		
		var jstest_birth_d = document.getElementById("testengine_birth_d").options[jstest_birth_d_n].value;
		var jstest_birth_m = document.getElementById("testengine_birth_m").options[jstest_birth_m_n].value;
		var jstest_birth_y = document.getElementById("testengine_birth_y").options[jstest_birth_y_n].value;
		
		var jstest_birth_date = jstest_birth_y + ',' + jstest_birth_m + ',' + jstest_birth_d;

		if ((jstest_birth_d > 0) && (jstest_birth_m > 0) && (jstest_birth_y > 0)) {
			document.getElementById("testengine_birth_d").style.backgroundColor = "#ffffff";
			document.getElementById("testengine_birth_m").style.backgroundColor = "#ffffff";
			document.getElementById("testengine_birth_y").style.backgroundColor = "#ffffff";
		} else {
			document.getElementById("testengine_button").disabled = false;
			document.getElementById("testengine_result").innerHTML = "<div class=\"testengine_nok\"><?php echo $iblocks['data-alert-1']?></div>";
			document.getElementById("testengine_birth_d").style.backgroundColor = "#ffcccc";
			document.getElementById("testengine_birth_m").style.backgroundColor = "#ffcccc";
			document.getElementById("testengine_birth_y").style.backgroundColor = "#ffcccc";
			document.getElementById("testengine_birth_d").focus();
			return;
		}
		
		// Sex check
		if (document.getElementById("testengine_sex_m").checked) var jstest_sex = "M";
		else if (document.getElementById("testengine_sex_f").checked) var jstest_sex = "F";
		if (jstest_sex) {
			document.getElementById("testengine_sex").style.backgroundColor = "#ffffff";
		} else {
			document.getElementById("testengine_button").disabled = false;
			document.getElementById("testengine_result").innerHTML = "<div class=\"testengine_nok\"><?php echo $iblocks['data-alert-2']?></div>";
			document.getElementById("testengine_sex").style.backgroundColor = "#ffcccc";
			document.getElementById("testengine_sex_f").focus();
			return;
		}
		
		
		
		// Fields check
		var jstest_f_str = "";

		var jstest_personals = document.getElementsByName('testengine_personal');
		for (var jstest_i=0; jstest_i<jstest_personals.length; jstest_i++){
			
			var jstest_personal_id = jstest_personals[jstest_i].id;
			var jstest_el = document.getElementById(jstest_personal_id);
			
			// Required fields check
			if (jstest_el.required) {
				if (jstest_el.value) {
					jstest_el.style.backgroundColor = "#ffffff";
				} else {
					document.getElementById("testengine_button").disabled = false;
					document.getElementById("testengine_result").innerHTML = "<div class=\"testengine_nok\"><?php echo $iblocks['data-alert-3']?> \"" + jstest_el.title + "\"</div>";
					jstest_el.style.backgroundColor = "#ffcccc";
					jstest_el.focus();
					return;
				}
			}

			// E-mail check
			if ((jstest_personal_id == 'testengine_email') && (jstest_el.value != '')) {
				if ((jstest_el.value.indexOf('.') == -1) || (jstest_el.value.indexOf('@') == -1) || (jstest_el.value.indexOf('@') < 1)) {
					document.getElementById("testengine_button").disabled = false;
					document.getElementById("testengine_result").innerHTML = "<div class=\"testengine_nok\"><?php echo $iblocks['data-alert-6']?></div>";
					jstest_el.style.backgroundColor = "#ffcccc";
					jstest_el.focus();
					return;
				} else {
					jstest_el.style.backgroundColor = "#ffffff";
				}
			}

			// Phone check
			if ((jstest_personal_id == 'testengine_phone') && (jstest_el.value != '')) {
				if (jstest_el.value.match(/^.{5,}$/)) {
					jstest_el.style.backgroundColor = "#ffffff";
				} else {
					document.getElementById("testengine_button").disabled = false;
					document.getElementById("testengine_result").innerHTML = "<div class=\"testengine_nok\"><?php echo $iblocks['data-alert-7']?></div>";
					jstest_el.style.backgroundColor = "#ffcccc";
					jstest_el.focus();
					return;
				}
			}

			// Query from fields
			jstest_f_str += "&" + jstest_personal_id + "=" + jstest_el.value;
		}
		
		
		
		// Extra fields check
		var jstest_ef_str = "";

		var jstest_extras = document.getElementsByName('testengine_extra');
		for (var jstest_i=0; jstest_i<jstest_extras.length; jstest_i++){
			
			var jstest_ef_id = jstest_extras[jstest_i].id;
			var jstest_eft_id = "title_" + jstest_extras[jstest_i].id;
			
			var jstest_ef = document.getElementById(jstest_ef_id);
			var jstest_eft = document.getElementById(jstest_eft_id);
			
			if (jstest_ef.required) {
				if (jstest_ef.value) {
					jstest_ef.style.backgroundColor = "#ffffff";
				} else {
					document.getElementById("testengine_button").disabled = false;
					document.getElementById("testengine_result").innerHTML = "<div class=\"testengine_nok\"><?php echo $iblocks['data-alert-3']?> \"" + jstest_ef.title + "\"</div>";
					jstest_ef.style.backgroundColor = "#ffcccc";
					jstest_ef.focus();
					return;
				}
			}
		 
			// Query from extra fields
			jstest_ef_str += "&" + jstest_ef_id + "=" + jstest_ef.value;
			jstest_ef_str += "&" + jstest_eft_id + "=" + jstest_eft.value;
		}


		
		// Check answers
		var jstest_answer_str = "";
		var jstest_answer = "";
		for (var jstest_i=1; jstest_i<=200; jstest_i++) {
			jstest_answer = "0";
			if (document.getElementById("testengine_q" + jstest_i + "_1").checked) jstest_answer = "1";
			if (document.getElementById("testengine_q" + jstest_i + "_2").checked) jstest_answer = "2";
			if (document.getElementById("testengine_q" + jstest_i + "_3").checked) jstest_answer = "3";
			
			// Not answered question
			if (jstest_answer == "0") {
				document.getElementById("testengine_button").disabled = false;
				document.getElementById("testengine_result").innerHTML = "<div class=\"testengine_nok\"><?php echo $iblocks['data-alert-4']?></div>";
				document.getElementById("testengine_" + jstest_i).style.backgroundColor = "#ffcccc";
				document.getElementById("testengine_q" + jstest_i + "_3").focus();
				return;
			} else {
				jstest_answer_str += jstest_answer;
				document.getElementById("testengine_" + jstest_i).style.backgroundColor = "#ffffff";
			}
		}


		
		<?php if ($settings['pers_info_consent']) { ?>
		// Agreement check
		if (document.getElementById("testengine_consent").checked) {
			document.getElementById("testengine_sex").style.backgroundColor = "#ffffff";
		} else {
			document.getElementById("testengine_button").disabled = false;
			document.getElementById("testengine_result").innerHTML = "<div class=\"testengine_nok\"><?php echo $iblocks['data-alert-5']?></div>";
			document.getElementById("testengine_consent_div").style.backgroundColor = "#ffcccc";
			document.getElementById("testengine_consent").focus();
			return;
		}
		<?php } ?>
		
		////////////////// All fields OK //////////////////

		// Query from lang
		jstest_request = "lang_id=<?php echo $lang_id?>";

		// Query from answers
		jstest_request += "&answers=" + jstest_answer_str;
		
		// Query from birth_date
		jstest_request += "&birth_date=" + jstest_birth_date;

		// Query from sex
		jstest_request += "&sex=" + jstest_sex;
		
		// Query from fields
		jstest_request += jstest_f_str;

		// Query extra from
		jstest_request += jstest_ef_str;
		
		<?php if ($settings['pers_info_consent']) { ?>
		// Query from agreement
		jstest_request += "&pers_info_consent=1";
		<?php } ?>

		// Sending test
		var xmlhttp = getXmlHttp();
		xmlhttp.open('POST', '/<?php echo $CONFIG['root_folder']?>/client/test.php', true);
		xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlhttp.send(jstest_request);
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4) {
				if(xmlhttp.status == 200) {
					document.getElementById("testengine_result").innerHTML = xmlhttp.responseText;
					document.getElementById("testengine_form").innerHTML = '';
					scrollTo(0,0);
				}
			}
		}
	}
	