/**
 * Save state of main menu
 */
jQuery(function () {
    var expandMainMenu = tools.cookie.get('expandMainMenu');

    if (expandMainMenu === undefined){
        expandMainMenu = 'true';
    }

    jQuery('.menu-button').on('click', function () {
        setTimeout(function () {
            tools.cookie.set('expandMainMenu', !jQuery('.sidebar').hasClass('closed'));
        });
    });

    if (expandMainMenu === 'true') {
        jQuery('.sidebar').removeClass('closed');
        jQuery('#page-wrapper').removeClass('maximized');
    }
});