window.custom = {};

/*=======================================
=            SCROLLBAR WIDTH            =
=======================================*/

 window.custom.getScrollBarWidth = function () {
  var inner = document.createElement('p');
  inner.style.height = "200px";
  inner.style.width = "100%";
  var outer = document.createElement('div');
  outer.style.visibility = "hidden";
  outer.style.position = "absolute";
  outer.style.overflow = "hidden";
  outer.style.height = "150px";
  outer.style.width = "200px";
  outer.style.left = "0px";
  outer.style.top = "0px";
  outer.appendChild (inner);
  document.body.appendChild (outer);
  var w1 = inner.offsetWidth;
  outer.style.overflow = 'scroll';
  var w2 = inner.offsetWidth;
  if (w1 == w2) w2 = outer.clientWidth;
  document.body.removeChild (outer);
  window.custom.scrollBarWidth = w1 - w2;
  return window.custom.scrollBarWidth;
 };
 window.custom.is_scroll = function(){
  return $(document).height() > $(window).height();
 };

 window.custom.getScrollBarWidth();

/*=====  End of SCROLLBAR WIDTH  ======*/

requirejs.config({
	baseUrl: 'js',
	paths: {
		// libs
		jquery: '/js/lib/jquery-1.9.1',
		// bs
		bs_affix: '/js/bs/affix',
		bs_alert: '/js/bs/alert',
		bs_button: '/js/bs/button',
		bs_carousel: '/js/bs/carousel',
		bs_collapse: '/js/bs/collapse',
		bs_dropdown: '/js/bs/dropdown',
		bs_modal: '/js/bs/modal',
		bs_popover: '/js/bs/popover',
		bs_scrollspy: '/js/bs/scrollspy',
		bs_tab: '/js/bs/tab',
		bs_tooltip: '/js/bs/tooltip',
		bs_transition: '/js/bs/transition',
		// validate js
		validate_js: '/js/plugins/jquery.validate.min',
		// input mask
		mask_js: '/js/plugins/mask',
		// slick
		slick_js: '/js/plugins/slick',
		////sliders
		mainTopSlider:'/js/components/mainTopSlider',
		mnewsSlider:'/js/components/mnewsSlider',
		officeSlider:'/js/components/officeSlider',
		// modernizr
		modernizr: '/js/plugins/modernizr',
		// bs select
		bsselect_js: '/js/plugins/bootstrap-select',
		// parts
		header_part: '/js/components/header',
		videos_part: '/js/components/videos',
		test_part: '/js/components/test',
		review_part: '/js/components/review',
		books_part: '/js/components/books'

	}
});

requirejs(['jquery'], function ($) {

	// modal center
	function centerModal() {
	    $(this).css('display', 'block');
	    var $dialog  = $(this).find(".modal-dialog"),
	    offset       = ($(window).height() - $dialog.height()) / 2,
	    bottomMargin = parseInt($dialog.css('marginBottom'), 10);
	    if(offset < bottomMargin) offset = bottomMargin;
	    $dialog.css("margin-top", offset);
	}
	$(document).on('show.bs.modal', '.modal', centerModal);
	$(window).on("resize", function () {
		$('.modal:visible').each(centerModal);
	});

	/*======================================
	=            DETECT BROWSER            =
	======================================*/

	 // Get IE or Edge browser version
	 function detectBrowser() {
	  var ua = window.navigator.userAgent,
	   html = $('html');
	  if (ua.indexOf('MSIE ') > 0) {
	   html.addClass('msie msie' + parseInt(ua.substring(ua.indexOf('MSIE ') + 5, ua.indexOf('.', ua.indexOf('MSIE '))), 10));
	  };
	  if (ua.indexOf('Trident/') > 0) {
	   html.addClass('ie ie' + parseInt(ua.substring(ua.indexOf('rv:') + 3, ua.indexOf('.', ua.indexOf('rv:'))), 10));
	  };
	  var edge = ua.indexOf('Edge/');
	  if (edge > 0) {
	   html.addClass('edge');
	  };
	  if (ua.toLowerCase().indexOf('safari') != -1) { 
	   if (ua.toLowerCase().indexOf('chrome') > -1) {
	    html.addClass('chrome');
	   } else {
	    html.addClass('safari');
	   };
	  };
	 };
	 detectBrowser();

	/*=====  End of DETECT BROWSER  ======*/

	require(['modernizr'], function () {
		/*==================================
		=            object fit            =
		==================================*/		
		if ($('.ofit-block').length > 0 && !Modernizr.objectfit) {
		    $('.ofit-block').each(function () {
		    var src = $(this).find('> img').attr('src');
		    $(this).css('background-image','url(' + src + ')');
		    })
		}
		/*=====  End of object fit  ======*/
	});

	

	/*========================================
	=            footer on bottom            =
	========================================*/
	var footerOnBottom = function () {
	  var wh = $(window).innerHeight();
	  var fh = $('.footer').outerHeight();
	  $('.content-without-footer').css('min-height', wh-fh);
	}
	$(window).on('load', function () {
	  footerOnBottom();
	})
	$(document).ready(function () {
	  footerOnBottom();
	})
	$(window).on('resize', function () {
	  footerOnBottom();
	})
	/*=====  End of footer on bottom  ======*/

	/*===============================
	=            sliders            =
	===============================*/
	
	if ($('.slick-slider').length > 0) {
		require(['slick_js'], function () {
			if ($('.mtslider').length > 0) {
				require(['mainTopSlider']);
			}

			if ($('.mnews__slider').length > 0) {
				require(['mnewsSlider']);
			}

			if ($('.office').length > 0) {
				require(['officeSlider']);
			}
		});
	}
	
	/*=====  End of sliders  ======*/


	/*=============================
	=            parts            =
	=============================*/
	
	if ($('header.header').length > 0) {
		require(['header_part']);
	}

	if ($('.videos__item__play').length > 0) {
		require(['bs_modal', 'bs_transition'], function () {
			require(['videos_part']);
		});
	}

	if ($('.test-wrap').length > 0) {
		require(['validate_js'], function () {
			jQuery.validator.addMethod("emailfull", function(value, element) {
			  return this.optional(element) || /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i.test(value);
			}, "Please enter valid email address!");

			require(['test_part']);
		});		
	}

	if ($('.reviews').length > 0) {
		require(['bs_modal', 'bs_transition'], function () {
			require(['review_part']);
		});
	}

	if ($('.books').length > 0) {
		require(['bs_modal', 'bs_transition'], function () {
			require(['books_part']);
		});
	}

	if ($('.modal').length > 0) {
		require(['bs_modal', 'bs_transition']);
	}

	/*=====  End of parts  ======*/
	
	/*====================================
	=            selectpicker            =
	====================================*/
	
	if ($('.selectpicker').length > 0) {
		require(['bs_dropdown', 'bsselect_js'], function () {
			$('.selectpicker').selectpicker();
		});
	}
	
	/*=====  End of selectpicker  ======*/

	/*==================================
	=            input mask            =
	==================================*/
	
	if ($('.masked-input').length > 0) {
		require(['mask_js'], function () {
			$('.masked-input').each(function () {
				var th = $(this),
					m = th.attr('data-mask');
				th.mask(m);
			})
		});
	}
	
	/*=====  End of input mask  ======*/
	
	
});