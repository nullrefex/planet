// меню на планшете
$('.header__sm-menu-trigger').on('click', function(e) {
	$('header.header').toggleClass('__menu-open');
	e.preventDefault();
});
// меню на мобильном
$('.header__xs-menu-trigger').on('click', function(e) {
	$('header.header').toggleClass('__menu-open');
	e.preventDefault();
});