$('.books__list__it__readall a').on('click', function(e) {
	var th = $(this),
		par = th.closest('.books__list__it'),
		title = par.find('.books__list__it__title').html(),
		txt = par.find('.books__list__it__full').html();

	$('.book-modal__title').html(title);
	$('.book-modal__txt').html(txt);

	$('#fullBook').modal('show');

	e.preventDefault();
});

$('#fullBook').on('hidden.bs.modal', function (e) {
	$('.book-modal__title').empty();
	$('.book-modal__txt').empty();
})

$(window).on('resize', function() {
	if ($(window).innerWidth() < 768) {
		$('#fullBook').modal('hide');
	}
});