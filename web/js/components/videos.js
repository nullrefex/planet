$('.videos__item__play').on('click', function(e) {
	var th = $(this),
		video = th.attr('data-video'),
		videoHTML = '<iframe src="' + video + '?autoplay=1" frameborder="0" allowfullscreen></iframe>'

	$('.video-modal__video-wrap').append(videoHTML);

	$('#videoModal').modal('show');
	
	e.preventDefault();
});


$('#videoModal').on('hidden.bs.modal', function (e) {
  $('.video-modal__video-wrap').empty();
})