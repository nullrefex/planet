// слайдер сверху на главной
var slider = $('.mtslider');

slider.slick({
	dots: true,
	infinite: true,
	speed: 500,
	slidesToShow: 1,
	slidesToScroll: 1,
	prevArrow: '',
	nextArrow: '',
	edgeFriction: 0,
	adaptiveHeight: true,
	autoplay: true,
	autoplaySpeed: 4000,
})