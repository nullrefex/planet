// слайдер новостей на главной
var slider = $('.mnews__slider');

slider.slick({
	dots: true,
	infinite: true,
	speed: 500,
	slidesToShow: 3,
	slidesToScroll: 3,
	prevArrow: '',
	nextArrow: '',
	edgeFriction: 0,
	adaptiveHeight: true,
	responsive: [
		{
			breakpoint: 992,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2,
			}
		},
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
			}
		}
	]
})