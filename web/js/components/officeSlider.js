// слайдер с фото оффиса
var slider = $('.office__top-slider'),
	sliderPrevBtn = slider.siblings('.office__nav-slider__btn.__prev'),
	sliderNextBtn = slider.siblings('.office__nav-slider__btn.__next');
	sliderNav = $('.office__nav-slider'),
	navPrevBtn = sliderNav.siblings('.office__nav-slider__btn.__prev'),
	navNextBtn = sliderNav.siblings('.office__nav-slider__btn.__next');

slider.slick({
	dots: false,
	infinite: true,
	speed: 500,
	slidesToShow: 1,
	slidesToScroll: 1,
	prevArrow: sliderPrevBtn,
	nextArrow: sliderNextBtn,
	edgeFriction: 0,
	adaptiveHeight: true,
	autoplay: true,
	autoplaySpeed: 4000,
	asNavFor: sliderNav,
	responsive: [
		{
			breakpoint: 992,
			settings: {
				dots: true,
			}
		}
	]
})

sliderNav.slick({
	dots: false,
	infinite: true,
	speed: 500,
	slidesToShow: 4,
	slidesToScroll: 1,
	prevArrow: navPrevBtn,
	nextArrow: navNextBtn,
	edgeFriction: 0,
	asNavFor: slider,
	focusOnSelect: true,
})