var userObj = {};

var testWrapHeight = function () {
	var wh = $(window).innerHeight(),
		hh = $('header.header').outerHeight(),
		fh = $('footer.footer').outerHeight();

	$('.test').css('min-height', wh - hh - fh);
	$('.test__reg').css('min-height', wh - hh - fh);
	$('.test__done').css('min-height', wh - hh - fh);
}

testWrapHeight();

$(window).on('resize', function() {
	testWrapHeight();
});

var createQuestionHTML = function (name, title, answers) {
	var questionHTML = '',
		answersHTML = '';

	questionHTML += '<div class="test__question">';
	questionHTML +=     '<div class="test__question__title"><span>' + title + '</span></div>';
	questionHTML +=     '<div class="test__question__answers">';

	$.each(answers, function(index, el) {	
		answersHTML +=       '<div class="test__question__answers__it">';
		answersHTML +=         '<label>';
		answersHTML +=           '<input type="radio" name="' + name + '" value="' + el.value + '" class="hidden-input"><span class="check-icon-w">';
		answersHTML +=             '<svg class="svg-icon check-icon">';
		answersHTML +=               '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#check-icon"></use>';
		answersHTML +=             '</svg></span><span class="check-txt">' + el.text + '</span>';
		answersHTML +=         '</label>';
		answersHTML +=       '</div>';
	});

	questionHTML += answersHTML;

	questionHTML +=     '</div>';
	questionHTML += '</div>';

	return questionHTML;
}

var totalQuestions,
	currentQuestion = 1,
	questionsArr;

(function (questions) {
	questionsArr = questions;
	totalQuestions = questionsArr.length;

	$('.test__progress__txt span').text(currentQuestion + ' / ' + totalQuestions);
	$('.test__progress__line__inn').css('width', currentQuestion/totalQuestions*100 + '%');

	var firstQuestion = createQuestionHTML(questionsArr[currentQuestion - 1].name, questionsArr[currentQuestion - 1].title, questionsArr[currentQuestion - 1].answers);

	$('.test__question-wrap').append(firstQuestion);
})(extractQuestions());

function extractQuestions() {
    var list = [];
    $(".testengine_block_4 .testengine_table tr").each(function (index, el) {
        list.push({
            name: "testengine_q" + (index + 1),
            title: $(el).find(".testengine_td_question").text(),
            answers: [
                {
                    "value": 1,
                    "text": "<b>Да</b> или <b>скорее всего да</b>"
                },
                {
                    "value": 2,
                    "text": "<b>Может быть</b> или <b>неуверен</b>"
                },
                {
                    "value": 3,
                    "text": "<b>Нет</b> или <b>скорее всего нет</b>"
                }
            ]
        });
    });
    return list;
}

function updateOldForm(userObj) {
    $('#testengine_birth_y').html($('[name="byear"]').html());
    $('#testengine_birth_d').val(parseInt(userObj.dateofbirth.day));
    $('#testengine_birth_m').val(parseInt(userObj.dateofbirth.month));
    $('#testengine_birth_y').val(userObj.dateofbirth.year);
    $((userObj.gender === 'male') ? '#testengine_sex_m' : '#testengine_sex_f').attr('checked', true);
    $('#testengine_last_name').val(userObj.surname);
    $('#testengine_first_name').val(userObj.name);
    $('#testengine_email').val(userObj.email);
    $('#testengine_phone').val(userObj.tel);
    $('#testengine_address').val(userObj.city);
    $('[name="testengine_extra"]').val(userObj.from);
}

$('#testRegForm').validate({
	errorClass:'error-input',
	validClass:'success',
	highlight: function (element, errorClass, validClass) { 
		$(element).closest('.test__reg__col__inwrap').addClass(errorClass).removeClass(validClass); 
	}, 
	unhighlight: function (element, errorClass, validClass) { 
		$(element).closest('.test__reg__col__inwrap').removeClass(errorClass).addClass(validClass); 
	},
	errorPlacement: function(error, element) { },
	submitHandler: function(form) {
		userObj.name = $('#testRegForm').find('[name="name"]').val();
		userObj.surname = $('#testRegForm').find('[name="surname"]').val();
		userObj.dateofbirth = {};
		userObj.dateofbirth.day = $('#testRegForm').find('[name="bday"]').val();
		userObj.dateofbirth.month = $('#testRegForm').find('[name="bmonth"]').val();
		userObj.dateofbirth.year = $('#testRegForm').find('[name="byear"]').val();
		userObj.email = $('#testRegForm').find('[name="email"]').val();
        userObj.gender = $('#testRegForm').find('[name="gender"]:checked').val();
		userObj.tel = $('#testRegForm').find('[name="tel"]').val();
		userObj.city = $('#testRegForm').find('[name="city"]').val();
		userObj.from = $('#testRegForm').find('[name="from"]').val();
		userObj.answers = [];

        updateOldForm(userObj);

		$('.test__question-wrap').find('.test__question').addClass('active');
		$('.test__reg').removeClass('active');
		$('.test').addClass('active');

		$('html,body').animate({
		   scrollTop: $(".test-wrap").offset().top
		});

	},
	rules: {
		bday: {
			required: true
		},
		bmonth: {
			required: true
		},
		byear: {
			required: true
		},
		name: {
			required: true
		},
		surname: {
			required: true
		},
		email: {
			required: true,
			emailfull: true
		},
		tel: {
			required: true
		},
		city: {
			required: true
		},
        from: {
			required: true
		}
	}
});

$('.selectpicker').on('change', function () {
	$(this).blur();
})

$('.test__done .btn.fw-btn').on('click', function (e) {
    e.preventDefault(e);
    location.reload();
    return false;
});

$('#debug').on('click', function (e) {
    e.preventDefault(e);
    jQuery('#testengine').show();
    return false;
});

$('.test__next-btn button').on('click', function() {

	if ($('.test__question-wrap').find('.test__question.active').find('input[type="radio"]:checked').length == 0) {
		$('.test__question-wrap').find('.test__question.active').find('.test__question__answers').addClass('shake animated');
		setTimeout(function () {
			$('.test__question-wrap').find('.test__question.active').find('.test__question__answers').removeClass('shake animated');
		}, 1000);
	} else {
        var checkbox;
		if (currentQuestion < totalQuestions) {
			currentQuestion = currentQuestion + 1;

			$('html,body').animate({
			   scrollTop: $(".test-wrap").offset().top
			});

			if (currentQuestion == totalQuestions) {
				$('.test__next-btn button').text('Завершить тест');
			}

			$('.test__progress__txt span').text(currentQuestion + ' / ' + totalQuestions);
			$('.test__progress__line__inn').css('width', currentQuestion/totalQuestions*100 + '%');

			var nextQuestion = createQuestionHTML(questionsArr[currentQuestion - 1].name, questionsArr[currentQuestion - 1].title, questionsArr[currentQuestion - 1].answers);

			$('.test__question-wrap').append(nextQuestion);

			userObj.answers.push($('.test__question-wrap').find('.test__question.active').find('input[type="radio"]:checked').val());

            checkbox = $('.test__question-wrap').find('.test__question.active').find('input[type="radio"]:checked');
            $('#' + checkbox.attr('name') + '_' + checkbox.val()).prop('checked', true);
	
			setTimeout(function () {
				$('.test__question-wrap').find('.test__question').removeClass('active');
				$('.test__question-wrap').find('.test__question').last().addClass('active');
			}, 100);

			setTimeout(function () {
				$('.test__question-wrap').find('.test__question').not('.active').remove();
			}, 500);

		} else {
			userObj.answers.push($('.test__question-wrap').find('.test__question.active').find('input[type="radio"]:checked').val());

			checkbox = $('.test__question-wrap').find('.test__question.active').find('input[type="radio"]:checked');
            $('#' + checkbox.attr('name') + '_' + checkbox.val()).prop('checked', true);

			console.log(userObj);

            var oldTestTable = jQuery('.testengine_block_4 .testengine_table');
            var oldTestRows = oldTestTable.find('tr');

            for (var i = 0; i < userObj.answers.length; i++) {
                var answer = userObj.answers[i];
                oldTestRows.eq(i).find('input[value=' + answer + ']').attr('checked', true);
            }

			$('html,body').animate({
			   scrollTop: $(".test-wrap").offset().top
			});

			$('.test').removeClass('active');
			$('.test__done').addClass('active');

            $('#testengine_consent').attr('checked',true);

            Get_Test_Result();
		}

	}


});