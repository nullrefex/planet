$('.reviews__item__txt .__read-more').on('click', function(e) {
	var th = $(this),
		par = th.closest('.reviews__item'),
		txt1 = par.find('.reviews__item__onimg').html(),
		txt2 = par.find('.reviews__item__full-text').html(),
		author = par.find('.reviews__item__author').html();

	$('.review-modal__txt1').html(txt1);
	$('.review-modal__txt2').html(txt2);
	$('.review-modal__author').html(author);

	$('#fullReview').modal('show');

	e.preventDefault();
});

$('#fullReview').on('hidden.bs.modal', function (e) {
	$('.review-modal__txt1').empty();
	$('.review-modal__txt2').empty();
	$('.review-modal__author').empty();
})

$(window).on('resize', function() {
	if ($(window).innerWidth() < 992) {
		$('#fullReview').modal('hide');
	}
});