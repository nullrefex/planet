$.fn.jRMenuMore = function(action, btnElementWidth) {
	if (action == 'init') {
		$(this).each(function(){
			$(this).addClass("rj-menu");    
			alignMenu(this);
			var robj=this;
			function alignMenu(obj) {
				var w = 0;
				var mw = $(obj).width() - btnElementWidth;
				var i = -1;
				var menuhtml = '';
				jQuery.each($(obj).children(), function() {
					i++;
					w += $(this).outerWidth(true);
					if (mw < w) {
						menuhtml += $('<div>').append($(this).clone()).html();
						$(this).remove();
					}
				});

				$(obj).append( '<li href="#" class="hideshow">' + '<a href="#" class="hideshow-btn">' +
					'<span>Еще</span>'+
					'</a>' +'<ul class="reset-list">'+ menuhtml + '</ul></li>');

				$(obj).children("li.hideshow ul").css("top",
						$(obj).children("li.hideshow").outerHeight(true) + "px");
				$(obj).children("li.hideshow").click(function(e) {
					$(this).toggleClass('active');
					e.preventDefault();
				});
			}
		});
	}
	if (action == 'destroy') {
		$(this).each(function(){
			$(this).removeClass("rj-menu");
			$(this).append($(this).find('.hideshow ul li').clone());
			$(this).find('.hideshow').remove();
		});
	}
}

$(document).click(function(event) {
    if ($(event.target).closest(".rj-menu .hideshow").length) return;
    $(".rj-menu .hideshow").removeClass('active');
});