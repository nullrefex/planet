<?php

namespace app\migrations;

use nullref\cms\models\Block;
use yii\db\Migration;

class M170711123901_CMS__add_main_slider extends Migration
{
    public function safeUp()
    {
        /** @var Block $existBlock */

        $existBlock = Block::findOne(['id' => 'main-slider-ru']);
        if ($existBlock) {
            $oldId = 'old_' . $existBlock->id;
            $existBlock->id = $oldId;
            $existBlock->save();
        }
        $this->insert(Block::tableName(), [
            'id' => 'main-slider-ru',
            'class_name' => 'main_slider',
            'name' => 'Слайдер на главной (RU)',
            'visibility' => '1',
            'config' => serialize([
                "items" => [
                    [
                        "title" => "Группа «Планета»",
                        "text" => "Это команда профессионалов-одиторов, специализирующихся на предоставлении владельцам и ТОП-менеджерам компаний помощи в области душевного здоровья, самосовершенствования и личностной мотивации",
                        "image" => "/img/content/mtslider/slide1.jpg"
                    ],
                    [
                        "title" => "Миссия одиторской группы «Планета»:",
                        "text" => "Усиление владельцев и руководителей посредством увеличения объема жизненной силы",
                        "image" => "/img/content/mtslider/slide2.jpg"
                    ],
                    [
                        "title" => "Миссия одиторской группы «Планета»:",
                        "text" => "Усиление владельцев и руководителей посредством увеличения объема жизненной силы",
                        "image" => "/img/content/mtslider/slide3.jpg"
                    ]
                ],
                "id" => NULL
            ]),
            'created_at' => 1501603103,
            'updated_at' => 1501603103,
        ]);

        $existBlock = Block::findOne(['id' => 'main-slider-en']);
        if ($existBlock) {
            $oldId = 'old_' . $existBlock->id;
            $existBlock->id = $oldId;
            $existBlock->save();
        }
        $this->insert(Block::tableName(), [
            'id' => 'main-slider-en',
            'class_name' => 'main_slider',
            'name' => 'Слайдер на главной (EN)',
            'visibility' => '1',
            'config' => serialize([
                "items" => [
                    [
                        "title" => "Группа «Планета»",
                        "text" => "Это команда профессионалов-одиторов, специализирующихся на предоставлении владельцам и ТОП-менеджерам компаний помощи в области душевного здоровья, самосовершенствования и личностной мотивации",
                        "image" => "/img/content/mtslider/slide1.jpg"
                    ],
                    [
                        "title" => "Миссия одиторской группы «Планета»:",
                        "text" => "Усиление владельцев и руководителей посредством увеличения объема жизненной силы",
                        "image" => "/img/content/mtslider/slide2.jpg"
                    ],
                    [
                        "title" => "Миссия одиторской группы «Планета»:",
                        "text" => "Усиление владельцев и руководителей посредством увеличения объема жизненной силы",
                        "image" => "/img/content/mtslider/slide3.jpg"
                    ]
                ],
                "id" => NULL
            ]),
            'created_at' => 1501603103,
            'updated_at' => 1501603103,
        ]);


    }

    public function safeDown()
    {
        /** @var Block $oldBlock */

        $this->delete(Block::tableName(), ['id' => 'main-slider-ru']);

        $oldBlock = Block::findOne(['id' => 'old_main-slider-ru']);
        if ($oldBlock) {
            $oldBlock->id = 'main-slider-ru';
            $oldBlock->save();
        }


        $this->delete(Block::tableName(), ['id' => 'main-slider-en']);

        $oldBlock = Block::findOne(['id' => 'old_main-slider-en']);
        if ($oldBlock) {
            $oldBlock->id = 'main-slider-en';
            $oldBlock->save();
        }


        return true;
    }
}
