<?php

namespace app\migrations;

use nullref\cms\models\Block;
use yii\db\Migration;

class M170802090300_CMS_add_blocks extends Migration
{
    public function safeUp()
    {
        /** @var Block $existBlock */

        $existBlock = Block::findOne(['id' => 'footer-contacts']);
        if ($existBlock) {
            $oldId = 'old_' . $existBlock->id;
            $existBlock->id = $oldId;
            $existBlock->save();
        }
        $this->insert(Block::tableName(), [
            'id' => 'footer-contacts',
            'class_name' => 'html',
            'name' => 'Контакты в футтере',
            'visibility' => '1',
            'config' => serialize([
                "content" => "<div class=\"footer__info__mail\"><svg class=\"svg-icon mail-icon\"> <use xlink:href=\"#mail-icon\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"></use> </svg>\r\n\r\n<div class=\"footer__info__mail__it\"><a href=\"mailto:planeta8dgroup@gmail.com\">planeta8dgroup@gmail.com</a></div>\r\n</div>\r\n\r\n<div class=\"footer__info__phones\"><svg class=\"svg-icon phone-icon\"> <use xlink:href=\"#phone-icon\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"></use> </svg>\r\n\r\n<div class=\"footer__info__phones__it\"><a href=\"tel:+38(099)2810257\">+38 (099) 281 02 57</a></div>\r\n\r\n<div class=\"footer__info__phones__it\"><a href=\"tel:+38(050)8037379\">+38 (050) 803 73 89</a></div>\r\n</div>\r\n",
                "tag" => "",
                "tagClass" => "",
                "id" => NULL
            ]),
            'created_at' => 1501664599,
            'updated_at' => 1501664599,
        ]);


        $existBlock = Block::findOne(['id' => 'leaders-en']);
        if ($existBlock) {
            $oldId = 'old_' . $existBlock->id;
            $existBlock->id = $oldId;
            $existBlock->save();
        }
        $this->insert(Block::tableName(), [
            'id' => 'leaders-en',
            'class_name' => 'html',
            'name' => 'Руководители (EN)',
            'visibility' => '1',
            'config' => serialize([
                "content" => "<div class=\"mleaders__it\">\r\n    <div class=\"container\">\r\n        <div class=\"mleaders__it__top\">\r\n            <div class=\"mleaders__it__top__img\">\r\n                <div class=\"mleaders__it__top__img__round\"></div>\r\n                <img alt=\"\" src=\"/img/content/mleaders/1.png\" />\r\n            </div>\r\n\r\n            <div class=\"mleaders__it__top__txt\">\r\n                <div class=\"mleaders__it__top__name\">\r\n                    <h3>Маргарита Сотникова</h3>\r\n                </div>\r\n\r\n                <div class=\"mleaders__it__top__prof\"><span>Руководитель группы «Планета», профессиональный одитор</span>\r\n                </div>\r\n\r\n                <div class=\"mleaders__it__top__desc\">\r\n                    <div class=\"mleaders__it__top__desc__inn\">\r\n                        <p>В жизни каждого человека бывают моменты, когда он нуждается в помощи. Стрессы, непонимание окружающих, проблемы в семье или на работе, потеря близких, депрессия — со всем этим человеку сложно справляться в одиночку. Одитор — это\r\n                            тот, кто может помочь вам преодолеть удары судьбы, восстановить утраченное душевное равновесие, вернуть жизненные силы.</p>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"mleaders__it __gray\">\r\n    <div class=\"container\">\r\n        <div class=\"mleaders__it__top\">\r\n            <div class=\"mleaders__it__top__img\">\r\n                <div class=\"mleaders__it__top__img__round\"></div>\r\n                <img alt=\"\" src=\"/img/content/mleaders/2.png\" />\r\n            </div>\r\n\r\n            <div class=\"mleaders__it__top__txt\">\r\n                <div class=\"mleaders__it__top__name\">\r\n                    <h3>Евгений Сотников</h3>\r\n                </div>\r\n\r\n                <div class=\"mleaders__it__top__prof\"><span>Бизнесмен, профессиональный бизнес-консультант, одитор группы</span>\r\n                </div>\r\n\r\n                <div class=\"mleaders__it__top__desc\">\r\n                    <div class=\"mleaders__it__top__desc__inn\">\r\n                        <p>Занимаясь реабилитацией компаний в периоды финансовых кризисов, я пришел к выводу, что для спасения бизнеса недостаточно просто навести в нем порядок: оптимизировать бизнес-процессы, внедрить структуру, взять под контроль финансы.\r\n                            Все это, несомненно, дает результат, но через время наступает очередной кризис, и рецидив повторяется. Все трудности в компании проистекают прежде всего от внутреннего состояния ее владельца, - создателя компании. Поэтому личная\r\n                            работа с владельцемпосредством одитинга – специального вида личностных консультаций, направленных на переосмысление жизни и устранение накопленной в его разуме боли, вызванной неудачами и потерями, является наиболее приоритетной.</p>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"mleaders__it__bottom\">\r\n            <p>В ходе одитинга мы возвращаем человеку его изначальную жизненную энергию, возрождаем способность противостоять чему бы то ни было в жизни и бизнесе. Мы останавливаем нисходящую спираль ухудшения его внутреннего состояния. Мы приводим его в\r\n                то состояние, в котором он способен контролировать и принимать оптимальные решения в больших масштабах, а значит способен привести к процветанию большую компанию, нежели та, с которой он имеет дело сегодня.</p>\r\n        </div>\r\n    </div>\r\n</div>",
                "tag" => "div",
                "tagClass" => "mleaders",
                "id" => NULL
            ]),
            'created_at' => 1501664600,
            'updated_at' => 1501664600,
        ]);


        $existBlock = Block::findOne(['id' => 'leaders-ru']);
        if ($existBlock) {
            $oldId = 'old_' . $existBlock->id;
            $existBlock->id = $oldId;
            $existBlock->save();
        }
        $this->insert(Block::tableName(), [
            'id' => 'leaders-ru',
            'class_name' => 'html',
            'name' => 'Руководители (RU)',
            'visibility' => '1',
            'config' => serialize([
                "content" => "<div class=\"mleaders__it\">\r\n    <div class=\"container\">\r\n        <div class=\"mleaders__it__top\">\r\n            <div class=\"mleaders__it__top__img\">\r\n                <div class=\"mleaders__it__top__img__round\"></div>\r\n                <img alt=\"\" src=\"/img/content/mleaders/1.png\" />\r\n            </div>\r\n\r\n            <div class=\"mleaders__it__top__txt\">\r\n                <div class=\"mleaders__it__top__name\">\r\n                    <h3>Маргарита Сотникова</h3>\r\n                </div>\r\n\r\n                <div class=\"mleaders__it__top__prof\"><span>Руководитель группы «Планета», профессиональный одитор</span>\r\n                </div>\r\n\r\n                <div class=\"mleaders__it__top__desc\">\r\n                    <div class=\"mleaders__it__top__desc__inn\">\r\n                        <p>В жизни каждого человека бывают моменты, когда он нуждается в помощи. Стрессы, непонимание окружающих, проблемы в семье или на работе, потеря близких, депрессия — со всем этим человеку сложно справляться в одиночку. Одитор — это\r\n                            тот, кто может помочь вам преодолеть удары судьбы, восстановить утраченное душевное равновесие, вернуть жизненные силы.</p>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"mleaders__it __gray\">\r\n    <div class=\"container\">\r\n        <div class=\"mleaders__it__top\">\r\n            <div class=\"mleaders__it__top__img\">\r\n                <div class=\"mleaders__it__top__img__round\"></div>\r\n                <img alt=\"\" src=\"/img/content/mleaders/2.png\" />\r\n            </div>\r\n\r\n            <div class=\"mleaders__it__top__txt\">\r\n                <div class=\"mleaders__it__top__name\">\r\n                    <h3>Евгений Сотников</h3>\r\n                </div>\r\n\r\n                <div class=\"mleaders__it__top__prof\"><span>Бизнесмен, профессиональный бизнес-консультант, одитор группы</span>\r\n                </div>\r\n\r\n                <div class=\"mleaders__it__top__desc\">\r\n                    <div class=\"mleaders__it__top__desc__inn\">\r\n                        <p>Занимаясь реабилитацией компаний в периоды финансовых кризисов, я пришел к выводу, что для спасения бизнеса недостаточно просто навести в нем порядок: оптимизировать бизнес-процессы, внедрить структуру, взять под контроль финансы.\r\n                            Все это, несомненно, дает результат, но через время наступает очередной кризис, и рецидив повторяется. Все трудности в компании проистекают прежде всего от внутреннего состояния ее владельца, - создателя компании. Поэтому личная\r\n                            работа с владельцемпосредством одитинга – специального вида личностных консультаций, направленных на переосмысление жизни и устранение накопленной в его разуме боли, вызванной неудачами и потерями, является наиболее приоритетной.</p>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"mleaders__it__bottom\">\r\n            <p>В ходе одитинга мы возвращаем человеку его изначальную жизненную энергию, возрождаем способность противостоять чему бы то ни было в жизни и бизнесе. Мы останавливаем нисходящую спираль ухудшения его внутреннего состояния. Мы приводим его в\r\n                то состояние, в котором он способен контролировать и принимать оптимальные решения в больших масштабах, а значит способен привести к процветанию большую компанию, нежели та, с которой он имеет дело сегодня.</p>\r\n        </div>\r\n    </div>\r\n</div>",
                "tag" => "div",
                "tagClass" => "mleaders",
                "id" => NULL
            ]),
            'created_at' => 1501664602,
            'updated_at' => 1501664602,
        ]);


        $existBlock = Block::findOne(['id' => 'main-service-en']);
        if ($existBlock) {
            $oldId = 'old_' . $existBlock->id;
            $existBlock->id = $oldId;
            $existBlock->save();
        }
        $this->insert(Block::tableName(), [
            'id' => 'main-service-en',
            'class_name' => 'html',
            'name' => 'Услуга (на главной) (EN)',
            'visibility' => '1',
            'config' => serialize([
                "content" => "<div class=\"container\">\r\n<div class=\"mrelife__mob-img hidden-lg hidden-md\">\r\n<div class=\"mrelife__mob-img__img ofit-block\"><img alt=\"\" src=\"/img/content/mrelife/img.jpg\" /></div>\r\n\r\n<div class=\"mrelife__mob-img__cnt\">\r\n<div class=\"mrelife__cnt__left__title\"><span>Ремонт жизни</span></div>\r\n\r\n<div class=\"mrelife__cnt__left__btn\"><a class=\"btn btn-yellow fw-btn hidden-xs\" href=\"#\">Узнать больше ›</a> <a class=\"mrelife__cnt__mob-link hidden-lg hidden-md hidden-sm\" href=\"#\"> <i>Узнать больше</i> › </a></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"mrelife__cnt\">\r\n<div class=\"mrelife__cnt__img ofit-block hidden-sm hidden-xs\"><img alt=\"\" src=\"/img/content/mrelife/img.jpg\" /></div>\r\n\r\n<div class=\"mrelife__cnt__inn\">\r\n<div class=\"mrelife__cnt__left hidden-sm hidden-xs\">\r\n<div class=\"mrelife__cnt__left__title\"><span>Ремонт жизни</span></div>\r\n\r\n<div class=\"mrelife__cnt__left__btn\"><a class=\"btn btn-yellow fw-btn\" href=\"#\">Узнать больше ›</a></div>\r\n</div>\r\n\r\n<div class=\"mrelife__cnt__right\">\r\n<div class=\"mrelife__cnt__right__title\"><span>Наша цель - восстановить и увеличить жизненную энергию владельца (ТОП-менеджера), помочь ему вывести компанию на новый уровень развития. </span></div>\r\n\r\n<div class=\"mrelife__cnt__right__txt\">\r\n<p>Владельцы и ТОП-менеджеры компаний проходят программы индивидуальных консультаций по восстановлению жизненной силы и увеличению личного потенциала, называющиеся одитингом. Шаг за шагом они возвращают утраченную жизненную энергию, восстанавливают способности, уверенность в себе и готовностьсмотреть в глаза трудностям. Другими словами, возвращают способность играть в игры под названием «жизнь» и «бизнес», получая от этого удовольствие.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n",
                "tag" => "div",
                "tagClass" => "mrelife",
                "id" => NULL
            ]),
            'created_at' => 1501664605,
            'updated_at' => 1501664605,
        ]);


        $existBlock = Block::findOne(['id' => 'main-service-ru']);
        if ($existBlock) {
            $oldId = 'old_' . $existBlock->id;
            $existBlock->id = $oldId;
            $existBlock->save();
        }
        $this->insert(Block::tableName(), [
            'id' => 'main-service-ru',
            'class_name' => 'html',
            'name' => 'Услуга (на главной) (RU)',
            'visibility' => '1',
            'config' => serialize([
                "content" => "<div class=\"container\">\r\n<div class=\"mrelife__mob-img hidden-lg hidden-md\">\r\n<div class=\"mrelife__mob-img__img ofit-block\"><img alt=\"\" src=\"/img/content/mrelife/img.jpg\" /></div>\r\n\r\n<div class=\"mrelife__mob-img__cnt\">\r\n<div class=\"mrelife__cnt__left__title\"><span>Ремонт жизни</span></div>\r\n\r\n<div class=\"mrelife__cnt__left__btn\"><a class=\"btn btn-yellow fw-btn hidden-xs\" href=\"#\">Узнать больше ›</a> <a class=\"mrelife__cnt__mob-link hidden-lg hidden-md hidden-sm\" href=\"#\"> <i>Узнать больше</i> › </a></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"mrelife__cnt\">\r\n<div class=\"mrelife__cnt__img ofit-block hidden-sm hidden-xs\"><img alt=\"\" src=\"/img/content/mrelife/img.jpg\" /></div>\r\n\r\n<div class=\"mrelife__cnt__inn\">\r\n<div class=\"mrelife__cnt__left hidden-sm hidden-xs\">\r\n<div class=\"mrelife__cnt__left__title\"><span>Ремонт жизни</span></div>\r\n\r\n<div class=\"mrelife__cnt__left__btn\"><a class=\"btn btn-yellow fw-btn\" href=\"#\">Узнать больше ›</a></div>\r\n</div>\r\n\r\n<div class=\"mrelife__cnt__right\">\r\n<div class=\"mrelife__cnt__right__title\"><span>Наша цель - восстановить и увеличить жизненную энергию владельца (ТОП-менеджера), помочь ему вывести компанию на новый уровень развития. </span></div>\r\n\r\n<div class=\"mrelife__cnt__right__txt\">\r\n<p>Владельцы и ТОП-менеджеры компаний проходят программы индивидуальных консультаций по восстановлению жизненной силы и увеличению личного потенциала, называющиеся одитингом. Шаг за шагом они возвращают утраченную жизненную энергию, восстанавливают способности, уверенность в себе и готовностьсмотреть в глаза трудностям. Другими словами, возвращают способность играть в игры под названием «жизнь» и «бизнес», получая от этого удовольствие.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n",
                "tag" => "div",
                "tagClass" => "mrelife",
                "id" => NULL
            ]),
            'created_at' => 1501664609,
            'updated_at' => 1501664609,
        ]);


        $existBlock = Block::findOne(['id' => 'office-en']);
        if ($existBlock) {
            $oldId = 'old_' . $existBlock->id;
            $existBlock->id = $oldId;
            $existBlock->save();
        }
        $this->insert(Block::tableName(), [
            'id' => 'office-en',
            'class_name' => 'office',
            'name' => 'Наш офис (EN)',
            'visibility' => '1',
            'config' => serialize([
                "title" => "Our office",
                "subtitle" => "Мы находится в столице Украины - Киеве, одном из прекраснейших городов Европы",
                "items" => [
                    [
                        "order" => "",
                        "image" => "/img/content/services/s1.jpg"
                    ],
                    [
                        "order" => "",
                        "image" => "/img/content/services/s1.jpg"
                    ],
                    [
                        "order" => "",
                        "image" => "/img/content/services/s1.jpg"
                    ],
                    [
                        "order" => "",
                        "image" => "/img/content/services/s1.jpg"
                    ],
                    [
                        "order" => "",
                        "image" => "/img/content/services/s1.jpg"
                    ],
                    [
                        "order" => "",
                        "image" => "/img/content/services/s1.jpg"
                    ]
                ],
                "id" => NULL
            ]),
            'created_at' => 1501664614,
            'updated_at' => 1501664614,
        ]);


        $existBlock = Block::findOne(['id' => 'office-ru']);
        if ($existBlock) {
            $oldId = 'old_' . $existBlock->id;
            $existBlock->id = $oldId;
            $existBlock->save();
        }
        $this->insert(Block::tableName(), [
            'id' => 'office-ru',
            'class_name' => 'office',
            'name' => 'Наш офис (RU)',
            'visibility' => '1',
            'config' => serialize([
                "title" => "Наш офис",
                "subtitle" => "Мы находится в столице Украины - Киеве, одном из прекраснейших городов Европы",
                "items" => [
                    [
                        "order" => "",
                        "image" => "/img/content/services/s1.jpg"
                    ],
                    [
                        "order" => "",
                        "image" => "/img/content/services/s1.jpg"
                    ],
                    [
                        "order" => "",
                        "image" => "/img/content/services/s1.jpg"
                    ],
                    [
                        "order" => "",
                        "image" => "/img/content/services/s1.jpg"
                    ],
                    [
                        "order" => "",
                        "image" => "/img/content/services/s1.jpg"
                    ],
                    [
                        "order" => "",
                        "image" => "/img/content/services/s1.jpg"
                    ]
                ],
                "id" => NULL
            ]),
            'created_at' => 1501664620,
            'updated_at' => 1501664620,
        ]);


    }

    public function safeDown()
    {
        /** @var Block $oldBlock */

        $this->delete(Block::tableName(), ['id' => 'footer-contacts']);

        $oldBlock = Block::findOne(['id' => 'old_footer-contacts']);
        if ($oldBlock) {
            $oldBlock->id = 'footer-contacts';
            $oldBlock->save();
        }


        $this->delete(Block::tableName(), ['id' => 'leaders-en']);

        $oldBlock = Block::findOne(['id' => 'old_leaders-en']);
        if ($oldBlock) {
            $oldBlock->id = 'leaders-en';
            $oldBlock->save();
        }


        $this->delete(Block::tableName(), ['id' => 'leaders-ru']);

        $oldBlock = Block::findOne(['id' => 'old_leaders-ru']);
        if ($oldBlock) {
            $oldBlock->id = 'leaders-ru';
            $oldBlock->save();
        }


        $this->delete(Block::tableName(), ['id' => 'main-service-en']);

        $oldBlock = Block::findOne(['id' => 'old_main-service-en']);
        if ($oldBlock) {
            $oldBlock->id = 'main-service-en';
            $oldBlock->save();
        }


        $this->delete(Block::tableName(), ['id' => 'main-service-ru']);

        $oldBlock = Block::findOne(['id' => 'old_main-service-ru']);
        if ($oldBlock) {
            $oldBlock->id = 'main-service-ru';
            $oldBlock->save();
        }


        $this->delete(Block::tableName(), ['id' => 'office-en']);

        $oldBlock = Block::findOne(['id' => 'old_office-en']);
        if ($oldBlock) {
            $oldBlock->id = 'office-en';
            $oldBlock->save();
        }


        $this->delete(Block::tableName(), ['id' => 'office-ru']);

        $oldBlock = Block::findOne(['id' => 'old_office-ru']);
        if ($oldBlock) {
            $oldBlock->id = 'office-ru';
            $oldBlock->save();
        }


        return true;
    }
}
