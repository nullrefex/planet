<?php

namespace app\migrations;

use nullref\cms\models\Block;
use yii\db\Migration;

class M170711123900_CMS__add_main_menu extends Migration
{
    public function safeUp()
    {
        /** @var Block $existBlock */

        $existBlock = Block::findOne(['id' => 'main-menu']);
        if ($existBlock) {
            $oldId = 'old_' . $existBlock->id;
            $existBlock->id = $oldId;
            $existBlock->save();
        }
        $this->insert(Block::tableName(), [
            'id' => 'main-menu',
            'class_name' => 'menu',
            'name' => 'Главное меню',
            'visibility' => '1',
            'config' => serialize([
                "items" => [
                    [
                        "selected" => true,
                        "title" => "Главная",
                        "data" => [
                            "url" => "/"
                        ]
                    ],
                    [
                        "selected" => true,
                        "title" => "Услуги",
                        "data" => [
                            "url" => "/services"
                        ]
                    ],
                    [
                        "selected" => true,
                        "title" => "Тест ОСА",
                        "data" => [
                            "url" => "/test"
                        ]
                    ],
                    [
                        "selected" => true,
                        "title" => "Наша команда",
                        "data" => [
                            "url" => "/team"
                        ]
                    ],
                    [
                        "selected" => true,
                        "title" => "Отзывы",
                        "data" => [
                            "url" => "/reviews"
                        ]
                    ],
                    [
                        "selected" => true,
                        "title" => "Наши книги",
                        "data" => [
                            "url" => "/books"
                        ]
                    ],
                    [
                        "selected" => true,
                        "title" => "Статьи",
                        "data" => [
                            "url" => "/articles"
                        ]
                    ],
                    [
                        "selected" => true,
                        "title" => "Видео",
                        "data" => [
                            "url" => "/videos"
                        ]
                    ]
                ],
                "options" => [

                ],
                "id" => NULL
            ]),
            'created_at' => 1499776772,
            'updated_at' => 1499776772,
        ]);


    }

    public function safeDown()
    {
        /** @var Block $oldBlock */

        $this->delete(Block::tableName(), ['id' => 'main-menu']);

        $oldBlock = Block::findOne(['id' => 'old_main-menu']);
        if ($oldBlock) {
            $oldBlock->id = 'main-menu';
            $oldBlock->save();
        }


        return true;
    }
}
