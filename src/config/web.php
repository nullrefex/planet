<?php

$params = require(__DIR__ . '/params.php');
$modules = require(__DIR__ . '/modules.php');

$config = [
    'id' => 'app',
    'name' => 'Planeta',
    'language' => 'ru',
    'basePath' => dirname(__DIR__),
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'runtimePath' => dirname(dirname(__DIR__)) . '/runtime',
    'bootstrap' => [
        'log',
        'languageManager',
    ],
    'controllerMap' => [
        'elfinder-backend' => [
            'class' => 'mihaildev\elfinder\Controller',
            'user' => 'user',
            'access' => ['@'],
            'disabledCommands' => ['netmount'],
            'roots' => [
                [
                    'path' => 'uploads',
                    'name' => 'Uploads',
                    'options' => [
                        'attributes' => [
                            [
                                'pattern' => '/\.(?:gitignore)$/',
                                'read' => false,
                                'write' => false,
                                'hidden' => true,
                                'locked' => false
                            ],
                        ],
                    ],
                ],
                [
                    'path' => 'img',
                    'name' => 'Images',
                    'options' => [
                        'attributes' => [
                            [
                                'pattern' => '/\.(?:gitignore)$/',
                                'read' => false,
                                'write' => false,
                                'hidden' => true,
                                'locked' => false
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'components' => [
        'assetManager' => [
            'class' => \app\components\AssetManager::class,
            'bundles' => [
          /*      'mihaildev\elfinder\Assets' => [
                    'js' => [
                        '/vendors/elfinder/elfinder.full.js',
                    ],
                ],*/
                nullref\fulladmin\assets\AdminAsset::class => [
                    'js' => [
                        'js/admin/scripts.js',
                        'js/tools.js',
                    ],
                    'css' => [],
                ],
            ],
        ],
        'formatter' => [
            'class' => 'app\components\Formatter',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => YII_ENV_DEV,
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@nullref/fulladmin/views' => '@app/modules/admin/views',
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/team' => '/site/team',
                '/test' => '/site/test',
                '/articles' => '/site/articles',
                '/videos' => '/site/videos',
                '/books' => '/site/books',
                '/services' => '/site/services',
                '/service/<id>' => '/site/service',
                '/article/<id>' => '/site/article',
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'languageManager' => [
            'class' => 'app\components\LanguageManager',
            'languages' => [
                ['id' => 1, 'slug' => 'ru', 'title' => 'RU'],
                ['id' => 2, 'slug' => 'en', 'title' => 'EN'],
            ],
        ],
        'i18n' => [
            'translations' => [
                '*' => ['class' => 'yii\i18n\PhpMessageSource'],
                'admin' => ['class' => nullref\core\components\i18n\PhpMessageSource::className()],
                'user' => ['class' => nullref\core\components\i18n\PhpMessageSource::className()],
                'category' => ['class' => nullref\core\components\i18n\PhpMessageSource::className()],
            ],
        ],
        'request' => [
            'class' => 'app\components\Request',
            'baseUrl' => '',
            'cookieValidationKey' => 'RiAveGUdUACvWZppHVevMJRGd5Rij8uh',
        ],
    ],
    'modules' => $modules,
    'params' => $params,
    'on beforeAction' => function ($event) {
        if (Yii::$app->controller instanceof \nullref\core\interfaces\IAdminController) {
            Yii::$app->language = 'ru';
        }
    },
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
