<?php

$params = require(__DIR__ . '/params.php');
$modules = require(__DIR__ . '/modules.php');

Yii::setAlias('@webroot', dirname(dirname(__DIR__)) . '/web');

return [
    'id' => 'console-app',
    'basePath' => dirname(__DIR__),
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'runtimePath' => dirname(dirname(__DIR__)) . '/runtime',
    'bootstrap' => ['log', 'core'],
    'modules' => $modules,
    'params' => $params,
    'components' => [
        'db' => require(__DIR__ . '/db.php'),
        'languageManager' => [
            'class' => 'app\components\LanguageManager',
            'languageSession' => false,
            'languageCookie' => false,
            'languages' => [
                ['id' => 1, 'slug' => 'ru', 'title' => 'RU'],
                ['id' => 2, 'slug' => 'en', 'title' => 'EN'],
            ],
        ],
        'i18n' => [
            'translations' => [
                '*' => ['class' => 'yii\i18n\PhpMessageSource'],
                'admin' => ['class' => nullref\core\components\i18n\PhpMessageSource::className()],
                'user' => ['class' => nullref\core\components\i18n\PhpMessageSource::className()],
                'category' => ['class' => nullref\core\components\i18n\PhpMessageSource::className()],
            ],
        ],
    ],
];
