<?php

return \yii\helpers\ArrayHelper::merge(require(__DIR__ . '/installed_modules.php'), [
    'core' => [
        'class' => 'nullref\core\Module',
    ],
    'admin' => [
        'controllerNamespace' => '\app\modules\admin\controllers',
    ],
    'cms' => [
        'class' => 'nullref\cms\Module',
        'urlPrefix' => '',
        'components' => [
            'layoutManager' => [
                'class' => '\nullref\cms\components\PageLayoutManager',
                'list' => [
                    '@app/views/layouts/main' => 'Main',
                    '@app/views/layouts/article' => 'Article',
                ],
            ],
            'blockManager' => [
                'class' => 'app\components\BlockManager',
            ],
        ],
    ],
    'books' => [
        'class' => 'app\modules\books\Module',
    ],
    'services' => [
        'class' => 'app\modules\services\Module',
        'components' => [
            'blockManager' => [
                'class' => 'app\modules\services\components\BlockManager',
            ],
        ],
    ],
    'reviews' => [
        'class' => 'app\modules\reviews\Module',
    ],
    'team' => [
        'class' => 'app\modules\team\Module',
    ],
    'videos' => [
        'class' => 'app\modules\videos\Module',
    ],
    'articles' => [
        'class' => 'app\modules\articles\Module',
        'components' => [
            'blockManager' => [
                'class' => 'app\modules\articles\components\BlockManager',
            ],
        ],
    ],
]);