<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\widgets;


use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use yii\base\Widget;

class Fb extends Widget
{
    protected $items;

    public function init()
    {
        $items = \Yii::$app->cache->get('fb.posts');
        if (!$items) {
            $items = $this->getItems();
            \Yii::$app->cache->set('fb.posts', $items, 60 * 5);
        }
        $this->items = $items;
        parent::init();
    }

    public function getItems()
    {
        $items = [];
        $fb = new Facebook([
            'app_id' => '327597067689280',
            'app_secret' => '215c7ad4452f2ec8264c6c42f66a8bac',
            'default_graph_version' => 'v2.10',
            'default_access_token' => '327597067689280|sEUMUGfgIH15ouIQmf-Vw2Qbsz4',
        ]);

        try {
            $response = $fb->get('/1369537053164308/posts', '327597067689280|sEUMUGfgIH15ouIQmf-Vw2Qbsz4');

            $data = $response->getDecodedBody();
            foreach ($data['data'] as $datum) {
                $href = str_replace('_', '/posts/', $datum['id']);
                $items[] = '<div data-width="350" class="fb-post" data-href="https://www.facebook.com/' . $href . '"></div>';
            }

        } catch (FacebookResponseException $e) {
            //echo 'Graph returned an error: ' . $e->getMessage();
            //exit;
        } catch (FacebookSDKException $e) {
            //echo 'Facebook SDK returned an error: ' . $e->getMessage();
            //exit;
        }
        return $items;
    }

    public function run()
    {
        return $this->render('fb', [
            'items' => $this->items,
        ]);
    }


}