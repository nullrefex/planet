<?php

namespace app\widgets;

use app\helpers\Languages;
use Yii;
use yii\base\Widget;
use yii\helpers\Url;

class Language extends Widget
{
    private $_isError;
    public $languages = [];

    public function init()
    {
        $route = Yii::$app->controller->route;
        $params = $_GET;
        $this->_isError = $route === Yii::$app->errorHandler->errorAction;
        array_unshift($params, '/' . $route);
        $currentSlug = Languages::get()->getSlug();
        foreach (Languages::getSlugedMap() as $language) {
            $current = false;
            if ($language->getSlug() === $currentSlug) {
                $current = true;
            }
            $title = Languages::getShortNames()[$language->getSlug()];
            $this->languages[] = [
                'label' => $title,
                'url' => '/' . $language->getSlug() . Url::current(),
                'slug' => $language->getSlug(),
                'current' => $current
            ];
        }
        parent::init();
    }

    public function run()
    {
        if ($this->_isError) {
            return '';
        } else {
            return $this->render('language', [
                'languages' => $this->languages,
            ]);
        }
    }
}