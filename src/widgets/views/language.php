<div class="footer__info__lang clearfix">
    <?php foreach ($languages as $language): ?>
        <a href="<?= $language['url'] ?>"><?= $language['label'] ?>
            <div style="background-image: url(/img/template/<?= $language['slug'] ?>-flag.jpg);" class="__flag"></div>
        </a>
    <?php endforeach; ?>
</div>