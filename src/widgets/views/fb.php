<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $items array
 */
?>
<div class="mnews">
    <div class="container">
        <div class="mnews__title">
            <h2><span><?= Yii::t('app','News') ?></span></h2>
            <div class="mnews__title__btn"><a href="https://www.facebook.com/planeta8d" class="btn btn-blue fw-btn">
                    <svg class="svg-icon fb-icon hidden-sm hidden-xs">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#fb-icon"></use>
                    </svg>
                    <?= Yii::t('app','Follow us on Facebook') ?></a></div>
        </div>
        <div class="mnews__slider-wrap">
            <div class="mnews__slider slick-slider">
                <?php foreach ($items as $item): ?>
                    <div class="mnews__slider__slide">
                        <div class="mnews__slider__slide__inn">
                            <?= $item ?>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
