<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $this \yii\web\View
 *
 * @var $title string
 * @var $subtitle string
 */
?>

<div class="team__title __w-subtitle clearfix">
    <h1>
        <span><?= $title ?></span>
    </h1>
    <div class="team__title__subtitle">
        <span><?= $subtitle ?></span>
    </div>
</div>