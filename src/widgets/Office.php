<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\widgets;


use Yii;
use yii\base\Widget;

class Office extends Widget
{
    public $photos = [];

    public function run()
    {
        return Yii::$app->getModule('cms')->blockManager->getWidget('office-' . Yii::$app->language);
    }
}