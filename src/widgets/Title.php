<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\widgets;


use yii\base\Widget;

class Title extends Widget
{
    public $title;

    public $subtitle;

    public function run()
    {
        return $this->render('title', [
            'title' => $this->title,
            'subtitle' => $this->subtitle,
        ]);
    }
}