<?php

namespace app\blocks\main\relife;

use nullref\cms\components\Block as BaseBlock;
/**
* Class Block
*/
class Block extends BaseBlock
{
public $content;

public function getName()
{
return 'relife Block';
}

public function rules()
{
return [
[['content'],'required'],
];
}
}