<?php

namespace app\blocks\main\slider;

use nullref\cms\components\Widget as BaseWidget;


class Widget extends BaseWidget
{
    public $items = [];

    public function run()
    {
        return $this->render('slider', ['items' => $this->items]);
    }
}