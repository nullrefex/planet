<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $items array
 */
?>
<div class="mtslider-wrap">
    <div class="mtslider slick-slider">
        <?php foreach ($items as $item): ?>
            <div class="mtslider__slide">
                <div class="mtslider__slide__img ofit-block"><img src="<?= $item['image'] ?>" alt=""></div>
                <div class="container">
                    <div class="mtslider__slide__wrap">
                        <div class="mtslider__slide__title">
                            <h3><?= $item['title'] ?></h3>
                        </div>
                        <div class="mtslider__slide__txt">
                            <p><?= $item['text'] ?></p>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach ?>
        <!--<div class="mtslider__slide">
            <div class="mtslider__slide__img ofit-block"><img src="img/content/mtslider/slide1.jpg" alt=""></div>
            <div class="container">
                <div class="mtslider__slide__wrap">
                    <div class="mtslider__slide__title">
                        <h3>Группа «Планета»</h3>
                    </div>
                    <div class="mtslider__slide__txt">
                        <p>Это команда профессионалов-одиторов, специализирующихся на предоставлении владельцам и ТОП-менеджерам компаний помощи в области душевного здоровья, самосовершенствования и личностной мотивации</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="mtslider__slide">
            <div class="mtslider__slide__img ofit-block"><img src="img/content/mtslider/slide2.jpg" alt=""></div>
            <div class="container">
                <div class="mtslider__slide__wrap">
                    <div class="mtslider__slide__title">
                        <h3>Миссия одиторской группы «Планета»:</h3>
                    </div>
                    <div class="mtslider__slide__txt">
                        <p>Усиление владельцев и руководителей посредством увеличения объема жизненной силы</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="mtslider__slide">
            <div class="mtslider__slide__img ofit-block"><img src="img/content/mtslider/slide3.jpg" alt=""></div>
            <div class="container">
                <div class="mtslider__slide__wrap">
                    <div class="mtslider__slide__title">
                        <h3>Миссия одиторской группы «Планета»:</h3>
                    </div>
                    <div class="mtslider__slide__txt">
                        <p>Усиление владельцев и руководителей посредством увеличения объема жизненной силы</p>
                    </div>
                </div>
            </div>
        </div>-->
    </div>
</div>
