<?php

namespace app\blocks\main\slider;

use nullref\cms\components\Block as BaseBlock;

/**
 * Class Block
 * @package app\blocks\main\slider
 */
class Block extends BaseBlock
{
    public $items;

    public function getName()
    {
        return 'Слайдер на главной';
    }

    public function attributeLabels()
    {
        return [
            'items' => 'Слайды',
        ];
    }

    public function rules()
    {
        return [
            [['items'], 'required'],
        ];
    }
}