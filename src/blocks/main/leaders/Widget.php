<?php

namespace app\blocks\main\leaders;

use nullref\cms\components\Widget as BaseWidget;
use yii\helpers\Html;


class Widget extends BaseWidget
{
public $content;

public function run()
{
return $this->content;
}
}