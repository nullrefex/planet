<?php

namespace app\blocks\main\leaders;

use nullref\cms\components\Block as BaseBlock;
/**
* Class Block
*/
class Block extends BaseBlock
{
public $content;

public function getName()
{
return 'leaders Block';
}

public function rules()
{
return [
[['content'],'required'],
];
}
}