<?php

use mihaildev\elfinder\InputFile;

/**
 * @var $form \yii\widgets\ActiveForm
 * @var $block \nullref\cms\components\Block
 */
echo $form->field($block, 'title')->textInput();
echo $form->field($block, 'content')->textarea();
echo $form->field($block, 'img')->widget(InputFile::className(), [
    //'language' => 'ru',
    'controller' => 'elfinder-backend',
   // 'filter' => 'image',
    'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
    'options' => ['class' => 'form-control'],
    'buttonOptions' => ['class' => 'btn btn-default'],
    'multiple' => false,
]);