<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $title string
 * @var $content string
 * @var $img string
 */
?>
<div class="article__top">
    <div class="article__top__img ofit-block"><img src="<?= $img ?>" alt=""></div>
    <div class="container">
        <div class="article__top__title">
            <h1><?= $title ?></h1>
        </div>
        <div class="article__top__txt">
            <p><?= $content ?></p>
        </div>
    </div>
</div>