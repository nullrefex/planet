<?php

namespace app\blocks\article_top;

use nullref\cms\components\Widget as BaseWidget;


class Widget extends BaseWidget
{
    public $title;
    public $content;
    public $img;

    public function run()
    {
        return $this->render('view', [
            'title' => $this->title,
            'content' => $this->content,
            'img' => $this->img,
        ]);
    }
}