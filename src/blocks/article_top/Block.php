<?php

namespace app\blocks\article_top;

use nullref\cms\components\Block as BaseBlock;

/**
 * Class Block
 */
class Block extends BaseBlock
{
    public $title;
    public $content;
    public $img;

    public function getName()
    {
        return 'ArticleTop Block';
    }

    public function rules()
    {
        return [
            [['content', 'title', 'img'], 'required'],
        ];
    }
}