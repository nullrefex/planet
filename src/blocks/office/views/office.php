<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $title string
 * @var $subtitle string
 * @var $items array
 */
?>
<div class="office">
    <div class="team__title __w-subtitle clearfix">
        <h1><span><?= $title ?></span></h1>
        <div class="team__title__subtitle"><span><?= $subtitle ?></span></div>
    </div>
    <div class="office__sliders">
        <div class="office__top-slider-wrap">
            <a href="#" class="office__nav-slider__btn __prev visible-sm visible-xs"></a>
            <a href="#" class="office__nav-slider__btn __next visible-sm visible-xs"></a>
            <div class="office__top-slider slick-slider">
                <?php foreach ($items as $item): ?>
                    <div class="office__top-slider__slide">
                        <div class="office__top-slider__slide__img ofit-block">
                            <img src="<?= $item['image'] ?>" alt="">
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
        <div class="office__nav-slider-wrap hidden-sm hidden-xs">
            <a href="#" class="office__nav-slider__btn __prev"></a>
            <a href="#" class="office__nav-slider__btn __next"></a>
            <div class="office__nav-slider slick-slider">
                <?php foreach ($items as $item): ?>
                    <div class="office__nav-slider__slide">
                        <div class="office__nav-slider__slide__img ofit-block">
                            <img src="<?= $item['image'] ?>" alt="">
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
