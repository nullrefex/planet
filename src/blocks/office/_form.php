<?php

/**
 * @var $this \yii\web\View
 * @var $block \app\blocks\main\slider\Block
 */
use app\components\Helper;
use mihaildev\elfinder\InputFile;
use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\MultipleInputColumn;
use yii\helpers\ArrayHelper;

$this->registerJs(<<<JS
            var counter = 1;
            jQuery('#slider-input').on('init addNewRow',function() {
            counter++;
                var row = jQuery(this).find('.input-group').last(),
                input = row.find('input'),
                id = 'new_w' + counter,
                btn = row.find('button');
                
                counter++;
                input.attr('id', id);
                btn.attr('id', id + '_button');
                mihaildev.elFinder.register(id, function (file, id) {
                    $('#' + id).val(file.url).trigger('change');
                    return true;
                });
                btn.off().on('click', function () {
                    mihaildev.elFinder.openManager({
                        "url": "/elfinder-backend/manager?filter=image&callback=" + id + "&lang=en",
                        "width": "auto",
                        "height": "auto",
                        "id": "id"
                    });
                });
            });
JS
);

echo $form->field($block, 'title')->textInput();
echo $form->field($block, 'subtitle')->textInput();
echo $form->field($block, 'items')->widget(MultipleInput::className(), [
    'id' => 'slider-input',
    'allowEmptyList' => false,
    'columns' => [
        [
            'name' => 'order',
            'type' => MultipleInputColumn::TYPE_HIDDEN_INPUT,
        ],
        [
            'name' => 'image',
            'type' => InputFile::className(),
            'options' => ArrayHelper::merge(Helper::getInputFileOptions(), [
                'controller' => 'elfinder-backend'
            ]),
        ],
    ],
    'enableGuessTitle' => true,
    'addButtonPosition' => MultipleInput::POS_HEADER,
]);