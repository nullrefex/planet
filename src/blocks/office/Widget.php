<?php

namespace app\blocks\office;

use nullref\cms\components\Widget as BaseWidget;


class Widget extends BaseWidget
{
    public $title;

    public $subtitle;

    public $items;

    public function run()
    {
        return $this->render('office', [
            'title' => $this->title,
            'subtitle' => $this->subtitle,
            'items' => $this->items,
        ]);
    }
}