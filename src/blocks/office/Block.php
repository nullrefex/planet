<?php

namespace app\blocks\office;

use nullref\cms\components\Block as BaseBlock;

/**
 * Class Block
 */
class Block extends BaseBlock
{
    public $title;

    public $subtitle;

    public $items;

    public function getName()
    {
        return 'Офис';
    }

    public function attributeLabels()
    {
        return [
            'items' => 'Слайды',
            'title' => 'Загаловок',
            'subtitle' => 'Подзагаловок',
        ];
    }

    public function rules()
    {
        return [
            [['title', 'subtitle'], 'required'],
            ['items', 'safe'],
        ];
    }
}