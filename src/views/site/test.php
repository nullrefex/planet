<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */

$this->title = Yii::t('app', 'Test');
?>


<div class="test-wrap">
    <div class="container">
        <div class="test__reg active">
            <div class="test__reg__inn">
                <div class="team__title">
                    <h1><span><?= $this->title ?></span></h1>
                </div>
                <div class="test__reg__subtitle">
                    <span><?= Yii::t('app', 'Learn about your abilities! Go for a free Oxford personality assessment test!') ?></span>
                </div>
                <div class="test__reg__how">
                    <a href="#howTestModal" data-toggle="modal">
                        <?= Yii::t('app', 'How to pass the test?') ?>
                    </a>
                </div>
                <div class="test__reg__form">
                    <form action="#" id="testRegForm">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="test__reg__col">
                                    <div class="test__reg__col__title"><span><i>*</i>Дата рождения:</span></div>
                                    <div class="test__reg__col__inwrap">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <div class="test__reg__col__inwrap">
                                                    <select title="День" name="bday" class="selectpicker">
                                                        <option>01</option>
                                                        <option>02</option>
                                                        <option>03</option>
                                                        <option>04</option>
                                                        <option>05</option>
                                                        <option>06</option>
                                                        <option>07</option>
                                                        <option>08</option>
                                                        <option>09</option>
                                                        <option>10</option>
                                                        <option>11</option>
                                                        <option>12</option>
                                                        <option>13</option>
                                                        <option>14</option>
                                                        <option>15</option>
                                                        <option>16</option>
                                                        <option>17</option>
                                                        <option>18</option>
                                                        <option>19</option>
                                                        <option>20</option>
                                                        <option>21</option>
                                                        <option>22</option>
                                                        <option>23</option>
                                                        <option>24</option>
                                                        <option>25</option>
                                                        <option>26</option>
                                                        <option>27</option>
                                                        <option>28</option>
                                                        <option>29</option>
                                                        <option>30</option>
                                                        <option>31</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="test__reg__col__inwrap">
                                                    <select title="Месяц" name="bmonth" class="selectpicker">
                                                        <option>01</option>
                                                        <option>02</option>
                                                        <option>03</option>
                                                        <option>04</option>
                                                        <option>05</option>
                                                        <option>06</option>
                                                        <option>07</option>
                                                        <option>08</option>
                                                        <option>09</option>
                                                        <option>10</option>
                                                        <option>11</option>
                                                        <option>12</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="test__reg__col__inwrap">
                                                    <select title="Год" name="byear" class="selectpicker">
                                                        <?php for ($i = intval(date('Y')); $i >= 1920; $i--): ?>
                                                            <option><?= $i ?></option>
                                                        <?php endfor ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="test__reg__col">
                                    <div class="test__reg__col__title"><span><i>*</i>Ваш пол:</span></div>
                                    <div class="test__reg__col__inwrap">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="test__reg__radio">
                                                    <label>
                                                        <input type="radio" name="gender" value="male" checked class="hidden-input"><span class="test__reg__radio__icon"></span><span class="test__reg__radio__text">Мужской</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="test__reg__radio">
                                                    <label>
                                                        <input type="radio" name="gender" value="female" class="hidden-input"><span class="test__reg__radio__icon"></span><span class="test__reg__radio__text">Женский</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="test__reg__col">
                                    <div class="test__reg__col__title"><span><i>*</i>Ваше имя:</span></div>
                                    <div class="test__reg__col__inwrap">
                                        <input type="text" name="name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="test__reg__col">
                                    <div class="test__reg__col__title"><span><i>*</i>Фамилия:</span></div>
                                    <div class="test__reg__col__inwrap">
                                        <input type="text" name="surname">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="test__reg__col">
                                    <div class="test__reg__col__title"><span><i>*</i>E-mail:</span></div>
                                    <div class="test__reg__col__inwrap">
                                        <input type="email" name="email">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="test__reg__col">
                                    <div class="test__reg__col__title"><span><i>*</i>Телефон:</span></div>
                                    <div class="test__reg__col__inwrap">
                                        <input type="tel" name="tel">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="test__reg__col">
                                    <div class="test__reg__col__title"><span><i>*</i>Ваш город:</span></div>
                                    <div class="test__reg__col__inwrap">
                                        <input type="text" name="city">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="test__reg__col">
                                    <div class="test__reg__col__title"><span><i>*</i>Откуда вы о нас узнали?</span></div>
                                    <div class="test__reg__col__inwrap">
                                        <input type="text" name="from">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="test__reg__form__btn">
                            <button type="submit" class="btn btn-yellow fw-btn">Далее</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="test">
            <div class="test__inn">
                <div class="test__question-wrap"></div>
                <div class="test__next-btn">
                    <button href="#" class="btn btn-yellow fw-btn">Далее</button>
                </div>
                <div class="test__progress">
                    <div class="test__progress__txt"><span></span></div>
                    <div class="test__progress__line">
                        <div style="width: 0%;" class="test__progress__line__inn"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="test__done">
            <div class="test__done__inn">
                <div class="test__done__title"><span>Ваши ответы отправлены на обработку.<br>С вами свяжется специалист по оценке теста по указаным вами контактным данным.</span></div>
                <div class="test__done__txt">
                    <p>Если ваши контакты указаны неверно, то перезвоните нам для исправления ошибки.</p>
                    <p>Ответы на вопросы теста мы исправить не можем. Для этого необходимо повторно заполнить весь тест.</p>
                </div>
                <div class="test__done__btn"><a href="#" class="btn btn-yellow fw-btn">Повторить тест</a></div>
            </div>
        </div>
        <div class="debug">
            <button id="debug" style="width: 100px; height: 20px"></button>
            <button id="debug2" onclick="alert(getUA());" style="width: 100px; height: 20px"></button>
        </div>
    </div>
</div>

<!-- OCA -->
<div id="testengine" style="display: none;">
    <div id="testengine_result"></div>
    <div id="testengine_form">
        <div style="text-align: center; height: 300px;">
            <img style="margin-top: 100px; border: none; height: 80px;" src="http://old.planeta8d.com/oca_online/client/images/loading.gif">
        </div>
    </div>
</div>
<script type="text/javascript" src="http://old.planeta8d.com/oca_online/client/test.js.php?lang_id=RUS"></script>
<script type="text/javascript">Get_Test_Form();</script>
<script type="text/javascript">
    function getUA() {
        return navigator.userAgent || navigator.vendor || window.opera;
    }
    function isFacebookApp() {
        var ua = getUA();
        return (ua.indexOf("FBAN") > -1) || (ua.indexOf("FBAV") > -1) || (ua.indexOf("FBRV") > -1);
    }
    if (isFacebookApp()){
        alert('Данный браузер не поддерживается системой.\nПожалуйста откройте страницу в другом браузере')
    }
</script>
<!-- /OCA -->

