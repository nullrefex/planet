<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $this \yii\web\View
 */
use app\modules\services\widgets\Services;
use app\widgets\Office;
use app\widgets\Title;
$this->title = Yii::t('app', 'Services');
?>

<div class="services">
    <div class="container">
        <?= Title::widget(['title' => $this->title]) ?>

        <?= Services::widget() ?>

        <?= Office::widget() ?>
    </div>
</div>



