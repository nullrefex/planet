<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $this \yii\web\View
 * @var $model \app\modules\services\models\Service
 */
use app\modules\services\components\BlockManager;



$this->title = $model->name;
?>

<?php foreach ($model->blocks as $block): ?>
    <?= $block->getWidget() ?>
<?php endforeach ?>
 