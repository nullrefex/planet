<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $this \yii\web\View
 * @var $topModels \app\modules\team\models\Human[]
 * @var $models \app\modules\team\models\Human[]
 */

$this->title = Yii::t('app', 'Our Team');
?>

<div class="team container">
    <div class="team__title">
        <h1><span><?= $this->title ?></span></h1>
    </div>
    <div class="team__subtitle">
        <span>Группа "Планета" была создана 8 лет назад. Мы помогли многим людям со всех уголков планеты.</span>
    </div>
    <div class="team__tops">
        <?php foreach ($topModels as $model): ?>
            <div class="team__item">
                <div class="team__item__img"><img src="<?= $model->image ?>" alt="<?= $model->name ?>"></div>
                <div class="team__item__name"><span><?= $model->name ?></span></div>
                <div class="team__item__prof"><span><?= $model->description ?></span></div>
            </div>
        <?php endforeach ?>
    </div>
    <div class="team__peoples">
        <?php foreach ($models as $model): ?>
            <div class="team__item">
                <div class="team__item__img"><img src="<?= $model->image ?>" alt="<?= $model->name ?>"></div>
                <div class="team__item__name"><span><?= $model->name ?></span></div>
                <div class="team__item__prof"><span><?= $model->description ?></span></div>
            </div>
        <?php endforeach ?>
    </div>
</div>
