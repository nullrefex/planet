<?php

/* @var $this yii\web\View */
use app\modules\books\widgets\BookList;

$this->title = Yii::t('app', 'Our books');
?>

<div class="books">
    <div class="container">
        <div class="team__title">
            <h1><span><?= $this->title ?></span></h1>
        </div>
        <div class="books__subtitle">
            <span><?= Yii::t('app', 'Many people think that their success and prosperity depends solely on luck. But this is not so. Life, business, work are subject to certain laws, knowing that a person can achieve his goals much faster and more efficiently. These laws, as well as tools for better survival are described in the following books, which you can order in the "Planet" group.') ?></span>
        </div>

        <?= BookList::widget() ?>
    </div>
</div>
