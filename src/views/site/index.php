<?php

/* @var $this yii\web\View */
use app\widgets\Fb;

$this->title = Yii::$app->name;
?>
<?= Yii::$app->getModule('cms')->blockManager->getWidget('main-slider-' . Yii::$app->language) ?>
<?= Yii::$app->getModule('cms')->blockManager->getWidget('leaders-' . Yii::$app->language) ?>
<?= Yii::$app->getModule('cms')->blockManager->getWidget('main-service-' . Yii::$app->language) ?>
<?= Fb::widget() ?>