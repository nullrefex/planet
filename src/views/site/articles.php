<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $this \yii\web\View
 * @var $models \app\modules\articles\models\Article[]
 */
use yii\helpers\Url;

$this->title = Yii::t('app', 'Articles');

$chunks = array_chunk($models, 2);
?>

<div class="articles">
    <div class="container">
        <div class="team__title">
            <h1><span><?= $this->title ?></span></h1>
        </div>
        <div class="articles__list">
            <?php foreach ($chunks as $models): ?>
                <div class="row">
                    <?php foreach ($models as $model):
                        $url = Url::to(['/site/article', 'id' => $model->id]);
                        ?>
                        <div class="col-md-6">
                            <div class="articles__item">
                                <div class="articles__item__img ofit-block">
                                    <img src="<?= $model->image ?>" alt="<?= $model->name ?>">
                                </div>
                                <div class="articles__item__inn">
                                    <div class="articles__item__title">
                                        <span><?= $model->name ?></span>
                                    </div>
                                    <div class="articles__item__btn">
                                        <a href="<?= $url ?>"
                                           class="btn btn-yellow fw-btn hidden-xs"><?= Yii::t('app', 'Learn more') ?> ›</a>
                                        <a href="<?= $url ?>" class="mrelife__cnt__mob-link hidden-lg hidden-md hidden-sm">
                                            <i><?= Yii::t('app', 'Learn more') ?></i> ›
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>
