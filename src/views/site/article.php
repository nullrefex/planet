<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $this \yii\web\View
 * @var $model \app\modules\articles\models\Article
 */

$this->title = $model->name;
?>

<div class="article">
    <div class="article__top">
        <div class="article__top__img ofit-block"><img src="<?= $model->image ?>" alt=""></div>
        <div class="container">
            <div class="article__top__title">
                <h1><?= $model->name ?></h1>
            </div>
            <div class="article__top__txt">
                <p><?= $model->description ?></p>
            </div>
        </div>
    </div>
</div>
<div class="article__cnt">
    <div class="container">
        <div class="article__block-list">
            <?php foreach ($model->blocks as $block): ?>
                <div class="article__block">
                    <?= $block->getWidget() ?>
                </div>
            <?php endforeach ?>
        </div>
        <div class="article__author">
            <span><?= $model->author ?></span>
        </div>
    </div>

</div>