<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 * @var $models \app\modules\videos\models\Video[]
 * @var $this \yii\web\View
 */
$this->title = Yii::t('app', 'Video');

$this->params['modals'][] = <<<HTML
        <div id="videoModal" class="modal fade modal-scale video-modal">
          <div class="modal-dialog">
            <div class="modal-content"><a href="#" data-dismiss="modal" class="modal-close"></a>
              <div class="video-modal__video-wrap"></div>
            </div>
          </div>
        </div>
HTML;

$chunks = array_chunk($models, 2);
?>

<div class="videos">
    <div class="container">
        <div class="team__title">
            <h1><span><?= $this->title ?></span></h1>
        </div>
        <div class="videos__list">
            <?php foreach ($chunks as $models): ?>
                <div class="row">
                    <?php foreach ($models as $model): ?>
                        <div class="col-md-6">
                            <div class="videos__item">
                                <div class="videos__item__img ofit-block">
                                    <img src="<?= $model->image ?>" alt="<?= $model->name ?>">
                                    <a href="#" data-video="<?= $model->url ?>" class="videos__item__play">
                                        <svg class="svg-icon play-icon">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#play-icon"></use>
                                        </svg>
                                    </a>
                                </div>
                                <div class="videos__item__title"><span><?= $model->name ?></span></div>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>

