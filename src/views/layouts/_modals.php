<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $this \yii\web\View
 */
?>

<?php if (isset($this->params['modals']) && is_array($this->params['modals'])): ?>
    <?php foreach ($this->params['modals'] as $modal): ?>
        <?= $modal ?>
    <?php endforeach ?>
<?php endif ?>

