<?php
use app\assets\AppAsset;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Yii::$app->getModule('cms')->blockManager->getWidget('head') ?>
    <?= Html::csrfMetaTags() ?>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic"
          rel="stylesheet">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>

<?= $this->render('_svg') ?>

<div class="page-wrapp">
    <div class="content-without-footer">
        <?= $this->render('_header') ?>
        <?= $content ?>
    </div>
    <?= $this->render('_footer') ?>
</div>

<?= $this->render('_modals') ?>

<script data-main="/js/main" src="/js/require.js"></script>
<?php $this->endBody() ?>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '327597067689280',
            xfbml      : true,
            version    : 'v2.10'
        });
        FB.AppEvents.logPageView();
    };
</script>
<?= Yii::$app->getModule('cms')->blockManager->getWidget('footer') ?>
</body>
</html>
<?php $this->endPage() ?>
