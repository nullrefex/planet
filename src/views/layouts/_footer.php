<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */
use app\components\Helper;
use app\widgets\Language;
use yii\helpers\Url;

?>
<footer class="footer">
    <div class="container">
        <div class="footer__inn clearfix">
            <div class="footer__nav clearfix">
                <?php foreach (Helper::getFooterMenu() as $menu): ?>
                    <ul class="reset-list">
                        <?php foreach ($menu as $item): ?>

                            <li><a href="<?= $item['url'] ?>"><?= $item['label'] ?></a></li>
                        <?php endforeach ?>
                    </ul>
                <?php endforeach ?>
            </div>
            <div class="footer__btns">
                <div class="footer__btns__it">
                    <a href="<?= Url::to(['site/test']) ?>" class="btn btn-yellow fw-btn">
                        <?= Yii::t('app', 'Take the OCA test') ?>
                    </a>
                </div>
                <div class="footer__btns__it">
                    <a href="https://www.facebook.com/planeta8d" class="btn btn-blue fw-btn">
                        <svg class="svg-icon fb-icon hidden-sm hidden-xs">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#fb-icon"></use>
                        </svg>
                        <?= Yii::t('app', 'Follow us on Facebook') ?></a>
                </div>
            </div>
            <div class="footer__info">
                <?= Language::widget() ?>
                <?= Yii::$app->getModule('cms')->blockManager->getWidget('footer-contacts') ?>
                <div class="footer__info__copy">
                    <span>
                        ©<?= date('Y') ?> <?= Yii::t('app', 'Auditor Group "Planet"') ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
</footer>
