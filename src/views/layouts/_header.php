<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */
use app\components\Helper;
use yii\bootstrap\Nav;
use yii\helpers\Url;

?>
<header class="header">
    <div class="container">
        <div class="header__top">
            <div class="header__logo"><a href="/"><img src="/img/template/header-logo.png" alt=""></a></div>
            <div class="header__underlogo hidden-xs">
                <div class="header__underlogo__cnt"><span><?= Yii::t('app','A new level') ?></span></div>
            </div>
            <div class="header__top__btn-left hidden-xs">
                <a href="<?= Url::to(['/site/test']) ?>" class="btn btn-yellow fw-btn">
                    <?= Yii::t('app', 'Take the OCA test') ?>
                </a>
            </div>
            <div class="header__top__btn-right hidden-xs">
                <a href="https://www.facebook.com/planeta8d" class="btn btn-blue fw-btn">
                    <svg class="svg-icon fb-icon hidden-sm">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#fb-icon"></use>
                    </svg>
                    <?= Yii::t('app','Follow us on Facebook') ?></a>
            </div>
        </div>
    </div>
    <a href="#" class="header__sm-menu-trigger visible-sm"><span></span><span></span><span></span></a>
    <a href="#" class="header__xs-menu-trigger visible-xs"><span></span><span></span><span></span></a>
    <nav class="header__nav">
        <div class="container">
            <?= Nav::widget(['items' => Helper::getMainMenu(), 'options' => ['class' => 'reset-list']]) ?>
        </div>
    </nav>
</header>
