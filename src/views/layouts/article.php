<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $this \yii\web\View
 * @var $content string
 */
?>

<?php $this->beginContent('@app/views/layouts/main.php') ?>
<?= $content ?>
<?php $this->endContent() ?>