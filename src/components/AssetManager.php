<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\components;


use nullref\core\interfaces\IAdminController;
use Yii;
use yii\bootstrap\BootstrapAsset;
use yii\web\AssetManager as BaseAssetManager;

class AssetManager extends BaseAssetManager
{
    public function init()
    {
        parent::init();
        if (!(Yii::$app->controller instanceof IAdminController)) {
            $this->bundles[BootstrapAsset::class] = [
                'css' => [],
            ];
        }
    }

}