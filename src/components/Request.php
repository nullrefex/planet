<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2016 NRE
 */

namespace app\components;

use app\helpers\Languages;
use Yii;
use yii\helpers\StringHelper;
use yii\web\Request as BaseRequest;

class Request extends BaseRequest
{
    public function getUrl()
    {
        $url = parent::getUrl();
        $matches = null;
        if (preg_match('/\/([a-z]{2})/', $url, $matches)) {
            $languages = Languages::getSlugedMap();
            if (array_key_exists($matches[1], $languages)) {
                Languages::getManager()->setLanguage($languages[$matches[1]]);
                Yii::$app->getResponse()->redirect(StringHelper::byteSubstr($url, 3));
                Yii::$app->end();
            }
        }
        return $url;
    }

}