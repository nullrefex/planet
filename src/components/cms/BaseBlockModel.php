<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\components\cms;


use Yii;
use yii\caching\TagDependency;
use yii\db\ActiveRecord;

/**
 * Class BaseBlockModel
 * @package app\components\cms
 *
 * @property integer $id
 * @property string $config
 * @property string $class_name
 *
 * @property BaseBlockModel[] $blocks
 *
 */
abstract class BaseBlockModel extends ActiveRecord
{
    /**
     * @param BaseBlock $block
     */
    public function setData(BaseBlock $block)
    {
        $this->setDataInternal($block->getConfig());
    }

    /**
     * @param $data
     */
    public function setDataInternal($data)
    {
        $this->config = serialize($data);
    }

    /**
     */
    public function getBlock()
    {
        return $this->getManager()->getBlock($this->class_name, $this->getData());
    }

    /**
     * @return BaseBlockManager
     */
    public abstract function getManager();

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        TagDependency::invalidate(Yii::$app->cache, $this->getManager()->cachePrefix . $this->id);
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return unserialize($this->config);
    }

    /**
     */
    public function getWidget()
    {
        return $this->getManager()->getWidget($this->id);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlocks()
    {
        return $this->hasMany(self::className(), ['parent_block_id' => 'id'])
            ->indexBy('id')
            ->orderBy(['order' => SORT_ASC]);
    }
}