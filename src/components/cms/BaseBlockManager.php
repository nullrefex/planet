<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\components\cms;


use app\modules\services\models\ServiceBlock;
use nullref\cms\components\BlockManager as CmsBlockManager;
use Yii;
use yii\base\InvalidConfigException;
use yii\caching\TagDependency;
use yii\helpers\ArrayHelper;

class BaseBlockManager extends CmsBlockManager
{
    public $blockModelClass;

    public function init()
    {
        parent::init();

        if (empty($this->cachePrefix)) {
            throw new InvalidConfigException('cachePrefix should be set');
        }
        if (empty($this->blockModelClass)) {
            throw new InvalidConfigException('blockModelClass should be set');
        }
    }

    /**
     * @param $id
     * @param array $config
     * @return \nullref\cms\components\Widget
     */
    public function getWidget($id, $config = [])
    {
        /** @var ServiceBlock $block */
        $blockModelClass = $this->blockModelClass;

        $block = call_user_func([$blockModelClass, 'getDb'])->cache(function () use ($id, $blockModelClass) {
            return call_user_func([$blockModelClass, 'find'])->where(['id' => $id])->one();
        }, null, new TagDependency(['tags' => $this->cachePrefix . $id]));

        if ($block) {
            $config = ArrayHelper::merge($config, $block->getData());

            $childrenBlocks = $block->getBlocks()->select('id')->column();
            $config['blocks'] = [];
            foreach ($childrenBlocks as $id) {
                $config['blocks'][$id] = $this->getWidget($id);
            }
            $config['class'] = $this->getList()[$block->class_name] . self::CLASS_WIDGET;
        } else {
            Yii::error('Can\'t find service block with id "' . $id . '""');
            $config = [
                'class' => $this->emptyBlockClass,
                'id' => $id,
            ];
        }
        /** @var \nullref\cms\components\Widget $widget */
        $widget = Yii::createObject($config);
        if ($block) {
            $blockObj = $this->getBlock($block->class_name, $block->getData());
            $blockObj->id = $block->id;
            $widget->setBlock($blockObj);
        }
        return $widget;
    }
}