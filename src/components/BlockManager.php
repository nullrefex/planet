<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\components;


use nullref\cms\components\BlockManager as BaseBlockManager;

class BlockManager extends BaseBlockManager
{
    public function getList()
    {
        return [
            'html' => 'nullref\cms\blocks\html',
            'office' => 'app\blocks\office',
            'main_slider' => 'app\blocks\main\slider',
        ];
    }

}