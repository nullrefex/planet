<?php

namespace app\components;

use mihaildev\elfinder\ElFinder;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\View;

/**
 * @author    Dmytro Karpovych
 * @copyright 2015 NRE
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Helper
{
    public static function getTitle()
    {
        $view = Yii::$app->view;
        if (array_key_exists('cms.page.meta', $view->params) && array_key_exists('title', $view->params['cms.page.meta'])) {
            return $view->params['cms.page.meta']['title'];
        }
        return $view->title;
    }

    /**
     * Merge option for file input
     * @param array $options
     * @return array
     */
    public static function getInputFileOptions($options = [])
    {
        $defaults = [
            'buttonName' => 'Обзор',
            'language' => 'ru',
            'controller' => 'elfinder-backend',
            'filter' => 'image',
            'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
            'options' => ['class' => 'form-control'],
            'buttonOptions' => ['class' => 'btn btn-default'],
            'multiple' => false
        ];

        return array_merge($defaults, $options);
    }

    public static function getEditorOptions($options = [])
    {

        list(, $footnotesUrl) = Yii::$app->assetManager->publish('@nullref/cms/assets/ckeditor-plugins/codemirror');
        Yii::$app->view->registerJs("CKEDITOR.plugins.addExternal( 'codemirror', '" . $footnotesUrl . "/','plugin.js');
        Object.keys(CKEDITOR.dtd.\$removeEmpty).forEach(function(key){CKEDITOR.dtd.\$removeEmpty[key] = 0;});
        ", View::POS_END);

        $defaults = [
            'id' => 'editor',
            'editorOptions' => [
                'preset' => 'full',
                'inline' => false,
                'extraPlugins' => 'codemirror',
                'allowedContent' => true,
                'basicEntities' => false,
                'entities' => false,
                'entities_greek' => false,
                'entities_latin' => false,
                'htmlEncodeOutput' => false,
                'entities_processNumerical' => false,
                'fillEmptyBlocks' => false,
                'fullPage' => false,
                'codemirror' => [
                    'autoCloseBrackets' => true,
                    'autoCloseTags' => true,
                    'autoFormatOnStart' => true,
                    'autoFormatOnUncomment' => true,
                    'continueComments' => true,
                    'enableCodeFolding' => true,
                    'enableCodeFormatting' => true,
                    'enableSearchTools' => true,
                    'highlightMatches' => true,
                    'indentWithTabs' => false,
                    'lineNumbers' => true,
                    'lineWrapping' => true,
                    'mode' => 'htmlmixed',
                    'matchBrackets' => true,
                    'matchTags' => true,
                    'showAutoCompleteButton' => true,
                    'showCommentButton' => true,
                    'showFormatButton' => true,
                    'showSearchButton' => true,
                    'showTrailingSpace' => true,
                    'showUncommentButton' => true,
                    'styleActiveLine' => true,
                    'theme' => 'default',
                    'useBeautify' => true,
                ],
            ],
        ];

        $defaults['editorOptions'] = ElFinder::ckeditorOptions('elfinder-backend', $defaults['editorOptions']);

        return ArrayHelper::merge($defaults, $options);
    }

    /**
     * @return array
     */
    public static function getMainMenu()
    {
        return [
            [
                'label' => Yii::t('app', 'Home'),
                'url' => '/',
                'active' => Yii::$app->request->url == '/',
            ],
            [
                'label' => Yii::t('app', 'Services'),
                'url' => '/services',
                'active' => (Yii::$app->request->url == '/services')
                    || preg_match('/^\/service\/\d$/', Yii::$app->request->url),
            ],
            [
                'label' => Yii::t('app', 'Test'),
                'url' => '/test',
                'active' => Yii::$app->request->url == '/test',
            ],
            [
                'label' => Yii::t('app', 'Team'),
                'url' => '/team',
                'active' => Yii::$app->request->url == '/team',
            ],
            [
                'label' => Yii::t('app', 'Reviews'),
                'url' => '/reviews',
                'active' => Yii::$app->request->url == '/reviews',
            ],
            [
                'label' => Yii::t('app', 'Books'),
                'url' => '/books',
                'active' => Yii::$app->request->url == '/books',
            ],
            [
                'label' => Yii::t('app', 'Articles'),
                'url' => '/articles',
                'active' => Yii::$app->request->url == '/articles'
                    || preg_match('/^\/article\/\d$/', Yii::$app->request->url),
            ],
            [
                'label' => Yii::t('app', 'Videos'),
                'url' => '/videos',
                'active' => Yii::$app->request->url == '/videos',
            ],
        ];
    }

    public static function getFooterMenu()
    {
        return [
            [
                [
                    'label' => Yii::t('app', 'Home'),
                    'url' => '/',
                    'active' => Yii::$app->request->url == '/'
                        || Yii::$app->request->url == '/site/index',
                ],
                [
                    'label' => Yii::t('app', 'Services'),
                    'url' => '/services',
                    'active' => (Yii::$app->request->url == '/services')
                        || preg_match('/^\/service\/\d$/', Yii::$app->request->url),
                ],
                [
                    'label' => Yii::t('app', 'Test'),
                    'url' => '/test',
                    'active' => Yii::$app->request->url == '/test',
                ],
                [
                    'label' => Yii::t('app', 'Team'),
                    'url' => '/team',
                    'active' => Yii::$app->request->url == '/team',
                ],
            ],
            [

                [
                    'label' => Yii::t('app', 'Reviews'),
                    'url' => '/reviews',
                    'active' => Yii::$app->request->url == '/reviews',
                ],
                [
                    'label' => Yii::t('app', 'Books'),
                    'url' => '/books',
                    'active' => Yii::$app->request->url == '/books',
                ],
                [
                    'label' => Yii::t('app', 'Articles'),
                    'url' => '/articles',
                    'active' => Yii::$app->request->url == '/articles'
                        || preg_match('/^\/article\/\d$/', Yii::$app->request->url),
                ],
                [
                    'label' => Yii::t('app', 'Videos'),
                    'url' => '/videos',
                    'active' => Yii::$app->request->url == '/videos',
                ],
            ]
        ];
    }

}