<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2016 NRE
 */

namespace app\components;

use nullref\core\components\LanguageManager as BaseLanguageManager;
use nullref\core\interfaces\ILanguage;
use Yii;

class LanguageManager extends BaseLanguageManager
{
    public function setLanguage(ILanguage $language)
    {
        parent::setLanguage($language);
        Yii::$app->language = $this->_language->getSlug();
    }

}