<?php
/**
 */

namespace app\components;


class Formatter extends \yii\i18n\Formatter
{
    public $dateFormat = 'dd.MM.yyyy';

    public $datetimeFormat = 'HH:mm dd.MM.yyyy';

    public $timeFormat = 'HH:mm:ss';

    public function asImage($value, $options = [])
    {
        if (!isset($options['style'])) {
            $options['style'] = 'max-width: 200px; max-height: 200px; ';
        }
        return parent::asImage($value, $options);
    }
}