<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2016 NRE
 */


namespace app\behaviors;


use app\helpers\Languages;
use yii\base\Behavior;

/**
 * Class HasLanguage
 * @package app\behaviors
 *
 * @property $languageTitle
 */
class HasLanguage extends Behavior
{
    public $attribute = 'lang';

    public function getLanguageTitle()
    {
        $langId = $this->owner->{$this->attribute};

        return Languages::getTitleById($langId);
    }
}