<?php

namespace app\modules\services\blocks\container_revs;

use app\components\cms\BaseBlock;


/**
 * Class Block
 *
 * @property $title_ru
 * @property $title_en
 */
class Block extends BaseBlock
{
    public $title;

    /**
     * @return array
     */
    public function getMultilingualAttributes()
    {
        return ['title'];
    }

    public function attributeLabels()
    {
        return [
            'title_ru' => 'Заголовок',
            'title_en' => 'Заголовок',
        ];
    }

    public function getName()
    {
        if ($this->title_ru) {
            return 'Блок (Отзывы) - ' . strip_tags($this->title_ru);
        }
        return 'Блок (Отзывы)';
    }

    public function rules()
    {
        return [
            [['title'], 'required'],
        ];
    }
}