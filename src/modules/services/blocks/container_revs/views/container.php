<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $title string
 * @var $blocks \app\components\cms\BaseWidget[]
 */

?>
<div class="service-revs">
    <div class="container">
        <div class="team__title">
            <h1>
                <span>
                    <?= $title ?>
                </span>
            </h1>
        </div>
        <div class="service-revs__list">
            <?php foreach ($blocks as $block): ?>
                <?= $block ?>
            <?php endforeach ?>
        </div>
    </div>
</div>