<?php

namespace app\modules\services\blocks\container_revs;

use app\components\cms\BaseWidget;

class Widget extends BaseWidget
{
    public $title;

    public function run()
    {
        $lang = $this->languageManager->getLanguage()->getSlug();

        return $this->render('container', [
            'title' => $this->title[$lang],
            'blocks' => $this->blocks,
        ]);
    }
}