<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $title string
 * @var $blocks \app\components\cms\BaseWidget[]
 */

?>
<div class="service-details">
    <div class="container">
        <div class="service-details__title">
            <h3>
                <?= $title ?>
            </h3>
        </div>
        <div class="service-top__list">
            <?php foreach ($blocks as $block): ?>
                <?= $block ?>
            <?php endforeach ?>
        </div>
    </div>
</div>