<?php


use app\components\Helper;
use mihaildev\ckeditor\CKEditor;
use nullref\core\widgets\Multilingual;
use yii\widgets\ActiveForm;

/**
 * @var $block \app\modules\services\blocks\container\Block
 */


echo Multilingual::widget(['model' => $block,
    'tab' => function (ActiveForm $form, $model) {
        echo $form->field($model, 'title')->widget(CKEditor::className(), Helper::getEditorOptions());
    }
]);