<?php

/**
 * @var $block \app\modules\services\blocks\container\Block
 */

use app\components\Helper;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\InputFile;
use nullref\core\widgets\Multilingual;
use yii\widgets\ActiveForm;

echo Multilingual::widget(['model' => $block,
    'tab' => function (ActiveForm $form, $block) {
        echo $form->field($block, 'name')->textInput();

        echo $form->field($block, 'post')->textInput();

        echo $form->field($block, 'content')->widget(CKEditor::className(), Helper::getEditorOptions());
    }
]);

echo $form->field($block, 'image')->widget(InputFile::className(), Helper::getInputFileOptions());

