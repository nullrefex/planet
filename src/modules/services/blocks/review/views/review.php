<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $name string
 * @var $post string
 * @var $content string
 * @var $image string
 */

?>

<div class="service-revs__it">
    <div class="service-revs__it__photo"><img src="<?= $image ?>" alt=""></div>
    <div class="service-revs__it__cnt">
        <div class="service-revs__it__name"><span><?= $name ?></span></div>
        <div class="service-revs__it__prof"><span><?= $post ?></span></div>
        <div class="service-revs__it__txt">
            <div class="service-revs__it__txt__inn">
                <?= $content ?>
            </div>
        </div>
    </div>
</div>