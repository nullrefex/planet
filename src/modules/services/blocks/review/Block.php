<?php

namespace app\modules\services\blocks\review;

use app\components\cms\BaseBlock;

/**
 * Class Block
 */
class Block extends BaseBlock
{
    public $name;

    public $post;

    public $content;

    public $image;

    public function attributeLabels()
    {
        return [
            'name_ru' => 'Имя',
            'name_en' => 'Имя',
            'post_ru' => 'Пост',
            'post_en' => 'Пост',
            'content_ru' => 'Контент',
            'content_en' => 'Контент',
            'image' => 'Изображение',
        ];
    }

    /**
     * @return array
     */
    public function getMultilingualAttributes()
    {
        return ['name', 'post', 'content'];
    }

    public function getName()
    {
        if ($this->name_ru) {
            return 'Отзыв - ' . $this->name_ru;
        }
        return 'Отзыв';
    }

    public function rules()
    {
        return [
            [['name', 'post', 'content', 'image'], 'required'],
        ];
    }
}