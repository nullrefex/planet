<?php

namespace app\modules\services\blocks\review;

use app\components\cms\BaseWidget;

class Widget extends BaseWidget
{
    public $name;

    public $post;

    public $content;

    public $image;

    public function run()
    {
        $lang = $this->languageManager->getLanguage()->getSlug();

        return $this->render('review', [
            'name' => $this->name[$lang],
            'post' => $this->post[$lang],
            'content' => $this->content[$lang],
            'image' => $this->image,
        ]);
    }
}