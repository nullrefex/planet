<?php

namespace app\modules\services\blocks\details;

use app\components\cms\BaseBlock;
use yii\helpers\StringHelper;

/**
 * Class Block
 */
class Block extends BaseBlock
{
    public $is_reverse;

    public $content;

    public $image;

    public function attributeLabels()
    {
        return [
            'content_ru' => 'Контент',
            'content_en' => 'Контент',
            'image' => 'Изображение',
            'is_reverse' => 'Реверс',
        ];
    }

    /**
     * @return array
     */
    public function getMultilingualAttributes()
    {
        return ['content'];
    }

    public function getName()
    {
        if ($this->content_ru) {
            return 'Пункт - ' . StringHelper::truncateWords(strip_tags($this->content_ru), 5);
        }
        return 'Пункт (Описание)';
    }

    public function rules()
    {
        return [
            [['content'], 'required'],
            [['image'], 'safe'],
            [['is_reverse'], 'boolean'],
        ];
    }
}