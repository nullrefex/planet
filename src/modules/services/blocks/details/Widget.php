<?php

namespace app\modules\services\blocks\details;

use app\components\cms\BaseWidget;

class Widget extends BaseWidget
{
    public $title;

    public $content;

    public $image;

    public $is_reverse;

    public function run()
    {
        $lang = $this->languageManager->getLanguage()->getSlug();

        return $this->render('details', [
            'title' => $this->title[$lang],
            'content' => $this->content[$lang],
            'image' => $this->image,
            'is_reverse' => $this->is_reverse,
        ]);
    }
}