<?php

namespace app\modules\services\blocks\top_item;

use app\components\cms\BaseWidget;

class Widget extends BaseWidget
{
    public $title;

    public $content;

    public $images;

    public function run()
    {
        $lang = $this->languageManager->getLanguage()->getSlug();

        return $this->render('top_item', [
            'title' => $this->title[$lang],
            'content' => $this->content[$lang],
            'images' => $this->images,
        ]);
    }
}