<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $title string
 * @var $content string
 * @var $blocks \nullref\cms\components\Widget[]
 * @var $images array
 */

?>
<div class="service-top__it">
    <div class="service-top__it__title"><span><?= $title ?></span></div>
    <div class="service-top__it__txt">
        <p><?= $content ?></p>
    </div>
    <?php if ($images): ?>
        <div class="service-top__it__imgs">
            <div class="row">
                <?php foreach ($images as $image): ?>
                    <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="service-top__it__imgs__it ofit-block">
                            <img src="<?= $image['image'] ?>" alt="">
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    <?php endif ?>
</div>