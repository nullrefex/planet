<?php

namespace app\modules\services\blocks\top_item;

use app\components\cms\BaseBlock;

/**
 * Class Block
 */
class Block extends BaseBlock
{
    public $title;

    public $content;

    public $images;

    /**
     * @return array
     */
    public function getMultilingualAttributes()
    {
        return ['title', 'content'];
    }

    public function attributeLabels()
    {
        return [
        'title_ru' => 'Заголовок',
        'title_en' => 'Заголовок',
        'content_ru' => 'Контент',
        'content_en' => 'Контент',
        'images' => 'Изображения',
        ];
    }

    public function getName()
    {
        return 'Пункт списка (Шапка)';
    }

    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['images'], 'safe'],
        ];
    }
}