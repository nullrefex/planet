<?php

namespace app\modules\services\models;

use app\helpers\Languages;
use nullref\useful\behaviors\TranslationBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%service}}".
 *
 * @property int $id
 * @property string $name
 * @property string $image
 * @property string $content
 *
 * @property ServiceBlock[] $blocks
 *
 */
class Service extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%service}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['name', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'name_en' => Yii::t('app', 'Name'),
            'name_ru' => Yii::t('app', 'Name'),
            'image' => Yii::t('app', 'Image'),
            'content' => Yii::t('app', 'Content'),
            'content_en' => Yii::t('app', 'Content'),
            'content_ru' => Yii::t('app', 'Content'),
        ];
    }

    /**
     * @inheritdoc
     * @return ServiceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ServiceQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'multilanguage' => [
                'class' => TranslationBehavior::className(),
                'languages' => Languages::getSlugMap(),
                'defaultLanguage' => Languages::get()->getSlug(),
                'relation' => 'translations',
                'attributeNamePattern' => '{attr}_{lang}',
                'languageField' => 'language',
                'langClassName' => ServiceTranslation::className(),
                'translationAttributes' => [
                    'name', 'content',
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllBlocks()
    {
        return $this->hasMany(ServiceBlock::className(), ['service_id' => 'id']);
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        /** @var ServiceBlock $block */
        foreach ($this->getAllBlocks()->each() as $block) {
            $block->delete();
        }
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ServiceTranslation::className(), ['service_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlocks()
    {
        return $this->hasMany(ServiceBlock::className(), ['service_id' => 'id'])->andWhere(['parent_block_id' => 0])
            ->orderBy(['service_block.order' => SORT_ASC]);
    }
}
