<?php

namespace app\modules\services\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%service_translation}}".
 *
 * @property integer $id
 * @property integer $language
 * @property string $name
 * @property string $content
 * @property integer $service_id
 */
class ServiceTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%service_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'service_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['content'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'language' => Yii::t('app', 'Language'),
            'name' => Yii::t('app', 'Name'),
            'content' => Yii::t('app', 'Content'),
            'service_id' => Yii::t('app', 'Service ID'),
        ];
    }
}
