<?php

namespace app\modules\services\models;

use app\components\cms\BaseBlockModel;
use Yii;

/**
 * This is the model class for table "{{%service_block}}".
 *
 * @property int $id
 * @property string $class_name
 * @property int $service_id
 * @property int $parent_block_id
 * @property double $order
 * @property string $config
 *
 * @property Service $service
 * @property ServiceBlock[] $blocks
 */
class ServiceBlock extends BaseBlockModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%service_block}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class_name', 'service_id',], 'required'],
            [['service_id', 'parent_block_id'], 'integer'],
            [['parent_block_id', 'order'], 'default', 'value' => 0],
            [['order'], 'number'],
            [['config'], 'string'],
            [['class_name'], 'string', 'max' => 255],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Service::className(), 'targetAttribute' => ['service_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'class_name' => 'Class Name',
            'service_id' => 'Service ID',
            'parent_block_id' => 'Parent Block ID',
            'order' => 'Порядок сортировки',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    /**
     * @return null|object
     */
    public function getManager()
    {
        return Yii::$app->getModule('services')->get('blockManager');
    }
}
