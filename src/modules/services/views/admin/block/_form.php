<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\services\models\ServiceBlock */
/* @var $block \nullref\cms\components\Block */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="service-block-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary([$model, $block]) ?>

    <?= $form->field($model, 'order')->textInput() ?>

    <?= $this->renderFile($block->getForm(), ['form' => $form, 'block' => $block]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
