<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\services\models\ServiceBlock */
/* @var $block \nullref\cms\components\Block */

$this->title = 'Создать служебный блок';
$this->params['breadcrumbs'][] = ['label' => 'Сервисные блоки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-block-create">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <p>
        <?= Html::a('Список', ['index'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= $this->render('_form', [
        'model' => $model,
        'block' => $block,
    ]) ?>

</div>
