<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\services\models\ServiceBlock */
/* @var $block \nullref\cms\components\Block */

$this->title = 'Обновить служебный блок: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Сервисные блоки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="service-block-update">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <p>
        <?= Html::a('Список', ['index', 'id' => $model->service_id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= $this->render('_form', [
        'model' => $model,
        'block' => $block,
    ]) ?>

</div>
