<?php

use rmrevin\yii\fontawesome\FA;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\services\models\ServiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Услуги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-index">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <p>
        <?= Html::a('Добавить Услугу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'image:image',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} {blocks}',
                'buttons' => [
                    'blocks' => function ($url, $model, $key) {
                        return Html::a(FA::i(FA::_LIST), ['/services/admin/block/index', 'id' => $model->id]);
                    },
                ]
            ],
        ],
    ]); ?>

</div>
