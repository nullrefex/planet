<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\services\widgets;


use app\modules\services\models\Service;
use yii\base\Widget;

class Services extends Widget
{
    /** @var Service[] */
    public $models;

    public function init()
    {
        $this->models = Service::find()->all();
    }

    public function run()
    {
        return $this->render('services', ['models' => $this->models]);
    }
}