<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $models \app\modules\services\models\Service[]
 */
use yii\helpers\Url;

$chunks = array_chunk($models, 2);
?>

<div class="services__list">
    <?php foreach ($chunks as $models): ?>
        <div class="row">
            <?php foreach ($models as $model): ?>
                <div class="col-md-6">
                    <div class="articles__item">
                        <div class="articles__item__img ofit-block">
                            <img src="<?= $model->image ?>"
                                 alt="<?= $model->name ?>">
                        </div>
                        <div class="articles__item__inn">
                            <div class="articles__item__title"><span><?= $model->name ?></span></div>
                            <div class="articles__item__btn"><a
                                        href="<?= Url::to(['/site/service', 'id' => $model->id]) ?>"
                                        class="btn btn-yellow fw-btn hidden-xs"><?= Yii::t('app', 'Learn more') ?> ›</a>
                                <a href="<?= Url::to(['/site/service', 'id' => $model->id]) ?>" class="mrelife__cnt__mob-link hidden-lg hidden-md hidden-sm">
                                    <i><?= Yii::t('app', 'Learn more') ?></i> ›</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    <?php endforeach ?>
</div>
 