<?php

namespace app\modules\services;

use nullref\core\interfaces\IAdminModule;
use Yii;
use yii\base\Module as BaseModule;

/**
 * services module definition class
 */
class Module extends BaseModule implements IAdminModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\services\controllers';


    public static function getAdminMenu()
    {
        return [
            'label' => Yii::t('app', 'Services'),
            'icon' => 'handshake-o',
            'order' => 2,
            'url' => ['/services/admin/service'],
        ];
    }
}
