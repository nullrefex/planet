<?php

namespace app\modules\services\migrations;

use nullref\core\traits\MigrationTrait;
use yii\db\Migration;

class M170726191910Services__add_block_tables extends Migration
{
    use MigrationTrait;

    public function up()
    {
        $this->createTable('{{%service_block}}', [
            'id' => $this->primaryKey(),
            'class_name' => $this->string()->notNull(),
            'service_id' => $this->integer()->notNull(),
            'config' => $this->text(),
            'parent_block_id' => $this->integer()->notNull(),
            'order' => $this->float(),
        ], $this->getTableOptions());

        $this->createIndex('service_idx', '{{%service_block}}', 'service_id');
        $this->addForeignKey('service_fk', '{{%service_block}}', 'service_id', '{{%service}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('service_fk', '{{%service_block}}');
        $this->dropIndex('service_idx', '{{%service_block}}');

        $this->dropTable('{{%service_block}}');
    }
}
