<?php

namespace app\modules\services\migrations;

use yii\db\Migration;

class M170711111038Services__create_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%service}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'image' => $this->string(),
            'content' => $this->text(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%service}}');
    }
}
