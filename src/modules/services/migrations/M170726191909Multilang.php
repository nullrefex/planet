<?php

namespace app\modules\services\migrations;

use app\modules\services\models\Service;
use yii\db\Migration;

class M170726191909Multilang extends Migration
{
    const TABLE_NAME = '{{%service_translation}}';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'language' => $this->integer(),
            'service_id' => $this->integer(),
            'name' => $this->string(),
            'content' => $this->text(),
        ]);
        $this->dropColumn(Service::tableName(), 'name');
        $this->dropColumn(Service::tableName(), 'content');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
        $this->addColumn(Service::tableName(), 'name', $this->string());
        $this->addColumn(Service::tableName(), 'content', $this->text());
        return true;
    }
}
