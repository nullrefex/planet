<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\services\components;


use app\components\cms\BaseBlockManager;
use app\modules\services\models\ServiceBlock;

class BlockManager extends BaseBlockManager
{
    public $cachePrefix = 'service.block.';

    public function init()
    {
        $this->blockModelClass = ServiceBlock::className();
        parent::init();
    }

    public function getList()
    {
        return [
            'container_header' => 'app\modules\services\blocks\container_header',
            'container_details' => 'app\modules\services\blocks\container_details',
            'container_revs' => 'app\modules\services\blocks\container_revs',
            'top_item' => 'app\modules\services\blocks\top_item',
            'details' => 'app\modules\services\blocks\details',
            'review' => 'app\modules\services\blocks\review',
        ];
    }
}