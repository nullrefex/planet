<?php

namespace app\modules\articles\models;

use app\components\cms\BaseBlockModel;
use Yii;
use yii\caching\TagDependency;

/**
 * This is the model class for table "{{%article_block}}".
 *
 * @property int $id
 * @property string $class_name
 * @property int $article_id
 * @property int $parent_block_id
 * @property double $order
 * @property string $config
 *
 * @property Article $article
 */
class ArticleBlock extends BaseBlockModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article_block}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class_name', 'article_id'], 'required'],
            [['article_id', 'parent_block_id'], 'integer'],
            [['order'], 'number'],
            [['config'], 'string'],
            [['parent_block_id', 'order'], 'default', 'value' => 0],
            [['class_name'], 'string', 'max' => 255],
            [['article_id'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['article_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'class_name' => 'Class Name',
            'article_id' => 'Article ID',
            'parent_block_id' => 'Parent Block ID',
            'order' => 'Порядок сортировки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }

    /**
     * @return null|object
     */
    public function getManager()
    {
        return Yii::$app->getModule('articles')->get('blockManager');
    }
}
