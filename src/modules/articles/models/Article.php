<?php

namespace app\modules\articles\models;

use app\helpers\Languages;
use nullref\useful\behaviors\TranslationBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%article}}".
 *
 * @property int $id
 * @property string $image
 * @property string $url
 * @property string $name
 * @property string $description
 * @property string $author
 *
 * @property ArticleBlock[] $blocks
 * @property ArticleTranslation[] $translations
 */
class Article extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article}}';
    }

    /**
     * @inheritdoc
     * @return ArticleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ArticleQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'author'], 'required'],
            [['image', 'url', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'image' => Yii::t('app', 'Image'),
            'url' => Yii::t('app', 'URL'),
            'name' => Yii::t('app', 'Name'),
            'name_ru' => Yii::t('app', 'Name'),
            'name_en' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'description_ru' => Yii::t('app', 'Description'),
            'description_en' => Yii::t('app', 'Description'),
            'author' => Yii::t('app', 'Author'),
            'author_ru' => Yii::t('app', 'Author'),
            'author_en' => Yii::t('app', 'Author'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'multilanguage' => [
                'class' => TranslationBehavior::className(),
                'languages' => Languages::getSlugMap(),
                'defaultLanguage' => Languages::get()->getSlug(),
                'relation' => 'translations',
                'attributeNamePattern' => '{attr}_{lang}',
                'languageField' => 'language',
                'langClassName' => ArticleTranslation::className(),
                'translationAttributes' => [
                    'name',
                    'description',
                    'author',
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlocks()
    {
        return $this->hasMany(ArticleBlock::className(), ['article_id' => 'id'])->andWhere(['parent_block_id' => 0])
            ->orderBy(['article_block.order' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllBlocks()
    {
        return $this->hasMany(ArticleBlock::className(), ['article_id' => 'id']);
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        /** @var ArticleBlock $block */
        foreach ($this->getAllBlocks()->each() as $block) {
            $block->delete();
        }
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ArticleTranslation::className(), ['article_id' => 'id']);
    }
}
