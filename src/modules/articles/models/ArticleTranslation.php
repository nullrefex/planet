<?php

namespace app\modules\articles\models;

use Yii;

/**
 * This is the model class for table "{{%article_translation}}".
 *
 * @property int $id
 * @property int $language
 * @property int $article_id
 * @property string $name
 * @property string $description
 * @property string $author
 *
 * @property Article $article
 */
class ArticleTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'article_id'], 'integer'],
            [['article_id'], 'required'],
            [['description'], 'string'],
            [['name', 'author'], 'string', 'max' => 255],
            [['article_id'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['article_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'article_id' => 'Article ID',
            'name' => 'Name',
            'description' => 'Description',
            'author' => 'Author',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }
}
