<?php

namespace app\modules\articles\controllers\admin;

use app\modules\articles\components\BlockManager;
use app\modules\articles\models\Article;
use app\modules\articles\models\ArticleBlock;
use nullref\core\interfaces\IAdminController;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * BlockController implements the CRUD actions for ArticleBlock model.
 */
class BlockController extends Controller implements IAdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param $id
     * @param int $parent_block_id
     * @return string
     */
    public function actionIndex($id, $parent_block_id = 0)
    {
        $model = Article::findOne($id);

        $query = $model->getBlocks();

        $query->where(['parent_block_id' => $parent_block_id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'parent_block_id' => $parent_block_id,
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ArticleBlock model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the ArticleBlock model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ArticleBlock the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ArticleBlock::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $class_name
     * @param $article_id
     * @param int $parent_block_id
     * @return string|\yii\web\Response
     */
    public function actionCreate($class_name, $article_id, $parent_block_id = 0)
    {
        $model = new ArticleBlock();
        $model->class_name = $class_name;
        $model->article_id = $article_id;
        $model->parent_block_id = $parent_block_id;

        /** @var BlockManager $blockManager */
        $blockManager = Yii::$app->getModule('articles')->get('blockManager');

        /** @var \nullref\cms\components\Block $block */
        $block = $blockManager->getBlock($model->class_name);

        if ($block->load(Yii::$app->request->post()) && ($block->validate()) &&
            $model->load(Yii::$app->request->post()) && ($model->validate())
        ) {
            $model->setData($block);
            if ($model->save()) {
                return $this->redirect(['index',
                    'id' => $model->article_id,
                    'parent_block_id' => $model->parent_block_id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'block' => $block,
        ]);

    }

    /**
     * Updates an existing ArticleBlock model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        /** @var BlockManager $blockManager */
        $blockManager = Yii::$app->getModule('articles')->get('blockManager');

        /** @var \nullref\cms\components\Block $block */
        $block = $blockManager->getBlock($model->class_name);

        $block->setAttributes($model->getData());

        if ($block->load(Yii::$app->request->post()) && ($block->validate()) &&
            $model->load(Yii::$app->request->post()) && ($model->validate())
        ) {
            $model->setData($block);
            if ($model->save()) {
                return $this->redirect(['index',
                    'id' => $model->article_id,
                    'parent_block_id' => $model->parent_block_id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
            'block' => $block,
        ]);

    }

    /**
     * Deletes an existing ArticleBlock model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $model->delete();

        return $this->redirect(['index', 'id' => $model->article_id, 'parent_block_id' => $model->parent_block_id]);
    }
}
