<?php

namespace app\modules\articles\blocks\textOnImage;

use app\components\cms\BaseBlock;
use Yii;
use yii\helpers\StringHelper;

/**
 * Class Block
 */
class Block extends BaseBlock
{
    public $content;

    public $image;

    /**
     * @return array
     */
    public function getMultilingualAttributes()
    {
        return ['content'];
    }

    public function getName()
    {
        if ($this->content_default) {
            return 'Текст на фоне - ' . StringHelper::truncateWords(strip_tags($this->content_default), 5);
        }
        return 'Текст на фоне';
    }

    public function rules()
    {
        return [
            [['content', 'image'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'content_ru' => Yii::t('app', 'Content'),
            'content_en' => Yii::t('app', 'Content'),
            'image' => Yii::t('app', 'Image'),
        ];
    }
}