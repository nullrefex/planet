<?php

namespace app\modules\articles\blocks\textOnImage;

use app\components\cms\BaseWidget;

class Widget extends BaseWidget
{
    public $content;

    public $image;

    public function run()
    {
        $lang = $this->languageManager->getLanguage()->getSlug();

        return $this->render('text_on_image', [
            'content' => $this->content[$lang],
            'image' => $this->image,
        ]);
    }
}