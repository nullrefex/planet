<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $content string
 * @var $image string
 */

?>

<div class="article__txt-on-img">
    <div class="article__txt-on-img__img ofit-block"><img src="<?= $image ?>" alt=""></div>
    <div class="article__txt-on-img__txt">
        <?= $content ?>
    </div>
</div>