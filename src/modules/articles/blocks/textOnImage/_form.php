<?php

/**
 * @var $block \app\modules\articles\blocks\textOnImage\Block
 */

use app\components\Helper;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\InputFile;
use nullref\core\widgets\Multilingual;
use yii\widgets\ActiveForm;


echo Multilingual::widget(['model' => $block,
    'tab' => function (ActiveForm $form, $block) {
        echo $form->field($block, 'content')->widget(CKEditor::className(), Helper::getEditorOptions());
    }
]);

echo $form->field($block, 'image')->widget(InputFile::className(), Helper::getInputFileOptions());