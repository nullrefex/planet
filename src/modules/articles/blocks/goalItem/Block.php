<?php

namespace app\modules\articles\blocks\goalItem;

use app\components\cms\BaseBlock;
use Yii;

/**
 * Class Block
 */
class Block extends BaseBlock
{
    public $title;

    public $content;

    public $images;

    /**
     * @return array
     */
    public function getMultilingualAttributes()
    {
        return ['title', 'content'];
    }

    public function getName()
    {
        return 'Цель';
    }

    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['images'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'content_ru' => Yii::t('app', 'Content'),
            'content_en' => Yii::t('app', 'Content'),
            'title_ru' => Yii::t('app', 'Title'),
            'title_en' => Yii::t('app', 'Title'),
            'images' => Yii::t('app', 'Image'),
        ];
    }
}