<?php

namespace app\modules\articles\blocks\textWithImage;

use app\components\cms\BaseWidget;

class Widget extends BaseWidget
{
    public $content;

    public $is_reverse;

    public $image;

    public function run()
    {
        $lang = $this->languageManager->getLanguage()->getSlug();

        return $this->render('text_with_image', [
            'content' => $this->content[$lang],
            'image' => $this->image,
            'is_reverse' => $this->is_reverse,
        ]);
    }
}