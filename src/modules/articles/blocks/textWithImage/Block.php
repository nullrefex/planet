<?php

namespace app\modules\articles\blocks\textWithImage;

use app\components\cms\BaseBlock;
use Yii;
use yii\helpers\StringHelper;

/**
 * Class Block
 */
class Block extends BaseBlock
{
    public $content;

    public $is_reverse;

    public $image;

    /**
     * @return array
     */
    public function getMultilingualAttributes()
    {
        return ['content'];
    }

    public function getName()
    {
        if ($this->content_default) {
            return 'Текст с изображением - ' . StringHelper::truncateWords(strip_tags($this->content_default), 5);
        }
        return 'Текст с изображением';
    }

    public function rules()
    {
        return [
            [['content', 'image'], 'required'],
            ['is_reverse', 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'content_ru' => Yii::t('app', 'Content'),
            'content_en' => Yii::t('app', 'Content'),
            'is_reverse' => Yii::t('app', 'Is Reverse'),
            'image' => Yii::t('app', 'Image'),
        ];
    }
}