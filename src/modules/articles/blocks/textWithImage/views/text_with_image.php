<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $title string
 * @var $content string
 * @var $image string
 * @var $is_reverse boolean
 */

?>

<div class="article__block">
    <div class="article__img-block <?= $is_reverse ? '__reverse' : '' ?>">
        <div class="article__img-block__img ofit-block">
            <img src="<?= $image ?>" alt="">
        </div>
        <div class="article__img-block__inn">
            <div class="article__img-block__txt">
                <?= $content ?>
            </div>
        </div>
    </div>
</div>