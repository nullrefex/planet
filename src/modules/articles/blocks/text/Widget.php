<?php

namespace app\modules\articles\blocks\text;

use app\components\cms\BaseWidget;

class Widget extends BaseWidget
{
    public $content;

    public function run()
    {
        $lang = $this->languageManager->getLanguage()->getSlug();
        
        return $this->render('text', [
            'content' => $this->content[$lang],
        ]);
    }
}