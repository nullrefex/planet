<?php

namespace app\modules\articles\blocks\text;

use app\components\cms\BaseBlock;
use Yii;
use yii\helpers\StringHelper;

/**
 * Class Block
 */
class Block extends BaseBlock
{
    public $content;

    public function getName()
    {
        if ($this->content_default) {
            return 'Текст - ' . StringHelper::truncateWords(strip_tags($this->content_default), 5);
        }
        return 'Текст';
    }


    public function rules()
    {
        return [
            [['content'], 'required'],
        ];
    }

    public function getMultilingualAttributes()
    {
        return ['content'];
    }

    public function attributeLabels()
    {
        return [
            'content_ru' => Yii::t('app', 'Content'),
            'content_en' => Yii::t('app', 'Content'),
        ];
    }
}