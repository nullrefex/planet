<?php

/**
 * @var $block \app\modules\articles\blocks\text\Block
 */

use app\components\Helper;
use mihaildev\ckeditor\CKEditor;
use nullref\core\widgets\Multilingual;
use yii\widgets\ActiveForm;

echo Multilingual::widget(['model' => $block,
    'tab' => function (ActiveForm $form, $model) {
        echo $form->field($model, 'content')->widget(CKEditor::className(), Helper::getEditorOptions());
    }
]);