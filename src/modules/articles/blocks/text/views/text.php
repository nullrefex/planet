<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $content string
 */

?>
<div class="article__txt-block">
    <?= $content ?>
</div>