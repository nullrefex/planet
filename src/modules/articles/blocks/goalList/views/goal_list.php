<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $blocks array
 */
?>

<div class="service-top__list">
    <?php foreach ($blocks as $block): ?>
        <?= $block ?>
    <?php endforeach ?>
</div>