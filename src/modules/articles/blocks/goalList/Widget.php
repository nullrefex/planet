<?php

namespace app\modules\articles\blocks\goalList;

use app\components\cms\BaseWidget;

class Widget extends BaseWidget
{
    public $order;

    public function run()
    {
        return $this->render('goal_list', [
            'blocks' => $this->blocks,
        ]);
    }
}