<?php

namespace app\modules\articles\blocks\goalList;

use app\components\cms\BaseBlock;

/**
 * Class Block
 * @package app\modules\articles\blocks\images
 */
class Block extends BaseBlock
{
    public $order;

    public function getName()
    {
        return 'Список целей';
    }


    public function rules()
    {
        return [
            ['order', 'safe']
        ];
    }

    public function getMultilingualAttributes()
    {
        return [];
    }
}