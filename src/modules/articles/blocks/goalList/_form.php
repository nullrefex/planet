<?php

/**
 * @var $block \app\modules\articles\blocks\images\Block
 */

echo $form->field($block, 'order')->hiddenInput()->label(false);