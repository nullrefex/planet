<?php

namespace app\modules\articles\blocks\quote;

use app\components\cms\BaseWidget;

class Widget extends BaseWidget
{
    public $content;

    public function run()
    {
        $lang = $this->languageManager->getLanguage()->getSlug();

        return $this->render('quote', [
            'content' => $this->content[$lang],
        ]);
    }
}