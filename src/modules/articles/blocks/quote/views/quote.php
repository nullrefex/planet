<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $content string
 */

?>
<div class="article__quote-block">
    <div class="service-revs__it__txt">
        <div class="service-revs__it__txt__inn">
            <p><i><?= $content ?></i></p>
        </div>
    </div>
</div>