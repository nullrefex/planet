<?php

/**
 * @var $block \app\modules\services\blocks\container\Block
 */

use nullref\core\widgets\Multilingual;
use yii\widgets\ActiveForm;

echo Multilingual::widget(['model' => $block,
    'tab' => function (ActiveForm $form, $model) {
        echo $form->field($model, 'content')->textarea();
    }
]);