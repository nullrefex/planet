<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $images array
 */
?>
<div class="article__imgs-block">
    <div class="service-top__it__imgs">
        <div class="row">
            <?php foreach($images as $image): ?>
                <div class="col-md-3 col-sm-4 col-xs-6">
                    <div class="service-top__it__imgs__it ofit-block">
                        <img src="<?= $image['image'] ?>" alt="">
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>
