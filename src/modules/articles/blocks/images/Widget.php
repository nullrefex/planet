<?php

namespace app\modules\articles\blocks\images;

use app\components\cms\BaseWidget;

class Widget extends BaseWidget
{
    public $images;

    public function run()
    {
        return $this->render('images', [
            'images' => $this->images,
        ]);
    }
}