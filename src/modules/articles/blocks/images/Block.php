<?php

namespace app\modules\articles\blocks\images;

use app\components\cms\BaseBlock;
use Yii;

/**
 * Class Block
 */
class Block extends BaseBlock
{
    public $images;

    public function getName()
    {
        return 'Изображения';
    }


    public function rules()
    {
        return [
            [['images'], 'safe'],
        ];
    }

    public function getMultilingualAttributes()
    {
        return [];
    }

    public function attributeLabels()
    {
        return [
            'images' => Yii::t('app', 'Images'),
        ];
    }
}