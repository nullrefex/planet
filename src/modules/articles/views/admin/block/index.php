<?php

use app\modules\articles\models\ArticleBlock;
use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\Dropdown;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \app\modules\articles\models\Article */
/* @var $manager \app\modules\articles\components\BlockManager */
/* @var $parent_block_id integer|boolean */

$this->title = 'Блоки';
$this->params['breadcrumbs'][] = $this->title;

$items = [];

$manager = Yii::$app->getModule('articles')->get('blockManager');

foreach ($manager->getDropDownArray() as $key => $name) {
    $items[] = ['label' => $name, 'url' => ['create',
        'article_id' => $model->id,
        'class_name' => $key,
        'parent_block_id' => $parent_block_id,
    ]];
}

?>

<div class="article-block-index">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <p>
    <div class="btn-group">
        <?php if ($parent_block_id): ?>
            <?= Html::a(FA::i(FA::_ARROW_CIRCLE_LEFT), ['/services/admin/block/index', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?php endif ?>
        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
            Добавить блок <span class="caret"></span>
        </button>
        <?= Dropdown::widget(['items' => $items]) ?>
    </div>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Блок',
                'value' => function (ArticleBlock $model) {
                    return $model->getBlock()->getName();
                }
            ],

            'order',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} {blocks}',
                'buttons' => [
                    'blocks' => function ($url, ArticleBlock $model, $key) {
                        return Html::a(FA::i(FA::_LIST), ['/articles/admin/block/index',
                            'id' => $model->article_id, 'parent_block_id' => $model->id]);
                    },
                ],
            ],
        ],
    ]) ?>

</div>
