<?php

namespace app\modules\articles\migrations;

use nullref\core\traits\MigrationTrait;
use yii\db\Migration;

class M170711111040Articles__add_parent_block_id extends Migration
{
    use MigrationTrait;

    public function safeUp()
    {
        $this->addColumn('{{%article}}', 'top_image', $this->string());

        $this->addColumn('{{%article_block}}', 'parent_block_id', $this->integer()->notNull());

        $this->dropForeignKey('article_block_translation__article_block_fk', '{{%article_block_translation}}');
        $this->dropIndex('article_block_translation__article_block_idx', '{{%article_block_translation}}');

        $this->dropTable('{{%article_block_translation}}');

        $this->addColumn('{{%article_block}}', 'config', $this->text());

        $this->dropForeignKey('article_translation__article_fk', '{{%article_translation}}');
        $this->dropIndex('article_translation__article_idx', '{{%article_translation}}');
    }

    public function safeDown()
    {
        $this->createIndex('article_translation__article_idx', '{{%article_translation}}', 'article_id');
        $this->addForeignKey('article_translation__article_fk', '{{%article_translation}}', 'article_id', '{{%article}}', 'id');

        $this->dropColumn('{{%article_block}}', 'config');

        $this->createTable('{{%article_block_translation}}', [
            'id' => $this->primaryKey(),
            'language' => $this->integer(),
            'article_block_id' => $this->integer()->notNull(),
            'config' => $this->text(),
        ], $this->getTableOptions());

        $this->createIndex('article_block_translation__article_block_idx', '{{%article_block_translation}}', 'article_block_id');
        $this->addForeignKey('article_block_translation__article_block_fk', '{{%article_block_translation}}', 'article_block_id', '{{%article_block}}', 'id');

        $this->dropColumn('{{%article_block}}', 'parent_block_id');

        $this->dropColumn('{{%article}}', 'top_image', $this->string());
    }
}
