<?php

namespace app\modules\articles\migrations;

use nullref\core\traits\MigrationTrait;
use yii\db\Migration;

class M170711111038Articles__create_tables extends Migration
{
    use MigrationTrait;

    public function safeUp()
    {
        $this->createTable('{{%article}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->text(),
            'author' => $this->string(),
            'image' => $this->string(),
            'url' => $this->string(),
        ], $this->getTableOptions());

        $this->createTable('{{%article_block}}', [
            'id' => $this->primaryKey(),
            'class_name' => $this->string()->notNull(),
            'article_id' => $this->integer()->notNull(),
            'config' => $this->text(),
            'order' => $this->float(),
        ], $this->getTableOptions());

        $this->createIndex('article_idx', '{{%article_block}}', 'article_id');
        $this->addForeignKey('article_fk', '{{%article_block}}', 'article_id', '{{%article}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('article_fk', '{{%article_block}}');
        $this->dropIndex('article_idx', '{{%article_block}}');

        $this->dropTable('{{%article_block}}');

        $this->dropTable('{{%article}}');
    }
}
