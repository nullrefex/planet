<?php

namespace app\modules\articles\migrations;

use nullref\core\traits\MigrationTrait;
use yii\db\Migration;

class M170711111039Articles__create_multilang_tables extends Migration
{
    use MigrationTrait;

    public function safeUp()
    {
        $this->dropColumn('{{%article}}', 'name');
        $this->dropColumn('{{%article}}', 'description');
        $this->dropColumn('{{%article}}', 'author');

        $this->createTable('{{%article_translation}}', [
            'id' => $this->primaryKey(),
            'language' => $this->integer(),
            'article_id' => $this->integer()->notNull(),
            'name' => $this->string(),
            'description' => $this->text(),
            'author' => $this->string(),
        ], $this->getTableOptions());

        $this->createIndex('article_translation__article_idx', '{{%article_translation}}', 'article_id');
        $this->addForeignKey('article_translation__article_fk', '{{%article_translation}}', 'article_id', '{{%article}}', 'id');

        $this->dropColumn('{{%article_block}}', 'config');

        $this->createTable('{{%article_block_translation}}', [
            'id' => $this->primaryKey(),
            'language' => $this->integer(),
            'article_block_id' => $this->integer()->notNull(),
            'config' => $this->text(),
        ], $this->getTableOptions());

        $this->createIndex('article_block_translation__article_block_idx', '{{%article_block_translation}}', 'article_block_id');
        $this->addForeignKey('article_block_translation__article_block_fk', '{{%article_block_translation}}', 'article_block_id', '{{%article_block}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('article_block_translation__article_block_fk', '{{%article_block_translation}}');
        $this->dropIndex('article_block_translation__article_block_idx', '{{%article_block_translation}}');

        $this->dropTable('{{%article_block_translation}}');

        $this->addColumn('{{%article_block}}', 'config', $this->text());

        $this->dropForeignKey('article_translation__article_fk', '{{%article_translation}}');
        $this->dropIndex('article_translation__article_idx', '{{%article_translation}}');

        $this->dropTable('{{%article_translation}}');

        $this->addColumn('{{%article}}', 'name', $this->string());
        $this->addColumn('{{%article}}', 'description', $this->text());
        $this->addColumn('{{%article}}', 'author', $this->string());
    }
}
