<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\articles\components;


use app\components\cms\BaseBlockManager;
use app\modules\articles\models\ArticleBlock;

class BlockManager extends BaseBlockManager
{
    public $cachePrefix = 'article.block.';

    public function init()
    {
        $this->blockModelClass = ArticleBlock::className();
        parent::init();
    }


    public function getList()
    {
        return [
            'quote' => 'app\modules\articles\blocks\quote',
            'text' => 'app\modules\articles\blocks\text',
            'textWithImage' => 'app\modules\articles\blocks\textWithImage',
            'textOnImage' => 'app\modules\articles\blocks\textOnImage',
            'images' => 'app\modules\articles\blocks\images',
            'goalList' => 'app\modules\articles\blocks\goalList',
            'goalItem' => 'app\modules\articles\blocks\goalItem',
        ];
    }
}