<?php

namespace app\modules\articles;
use nullref\core\interfaces\IAdminModule;
use Yii;
use yii\base\Module as BaseModule;

/**
 * articles module definition class
 */
class Module extends BaseModule implements IAdminModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\articles\controllers';

    public static function getAdminMenu()
    {
        return [
            'label' => Yii::t('app', 'Articles'),
            'icon' => 'book',
            'order' => 2,
            'url' => ['/articles/admin/article'],
        ];
    }
}
