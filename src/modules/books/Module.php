<?php

namespace app\modules\books;

use nullref\core\interfaces\IAdminModule;
use Yii;
use yii\base\Module as BaseModule;

/**
 * books module definition class
 */
class Module extends BaseModule implements IAdminModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\books\controllers';


    public static function getAdminMenu()
    {
        return [
            'label' => Yii::t('app', 'Books'),
            'icon' => 'book',
            'order' => 2,
            'url' => ['/books/admin/book'],
        ];
    }
}
