<?php

namespace app\modules\books\migrations;

use yii\db\Migration;

class M170711111038Books__create_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%book}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'image' => $this->string(),
            'short_description' => $this->text(),
            'long_description' => $this->text(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%book}}');
    }
}
