<?php

namespace app\modules\books\migrations;

use app\modules\books\models\Book;
use yii\db\Migration;

class M170726185509Multilang extends Migration
{
    const TABLE_NAME = '{{%book_translation}}';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'language' => $this->integer(),
            'book_id' => $this->integer(),
            'name' => $this->string(),
            'short_description' => $this->text(),
            'long_description' => $this->text(),
        ]);

        $this->dropColumn(Book::tableName(), 'name');
        $this->dropColumn(Book::tableName(), 'short_description');
        $this->dropColumn(Book::tableName(), 'long_description');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);

        $this->addColumn(Book::tableName(), 'name', $this->string());
        $this->addColumn(Book::tableName(), 'short_description', $this->text());
        $this->addColumn(Book::tableName(), 'long_description', $this->text());
        return true;
    }
}
