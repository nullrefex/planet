<?php

namespace app\modules\books\models;

use Yii;
use nullref\useful\behaviors\TranslationBehavior;
use app\helpers\Languages;

/**
 * This is the model class for table "{{%book}}".
 *
 * @property int $id
 * @property string $name
 * @property string $image
 * @property string $short_description
 * @property string $long_description
 */
class Book extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%book}}';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'multilanguage' => [
                'class' => TranslationBehavior::className(),
                'languages' => Languages::getSlugMap(),
                'defaultLanguage' => Languages::get()->getSlug(),
                'relation' => 'translations',
                'attributeNamePattern' => '{attr}_{lang}',
                'languageField' => 'language',
                'langClassName' => BookTranslation::className(),
                'translationAttributes' => [
                    'name', 'short_description', 'long_description',
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return BookQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BookQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['short_description', 'long_description'], 'string'],
            [['name', 'image'], 'string', 'max' => 255],
            [['name', 'image'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'name_en' => Yii::t('app', 'Name'),
            'name_ru' => Yii::t('app', 'Name'),
            'image' => Yii::t('app', 'Image'),
            'short_description' => Yii::t('app', 'Short Description'),
            'short_description_en' => Yii::t('app', 'Short Description'),
            'short_description_ru' => Yii::t('app', 'Short Description'),
            'long_description' => Yii::t('app', 'Long Description'),
            'long_description_en' => Yii::t('app', 'Long Description'),
            'long_description_ru' => Yii::t('app', 'Long Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(BookTranslation::className(), ['book_id' => 'id']);
    }
}
