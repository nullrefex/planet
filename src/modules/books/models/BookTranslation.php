<?php

namespace app\modules\books\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%book_translation}}".
 *
 * @property integer $id
 * @property integer $language
 * @property string $name
 * @property string $short_description
 * @property string $long_description
 * @property integer $book_id
 */
class BookTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%book_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'book_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['short_description', 'long_description'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'language' => Yii::t('app', 'Language'),
            'name' => Yii::t('app', 'Name'),
            'short_description' => Yii::t('app', 'Short Description'),
            'long_description' => Yii::t('app', 'Long Description'),
            'book_id' => Yii::t('app', 'Book ID'),
        ];
    }
}
