<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */
?>
<div id="fullBook" class="modal fade modal-scale book-modal">
    <div class="modal-dialog">
        <div class="modal-content"><a href="#" data-dismiss="modal" class="modal-close"></a>
            <div class="book-modal__inn">
                <div class="book-modal__title"></div>
                <div class="book-modal__txt"></div>
            </div>
        </div>
    </div>
</div>
