<?php

use app\modules\books\models\Book;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var \yii\web\View $this
 * @var Book $model
 */

?>
<div class="books__list__it">
    <div class="books__list__it__img"><img src="<?= $model->image ?>" alt="<?= Html::encode($model->name) ?>"></div>
    <div class="books__list__it__desc">
        <div class="books__list__it__title"><span><?= Html::encode($model->name) ?></span></div>
        <div class="books__list__it__text hidden-xs">
            <?= HtmlPurifier::process($model->short_description) ?>
        </div>
        <div class="books__list__it__full visible-xs">
            <?php if ($model->long_description): ?>
                <?= HtmlPurifier::process($model->long_description) ?>
            <?php else: ?>
                <?= HtmlPurifier::process($model->short_description) ?>
            <?php endif ?>
        </div>
        <?php if ($model->long_description): ?>
            <div class="books__list__it__readall hidden-xs"><a href="#"><?= Yii::t('app', 'Read more') ?> ›</a></div>
        <?php endif ?>
    </div>
</div>