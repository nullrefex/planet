<?php

use app\modules\books\models\Book;
use app\modules\books\widgets\Book as BookWidget;

/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var \yii\web\View $this
 * @var Book[] $models
 */

$this->params['modals'][] = $this->render('_modal');
?>
<div class="books__list">
    <?php foreach ($models as $model): ?>
        <?= BookWidget::widget(['model' => $model]) ?>
    <?php endforeach ?>
</div>

