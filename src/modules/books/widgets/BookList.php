<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\books\widgets;


use app\modules\books\models\Book;
use yii\base\Widget;

class BookList extends Widget
{
    /** @var  Book[] */
    public $models;

    public function init()
    {
        $this->models = Book::find()->all();
    }

    public function run()
    {
        return $this->render('book-list', ['models' => $this->models]);
    }
}