<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\reviews\widgets;


use yii\base\InvalidConfigException;
use yii\base\Widget;

class Review extends Widget
{
    /** @var  Review|null */
    public $model;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        if ($this->model === null) {
            throw  new InvalidConfigException('$model should be set');
        }
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('review', ['model' => $this->model]);
    }
}