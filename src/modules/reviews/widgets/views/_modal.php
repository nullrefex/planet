<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */
?>
<div id="videoModal" class="modal fade modal-scale video-modal">
    <div class="modal-dialog">
        <div class="modal-content"><a href="#" data-dismiss="modal" class="modal-close"></a>
            <div class="video-modal__video-wrap"></div>
        </div>
    </div>
</div>