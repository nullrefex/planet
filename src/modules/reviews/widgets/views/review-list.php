<?php

use app\modules\reviews\models\Review;
use app\modules\reviews\widgets\Review as ReviewWidget;

/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var \yii\web\View $this
 * @var Review[] $models
 */

$this->params['modals'][] = $this->render('_modal');
?>
<div class="reviews__list">
    <?php foreach ($models as $model): ?>
        <?= ReviewWidget::widget(['model' => $model]) ?>
    <?php endforeach ?>
</div>

