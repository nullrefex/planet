<?php

use app\modules\reviews\models\Review;

/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var \yii\web\View $this
 * @var $model Review
 */

?>

<div class="reviews__item">
    <div class="reviews__item__mobtop visible-sm visible-xs">
        <div class="reviews__item__mobtop__img ofit-block"><img src="<?= $model->image ?>" alt=""></div>
        <div class="reviews__item__mobtop__inn">
            <div class="reviews__item__mobtop__inn__txt hidden-xs">
                <?= $model->text_on_image ?>
            </div>
        </div>
    </div>
    <div class="reviews__item__img ofit-block hidden-sm hidden-xs"><img src="<?= $model->image ?>" alt=""></div>
    <div class="reviews__item__inn">
        <div class="reviews__item__onimg hidden-sm">
            <?= $model->text_on_image ?>
        </div>
        <div class="reviews__item__cnt">
            <div class="reviews__item__txt hidden-sm hidden-xs">
                <?= $model->getTextWithReamMore() ?>
            </div>
            <div class="reviews__item__full-text visible-sm visible-xs">
                <?= $model->read_more ? $model->read_more : $model->text ?>
            </div>
            <div class="reviews__item__author"><span><?= $model->author ?></span></div>
        </div>
    </div>
</div>