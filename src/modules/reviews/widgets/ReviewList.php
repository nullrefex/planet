<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\reviews\widgets;


use app\modules\reviews\models\Review;
use yii\base\Widget;

class ReviewList extends Widget
{
    /** @var Review[] */
    public $models;

    public function init()
    {
        $this->models = Review::find()->all();
    }

    public function run()
    {
        return $this->render('review-list', ['models' => $this->models]);
    }
}