<?php

namespace app\modules\reviews;

use nullref\core\interfaces\IAdminModule;
use Yii;
use yii\base\Module as BaseModule;

/**
 * reviews module definition class
 */
class Module extends BaseModule implements IAdminModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\reviews\controllers';


    public static function getAdminMenu()
    {
        return [
            'label' => Yii::t('app', 'Reviews'),
            'icon' => 'eye',
            'order' => 2,
            'url' => ['/reviews/admin/review'],
        ];
    }
}
