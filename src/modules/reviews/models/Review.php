<?php

namespace app\modules\reviews\models;

use app\helpers\Languages;
use nullref\useful\behaviors\TranslationBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%review}}".
 *
 * @property int $id
 * @property string $image
 * @property string $author
 * @property string $text
 * @property string $read_more
 * @property string $text_on_image
 */
class Review extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%review}}';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'multilanguage' => [
                'class' => TranslationBehavior::className(),
                'languages' => Languages::getSlugMap(),
                'defaultLanguage' => Languages::get()->getSlug(),
                'relation' => 'translations',
                'attributeNamePattern' => '{attr}_{lang}',
                'languageField' => 'language',
                'langClassName' => ReviewTranslation::className(),
                'translationAttributes' => [
                    'author', 'text', 'text_on_image', 'read_more',
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'text_on_image', 'read_more'], 'string'],
            [['image', 'author'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'image' => Yii::t('app', 'Image'),
            'author' => Yii::t('app', 'Author'),
            'author_ru' => Yii::t('app', 'Author'),
            'author_en' => Yii::t('app', 'Author'),
            'text' => Yii::t('app', 'Text'),
            'text_ru' => Yii::t('app', 'Text'),
            'text_en' => Yii::t('app', 'Text'),
            'read_more' => Yii::t('app', 'Read more'),
            'read_more_ru' => Yii::t('app', 'Read more'),
            'read_more_en' => Yii::t('app', 'Read more'),
            'text_on_image_ru' => Yii::t('app', 'Text on image'),
            'text_on_image_en' => Yii::t('app', 'Text on image'),
        ];
    }

    /**
     * @inheritdoc
     * @return ReviewQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ReviewQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ReviewTranslation::className(), ['review_id' => 'id']);
    }

    /**
     * @return mixed|string
     */
    public function getTextWithReamMore()
    {
        if ($this->read_more) {

            $html = $this->text;

            $search = '</p>';

            $pos = strrpos($html, $search);

            $replace = '<a href="#" class="__read-more">' . Yii::t('app', 'Read all') . '  ›</a></p>';

            if ($pos !== false) {
                $html = substr_replace($html, $replace, $pos, strlen($search));
            }

            return $html;
        }

        return $this->text;
    }
}
