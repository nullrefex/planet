<?php

namespace app\modules\reviews\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%review_translation}}".
 *
 * @property integer $id
 * @property integer $language
 * @property string $author
 * @property string $text
 * @property string $text_on_image
 * @property integer $review_id
 */
class ReviewTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%review_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'review_id'], 'integer'],
            [['author'], 'string', 'max' => 255],
            [['text', 'text_on_image'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'language' => Yii::t('app', 'Language'),
            'author' => Yii::t('app', 'Author'),
            'text' => Yii::t('app', 'Text'),
            'text_on_image' => Yii::t('app', 'Text on image'),
            'review_id' => Yii::t('app', 'Review ID'),
        ];
    }
}
