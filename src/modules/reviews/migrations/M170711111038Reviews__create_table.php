<?php

namespace app\modules\reviews\migrations;

use yii\db\Migration;

class M170711111038Reviews__create_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%review}}', [
            'id' => $this->primaryKey(),
            'image' => $this->string(),
            'author' => $this->string(),
            'text' => $this->text(),
            'text_on_image' => $this->text(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%review}}');
    }
}
