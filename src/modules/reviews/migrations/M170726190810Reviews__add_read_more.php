<?php

namespace app\modules\reviews\migrations;

use yii\db\Migration;

class M170726190810Reviews__add_read_more extends Migration
{
    const TABLE_NAME = '{{%review_translation}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'read_more', $this->text());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'read_more');
        return true;
    }
}
