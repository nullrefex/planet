<?php

namespace app\modules\reviews\migrations;

use app\modules\reviews\models\Review;
use yii\db\Migration;

class M170726190804Multilang extends Migration
{
    const TABLE_NAME = '{{%review_translation}}';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'language' => $this->integer(),
            'review_id' => $this->integer(),
            'author' => $this->string(),
            'text' => $this->text(),
            'text_on_image' => $this->text(),
        ]);
        $this->dropColumn(Review::tableName(), 'author');
        $this->dropColumn(Review::tableName(), 'text');
        $this->dropColumn(Review::tableName(), 'text_on_image');
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
        $this->addColumn(Review::tableName(), 'author', $this->string());
        $this->addColumn(Review::tableName(), 'text', $this->text());
        $this->addColumn(Review::tableName(), 'text_on_image', $this->text());
        return true;
    }
}
