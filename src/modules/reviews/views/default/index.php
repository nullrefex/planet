<?php
/**
 *
 */
use app\modules\reviews\widgets\ReviewList;

$this->title = Yii::t('app', 'Reviews');
?>
<div class="reviews">
    <div class="container">
        <div class="team__title">
            <h1><span><?= $this->title ?></span></h1>
        </div>
        <?= ReviewList::widget() ?>
    </div>
</div>
<div id="fullReview" class="modal fade modal-scale review-modal">
    <div class="modal-dialog">
        <div class="modal-content"><a href="#" data-dismiss="modal" class="modal-close"></a>
            <div class="review-modal__inn">
                <div class="review-modal__txt1"></div>
                <div class="review-modal__txt2"></div>
                <div class="review-modal__author"></div>
            </div>
        </div>
    </div>
</div>