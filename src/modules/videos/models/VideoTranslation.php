<?php

namespace app\modules\videos\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%video_translation}}".
 *
 * @property integer $id
 * @property integer $language
 * @property string $name
 * @property string $url
 * @property integer $video_id
 */
class VideoTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%video_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'video_id'], 'integer'],
            [['name', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'language' => Yii::t('app', 'Language'),
            'name' => Yii::t('app', 'Name'),
            'url' => Yii::t('app', 'URL'),
            'human_id' => Yii::t('app', 'Human ID'),
        ];
    }
}
