<?php

namespace app\modules\videos\models;

use app\helpers\Languages;
use nullref\useful\behaviors\TranslationBehavior;
use Yii;

/**
 * This is the model class for table "{{%video}}".
 *
 * @property int $id
 * @property string $name
 * @property string $image
 * @property string $url
 */
class Video extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%video}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'image', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'name_ru' => Yii::t('app', 'Name'),
            'name_en' => Yii::t('app', 'Name'),
            'image' => Yii::t('app', 'Image'),
            'url' => Yii::t('app', 'URL'),
            'url_ru' => Yii::t('app', 'URL'),
            'url_en' => Yii::t('app', 'URL'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'multilanguage' => [
                'class' => TranslationBehavior::className(),
                'languages' => Languages::getSlugMap(),
                'defaultLanguage' => Languages::get()->getSlug(),
                'relation' => 'translations',
                'attributeNamePattern' => '{attr}_{lang}',
                'languageField' => 'language',
                'langClassName' => VideoTranslation::className(),
                'translationAttributes' => [
                    'name', 'url',
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(VideoTranslation::className(), ['video_id' => 'id']);
    }
}
