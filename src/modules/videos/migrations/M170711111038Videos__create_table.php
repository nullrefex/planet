<?php

namespace app\modules\videos\migrations;

use yii\db\Migration;

class M170711111038Videos__create_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%video}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'image' => $this->string(),
            'url' => $this->string(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%video}}');
    }
}
