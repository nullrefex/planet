<?php

namespace app\modules\videos\migrations;

use app\modules\videos\models\Video;
use yii\db\Migration;

class M170726193419Multilang_human extends Migration
{
    const TABLE_NAME = '{{%video_translation}}';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'language' => $this->integer(),
            'video_id' => $this->integer(),
            'name' => $this->string(),
            'url' => $this->string(),
        ]);

        $this->dropColumn(Video::tableName(), 'name');
        $this->dropColumn(Video::tableName(), 'url');
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
        $this->addColumn(Video::tableName(), 'name', $this->string());
        $this->addColumn(Video::tableName(), 'url', $this->string());
        return true;
    }
}
