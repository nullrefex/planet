<?php

namespace app\modules\videos;

use nullref\core\interfaces\IAdminModule;
use Yii;
use yii\base\Module as BaseModule;

/**
 * services module definition class
 */
class Module extends BaseModule implements IAdminModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\videos\controllers';


    public static function getAdminMenu()
    {
        return [
            'label' => Yii::t('app', 'Videos'),
            'icon' => 'play',
            'order' => 2,
            'url' => ['/videos/admin/video'],
        ];
    }
}
