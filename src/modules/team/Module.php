<?php

namespace app\modules\team;

use nullref\core\interfaces\IAdminModule;
use Yii;
use yii\base\Module as BaseModule;

/**
 * services module definition class
 */
class Module extends BaseModule implements IAdminModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\team\controllers';


    public static function getAdminMenu()
    {
        return [
            'label' => Yii::t('app', 'Team'),
            'icon' => 'users',
            'order' => 2,
            'url' => ['/team/admin/human'],
        ];
    }
}
