<?php

namespace app\modules\team\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%human_translation}}".
 *
 * @property integer $id
 * @property integer $language
 * @property string $name
 * @property string $description
 * @property integer $human_id
 */
class HumanTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%human_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'human_id'], 'integer'],
            [['name', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'language' => Yii::t('app', 'Language'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'human_id' => Yii::t('app', 'Human ID'),
        ];
    }
}
