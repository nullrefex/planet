<?php

namespace app\modules\team\models;

/**
 * This is the ActiveQuery class for [[Human]].
 *
 * @see Human
 */
class HumanQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Human[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Human|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
