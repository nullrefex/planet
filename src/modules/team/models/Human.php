<?php

namespace app\modules\team\models;

use app\helpers\Languages;
use nullref\useful\behaviors\TranslationBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%human}}".
 *
 * @property int $id
 * @property string $name
 * @property string $image
 * @property string $description
 * @property int $is_top
 */
class Human extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%human}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_top'], 'integer'],
            [['name', 'image', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'name_ru' => Yii::t('app', 'Name'),
            'name_en' => Yii::t('app', 'Name'),
            'image' => Yii::t('app', 'Image'),
            'description' => Yii::t('app', 'Description'),
            'description_ru' => Yii::t('app', 'Description'),
            'description_en' => Yii::t('app', 'Description'),
            'is_top' => Yii::t('app', 'Is Top'),
        ];
    }

    /**
     * @inheritdoc
     * @return HumanQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new HumanQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'multilanguage' => [
                'class' => TranslationBehavior::className(),
                'languages' => Languages::getSlugMap(),
                'defaultLanguage' => Languages::get()->getSlug(),
                'relation' => 'translations',
                'attributeNamePattern' => '{attr}_{lang}',
                'languageField' => 'language',
                'langClassName' => HumanTranslation::className(),
                'translationAttributes' => [
                    'name', 'description',
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(HumanTranslation::className(), ['human_id' => 'id']);
    }
}
