<?php

use app\components\Helper;
use mihaildev\elfinder\InputFile;
use nullref\core\widgets\Multilingual;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\team\models\Human */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="human-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <?= Multilingual::widget(['model' => $model,
                'tab' => function (ActiveForm $form, $model) {
                    echo $form->field($model, 'name')->textInput(['maxlength' => true]);
                    echo $form->field($model, 'description')->textInput(['maxlength' => true]);
                }
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'image')->widget(InputFile::className(), Helper::getInputFileOptions()) ?>

            <?= $form->field($model, 'is_top')->checkbox() ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
