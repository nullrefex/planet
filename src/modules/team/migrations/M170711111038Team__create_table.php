<?php

namespace app\modules\team\migrations;

use yii\db\Migration;

class M170711111038Team__create_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%human}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'image' => $this->string(),
            'description' => $this->string(),
            'is_top' => $this->boolean()->defaultValue(false),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%human}}');
    }
}
