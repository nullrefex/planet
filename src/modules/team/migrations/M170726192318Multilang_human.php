<?php

namespace app\modules\team\migrations;

use app\modules\team\models\Human;
use yii\db\Migration;

class M170726192318Multilang_human extends Migration
{
    const TABLE_NAME = '{{%human_translation}}';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'language' => $this->integer(),
            'human_id' => $this->integer(),
            'name' => $this->string(),
            'description' => $this->string(),
        ]);
        $this->dropColumn(Human::tableName(), 'name');
        $this->dropColumn(Human::tableName(), 'description');
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
        $this->addColumn(Human::tableName(), 'name', $this->string());
        $this->addColumn(Human::tableName(), 'description', $this->string());
        return true;
    }
}
