<?php

namespace app\modules\team\migrations;

use app\modules\team\models\Human;
use yii\db\Migration;

class M170726192318Team__create_table extends Migration
{
    public function up()
    {
        $list = Human::find()->each();
        foreach ($list as $item) {
            $item->delete();
        }

        $data = [
            [
                'name' => 'Маргарита Сотникова',
                'description' => 'Руководитель группы «Планета», профессиональный одитор',
                'image' => '/img/content/team/1.png',
                'is_top' => true,
            ],
            [
                'name' => 'Евгений Сотников',
                'description' => 'Бизнесмен, профессиональный бизнес-консультант, одитор группы',
                'image' => '/img/content/team/2.png',
                'is_top' => true,
            ],
            [
                'name' => 'Пушкина Анна',
                'description' => 'Профессиональный одитор',
                'image' => '/img/content/team/3.png',
                'is_top' => false,
            ],
            [
                'name' => 'Романенко Татьяна',
                'description' => 'Профессиональный одитор',
                'image' => '/img/content/team/4.png',
                'is_top' => false,
            ],
            [
                'name' => 'Сысоева Ольга',
                'description' => 'Профессиональный одитор',
                'image' => '/img/content/team/5.png',
                'is_top' => false,
            ],
            [
                'name' => 'Федосеева Дарья',
                'description' => 'Профессиональный одитор',
                'image' => '/img/content/team/6.png',
                'is_top' => false,
            ],
            [
                'name' => 'Федосеев Владислав',
                'description' => 'Профессиональный одитор',
                'image' => '/img/content/team/7.png',
                'is_top' => false,
            ],
            [
                'name' => 'Некрасова Надежда',
                'description' => 'Профессиональный одитор',
                'image' => '/img/content/team/8.png',
                'is_top' => false,
            ],
            [
                'name' => 'Палий Николай',
                'description' => 'Профессиональный одитор',
                'image' => '/img/content/team/9.png',
                'is_top' => false,
            ],
            [
                'name' => 'Истомина Ирина',
                'description' => 'Профессиональный одитор',
                'image' => '/img/content/team/10.png',
                'is_top' => false,
            ],
            [
                'name' => 'Мацола Олег',
                'description' => 'Профессиональный одитор',
                'image' => '/img/content/team/11.png',
                'is_top' => false,
            ],
            [
                'name' => 'Полянский Кирилл',
                'description' => 'Профессиональный одитор',
                'image' => '/img/content/team/12.png',
                'is_top' => false,
            ],
            [
                'name' => 'Грицюк Андрей',
                'description' => 'Профессиональный супрвайзер',
                'image' => '/img/content/team/13.png',
                'is_top' => false,
            ],
            [
                'name' => 'Вайнберг Елена',
                'description' => 'Офис-менеджер',
                'image' => '/img/content/team/14.png',
                'is_top' => false,
            ],
            [
                'name' => 'Лизогубенко Валентина',
                'description' => 'Менеджер по работе с клиентами',
                'image' => '/img/content/team/15.png',
                'is_top' => false,
            ],
            [
                'name' => 'Мельникова Наталья',
                'description' => 'Менеджер по работе с клиентами',
                'image' => '/img/content/team/16.png',
                'is_top' => false,
            ],
            [
                'name' => 'Гапонова Инна',
                'description' => 'Офис-менеджер',
                'image' => '/img/content/team/17.png',
                'is_top' => false,
            ],
        ];

        foreach ($data as $datum) {
            $model = new Human();
            $model->image = $datum['image'];
            $model->is_top = intval($datum['is_top']);
            $model->name_en = $datum['name'];
            $model->name_ru = $datum['name'];
            $model->description_en = $datum['description'];
            $model->description_ru = $datum['description'];
            $model->save();
        }
    }

    public function down()
    {
        $list = Human::find()->each();
        foreach ($list as $item) {
            $item->delete();
        }
    }
}
