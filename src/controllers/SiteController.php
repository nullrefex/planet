<?php

namespace app\controllers;

use app\modules\articles\models\Article;
use app\modules\services\models\Service;
use app\modules\team\models\Human;
use app\modules\videos\models\Video;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionBooks()
    {
        return $this->render('books');
    }

    public function actionArticles()
    {
        $models = Article::find()->all();

        return $this->render('articles', [
            'models' => $models,
        ]);
    }

    public function actionArticle($id)
    {
        $model = Article::findOne($id);

        if ($model == null) {
            throw new NotFoundHttpException();
        }

        return $this->render('article', [
            'model' => $model,
        ]);
    }

    public function actionTest()
    {
        return $this->render('test');
    }

    public function actionServices()
    {
        return $this->render('services');
    }

    public function actionService($id)
    {
        $model = Service::findOne($id);

        if ($model == null) {
            throw new NotFoundHttpException();
        }
        return $this->render('service', [
            'model' => $model,
        ]);
    }

    public function actionTeam()
    {
        $topModels = Human::find()->andWhere(['is_top' => true])->all();
        $models = Human::find()->andWhere(['is_top' => false])->all();

        return $this->render('team', [
            'topModels' => $topModels,
            'models' => $models,
        ]);
    }

    public function actionVideos()
    {
        $models = Video::find()->all();

        return $this->render('videos', [
            'models' => $models,
        ]);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
}
